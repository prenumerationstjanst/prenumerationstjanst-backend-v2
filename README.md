# Prenumerationstjänst backend
Prenumerationstjänsten består av ett antal backendmoduler (detta projekt), samt en frontendmodul (prenumerationstjanst-gui).
Backend-modulerna har ännu inga direktkopplingar sinsemellan men deployas ändå som separata tjänster.

För att köra applikationen, se projektet `prenumerationstjanst-docker`.

## översikt över projektets moduler
#### api
maven: `se.skl.prenumerationstjanst:prenumerationstjanst-api`

Publikt API som används av frontendmodulen. Alla funktioner som styrs av användaren finns här.

#### commons
maven: `se.skl.prenumerationstjanst:prenumerationstjanst-commons`

Gemensamma klasser och konfigurationer, importeras som beroende av api, intsvc, notification-receiver och transfer.

#### intsvc
maven: `se.skl.prenumerationstjanst:prenumerationstjanst-intsvc`

Interna tjänster och funktioner, t ex schemalagda jobb och monitorering

#### notification-receiver
maven: `se.skl.prenumerationstjanst:prenumerationstjanst-notification-receiver`

Tar emot notifieringar från APIGW (EI) och startar hämtningsjobb i överföringsmotorn.

#### test-commons
maven: `se.skl.prenumerationstjanst:prenumerationstjanst-test-commons`

Gemensamma testfunktioner. Används av de övriga modulerna som ett test-beroende.

#### transfer
maven: `se.skl.prenumerationstjanst:prenumerationstjanst-transfer`

Överföringsmotorn. Överföringar initieras av api samt notification-receiver genom meddelanden på ActiveMQ-kö.

## bygga
`mvn clean install`

## köra enhetstester
`mvn clean test`

## köra enhetstester + integrationstester
`mvn clean verify -Pintegration-test`

## Köra applikationen
se `prenumerationstjanst-docker`
