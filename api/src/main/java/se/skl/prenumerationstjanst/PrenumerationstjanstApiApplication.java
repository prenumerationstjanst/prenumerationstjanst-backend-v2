package se.skl.prenumerationstjanst;

import org.h2.server.web.WebServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.filter.HttpPutFormContentFilter;
import se.skl.prenumerationstjanst.config.TestConfiguration;
import se.skl.prenumerationstjanst.web.ActivityIdFilter;
import se.skl.prenumerationstjanst.web.EndUserClientCorrelationIdFilter;
import se.skl.prenumerationstjanst.web.ProcessIdFilter;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
//exclude classes marked with @TestConfiguration
@ComponentScan(excludeFilters = @ComponentScan.Filter(TestConfiguration.class))
@EnableJms
@EnableCaching
@EnableJpaAuditing
@EnableWebSecurity
@EnableAsync
public class PrenumerationstjanstApiApplication extends SpringBootServletInitializer {

    private static final Logger logger = LoggerFactory.getLogger(PrenumerationstjanstApiApplication.class);

    private static final int PROCESS_ID_FILTER_ORDER = SecurityProperties.DEFAULT_FILTER_ORDER - 30; //before Spring Security
    private static final int ACTIVITY_ID_FILTER_ORDER = SecurityProperties.DEFAULT_FILTER_ORDER - 20; //before Spring Security
    private static final int END_USER_CORRELATION_ID_FILTER_ORDER = SecurityProperties.DEFAULT_FILTER_ORDER - 10; //after CorrelationIdFilter and before Spring Security

    public static void main(String[] args) {
        SpringApplication.run(PrenumerationstjanstApiApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(PrenumerationstjanstApiApplication.class);
    }

    @Override
    protected WebApplicationContext run(SpringApplication application) {
        MDC.put(PrenumerationstjanstConstants.MDC_PROCESS_ID_KEY, processIdGenerator().generateSanitizedId());
        MDC.put(PrenumerationstjanstConstants.MDC_ACTIVITY_ID_KEY, activityIdGenerator().generateSanitizedId());
        return super.run(application);
    }

    @Bean
    public UuidGenerator processIdGenerator() {
        return new UuidGenerator("PID-" + UuidGenerator.getHostName());
    }

    @Bean
    public UuidGenerator activityIdGenerator() {
        return new UuidGenerator("AID-" + UuidGenerator.getHostName());
    }

    /**
     * Servlet spec does not require form data to be parsed for PUT requests
     * This filter enables this behaviour for us
     *
     * @return filter to allow query parameters on PUT requests
     */
    @Bean
    public HttpPutFormContentFilter httpPutFormContentFilter() {
        return new HttpPutFormContentFilter();
    }

    @Bean
    public ProcessIdFilter processIdFilter() {
        return new ProcessIdFilter();
    }

    @Bean
    public FilterRegistrationBean eventIdFilterRegistration() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(processIdFilter());
        filterRegistrationBean.setOrder(PROCESS_ID_FILTER_ORDER);
        return filterRegistrationBean;
    }

    @Bean
    public ActivityIdFilter activityIdFilter() {
        return new ActivityIdFilter();
    }

    @Bean
    public FilterRegistrationBean correlationIdFilterRegistration() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(activityIdFilter());
        filterRegistrationBean.setOrder(ACTIVITY_ID_FILTER_ORDER);
        return filterRegistrationBean;
    }

    @Bean
    public FilterRegistrationBean clientCorrelationIdFilterRegistration() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(new EndUserClientCorrelationIdFilter());
        filterRegistrationBean.setOrder(END_USER_CORRELATION_ID_FILTER_ORDER);
        List<String> endUserUrlPatterns = new ArrayList<>();
        endUserUrlPatterns.add("/transfertypes");
        endUserUrlPatterns.add("/tos");
        endUserUrlPatterns.add("/user/*");
        endUserUrlPatterns.add("/authorize/*");
        filterRegistrationBean.setUrlPatterns(endUserUrlPatterns);
        return filterRegistrationBean;
    }
}
