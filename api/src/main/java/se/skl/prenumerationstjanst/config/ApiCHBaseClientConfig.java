package se.skl.prenumerationstjanst.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import se.skl.prenumerationstjanst.chbase.CHBaseConnectionFactory;
import se.skl.prenumerationstjanst.chbase.CHBaseRequestTemplateFactory;
import se.skl.prenumerationstjanst.chbase.impl.CHBaseConnectionFactoryImpl;
import se.skl.prenumerationstjanst.chbase.impl.CHBaseRequestTemplateFactoryImpl;

/**
 * @author Martin Samuelsson
 */
@Configuration
public class ApiCHBaseClientConfig {

    @Bean
    @ConditionalOnMissingBean(value = CHBaseConnectionFactory.class)
    public CHBaseConnectionFactory chBaseConnectionFactory() {
        return new CHBaseConnectionFactoryImpl();
    }

    @Bean
    @ConditionalOnMissingBean(value = CHBaseRequestTemplateFactory.class)
    public CHBaseRequestTemplateFactory chBaseRequestTemplateFactory() {
        return new CHBaseRequestTemplateFactoryImpl();
    }
}
