package se.skl.prenumerationstjanst.config;

import net.sf.ehcache.config.CacheConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collection;

/**
 * @author Martin Samuelsson
 */
@Configuration
public class ApiCacheConfig {

    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    @Autowired
    private Collection<CacheConfiguration> cacheConfigs;

    @Bean
    public CacheManager cacheManager() {
        return new EhCacheCacheManager(ehCacheManager());
    }

    @Bean(destroyMethod = "shutdown")
    public net.sf.ehcache.CacheManager ehCacheManager() {
        net.sf.ehcache.config.Configuration config = new net.sf.ehcache.config.Configuration();
        config.setUpdateCheck(false); //prevent ehcache from calling home to send diagnostics and check for updates
        cacheConfigs.forEach(config::addCache);
        return net.sf.ehcache.CacheManager.newInstance(config);
    }

    @Bean
    public CacheConfiguration citizenNameCache() {
        //name corresponds with value of @Cacheable annotation, see CitizenInformationServiceCachingImpl
        return createCacheConfiguration("citizenName", 10 * 60);
    }

    private CacheConfiguration createCacheConfiguration(String name, long timeToLiveSeconds) {
        CacheConfiguration cacheConfiguration = new CacheConfiguration();
        cacheConfiguration.setName(name);
        cacheConfiguration.setMemoryStoreEvictionPolicy("LRU");
        cacheConfiguration.setMaxEntriesLocalHeap(1000);
        cacheConfiguration.setTimeToLiveSeconds(timeToLiveSeconds);
        return cacheConfiguration;
    }
}
