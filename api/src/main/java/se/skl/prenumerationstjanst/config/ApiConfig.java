package se.skl.prenumerationstjanst.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import se.skl.prenumerationstjanst.service.*;
import se.skl.prenumerationstjanst.service.monitoring.SubscriptionMonitoringService;
import se.skl.prenumerationstjanst.service.monitoring.UserMonitoringService;

/**
 * @author Martin Samuelsson
 */
@Configuration
public class ApiConfig {

    @Bean
    public TransferService transferService() {
        return new TransferService();
    }

    @Bean
    public SubscriptionService subscriptionService() {
        return new SubscriptionService();
    }

    @Bean
    public TransformationService transformationService() {
        return new TransformationService();
    }

    @Bean
    public ApigwConsentService apigwConsentService() {
        return new ApigwConsentService();
    }

    @Bean
    public UrlService urlService() {
        return new UrlService();
    }

    @Bean
    public UserService userService() {
        return new UserService();
    }

    @Bean
    public UserContextService userContextService() {
        return new UserContextService();
    }

    @Bean
    public PreCheckService preCheckService() {
        return new PreCheckServiceImpl();
    }

    @Bean
    @ConditionalOnMissingBean(value = PhkInfoService.class)
    public PhkInfoService phkInfoService() {
        return new PhkInfoServiceImpl();
    }

    @Bean
    @Primary
    public CitizenInformationService citizenInformationService() {
        return new CitizenInformationServiceCachingImpl(citizenInformationServiceDefaultImpl());
    }

    @Bean
    public CitizenInformationService citizenInformationServiceDefaultImpl() {
        return new CitizenInformationServiceImpl();
    }

    @Bean
    public SubscriptionMonitoringService subscriptionMonitoringService() {
        return new SubscriptionMonitoringService();
    }

    @Bean
    public UserMonitoringService userMonitoringService() {
        return new UserMonitoringService();
    }

}
