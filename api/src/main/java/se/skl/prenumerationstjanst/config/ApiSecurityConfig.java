package se.skl.prenumerationstjanst.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.ExceptionTranslationFilter;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.util.matcher.*;
import se.skl.prenumerationstjanst.web.security.AgeRestrictionFilter;
import se.skl.prenumerationstjanst.web.security.StatelessCsrfFilter;
import se.skl.prenumerationstjanst.web.security.auth.*;

import javax.servlet.Filter;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Martin Samuelsson
 */
@Configuration
public class ApiSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(ApiSecurityConfig.class);

    @Value("${pt.userrole.idp_authenticated}")
    private String idpRole;

    @Value("${pt.userrole.self_authenticated}")
    private String userRole;

    @Value("${pt.userrole.user_context}")
    private String userContextRole;

    @Value("${pt.csrf.disable:false}")
    private boolean disableCsrfProtection;

    @Value("${pt.logout_success_url}")
    private String logoutSuccessUrl;

    @Autowired
    private PrenumerationstjanstUserDetailsService userDetailsService;

    @Autowired
    private PreAuthFilter preAuthFilter;

    @Autowired
    private UserFilter userFilter;

    @Bean
    public AuthenticationManager authenticationManager() {
        return new ProviderManager(Collections.singletonList(preAuthProvider()));
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(
                "/services/**", //used for server -> server calls
                "/console/**", //only active in dev
                "/favicon.ico",
                "/transfertypes",
                "/tos");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        http
                .csrf().disable()
                .addFilterBefore(statelessCsrfFilter(), CsrfFilter.class)
                .addFilterBefore(preAuthFilter, AbstractPreAuthenticatedProcessingFilter.class)
                .addFilterAfter(ageRestrictionFilterChain(), ExceptionTranslationFilter.class)
                .addFilterAfter(userFilterChain(), ExceptionTranslationFilter.class)
                .authorizeRequests()
                    .antMatchers(HttpMethod.DELETE, "/user/subscriptions/**")
                        .hasRole(userContextRole)
                    .antMatchers("/user/**", "/authorize/**")
                        .hasRole(idpRole)
                .and()
                .logout()
                    .logoutRequestMatcher(antPathRequestMatcher("/user/logout")) //support GET
                    .invalidateHttpSession(true)
                    .logoutSuccessUrl(logoutSuccessUrl)
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling()
                    .authenticationEntryPoint(prenumerationstjanstEntryPoint());
        // @formatter:on
    }

    @Bean
    public PreAuthenticatedAuthenticationProvider preAuthProvider() {
        PreAuthenticatedAuthenticationProvider preAuthProvider = new PreAuthenticatedAuthenticationProvider();
        preAuthProvider.setPreAuthenticatedUserDetailsService(userDetailsService);
        return preAuthProvider;
    }

    // --- PRE AUTH FILTER (start) --- //

    /**
     * Production preauth filter, expects a number of HTTP Headers,
     * see: pt.pre_auth_filter.header.* properties
     * @return
     * @throws Exception
     */
    @Bean
    @ConditionalOnMissingBean(value = PreAuthFilter.class)
    public PreAuthFilter preAuthFilter() throws Exception {
        PrenumerationstjanstPreAuthFilter preAuthFilter = new PrenumerationstjanstPreAuthFilter();
        preAuthFilter.setAuthenticationManager(authenticationManagerBean());
        return preAuthFilter;
    }

    /**
     * required to disable auto registration of the preAuthFilter
     * as a servlet filter
     * @return
     */
    @Bean
    public FilterRegistrationBean preAuthFilterRegistration() throws Exception {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(preAuthFilter);
        filterRegistrationBean.setEnabled(false);
        return filterRegistrationBean;
    }
    // --- PRE AUTH FILTER (end) --- //

    // --- REGISTERED USER FILTER (start) --- //

    /**
     * production user filter
     * @return
     * @throws Exception
     */
    @Bean
    @ConditionalOnMissingBean(value = UserFilter.class)
    public UserFilter userFilter() throws Exception {
        return new UserContextFilter();
    }

    /**
     * required to disable auto registration of the userFilter
     * as a servlet filter
     * @return
     */
    @Bean
    public FilterRegistrationBean userFilterRegistration() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(userFilter);
        filterRegistrationBean.setEnabled(false);
        return filterRegistrationBean;
    }

    /**
     * FilterChainProxy to invoke the user filter for all endpoints
     * that requires an authenticated user, this is handy to skip
     * looking up the user for non-secured endpoints.
     *
     * @return FilterChainProxy
     */
    @Bean
    public FilterChainProxy userFilterChain() {
        List<SecurityFilterChain> securityFilterChains = new ArrayList<>();
        securityFilterChains.add(
                filterChain(antPathRequestMatcher("/user/subscriptions/**", "DELETE"), userFilter));
        return new FilterChainProxy(securityFilterChains);
    }

    @Bean
    public FilterRegistrationBean userFilterChainRegistration() throws Exception {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(userFilterChain());
        filterRegistrationBean.setEnabled(false);
        return filterRegistrationBean;
    }
    // --- REGISTERED USER FILTER (end) --- //

    // --- AGE RESTRICTION FILTER (start) --- //
    @Bean
    public AgeRestrictionFilter ageRestrictionFilter() {
        return new AgeRestrictionFilter();
    }

    @Bean
    public FilterRegistrationBean ageRestrictionRegistration() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(ageRestrictionFilter());
        filterRegistrationBean.setEnabled(false);
        return filterRegistrationBean;
    }

    /**
     * FilterChainProxy to invoke the age restriction filter for all endpoints
     * that requires age restriction.
     *
     * @return FilterChainProxy
     */
    @Bean
    public FilterChainProxy ageRestrictionFilterChain() {
        List<SecurityFilterChain> securityFilterChains = new ArrayList<>();
        securityFilterChains.add(
                filterChain(antPathRequestMatcher("/user/subscriptions/**"), ageRestrictionFilter()));
        securityFilterChains.add(
                filterChain(antPathRequestMatcher("/user/relations"), ageRestrictionFilter()));
        securityFilterChains.add(
                filterChain(antPathRequestMatcher("/authorize/**"), ageRestrictionFilter()));

        return new FilterChainProxy(securityFilterChains);
    }

    @Bean
    public FilterRegistrationBean ageRestrictionFilterChainRegistration() throws Exception {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(ageRestrictionFilter());
        filterRegistrationBean.setEnabled(false);
        return filterRegistrationBean;
    }


    // --- AGE RESTRICTION FILTER (end) --- //

    @Bean
    @ConditionalOnMissingBean(value = AuthenticationDetailsSource.class)
    public AuthenticationDetailsSource<HttpServletRequest, PrenumerationstjanstAuthenticationDetails> prenumerationstjanstDetailsSource() {
        return new PrenumerationstjanstAuthenticationDetailsSource();
    }

    @Bean
    public StatelessCsrfFilter statelessCsrfFilter() {
        return new StatelessCsrfFilter(requireCsrfProtectionMatcher());
    }

    /**
     * Required to disable auto-registration of statelessCsrfFilter
     * @return
     */
    @Bean
    public FilterRegistrationBean statelessCsrfFilterRegistration() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(statelessCsrfFilter());
        filterRegistrationBean.setEnabled(false);
        return filterRegistrationBean;
    }


    @Bean
    public PrenumerationstjanstEntryPoint prenumerationstjanstEntryPoint() {
        return new PrenumerationstjanstEntryPoint();
    }

    private AntPathRequestMatcher antPathRequestMatcher(String pattern) {
        return new AntPathRequestMatcher(pattern);
    }

    private AntPathRequestMatcher antPathRequestMatcher(String pattern, String method) {
        return new AntPathRequestMatcher(pattern, method);
    }


    private DefaultSecurityFilterChain filterChain(RequestMatcher requestMatcher, Filter filter) {
        return new DefaultSecurityFilterChain(requestMatcher, filter);
    }

    /**
     * Helper to make it possible to disable csrf-protection via property
     *
     * If disableCsrfProtection=true, no requests will require protection,
     *
     * if disableCsrfProtection=false, all POST/PUT/DELETE-requests will require
     * protection exception for POST callbacks from CHBASE
     *
     * @return
     */
    private RequestMatcher requireCsrfProtectionMatcher() {
        if (disableCsrfProtection) {
            logger.warn("CSRF protection is DISABLED");
            return new NegatedRequestMatcher(AnyRequestMatcher.INSTANCE);
        } else {
            return new OrRequestMatcher(
                    //POST callbacks from CHBASE should not require csrf protection
                    new RegexRequestMatcher("^/(?!chbase).*$", "POST"),
                    new AntPathRequestMatcher("/**", "PUT"),
                    new AntPathRequestMatcher("/**", "DELETE")
            );
        }
    }
}
