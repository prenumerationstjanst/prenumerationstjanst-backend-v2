package se.skl.prenumerationstjanst.config;

import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import se.mvk.itintegration.userprofile.getsubjectofcare.v2.rivtabp21.GetSubjectOfCareResponderInterface;
import se.mvk.itintegration.userprofile.getsubjectofcare.v2.rivtabp21.GetSubjectOfCareResponderService;

/**
 * @author Martin Samuelsson
 */
@Configuration
public class ApiWsClientConfig {

    private static final Logger logger = LoggerFactory.getLogger(ApiWsClientConfig.class);

    @Value("${pt.subject_of_care_url}")
    private String getSubjectOfCareUrl;

    @Bean
    public GetSubjectOfCareResponderInterface getSubjectOfCareClient() {
        logger.info("Configuring getSubjectOfCareClient. URL: {}", getSubjectOfCareUrl);
        GetSubjectOfCareResponderInterface getSubjectOfCareResponderService = new GetSubjectOfCareResponderService()
                .getGetSubjectOfCareResponderPort();
        ClientProxy.getClient(getSubjectOfCareResponderService).getRequestContext()
                .put(Message.ENDPOINT_ADDRESS, getSubjectOfCareUrl);
        return getSubjectOfCareResponderService;
    }

}
