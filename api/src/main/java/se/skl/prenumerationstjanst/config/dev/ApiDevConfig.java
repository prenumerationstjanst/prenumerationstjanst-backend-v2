package se.skl.prenumerationstjanst.config.dev;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.AuthenticationManager;
import se.skl.prenumerationstjanst.service.PhkInfoService;
import se.skl.prenumerationstjanst.service.mock.PhkInfoServiceMockImpl;
import se.skl.prenumerationstjanst.service.monitoring.LoggingMonitoringService;
import se.skl.prenumerationstjanst.service.monitoring.MonitoringService;
import se.skl.prenumerationstjanst.web.security.auth.PreAuthFilter;
import se.skl.prenumerationstjanst.web.security.auth.PrenumerationstjanstAuthenticationDetails;
import se.skl.prenumerationstjanst.web.security.auth.UserFilter;
import se.skl.prenumerationstjanst.web.security.auth.mock.DevAuthenticationDetailsSource;
import se.skl.prenumerationstjanst.web.security.auth.mock.DevPreAuthFilter;
import se.skl.prenumerationstjanst.web.security.auth.mock.DevUserFilter;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Martin Samuelsson
 */
@Configuration
@Profile({"dev", "mockuser", "nophk"})
public class ApiDevConfig {

    private static final Logger logger = LoggerFactory.getLogger(ApiDevConfig.class);

    @Autowired
    private AuthenticationManager authenticationManager;

    /**
     * Preauth filter activated with the 'mockuser' profile
     * @return PreAuthFilter with mocked data (Tolvan)
     * @throws Exception
     */
    @Bean
    @Profile("mockuser")
    public PreAuthFilter devPreAuthFilter() throws Exception {
        DevPreAuthFilter devPreAuthFilter = new DevPreAuthFilter();
        devPreAuthFilter.setAuthenticationManager(authenticationManager);
        logger.warn("created for TEST: {}", devPreAuthFilter);
        return devPreAuthFilter;
    }

    @Bean(name = "userFilter")
    @Profile({"mockuser", "nophk"})
    public UserFilter devUserFilter() throws Exception {
        DevUserFilter devUserFilter = new DevUserFilter();
        logger.warn("created for TEST: {}", devUserFilter);
        return devUserFilter;
    }

    @Bean(name = "phkInfoService")
    @Profile("nophk")
    public PhkInfoService mockPhkInfoService() throws Exception {
        PhkInfoServiceMockImpl phkInfoServiceMock = new PhkInfoServiceMockImpl();
        logger.warn("created for TEST: {}", phkInfoServiceMock);
        return phkInfoServiceMock;
    }

    @Bean
    @Profile("mockuser")
    public AuthenticationDetailsSource<HttpServletRequest, PrenumerationstjanstAuthenticationDetails> devDetailsSource() {
        DevAuthenticationDetailsSource devAuthenticationDetailsSource = new DevAuthenticationDetailsSource();
        logger.warn("created for TEST: {}", devAuthenticationDetailsSource);
        return devAuthenticationDetailsSource;
    }

    @Bean
    @Profile("dev")
    public MonitoringService devMonitoringService() {
        return new LoggingMonitoringService();
    }

}
