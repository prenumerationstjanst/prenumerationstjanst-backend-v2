package se.skl.prenumerationstjanst.service;

import org.springframework.stereotype.Service;
import se.skl.prenumerationstjanst.dto.ApigwConsentDTO;
import se.skl.prenumerationstjanst.dto.SubscriptionDTO;
import se.skl.prenumerationstjanst.dto.api.SubscriptionApiDTO;

import java.util.Optional;

/**
 * @author Martin Samuelsson
 */
@Service
public class ApiTransformationService {

    public SubscriptionApiDTO toApiDTO(SubscriptionDTO subscriptionDTO) {
        return SubscriptionApiDTO.create()
                .withId(subscriptionDTO.getId())
                .withApprovedTransferType(subscriptionDTO.getApprovedTransferType())
                .withHealthcareExpiration(
                        Optional.ofNullable(subscriptionDTO.getApigwConsent())
                            .map(ApigwConsentDTO::getExpirationDateTime)
                            .orElse(null))
                .withHealthAccountExpiration(null) //TODO: add PHK expiration
                .withComplete(subscriptionDTO.getApigwConsent() != null);
    }
}
