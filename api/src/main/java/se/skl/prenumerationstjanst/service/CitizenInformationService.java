package se.skl.prenumerationstjanst.service;

/**
 * @author Martin Samuelsson
 */
public interface CitizenInformationService {
    /**
     * Returns the given name + last name in the form of
     * givenName + " " + lastName if possible, otherwise
     * the full firstName + lastName.
     *
     * If the names can not be determined, null is returned
     *
     * @param crn Citizen crn
     * @return full displayable name of a citizen, or null if not found
     */
    String getCitizenName(String crn);
}
