package se.skl.prenumerationstjanst.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;

/**
 * @author Martin Samuelsson
 */
public class CitizenInformationServiceCachingImpl implements CitizenInformationService {

    private static final Logger logger = LoggerFactory.getLogger(CitizenInformationServiceCachingImpl.class);

    private CitizenInformationService citizenInformationService;

    public CitizenInformationServiceCachingImpl(CitizenInformationService citizenInformationService) {
        this.citizenInformationService = citizenInformationService;
    }

    @Override
    @Cacheable(value="citizenName", unless = "#result == null")
    public String getCitizenName(String crn) {
        logger.info("getCitizenName(crn[{}])", crn);
        return citizenInformationService.getCitizenName(crn);
    }
}
