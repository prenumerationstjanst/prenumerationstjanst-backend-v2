package se.skl.prenumerationstjanst.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import se.mvk.itintegration.userprofile.getsubjectofcare.v2.rivtabp21.GetSubjectOfCareResponderInterface;
import se.mvk.itintegration.userprofile.getsubjectofcareresponder.v2.GetSubjectOfCareResponseType;
import se.mvk.itintegration.userprofile.getsubjectofcareresponder.v2.GetSubjectOfCareType;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Martin Samuelsson
 */
public class CitizenInformationServiceImpl implements CitizenInformationService {

    private static final Logger logger = LoggerFactory.getLogger(CitizenInformationServiceImpl.class);

    private static final Pattern GIVEN_NAME_PATTERN = Pattern.compile(".*/(.+?)/.*");

    @Autowired
    private GetSubjectOfCareResponderInterface getSubjectOfCareService;

    @Override
    public String getCitizenName(String crn) {
        logger.info("getCitizenName(crn[{}])", crn);
        GetSubjectOfCareType getSubjectOfCareType = new GetSubjectOfCareType();
        getSubjectOfCareType.setSubjectOfCare(crn);
        GetSubjectOfCareResponseType subjectOfCare;
        try {
            subjectOfCare = getSubjectOfCareService.getSubjectOfCare(getSubjectOfCareType);
        } catch (Exception e) {
            logger.error("Caught exception while calling GetSubjectOfCare", e);
            return null;
        }
        String firstName = subjectOfCare.getSubjectOfCare().getFirstName();
        String lastName = subjectOfCare.getSubjectOfCare().getLastName();
        Matcher givenNameMatcher = GIVEN_NAME_PATTERN.matcher(firstName);
        if (givenNameMatcher.matches()) {
            return givenNameMatcher.group(1) + " " + lastName;
        }
        return firstName + " " + lastName;
    }
}
