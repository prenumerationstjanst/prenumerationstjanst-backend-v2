package se.skl.prenumerationstjanst.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedCredentialsNotFoundException;
import org.springframework.stereotype.Service;
import se.skl.prenumerationstjanst.repository.UserContextRepository;
import se.skl.prenumerationstjanst.repository.UserRepository;
import se.skl.prenumerationstjanst.web.security.auth.CitizenContext;
import se.skl.prenumerationstjanst.web.security.auth.PrenumerationstjanstAuthenticationDetails;
import se.skl.prenumerationstjanst.web.security.auth.SimpleCitizen;

import java.util.Collection;

/**
 * @author Martin Samuelsson
 */
@Service
public class SecurityContextService {

    private static final Logger logger = LoggerFactory.getLogger(SecurityContextService.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserContextRepository userContextRepository;

    public boolean isRegisteredUser() {
        SimpleCitizen performingCitizen = getPerformingCitizen();
        //TODO: should this method take phkPersonId as well?
        return userRepository.findByCrn(performingCitizen.getCrn()) != null;
    }

    public boolean isRegisteredUserContext() {
        return userContextRepository.exists(getPerformingCitizenCrn(), getRepresentingCitizenCrn());
    }

    public CitizenContext getCurrentCitizenContext() {
        Authentication authentication = getAuthentication();
        if (authentication.getPrincipal() instanceof CitizenContext) {
            return ((CitizenContext) authentication.getPrincipal());
        }
        throw new IllegalStateException("Expected CitizenContext as principal, authentication not correct");
    }

    public SimpleCitizen getPerformingCitizen() {
        return getCurrentCitizenContext().getPerformingCitizen();
    }

    public SimpleCitizen getRepresentingCitizen() {
        return getCurrentCitizenContext().getRepresentingCitizen();
    }

    public String getPerformingCitizenCrn() {
        return getPerformingCitizen().getCrn();
    }

    public String getRepresentingCitizenCrn() {
        return getRepresentingCitizen().getCrn();
    }

    public Collection<? extends GrantedAuthority> getCurrentAuthorities() {
        Authentication authentication = getAuthentication();
        return authentication.getAuthorities();
    }

    public void setAuthentication(Authentication authentication) {
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    public String getSessionId() {
        return getAuthenticationDetails().getSessionId();
    }

    public PrenumerationstjanstAuthenticationDetails getAuthenticationDetails() {
        Authentication authentication = getAuthentication();
        Object details = authentication.getDetails();
        if (details instanceof PrenumerationstjanstAuthenticationDetails) {
            return (PrenumerationstjanstAuthenticationDetails) details;
        } else {
            logger.error("authentication details of incorrect type: {}", details.getClass().getName());
            throw new PreAuthenticatedCredentialsNotFoundException("authentication details are incorrect");
        }
    }

    private Authentication getAuthentication() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            throw new IllegalStateException("no authentication found");
        }
        return authentication;
    }
}
