package se.skl.prenumerationstjanst.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se.skl.prenumerationstjanst.model.KeyValue;
import se.skl.prenumerationstjanst.repository.KeyValueRepository;

/**
 * @author Martin Samuelsson
 */
@Service
@Transactional
public class SimpleKeyValueService {

    @Autowired
    private KeyValueRepository keyValueRepository;

    public void set(String key, String sessionId, String value) {
        KeyValue keyValue = keyValueRepository.findByKey(key, sessionId);
        if (keyValue != null) {
            keyValue.setValue(value);
        } else {
            keyValue = new KeyValue(key, sessionId, value);
        }
        keyValueRepository.save(keyValue);
    }

    public String get(String key, String sessionId) {
        KeyValue keyValue = keyValueRepository.findByKey(key, sessionId);
        if (keyValue != null) {
            return keyValue.getValue();
        }
        return null;
    }

}
