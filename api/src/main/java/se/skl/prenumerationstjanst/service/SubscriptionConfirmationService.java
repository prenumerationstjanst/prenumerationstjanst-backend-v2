package se.skl.prenumerationstjanst.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.skl.prenumerationstjanst.web.dto.SubscriptionConfirmationDTO;

import java.io.IOException;
import java.util.UUID;

/**
 * @author Martin Samuelsson
 */
@Service
public class SubscriptionConfirmationService {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionConfirmationService.class);

    private final SimpleKeyValueService simpleKeyValueService;

    private final ObjectMapper objectMapper;

    @Autowired
    public SubscriptionConfirmationService(SimpleKeyValueService simpleKeyValueService) {
        this.simpleKeyValueService = simpleKeyValueService;
        this.objectMapper = new ObjectMapper();
    }

    public String set(SubscriptionConfirmationDTO confirmation, String sessionId) {
        String key = UUID.randomUUID().toString();
        try {
            String csr = objectMapper.writeValueAsString(confirmation);
            simpleKeyValueService.set(key, sessionId, csr);
            return key;
        } catch (JsonProcessingException e) {
            logger.error("could not create json string from SubscriptionConfirmationDTO", e);
        }
        return null;
    }

    public SubscriptionConfirmationDTO get(String key, String sessionId) {
        String val = simpleKeyValueService.get(key, sessionId);
        if (val != null) {
            try {
                return objectMapper.readValue(val, SubscriptionConfirmationDTO.class);
            } catch (IOException e) {
                logger.error("could not create SubscriptionConfirmationDTO from json string", e);
            }
        }
        return null;
    }
}
