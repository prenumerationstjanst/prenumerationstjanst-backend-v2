package se.skl.prenumerationstjanst.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import se.skl.prenumerationstjanst.dto.api.ErrorApiDTO;
import se.skl.prenumerationstjanst.dto.api.ValidationErrorApiDTO;
import se.skl.prenumerationstjanst.exception.subscription.SubscriptionCreationFailedException;
import se.skl.prenumerationstjanst.exception.subscription.SubscriptionDeletionFailedException;
import se.skl.prenumerationstjanst.exception.subscription.SubscriptionNotFoundException;
import se.skl.prenumerationstjanst.exception.user.UserCreationFailedException;
import se.skl.prenumerationstjanst.exception.user.UserNotFoundException;
import se.skl.prenumerationstjanst.web.exception.NotFoundException;
import se.skl.prenumerationstjanst.web.exception.ValidationException;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Martin Samuelsson
 */
@ControllerAdvice
public class ApiExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(ApiExceptionHandler.class);

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(UserCreationFailedException.class)
    @ResponseBody
    public ErrorApiDTO handleUserCreationFailedException(UserCreationFailedException e) {
        logger.warn("encountered {}", UserCreationFailedException.class.getSimpleName());
        return new ErrorApiDTO("USER_CREATION_FAILED", "could not create user");
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(UserNotFoundException.class)
    @ResponseBody
    public ErrorApiDTO handleUserNotFoundException(UserNotFoundException e) {
        logger.warn("encountered {}", UserNotFoundException.class.getSimpleName());
        return new ErrorApiDTO("USER_NOT_FOUND", "could not find user");
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ValidationException.class)
    @ResponseBody
    public ValidationErrorApiDTO handleValidationException(ValidationException e) {
        logger.warn("encountered {}", ValidationException.class.getSimpleName());
        List<ValidationErrorApiDTO.FieldError> fieldErrors = e.getErrors().getFieldErrors().stream()
                .map(fieldError -> new ValidationErrorApiDTO.FieldError(fieldError.getField(), fieldError.getCode(),
                        fieldError.getRejectedValue()))
                .collect(Collectors.toList());
        return new ValidationErrorApiDTO("VALIDATION_FAILED", "validation failed", fieldErrors);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(SubscriptionCreationFailedException.class)
    @ResponseBody
    public ErrorApiDTO handleSubscriptionCreationFailedException(SubscriptionCreationFailedException e) {
        logger.warn("encountered {}: {}", SubscriptionCreationFailedException.class.getSimpleName(), e.getMessage());
        return new ErrorApiDTO("SUBSCRIPTION_CREATION_FAILED", e.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(SubscriptionDeletionFailedException.class)
    @ResponseBody
    public ErrorApiDTO handleSubscriptionModificationFailedException(SubscriptionDeletionFailedException e) {
        logger.warn("encountered {}: {}", SubscriptionDeletionFailedException.class.getSimpleName(), e.getMessage());
        return new ErrorApiDTO("SUBSCRIPTION_MODIFICATION_FAILED", e.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(SubscriptionNotFoundException.class)
    @ResponseBody
    public ErrorApiDTO handleSubscriptionNotFoundException(SubscriptionNotFoundException e) {
        logger.warn("encountered {}", SubscriptionNotFoundException.class.getSimpleName());
        return new ErrorApiDTO("SUBSCRIPTION_NOT_FOUND", "could not find subscription");
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException.class)
    @ResponseBody
    public ErrorApiDTO handleNotFoundException(NotFoundException e) {
        logger.warn("encountered {}", NotFoundException.class.getSimpleName());
        return new ErrorApiDTO("SUBCRIPTION_CONFIRMATION_NOT_FOUND", "could not find subscription confirmation");
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public ErrorApiDTO handleRuntimeException(RuntimeException e) {
        logger.error("encountered RuntimeException (" + e.getClass().getName() + ")", e);
        return new ErrorApiDTO("INTERNAL_SERVER_ERROR", "internal server error");
    }

}
