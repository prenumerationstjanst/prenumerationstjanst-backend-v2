package se.skl.prenumerationstjanst.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import se.skl.prenumerationstjanst.PrenumerationstjanstConstants;
import se.skl.prenumerationstjanst.service.SecurityContextService;
import se.skl.prenumerationstjanst.service.SimpleKeyValueService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Third-party CHBase apps (like ourselves) register a single callback URL with CHBase that is expected to handle
 * various kinds of actions based on request parameters and, in some cases, POST:ed form data.
 *
 * This controller serves as the single point of entry for various CHBase callbacks. Most are redirected to the
 * 'complete authorization' flow while target=HOME is redirected straight to /
 *
 * @author Martin Samuelsson
 */
@RestController
@RequestMapping(value = "/chbase")
public class CHBaseRoutingController {

    private static final Logger logger = LoggerFactory.getLogger(CHBaseRoutingController.class);

    @Autowired
    private SecurityContextService securityContextService;

    @Autowired
    private SimpleKeyValueService keyValueService;

    @RequestMapping
    public void redirect(@RequestParam(value = "target") String target,
                         @RequestParam(value = "actionqs") String actionqs,
                         @RequestParam(value = "wctoken", required = false) String wctoken, //form data
                         HttpServletResponse response) throws IOException {
        String sessionId = securityContextService.getSessionId();
        String redirect;
        if (target.equalsIgnoreCase("HOME")) {
            redirect = "/";
        } else {
            keyValueService.set(PrenumerationstjanstConstants.PHK_WCTOKEN_SESSION_KEY, sessionId, wctoken);
            redirect = String.format("/authorize/phk/complete?target=%s&actionqs=%s", target, actionqs);
        }
        response.sendRedirect(redirect);
    }
}
