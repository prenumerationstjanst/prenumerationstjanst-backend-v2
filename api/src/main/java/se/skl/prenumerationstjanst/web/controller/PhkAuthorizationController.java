package se.skl.prenumerationstjanst.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import se.skl.prenumerationstjanst.dto.RecordDTO;
import se.skl.prenumerationstjanst.dto.SubscriptionDTO;
import se.skl.prenumerationstjanst.dto.UserContextDTO;
import se.skl.prenumerationstjanst.dto.UserDTO;
import se.skl.prenumerationstjanst.model.TransferType;
import se.skl.prenumerationstjanst.service.*;
import se.skl.prenumerationstjanst.web.dto.SubscriptionConfirmationDTO;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.UUID;

import static se.skl.prenumerationstjanst.PrenumerationstjanstConstants.*;

/**
 * @author Martin Samuelsson
 */
@RestController
@RequestMapping(value = "/authorize/phk")
public class PhkAuthorizationController {

    private static final Logger logger = LoggerFactory.getLogger(PhkAuthorizationController.class);

    @Autowired
    private UrlService urlService;

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private TransferService transferService;

    @Autowired
    private PhkInfoService phkInfoService;

    @Autowired
    private SecurityContextService securityContextService;

    @Autowired
    SimpleKeyValueService cachedKeyValueService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserContextService userContextService;

    @Autowired
    private SubscriptionConfirmationService subscriptionResponseService;

    /**
     * Controller method that generates a 'state' value in the current session
     * and redirects the client to PHK CHBase for authentication and optionally authorization
     *
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "/auth/start")
    public void authStart(HttpServletResponse response) throws IOException {
        String sessionId = securityContextService.getSessionId();
        String state = UUID.randomUUID().toString();
        cachedKeyValueService.set(PHK_CLIENT_STATE_SESSION_KEY, sessionId, state);
        response.sendRedirect(urlService.getPhkRemoteAuthUrl(state));
    }

    /**
     * Controller method that handles various auth related callback scenarios.
     *
     * If the client returns with a successful auth related target, we can assume that this is related
     * to an ongoing user creation flow. The client will bring back a token (wctoken) that enables
     * us to fetch the users person id from CHBase. This will be used to create our User. After successful creation,
     * the user will be redirected to /#home in the GUI.
     *
     * @param target
     * @param state
     * @param wctoken
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "/complete")
    public void phkComplete(
            @RequestParam(value = "target") String target,
            @RequestParam(value = "actionqs", required = false) String state,
            @RequestParam(value = "wctoken", required = false) String wctoken, //form data
            HttpServletResponse response) throws IOException {
        String sessionId = securityContextService.getSessionId();
        cachedKeyValueService.get(PHK_WCTOKEN_SESSION_KEY, sessionId);
        if (wctoken == null || wctoken.isEmpty()) {
            wctoken = cachedKeyValueService.get(PHK_WCTOKEN_SESSION_KEY, sessionId); //if we came via CHBaseRoutingController
        }
        logger.debug("complete callback from PHK");
        String redirect = "/#home";
        if (target.equalsIgnoreCase("APPAUTHSUCCESS")
                || target.equalsIgnoreCase("SELECTEDRECORDCHANGED")) {
            String savedState = cachedKeyValueService.get(PHK_CLIENT_STATE_SESSION_KEY, sessionId);
            if (savedState != null && savedState.equals(state)) {
                if (wctoken != null) {
                    RecordDTO record = phkInfoService.getRecordForAuthToken(wctoken);
                    String performingCitizenCrn = securityContextService.getPerformingCitizenCrn();
                    UserDTO user = userService.getUser(performingCitizenCrn);
                    if (user != null) {
                        if (!user.getPhkPersonId().equalsIgnoreCase(record.getPersonId())) {
                            logger.warn("user already exists but with another phkPersonId, will update");
                            userService.updateUser(performingCitizenCrn, record.getPersonId());
                        } else {
                            logger.debug("user already exists");
                        }
                    } else {
                        logger.info("userService: {}", userService);
                        logger.info("performingCitizenCrn: {}", performingCitizenCrn);
                        logger.info("record: {}", record);
                        user = userService.createUser(performingCitizenCrn, record.getPersonId());
                        logger.debug("created user: {}", user);
                    }
                    String representingCitizenCrn = securityContextService.getRepresentingCitizenCrn();
                    UserContextDTO userContext =
                            userContextService.getUserContext(performingCitizenCrn, representingCitizenCrn);
                    if (userContext != null) {
                        if (!userContext.getPhkRecordId().equalsIgnoreCase(record.getRecordId())) {
                            logger.warn("userContext already exists but with another phkRecordId, will update");
                            userContextService.updateUserContext(performingCitizenCrn,
                                    representingCitizenCrn, record.getRecordId());
                        }
                    } else {
                        userContext = userContextService.createUserContext(performingCitizenCrn, representingCitizenCrn,
                                record.getRecordId());
                        logger.debug("created userContext: {}", userContext);
                    }
                    String ttString = cachedKeyValueService.get(TRANSFER_TYPES_SESSION_KEY, sessionId);
                    if (ttString != null) {
                        StringTokenizer stringTokenizer = new StringTokenizer(ttString, ",");
                        SubscriptionConfirmationDTO subscriptionConfirmationDTO = new SubscriptionConfirmationDTO();
                        subscriptionConfirmationDTO.setCreate(true);
                        while(stringTokenizer.hasMoreElements()) {
                            TransferType transferType = TransferType.valueOf(stringTokenizer.nextToken());
                            //noinspection Duplicates
                            try {
                                SubscriptionDTO subscription = subscriptionService.createSubscription(performingCitizenCrn,
                                        representingCitizenCrn, transferType);
                                subscriptionConfirmationDTO.getSuccess().add(subscription.getApprovedTransferType());
                                transferService.startInitialTransfersForSubscription(subscription.getId(),
                                        subscription.getApprovedTransferType());
                            } catch (Exception e) {
                                logger.error("Caught exception while creating subscription", e);
                                subscriptionConfirmationDTO.getFail().add(transferType.name());
                            }
                        }
                        String key = subscriptionResponseService.set(subscriptionConfirmationDTO, sessionId);
                        redirect = "/#subscription-confirmation/" + key;
                    } else {
                        logger.warn("no transfer types saved in session");
                        redirect = "/#500";
                    }
                } else {
                    logger.warn("no authorization code received");
                    redirect = "/#500";
                }
            } else {
                logger.warn("saved state {} is not equal to client state {}", savedState, state);
                redirect = "/#500";
            }
        } else if(target.equalsIgnoreCase("APPAUTHREJECT")) {
            logger.debug("user did not authorize us in PHK");
        }
        response.sendRedirect(redirect);
    }
}
