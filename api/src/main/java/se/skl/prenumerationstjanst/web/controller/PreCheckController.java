package se.skl.prenumerationstjanst.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import se.skl.prenumerationstjanst.service.PreCheckService;
import se.skl.prenumerationstjanst.service.SecurityContextService;

/**
 * @author Martin Samuelsson
 */
@RestController
@RequestMapping(value = "/user/precheck")
public class PreCheckController {

    @Autowired
    private SecurityContextService securityContextService;

    @Autowired
    private PreCheckService preCheckService;

    private static final Logger logger = LoggerFactory.getLogger(PreCheckController.class);

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Boolean preCheck() {
        String performingCitizenCrn = securityContextService.getPerformingCitizenCrn();
        String representingCitizenCrn = securityContextService.getRepresentingCitizenCrn();
        try {
            return preCheckService.preCheckAndExecuteCleanup(performingCitizenCrn, representingCitizenCrn);
        } catch(Exception e) { //TODO: PreCheckFailedException
            return null;
        }
    }
}
