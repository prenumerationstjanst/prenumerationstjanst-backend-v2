package se.skl.prenumerationstjanst.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import se.skl.prenumerationstjanst.PrenumerationstjanstConstants;
import se.skl.prenumerationstjanst.dto.SubscriptionDTO;
import se.skl.prenumerationstjanst.dto.api.SubscriptionApiDTO;
import se.skl.prenumerationstjanst.exception.subscription.SubscriptionCreationFailedException;
import se.skl.prenumerationstjanst.exception.subscription.SubscriptionDeletionFailedException;
import se.skl.prenumerationstjanst.model.TransferType;
import se.skl.prenumerationstjanst.service.*;
import se.skl.prenumerationstjanst.web.dto.SubscriptionActionRequestDTO;
import se.skl.prenumerationstjanst.web.dto.SubscriptionActionResponseDTO;
import se.skl.prenumerationstjanst.web.dto.SubscriptionConfirmationDTO;
import se.skl.prenumerationstjanst.web.exception.NotFoundException;
import se.skl.prenumerationstjanst.web.exception.ValidationException;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;

/**
 * @author Martin Samuelsson
 */
@RestController
@RequestMapping("/user/subscriptions")
public class SubscriptionController {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionController.class);

    @Autowired
    private SecurityContextService securityContextService;

    @Autowired
    private UserContextService userContextService;

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private UrlService urlService;

    @Autowired
    private ApiTransformationService apiTransformationService;

    @Autowired
    private TransferService transferService;

    @Autowired
    private SimpleKeyValueService keyValueService;

    @Autowired
    private SubscriptionConfirmationService subscriptionConfirmationService;

    @Autowired
    private PreCheckService preCheckService;

    @Value("${pt.relation_subscription_age_limit}")
    private int relationAgeLimit;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<SubscriptionApiDTO> getSubscriptions() {
        String performingCitizenCrn = securityContextService.getPerformingCitizenCrn();
        String representingCitizenCrn = securityContextService.getRepresentingCitizenCrn();
        return subscriptionService.getSubscriptionsForUserContext(performingCitizenCrn, representingCitizenCrn)
                .stream()
                .map(subscriptionDTO -> apiTransformationService.toApiDTO(subscriptionDTO))
                .collect(Collectors.toList());
    }

    @RequestMapping(method = RequestMethod.GET, path = "/status", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SubscriptionConfirmationDTO getCreateStatus(@RequestParam String key) {
        String sessionId = securityContextService.getSessionId();
        SubscriptionConfirmationDTO dto = subscriptionConfirmationService.get(key, sessionId);
        if (dto != null) {
            return dto;
        } else {
            throw new NotFoundException();
        }
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SubscriptionActionResponseDTO createSubscription(
            @Valid @RequestBody SubscriptionActionRequestDTO subscriptionActionRequestDTO,
            BindingResult bindingResult) throws IOException {
        if (bindingResult.hasErrors()) {
            throw new ValidationException(bindingResult);
        }
        String performingCitizenCrn = securityContextService.getPerformingCitizen().getCrn();
        String representingCitizenCrn = securityContextService.getRepresentingCitizen().getCrn();
        String sessionId = securityContextService.getSessionId();
        Set<TransferType> tts;
        if (subscriptionActionRequestDTO.getTransferTypes() == null) {
            throw new IllegalArgumentException("transferTypes can not be null");
        }
        try {
            tts = subscriptionActionRequestDTO.getTransferTypes().stream()
                    .map(TransferType::valueOf)
                    .collect(Collectors.toSet());
        } catch (IllegalArgumentException e) {
            throw new SubscriptionCreationFailedException("unknown transfer type");
        }
        SubscriptionActionResponseDTO subscriptionActionResponseDTO = new SubscriptionActionResponseDTO();
        Boolean preCheckSuccessful = preCheckService.preCheckAndExecuteCleanup(performingCitizenCrn, representingCitizenCrn);
        if (!preCheckSuccessful) {
            //if pre check is unsuccessful, save transferTypes in 'session',
            //respond with phkAuthorizeStartUrl and user will eventually return in PhkAuthorizationController
            //where subscription will finally be created
            StringJoiner stringJoiner = new StringJoiner(",");
            subscriptionActionRequestDTO.getTransferTypes().forEach(stringJoiner::add);
            keyValueService.set(PrenumerationstjanstConstants.TRANSFER_TYPES_SESSION_KEY, sessionId,
                    stringJoiner.toString());
            subscriptionActionResponseDTO.setPhkActionUrl(urlService.getPhkAuthorizeStartUrl());
            return subscriptionActionResponseDTO;
        } else {
            SubscriptionConfirmationDTO subscriptionConfirmationDTO = new SubscriptionConfirmationDTO();
            subscriptionConfirmationDTO.setCreate(true);
            tts.forEach(transferType -> {
                //noinspection Duplicates
                try {
                    SubscriptionDTO subscription = subscriptionService.createSubscription(performingCitizenCrn,
                            representingCitizenCrn, transferType);
                    subscriptionConfirmationDTO.getSuccess().add(transferType.name());
                    transferService.startInitialTransfersForSubscription(subscription.getId(),
                            subscription.getApprovedTransferType());
                } catch (Exception e) {
                    logger.error("Caught exception while creating subscription", e);
                    subscriptionConfirmationDTO.getFail().add(transferType.name());
                }
            });
            String key = subscriptionConfirmationService.set(subscriptionConfirmationDTO, sessionId);
            subscriptionActionResponseDTO.setSubscriptionConfirmationKey(key);
            return subscriptionActionResponseDTO;
        }
    }

    @RequestMapping(value="/delete", method = RequestMethod.POST)
    @ResponseBody
    public SubscriptionActionResponseDTO deleteSubscriptions(
            @Valid @RequestBody SubscriptionActionRequestDTO subscriptionActionRequestDTO,
            BindingResult  bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new ValidationException(bindingResult);
        }
        String performingCitizenCrn = securityContextService.getPerformingCitizen().getCrn();
        String representingCitizenCrn = securityContextService.getRepresentingCitizen().getCrn();
        String sessionId = securityContextService.getSessionId();
        Set<TransferType> tts;
        if (subscriptionActionRequestDTO.getTransferTypes() == null) {
            throw new IllegalArgumentException("transferTypes can not be null");
        }
        try {
            tts = subscriptionActionRequestDTO.getTransferTypes().stream()
                    .map(TransferType::valueOf)
                    .collect(Collectors.toSet());
        } catch (IllegalArgumentException e) {
            throw new SubscriptionDeletionFailedException("unknown transfer type");
        }
        SubscriptionConfirmationDTO subscriptionConfirmationDTO = new SubscriptionConfirmationDTO();
        subscriptionConfirmationDTO.setCreate(false);
        tts.forEach(transferType -> {
            try {
                subscriptionService.removeSubscription(performingCitizenCrn, representingCitizenCrn, transferType);
                subscriptionConfirmationDTO.getSuccess().add(transferType.name());
            } catch (Exception e) {
                logger.error("caught exception while trying to remove subscription", e);
                subscriptionConfirmationDTO.getFail().add(transferType.name());
            }
        });
        String key = subscriptionConfirmationService.set(subscriptionConfirmationDTO, sessionId);
        SubscriptionActionResponseDTO subscriptionActionResponseDTO = new SubscriptionActionResponseDTO();
        subscriptionActionResponseDTO.setSubscriptionConfirmationKey(key);
        return subscriptionActionResponseDTO;
    }
}
