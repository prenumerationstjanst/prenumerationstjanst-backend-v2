package se.skl.prenumerationstjanst.web.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import se.skl.prenumerationstjanst.model.TransferType;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * @author Martin Samuelsson
 */
@RestController
@RequestMapping(value = "/transfertypes")
public class TransferTypeController {

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<String> getTransferTypes() {
        return Arrays.asList(TransferType.values()).stream()
                .map(Enum::toString).collect(Collectors.toList());
    }
}
