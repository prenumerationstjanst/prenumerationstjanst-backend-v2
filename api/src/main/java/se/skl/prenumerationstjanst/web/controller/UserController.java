package se.skl.prenumerationstjanst.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import se.skl.prenumerationstjanst.dto.UserDTO;
import se.skl.prenumerationstjanst.dto.api.UserProfileApiDTO;
import se.skl.prenumerationstjanst.service.CitizenInformationService;
import se.skl.prenumerationstjanst.service.PhkInfoService;
import se.skl.prenumerationstjanst.service.SecurityContextService;
import se.skl.prenumerationstjanst.service.UserService;
import se.skl.prenumerationstjanst.web.security.auth.SimpleCitizen;

/**
 * @author Martin Samuelsson
 */
@RestController
@RequestMapping(value = "/user/profile")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private SecurityContextService securityContextService;

    @Autowired
    private CitizenInformationService citizenInformationService;

    @Autowired
    private PhkInfoService phkInfoService;

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public UserProfileApiDTO getUser() {
        return getCurrentUserOrCitizen();
    }

    private UserProfileApiDTO getCurrentUserOrCitizen() {
        SimpleCitizen performingCitizen = securityContextService.getPerformingCitizen();
        String citizenName = citizenInformationService.getCitizenName(performingCitizen.getCrn());
        UserDTO user = userService.getUser(performingCitizen.getCrn());
        boolean phkAuthorized = user != null
                && phkInfoService.checkPhkPersonId(user.getPhkPersonId()) == PhkInfoService.PHK_PERSON_STATUS.ACTIVE;
        return UserProfileApiDTO.create()
                .withName(citizenName != null ? citizenName : performingCitizen.getName())
                .withPhkAuthorized(phkAuthorized);
    }
}
