package se.skl.prenumerationstjanst.web.dto;

import org.hibernate.validator.constraints.NotEmpty;

import java.util.Set;

/**
 * @author Martin Samuelsson
 */
public class SubscriptionActionRequestDTO {
    @NotEmpty
    private Set<String> transferTypes;

    public Set<String> getTransferTypes() {
        return transferTypes;
    }

    public void setTransferTypes(Set<String> transferTypes) {
        this.transferTypes = transferTypes;
    }

    @Override
    public String toString() {
        return "CreateSubscriptionRequestDTO{" +
                "transferTypes=" + transferTypes +
                '}';
    }
}
