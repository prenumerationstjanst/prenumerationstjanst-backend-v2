package se.skl.prenumerationstjanst.web.dto;

import java.io.Serializable;

/**
 * @author Martin Samuelsson
 */
public class SubscriptionActionResponseDTO implements Serializable {

    private static final long serialVersionUID = 0L;

    private String subscriptionConfirmationKey;
    private String phkActionUrl;

    public String getSubscriptionConfirmationKey() {
        return subscriptionConfirmationKey;
    }

    public void setSubscriptionConfirmationKey(String subscriptionConfirmationKey) {
        this.subscriptionConfirmationKey = subscriptionConfirmationKey;
    }

    public String getPhkActionUrl() {
        return phkActionUrl;
    }

    public void setPhkActionUrl(String phkActionUrl) {
        this.phkActionUrl = phkActionUrl;
    }
}
