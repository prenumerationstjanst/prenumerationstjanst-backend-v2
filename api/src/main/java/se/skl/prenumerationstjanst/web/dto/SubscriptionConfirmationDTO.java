package se.skl.prenumerationstjanst.web.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Martin Samuelsson
 */
public class SubscriptionConfirmationDTO implements Serializable {
    private static final long serialVersionUID = 0L;

    private List<String> success = new ArrayList<>();
    private List<String> fail = new ArrayList<>();
    private Boolean create;

    public List<String> getSuccess() {
        return success;
    }

    public void setSuccess(List<String> success) {
        this.success = success;
    }

    public List<String> getFail() {
        return fail;
    }

    public void setFail(List<String> fail) {
        this.fail = fail;
    }

    public Boolean getCreate() {
        return create;
    }

    public void setCreate(Boolean create) {
        this.create = create;
    }

    @Override
    public String toString() {
        return "SubscriptionConfirmationDTO{" +
                "success=" + success +
                ", fail=" + fail +
                ", create=" + create +
                '}';
    }
}
