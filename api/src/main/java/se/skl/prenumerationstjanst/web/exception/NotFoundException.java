package se.skl.prenumerationstjanst.web.exception;

/**
 * @author Martin Samuelsson
 */
public class NotFoundException extends RuntimeException {
    private static final long serialVersionUID = 5621596047364342732L;
}
