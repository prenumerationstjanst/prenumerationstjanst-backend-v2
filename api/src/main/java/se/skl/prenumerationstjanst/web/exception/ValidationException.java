package se.skl.prenumerationstjanst.web.exception;

import org.springframework.validation.Errors;

/**
 * @author Martin Samuelsson
 */
public class ValidationException extends RuntimeException {

    private static final long serialVersionUID = 2579802617029910153L;
    private Errors errors;

    public ValidationException(Errors errors) {
        this.errors = errors;
    }

    public Errors getErrors() {
        return errors;
    }
}
