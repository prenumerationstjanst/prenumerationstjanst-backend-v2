package se.skl.prenumerationstjanst.web.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.filter.OncePerRequestFilter;
import se.skl.prenumerationstjanst.service.SecurityContextService;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter that checks that the citizen is of correct age to use the application
 *
 * Must be placed after PreAuthFilter in the Spring Security filter chain
 *
 * @author Martin Samuelsson
 */
public class AgeRestrictionFilter extends OncePerRequestFilter implements Filter {

    private static final Logger logger = LoggerFactory.getLogger(AgeRestrictionFilter.class);

    @Autowired
    private SecurityContextService securityContextService;

    @Value("${pt.age_restriction_filter.min_required_age:18}")
    private int minRequiredAge;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        int citizenAge = securityContextService.getPerformingCitizen().getAge();
        logger.debug("citizenAge: {}", citizenAge);
        if (citizenAge < minRequiredAge) {
            logger.warn("Age of citizen ({}) is below minRequiredAge ({})", citizenAge, minRequiredAge);
            throw new AgeRestrictionException();
        }
        filterChain.doFilter(request, response);
    }

    public static class AgeRestrictionException extends AuthenticationException {
        private static final long serialVersionUID = 2539182191497364998L;

        public AgeRestrictionException() {
            super("AGE_RESTRICTION");
        }
    }
}
