package se.skl.prenumerationstjanst.web.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Handle CSRF protection in a stateless fashion. Works on the principle that only pages
 * served from the same domain as the server may modify the cookies.
 *
 * Should be declared before the default CSRF filter
 * .addFilterBefore(statelessCsrfFilter(), CsrfFilter.class)
 *
 * The default CSRF filter should be disabled
 * .csrf().disabled()
 *
 * @author Martin Samuelsson
 */
public class StatelessCsrfFilter extends OncePerRequestFilter {

    private static final Logger logger = LoggerFactory.getLogger(StatelessCsrfFilter.class);
    public static final String CSRF_TOKEN_HEADER = "X-CSRF-TOKEN";
    public static final String CSRF_TOKEN_COOKIE = "CSRF-TOKEN";

    private AccessDeniedHandler accessDeniedHandler = new AccessDeniedHandlerImpl();
    private RequestMatcher requireCsrfProtectionMatcher;

    public StatelessCsrfFilter() {
        requireCsrfProtectionMatcher = CsrfFilter.DEFAULT_CSRF_MATCHER;
    }

    public StatelessCsrfFilter(RequestMatcher requireCsrfProtectionMatcher) {
        logger.info("configuring {} with {}", this.getClass().getName(), requireCsrfProtectionMatcher);
        this.requireCsrfProtectionMatcher = requireCsrfProtectionMatcher;
    }



    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {

        if (requireCsrfProtectionMatcher.matches(request)) {
            String tokenHeader = extractCsrfHeaderValue(request);
            logger.debug("CSRF token from header {}: {}", CSRF_TOKEN_HEADER, tokenHeader);
            if (tokenHeader == null || tokenHeader.isEmpty()) {
                accessDeniedHandler.handle(request, response,
                        new AccessDeniedException("CSRF token is missing in HTTP header"));
                return;
            }
            String tokenCookie = extractCsrfCookieValue(request);
            logger.debug("CSRF token from cookie {}: {}", CSRF_TOKEN_COOKIE, tokenCookie);
            if (tokenCookie == null || tokenCookie.isEmpty()) {
                accessDeniedHandler.handle(request, response,
                        new AccessDeniedException("CSRF token is missing in cookie"));
                return;
            }
            if (!tokenHeader.equals(tokenCookie)) {
                accessDeniedHandler.handle(request, response,
                        new AccessDeniedException("CSRF token mismatch"));
                return;
            }
        }
        filterChain.doFilter(request, response);
    }

    private String extractCsrfHeaderValue(HttpServletRequest httpServletRequest) {
        return httpServletRequest.getHeader(CSRF_TOKEN_HEADER);
    }

    private String extractCsrfCookieValue(HttpServletRequest httpServletRequest) {
        Cookie[] cookies = httpServletRequest.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(CSRF_TOKEN_COOKIE)) {
                return cookie.getValue();
            }
        }
        return null;
    }
}
