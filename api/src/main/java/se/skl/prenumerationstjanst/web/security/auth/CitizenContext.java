package se.skl.prenumerationstjanst.web.security.auth;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

/**
 * @author Martin Samuelsson
 */
public class CitizenContext extends User implements UserDetails {

    private final SimpleCitizen performingCitizen;
    private final SimpleCitizen representingCitizen;

    public CitizenContext (SimpleCitizen performingCitizen, SimpleCitizen representingCitizen,
                           Collection<? extends GrantedAuthority> authorities) {
        super(performingCitizen.getCrn(), "", authorities != null ? authorities : Collections.emptySet());
        this.performingCitizen = performingCitizen;
        this.representingCitizen = representingCitizen;

    }

    public SimpleCitizen getPerformingCitizen() {
        return performingCitizen;
    }

    public SimpleCitizen getRepresentingCitizen() {
        return representingCitizen;
    }

    public CitizenContext withAuthorities(Collection<? extends GrantedAuthority> authorities) {
        return new CitizenContext(performingCitizen, representingCitizen, authorities);
    }

    @Override
    public String toString() {
        return "CitizenContext{" +
                "performingCitizen=" + performingCitizen +
                ", representingCitizen=" + representingCitizen +
                "} " + super.toString();
    }
}
