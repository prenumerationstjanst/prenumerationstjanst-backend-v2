package se.skl.prenumerationstjanst.web.security.auth;

import javax.servlet.Filter;

/**
 * @author Martin Samuelsson
 */
public interface PreAuthFilter extends Filter {
}
