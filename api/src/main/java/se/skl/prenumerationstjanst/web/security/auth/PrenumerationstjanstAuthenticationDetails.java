package se.skl.prenumerationstjanst.web.security.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Martin Samuelsson
 */
public class PrenumerationstjanstAuthenticationDetails extends WebAuthenticationDetails {

    private static final long serialVersionUID = 4572348439751438067L;

    private static final Logger logger = LoggerFactory.getLogger(PrenumerationstjanstAuthenticationDetails.class);

    private String sessionId;

    private String userIpAddress;

    public PrenumerationstjanstAuthenticationDetails(HttpServletRequest request) {
        super(request);
        this.sessionId = request.getHeader("ajp_shib-session-id"); //TODO: props
        String xForwardedFor = request.getHeader("x-forwarded-for");
        if (xForwardedFor != null) {
            this.userIpAddress = xForwardedFor.split(",", 1)[0];
        } else {
            this.userIpAddress = request.getRemoteAddr();
        }
        logger.debug("extracted sessionId[{}], userIpAddress[{}]", sessionId, userIpAddress);
    }

    @Override
    public String getSessionId() {
        return sessionId;
    }

    public String getUserIpAddress() {
        return userIpAddress;
    }
}
