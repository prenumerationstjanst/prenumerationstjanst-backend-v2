package se.skl.prenumerationstjanst.web.security.auth;

import org.springframework.security.authentication.AuthenticationDetailsSource;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Martin Samuelsson
 */
public class PrenumerationstjanstAuthenticationDetailsSource implements AuthenticationDetailsSource<HttpServletRequest, PrenumerationstjanstAuthenticationDetails> {

    @Override
    public PrenumerationstjanstAuthenticationDetails buildDetails(HttpServletRequest context) {
        return new PrenumerationstjanstAuthenticationDetails(context);
    }
}
