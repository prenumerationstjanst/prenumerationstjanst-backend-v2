package se.skl.prenumerationstjanst.web.security.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;
import org.springframework.util.Assert;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 * @author Martin Samuelsson
 */
public class PrenumerationstjanstPreAuthFilter extends RequestHeaderAuthenticationFilter implements PreAuthFilter {

    private static final Logger logger = LoggerFactory.getLogger(PrenumerationstjanstPreAuthFilter.class);

    public static final String COMMON_NAME_NOT_FOUND_VALUE = "N/A";

    @Value("${pt.pre_auth_filter.header.ssn}")
    private String subjectSerialNumberHeader;

    @Value("${pt.pre_auth_filter.header.common_name}")
    private String subjectCommonNameHeader;

    @Value("${pt.pre_auth_filter.header.age}")
    private String ageHeader;

    @Value("${pt.pre_auth_filter.header.date_of_birth}")
    private String dateOfBirthHeader;

    @Value("${pt.pre_auth_filter.header.gender}")
    private String genderHeader;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private AuthenticationDetailsSource<HttpServletRequest, PrenumerationstjanstAuthenticationDetails>
            prenumerationstjanstAuthenticationDetailsSource;

    @Override
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
        return createCitizenContext(request);
    }
    //We are currently using the same SimpleCitizen for both performer and representing
    //since there is currently no 'ombudsfunktionalitet', this is the place to separate the two when
    //this functionality is added
    private CitizenContext createCitizenContext(HttpServletRequest request) {
        String name;
        if (request.getHeader(subjectCommonNameHeader) != null) {
            name = request.getHeader(subjectCommonNameHeader);
        } else {
            name = COMMON_NAME_NOT_FOUND_VALUE;
        }
        String crn = (String) super.getPreAuthenticatedPrincipal(request);
        String gender = request.getHeader(genderHeader);
        String dob = request.getHeader(dateOfBirthHeader);
        int age = request.getIntHeader(ageHeader);
        SimpleCitizen simpleCitizen = new SimpleCitizen(crn, name, gender, dob, age);
        return new CitizenContext(simpleCitizen, simpleCitizen, null);
    }

    @Override
    protected void initFilterBean() throws ServletException {
        super.setAuthenticationManager(authenticationManager);
        super.setPrincipalRequestHeader(subjectSerialNumberHeader);
        Assert.notNull(prenumerationstjanstAuthenticationDetailsSource, "AuthenticationDetailsSource must be set");
        super.setAuthenticationDetailsSource(prenumerationstjanstAuthenticationDetailsSource);
    }
}
