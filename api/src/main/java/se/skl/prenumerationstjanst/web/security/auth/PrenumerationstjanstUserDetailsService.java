package se.skl.prenumerationstjanst.web.security.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Martin Samuelsson
 */
@Component
public class PrenumerationstjanstUserDetailsService implements
        AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken>, InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(PrenumerationstjanstUserDetailsService.class);

    @Value("${pt.userrole.idp_authenticated}")
    private String idpAuthenticatedRoles;

    @Override
    public UserDetails loadUserDetails(PreAuthenticatedAuthenticationToken preAuthenticatedAuthenticationToken)
            throws UsernameNotFoundException {
        Object principal = preAuthenticatedAuthenticationToken.getPrincipal();
        if (!(principal instanceof CitizenContext)) {
            logger.error("Expected principal to be a CitizenContext, instead it was {}", principal.getClass().getName());
            throw new UsernameNotFoundException("invalid user");
        }
        List<GrantedAuthority> roles = Arrays.stream(idpAuthenticatedRoles.split(",")) //TODO: 'cache' this?
                .map(String::trim)
                .map(s -> {
                    if (!s.startsWith("ROLE_")) {
                        return "ROLE_" + s;
                    }
                    return s;
                })
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
        return ((CitizenContext) principal).withAuthorities(roles);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(idpAuthenticatedRoles, "property 'pt.userrole.idp_authenticated' can not be null");
        Assert.hasLength(idpAuthenticatedRoles, "property 'pt.userrole.idp_authenticated' can not be empty");
    }
}
