package se.skl.prenumerationstjanst.web.security.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.util.Assert;
import org.springframework.web.filter.OncePerRequestFilter;
import se.skl.prenumerationstjanst.service.SecurityContextService;
import se.skl.prenumerationstjanst.service.UserService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Martin Samuelsson
 */
public class PrenumerationstjanstUserFilter extends OncePerRequestFilter implements UserFilter {

    private static final Logger logger = LoggerFactory.getLogger(PrenumerationstjanstUserFilter.class);

    @Autowired
    private UserService userService;

    @Autowired
    private SecurityContextService securityContextService;

    @Value("${pt.userrole.self_authenticated}")
    private String userRole;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        Collection<? extends GrantedAuthority> currentAuthorities = securityContextService.getCurrentAuthorities();
        if (!currentAuthorities.stream().anyMatch(ga -> userRole.equals(ga.getAuthority()))
                && securityContextService.isRegisteredUser()) {
            PreAuthenticatedAuthenticationToken preAuthenticatedAuthenticationToken = new PreAuthenticatedAuthenticationToken(
                    securityContextService.getCurrentCitizenContext(),
                    "N/A",
                    getUserAuthorities(securityContextService.getCurrentAuthorities()));
            preAuthenticatedAuthenticationToken.setDetails(securityContextService.getAuthenticationDetails());
            securityContextService.setAuthentication(preAuthenticatedAuthenticationToken);
        }
        filterChain.doFilter(request, response);
    }

    private Collection<GrantedAuthority> getUserAuthorities(Collection<? extends GrantedAuthority> grantedAuthorities) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.addAll(grantedAuthorities);
        authorities.add(new SimpleGrantedAuthority(userRole));
        return authorities;
    }

    @Override
    protected void initFilterBean() throws ServletException {
        Assert.notNull(userRole, "property 'pt.userrole.self_authenticated' can not be null");
        Assert.hasLength(userRole.trim(), "property 'pt.userrole.self_authenticated' can not be empty");
        if (!userRole.startsWith("ROLE_")) {
            logger.debug("adding ROLE_ prefix to {}", userRole);
            userRole = "ROLE_" + userRole;
            logger.debug("userRole is now: {}", userRole);
        }
    }
}
