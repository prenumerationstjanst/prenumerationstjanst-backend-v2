package se.skl.prenumerationstjanst.web.security.auth;

import java.io.Serializable;

/**
 * @author Martin Samuelsson
 */
public class SimpleCitizen implements Serializable {
    private static final long serialVersionUID = 0L;

    private final String crn;
    private final String name;
    private final String gender;
    private final String dateOfBirth;
    private final int age;

    public SimpleCitizen(String crn, String name, String gender, String dateOfBirth, int age) {
        this.crn = crn;
        this.name = name;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.age = age;
    }

    public String getCrn() {
        return crn;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "SimpleCitizen{" +
                "crn='" + crn + '\'' +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", age=" + age +
                '}';
    }
}
