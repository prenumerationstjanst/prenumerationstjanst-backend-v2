package se.skl.prenumerationstjanst.web.security.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.util.Assert;
import org.springframework.web.filter.OncePerRequestFilter;
import se.skl.prenumerationstjanst.service.SecurityContextService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Martin Samuelsson
 */
public class UserContextFilter extends OncePerRequestFilter implements UserFilter {

    private static final Logger logger = LoggerFactory.getLogger(UserContextFilter.class);

    @Autowired
    private SecurityContextService securityContextService;

    @Value("${pt.userrole.user_context}")
    private String userContextRole;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        Collection<? extends GrantedAuthority> currentAuthorities = securityContextService.getCurrentAuthorities();

        if (!currentAuthorities.stream().anyMatch(ga -> userContextRole.equals(ga.getAuthority()))
                && securityContextService.isRegisteredUserContext()) {
            logger.debug("encountered a valid userContext but role not set, will set");
            PreAuthenticatedAuthenticationToken preAuthenticatedAuthenticationToken =
                    new PreAuthenticatedAuthenticationToken(securityContextService.getCurrentCitizenContext(),
                    "N/A",
                    getUserAuthorities(securityContextService.getCurrentAuthorities()));

            preAuthenticatedAuthenticationToken.setDetails(securityContextService.getAuthenticationDetails());

            securityContextService.setAuthentication(preAuthenticatedAuthenticationToken);
        }
        filterChain.doFilter(request, response);
    }

    private Collection<GrantedAuthority> getUserAuthorities(Collection<? extends GrantedAuthority> grantedAuthorities) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.addAll(grantedAuthorities);
        authorities.add(new SimpleGrantedAuthority(userContextRole));
        return authorities;
    }

    @Override
    protected void initFilterBean() throws ServletException {
        Assert.notNull(userContextRole, "property 'pt.userrole.user_context' can not be null");
        Assert.hasLength(userContextRole.trim(), "property 'pt.userrole.user_context' can not be empty");
        if (!userContextRole.startsWith("ROLE_")) {
            logger.debug("adding ROLE_ prefix to {}", userContextRole);
            userContextRole = "ROLE_" + userContextRole;
            logger.debug("userRole is now: {}", userContextRole);
        }
    }
}
