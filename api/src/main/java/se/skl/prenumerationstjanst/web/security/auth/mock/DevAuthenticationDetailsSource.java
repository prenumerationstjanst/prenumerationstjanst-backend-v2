package se.skl.prenumerationstjanst.web.security.auth.mock;

import org.springframework.security.authentication.AuthenticationDetailsSource;
import se.skl.prenumerationstjanst.web.security.auth.PrenumerationstjanstAuthenticationDetails;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * @author Martin Samuelsson
 */
public class DevAuthenticationDetailsSource implements AuthenticationDetailsSource<HttpServletRequest, PrenumerationstjanstAuthenticationDetails> {

    @Override
    public PrenumerationstjanstAuthenticationDetails buildDetails(HttpServletRequest context) {
        return new PrenumerationstjanstAuthenticationDetails(new HttpServletRequestWrapper(context) {
            @Override
            public String getHeader(String name) {
                if (name.equalsIgnoreCase("ajp_shib-session-id")) {
                    return "_DEV_SESSION_ID";
                }
                return super.getHeader(name);
            }
        });
    }
}
