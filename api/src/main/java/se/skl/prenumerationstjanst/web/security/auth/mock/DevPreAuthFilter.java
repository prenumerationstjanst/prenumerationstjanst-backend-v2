package se.skl.prenumerationstjanst.web.security.auth.mock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import se.skl.prenumerationstjanst.web.security.auth.CitizenContext;
import se.skl.prenumerationstjanst.web.security.auth.PreAuthFilter;
import se.skl.prenumerationstjanst.web.security.auth.PrenumerationstjanstAuthenticationDetails;
import se.skl.prenumerationstjanst.web.security.auth.SimpleCitizen;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 * @author Martin Samuelsson
 */
public class DevPreAuthFilter extends AbstractPreAuthenticatedProcessingFilter implements PreAuthFilter {

    @Autowired
    private AuthenticationDetailsSource<HttpServletRequest, PrenumerationstjanstAuthenticationDetails> authenticationDetailsSource;

    @Override
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
        return new CitizenContext(
                new SimpleCitizen("191212121212", "Tolvan Tolvansson", "M", "191212121212", 100),
                new SimpleCitizen("191212121212", "Tolvan Tolvansson", "M", "191212121212", 100),
                null
        );
    }

    @Override
    protected Object getPreAuthenticatedCredentials(HttpServletRequest httpServletRequest) {
        return "N/A";
    }

    @Override
    protected void initFilterBean() throws ServletException {
        super.setAuthenticationDetailsSource(authenticationDetailsSource);
    }
}
