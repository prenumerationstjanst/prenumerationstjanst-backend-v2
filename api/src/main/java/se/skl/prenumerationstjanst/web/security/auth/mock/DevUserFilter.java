package se.skl.prenumerationstjanst.web.security.auth.mock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.util.Assert;
import org.springframework.web.filter.OncePerRequestFilter;
import se.skl.prenumerationstjanst.dto.UserDTO;
import se.skl.prenumerationstjanst.service.SecurityContextService;
import se.skl.prenumerationstjanst.service.UserService;
import se.skl.prenumerationstjanst.web.security.auth.UserFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * @author Martin Samuelsson
 */
public class DevUserFilter extends OncePerRequestFilter implements UserFilter {

    private static final Logger logger = LoggerFactory.getLogger(DevUserFilter.class);

    @Autowired
    private SecurityContextService securityContextService;

    @Autowired
    private UserService userService;

    @Value("${pt.userrole.self_authenticated}")
    private String userRole;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        Collection<? extends GrantedAuthority> currentAuthorities = securityContextService.getCurrentAuthorities();
        if (!currentAuthorities.stream().anyMatch(ga -> userRole.equals(ga.getAuthority()))) {
            PreAuthenticatedAuthenticationToken preAuthenticatedAuthenticationToken = new PreAuthenticatedAuthenticationToken(
                    securityContextService.getCurrentCitizenContext(),
                    "N/A",
                    getUserAuthorities(securityContextService.getCurrentAuthorities()));
            preAuthenticatedAuthenticationToken.setDetails(securityContextService.getAuthenticationDetails());
            securityContextService.setAuthentication(preAuthenticatedAuthenticationToken);
        }
        logger.debug("authentication is now: {}", SecurityContextHolder.getContext().getAuthentication());

        if (!securityContextService.isRegisteredUser()) {
            logger.debug("Trying to register user since we want to pretend this is a real user");
            UserDTO user = userService.createUser(securityContextService.getPerformingCitizenCrn(), UUID.randomUUID()
                    .toString());
            logger.debug("created: {}", user);
        }
        filterChain.doFilter(request, response);
    }

    private Collection<GrantedAuthority> getUserAuthorities(Collection<? extends GrantedAuthority> grantedAuthorities) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.addAll(grantedAuthorities);
        authorities.add(new SimpleGrantedAuthority(userRole));
        return authorities;
    }

    @Override
    public void afterPropertiesSet() throws ServletException {
        Assert.notNull(userRole, "property 'pt.userrole.self_authenticated' can not be null");
        Assert.hasLength(userRole.trim(), "property 'pt.userrole.self_authenticated' can not be empty");
        if (!userRole.startsWith("ROLE_")) {
            logger.debug("adding ROLE_ prefix to {}", userRole);
            userRole = "ROLE_" + userRole;
            logger.debug("userRole is now: {}", userRole);
        }
    }
}
