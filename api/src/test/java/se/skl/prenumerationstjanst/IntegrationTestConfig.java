package se.skl.prenumerationstjanst;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import se.skl.prenumerationstjanst.chbase.CHBaseConnectionFactory;
import se.skl.prenumerationstjanst.chbase.CHBaseRequestTemplateFactory;
import se.skl.prenumerationstjanst.service.*;
import se.skl.prenumerationstjanst.service.monitoring.LoggingMonitoringService;
import se.skl.prenumerationstjanst.service.monitoring.MonitoringService;
import se.skl.prenumerationstjanst.service.monitoring.SubscriptionMonitoringService;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/**
 * @author Martin Samuelsson
 */
@Configuration
@Profile({"test", "jmstest"})
public class IntegrationTestConfig {

    @Bean
    @Primary
    public CHBaseConnectionFactory chBaseConnectionFactory() {
        return Mockito.mock(CHBaseConnectionFactory.class);
    }

    @Bean
    @Primary
    public CHBaseRequestTemplateFactory chBaseRequestTemplateFactory() {
        return Mockito.mock(CHBaseRequestTemplateFactory.class);
    }

    @Bean
    @Primary
    public PhkInfoService testPhkInfoService() {
        PhkInfoService mock = Mockito.mock(PhkInfoServiceImpl.class);
        when(mock.getShellRedirectUrl()).thenReturn("");
        return mock;
    }

    @Bean
    public MonitoringService loggingMonitoringService() {
        return new LoggingMonitoringService();
    }

    @Bean
    public SubscriptionService subscriptionService() {
        return new SubscriptionService();
    }

    @Bean
    public TransformationService transformationService() {
        return new TransformationService();
    }

    @Bean
    public ApigwConsentService apigwConsentService() {
        return new ApigwConsentService();
    }

    @Bean
    public SubscriptionMonitoringService subscriptionMonitoringService() {
        return new SubscriptionMonitoringService();
    }

    @Bean
    public PreCheckService preCheckService() {
        return new PreCheckServiceImpl();
    }
}
