package se.skl.prenumerationstjanst.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import se.skl.prenumerationstjanst.dto.ApigwConsentDTO;
import se.skl.prenumerationstjanst.dto.SubscriptionDTO;
import se.skl.prenumerationstjanst.dto.UserContextDTO;
import se.skl.prenumerationstjanst.dto.UserDTO;
import se.skl.prenumerationstjanst.dto.api.SubscriptionApiDTO;
import se.skl.prenumerationstjanst.model.TransferType;

import java.time.LocalDateTime;

import static org.junit.Assert.assertNull;

/**
 * @author Martin Samuelsson
 */
@RunWith(MockitoJUnitRunner.class)
public class ApiTransformationServiceTest {

    public static final String SUB_CRN = "exCrn";
    public static final String SUB_ID = "subId";
    private static final String PHK_PERSON_ID = "phkPersonId";
    private static final String PHK_RECORD_ID = "phkRecordId";

    private UserContextDTO userContextDTO;

    @InjectMocks
    private ApiTransformationService apiTransformationService;

    @Before
    public void setUp() throws Exception {
        userContextDTO = UserContextDTO.create()
                .withPhkRecordId(PHK_RECORD_ID)
                .withRepresentingCrn(SUB_CRN)
                .withUser(UserDTO.create().withCrn(SUB_CRN).withPhkPersonId(PHK_PERSON_ID));
    }

    @Test
    public void testToApiDTO_SubscriptionApiDTO_nullConsent() throws Exception {
        SubscriptionDTO subscriptionDTO = SubscriptionDTO.create()
                .withId(SUB_ID)
                .withApprovedTransferType(TransferType.MATERNITY_MEDICAL_HISTORY.name())
                .withUserContext(userContextDTO);
        SubscriptionApiDTO subscriptionApiDTO = apiTransformationService.toApiDTO(subscriptionDTO);
        Assert.assertEquals(subscriptionDTO.getId(), subscriptionApiDTO.getId());
        Assert.assertEquals(subscriptionDTO.getApprovedTransferType(), subscriptionApiDTO.getApprovedTransferType());
        assertNull(subscriptionApiDTO.getHealthcareExpiration());
        assertNull(subscriptionApiDTO.getHealthAccountExpiration());
    }

    @Test
    public void testToApiDTO_SubscriptionApiDTO_withApigwConsent() throws Exception {
        SubscriptionDTO subscriptionDTO = SubscriptionDTO.create()
                .withId(SUB_ID)
                .withApprovedTransferType(TransferType.MATERNITY_MEDICAL_HISTORY.name())
                .withUserContext(userContextDTO)
                .withApigwConsent(ApigwConsentDTO.create().withExpirationDateTime(LocalDateTime.now()));
        SubscriptionApiDTO subscriptionApiDTO = apiTransformationService.toApiDTO(subscriptionDTO);
        Assert.assertEquals(subscriptionDTO.getId(), subscriptionApiDTO.getId());
        Assert.assertEquals(subscriptionDTO.getApprovedTransferType(), subscriptionApiDTO.getApprovedTransferType());
        Assert.assertEquals(subscriptionDTO.getApigwConsent().getExpirationDateTime(), subscriptionApiDTO.getHealthcareExpiration());
        assertNull(subscriptionApiDTO.getHealthAccountExpiration());
    }

}