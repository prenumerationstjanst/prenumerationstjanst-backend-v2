package se.skl.prenumerationstjanst.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.mvk.itintegration.userprofile.getsubjectofcare.v2.rivtabp21.GetSubjectOfCareResponderInterface;
import se.mvk.itintegration.userprofile.getsubjectofcareresponder.v2.GetSubjectOfCareResponseType;
import se.mvk.itintegration.userprofile.v2.SubjectOfCareType;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

/**
 * @author Martin Samuelsson
 */
@RunWith(MockitoJUnitRunner.class)
public class CitizenInformationServiceImplTest {

    private static final Logger logger = LoggerFactory.getLogger(CitizenInformationServiceImplTest.class);

    @Mock
    private GetSubjectOfCareResponderInterface getSubjectOfCareResponderService;

    @InjectMocks
    private CitizenInformationServiceImpl citizenInformationService;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void getCitizenName_variantA() throws Exception {
        when(getSubjectOfCareResponderService.getSubjectOfCare(any()))
                .thenReturn(createSubjectOfCareResponse("Nils/Roland/Erik", "Järverup"));
        String citizenName = citizenInformationService.getCitizenName("");
        logger.debug("citizenName: {}", citizenName);
        assertEquals("Roland Järverup", citizenName);
    }

    @Test
    public void getCitizenName_variantB() throws Exception {
        when(getSubjectOfCareResponderService.getSubjectOfCare(any()))
                .thenReturn(createSubjectOfCareResponse("/Roland/Erik", "Järverup"));
        String citizenName = citizenInformationService.getCitizenName("");
        logger.debug("citizenName: {}", citizenName);
        assertEquals("Roland Järverup", citizenName);
    }

    @Test
    public void getCitizenName_variantC() throws Exception {
        when(getSubjectOfCareResponderService.getSubjectOfCare(any()))
                .thenReturn(createSubjectOfCareResponse("Nils/Roland/", "Järverup"));
        String citizenName = citizenInformationService.getCitizenName("");
        logger.debug("citizenName: {}", citizenName);
        assertEquals("Roland Järverup", citizenName);
    }

    @Test
    public void getCitizenName_variantD() throws Exception {
        when(getSubjectOfCareResponderService.getSubjectOfCare(any()))
                .thenReturn(createSubjectOfCareResponse("/Roland/", "Järverup"));
        String citizenName = citizenInformationService.getCitizenName("");
        logger.debug("citizenName: {}", citizenName);
        assertEquals("Roland Järverup", citizenName);
    }

    @Test
    public void getCitizenName_variantE() throws Exception {
        when(getSubjectOfCareResponderService.getSubjectOfCare(any()))
                .thenReturn(createSubjectOfCareResponse("Nils/Bengt Roland/Erik", "Järverup"));
        String citizenName = citizenInformationService.getCitizenName("");
        logger.debug("citizenName: {}", citizenName);
        assertEquals("Bengt Roland Järverup", citizenName);
    }

    @Test
    public void getCitizenName_variantF() throws Exception {
        when(getSubjectOfCareResponderService.getSubjectOfCare(any()))
                .thenReturn(createSubjectOfCareResponse("Nils/Bengt-Roland/Erik", "Järverup"));
        String citizenName = citizenInformationService.getCitizenName("");
        logger.debug("citizenName: {}", citizenName);
        assertEquals("Bengt-Roland Järverup", citizenName);
    }

    private GetSubjectOfCareResponseType createSubjectOfCareResponse(String firstName, String lastName) {
        GetSubjectOfCareResponseType response = new GetSubjectOfCareResponseType();
        SubjectOfCareType subjectOfCare = new SubjectOfCareType();
        subjectOfCare.setFirstName(firstName);
        subjectOfCare.setLastName(lastName);
        response.setSubjectOfCare(subjectOfCare);
        return response;
    }

}