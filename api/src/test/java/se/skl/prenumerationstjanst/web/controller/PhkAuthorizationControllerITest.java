package se.skl.prenumerationstjanst.web.controller;

import com.jayway.restassured.module.mockmvc.response.MockMvcResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import se.skl.prenumerationstjanst.PrenumerationstjanstApiApplication;
import se.skl.prenumerationstjanst.dto.RecordDTO;
import se.skl.prenumerationstjanst.repository.KeyValueRepository;
import se.skl.prenumerationstjanst.repository.UserRepository;
import se.skl.prenumerationstjanst.service.PhkInfoService;
import se.skl.prenumerationstjanst.service.SimpleKeyValueService;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.junit.Assert.*;
import static se.skl.prenumerationstjanst.PrenumerationstjanstConstants.PHK_CLIENT_STATE_SESSION_KEY;

/**
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
@SpringApplicationConfiguration({
        PrenumerationstjanstApiApplication.class
})
@ActiveProfiles("test")
public class PhkAuthorizationControllerITest extends ControllerIntegrationTestSupport {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SimpleKeyValueService simpleKeyValueService;

    @Autowired
    private KeyValueRepository keyValueRepository;

    @Autowired
    private PhkInfoService phkInfoService;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        userRepository.deleteAll();
        keyValueRepository.deleteAll();
        Mockito.when(phkInfoService.getRecordForAuthToken(Mockito.anyString()))
                .thenReturn(new RecordDTO("rId", "pId", "rName"));
    }


    @After
    public void tearDown() throws Exception {
        userRepository.deleteAll();
        keyValueRepository.deleteAll();
    }

    @Test
    public void testAuthorizationStart() throws Exception {
        //@formatter:off
        MockMvcResponse mockMvcResponse =
        given()
                .spec(getIdpUserRequestSpec())
                .log().all()
        .when()
                .get("/authorize/phk/auth/start");
        mockMvcResponse
        .then()
                .log().all()
                .statusCode(HttpStatus.FOUND.value());
        //@formatter:on
        assertNotNull(simpleKeyValueService.get(PHK_CLIENT_STATE_SESSION_KEY, SESSION_ID));
    }

    @Test
    public void testAuthorizationComplete() throws Exception {
        assertNull(userRepository.findByCrn(USER_CRN));
        //@formatter:off
        MockMvcResponse mockMvcResponse1 =
        given()
                .spec(getIdpUserRequestSpec())
                .log().all()
        .when()
                .get("/authorize/phk/auth/start");
        mockMvcResponse1
        .then()
                .log().all()
                .statusCode(HttpStatus.FOUND.value());
        //@formatter:on
        String state = simpleKeyValueService.get(PHK_CLIENT_STATE_SESSION_KEY, SESSION_ID);
        assertNotNull(state);
        assertTrue(mockMvcResponse1.header("Location").contains("actionqs"));
        assertTrue(mockMvcResponse1.header("Location").contains(state));

        //@formatter:off
        MockMvcResponse mockMvcResponse2 =
        given()
                .spec(getIdpUserRequestSpec())
                .queryParam("target", "AppAuthSuccess")
                .queryParam("actionqs", state)
                .queryParam("wctoken", "mepmep")
                .log().all()
        .when()
                .get("/authorize/phk/complete");
        mockMvcResponse2
        .then()
                .log().all()
                .statusCode(HttpStatus.FOUND.value());
        //@formatter:on
        assertNotNull(userRepository.findByCrn(USER_CRN));
//        assertTrue(mockMvcResponse2.header("Location").contains("#select-record/" + savedSubscription.getId()));
        //TODO: also check call to TransferService. Tried, but was unable to mock it :(
    }

    @Test
    public void testAuthorizationError() throws Exception {
        //@formatter:off
        MockMvcResponse mockMvcResponse1 =
        given()
                .spec(getIdpUserRequestSpec())
                .queryParam("target", "AppAuthReject")
                .log().all()
        .when()
                .get("/authorize/phk/complete");
        mockMvcResponse1
        .then()
                .log().all()
                .statusCode(HttpStatus.FOUND.value());
        //@formatter:on
        assertTrue(mockMvcResponse1.header("Location").contains("#"));
    }

}