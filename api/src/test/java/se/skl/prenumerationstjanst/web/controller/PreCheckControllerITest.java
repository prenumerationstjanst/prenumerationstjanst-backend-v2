package se.skl.prenumerationstjanst.web.controller;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.module.mockmvc.response.MockMvcResponse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import se.skl.prenumerationstjanst.PrenumerationstjanstApiApplication;
import se.skl.prenumerationstjanst.config.TestConfiguration;
import se.skl.prenumerationstjanst.service.PreCheckService;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.junit.Assert.assertEquals;

/**
 * @author Martin Samuelsson
 */
@SpringApplicationConfiguration({
        PrenumerationstjanstApiApplication.class,
        PreCheckControllerITest.TestConfig.class
})
public class PreCheckControllerITest extends ControllerIntegrationTestSupport {

    @Autowired
    private PreCheckService preCheckService;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        Mockito.reset(preCheckService);
    }

    @Test
    public void testPreCheck_NonActiveUser() {
        Mockito.when(preCheckService.preCheckAndExecuteCleanup(USER_CRN, USER_CRN))
                .thenReturn(Boolean.FALSE);
        //@formatter:off
        MockMvcResponse mockMvcResponse = given()
                .spec(getIdpUserRequestSpec())
                .contentType(ContentType.JSON)
                .log().all()
        .when()
                .get("/user/precheck");
        mockMvcResponse
        .then()
                .log().all();
        assertEquals("false", mockMvcResponse.getBody().print());
        //@formatter:on
    }

    @Test
    public void testPreCheck_ActiveUser() {
        Mockito.when(preCheckService.preCheckAndExecuteCleanup(USER_CRN, USER_CRN))
                .thenReturn(Boolean.TRUE);
        //@formatter:off
        MockMvcResponse mockMvcResponse = given()
                .spec(getIdpUserRequestSpec())
                .contentType(ContentType.JSON)
                .log().all()
        .when()
                .get("/user/precheck");
        mockMvcResponse
        .then()
                .log().all();
        assertEquals("true", mockMvcResponse.getBody().print());
        //@formatter:on
    }


    @TestConfiguration
    @Configuration
    public static class TestConfig {

        @Bean
        @Primary
        public PreCheckService preCheckService() {
            return Mockito.mock(PreCheckService.class);
        }
    }


}