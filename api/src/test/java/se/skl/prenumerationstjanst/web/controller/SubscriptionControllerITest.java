package se.skl.prenumerationstjanst.web.controller;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.module.mockmvc.response.MockMvcResponse;
import com.jayway.restassured.module.mockmvc.response.ValidatableMockMvcResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;
import org.springframework.jms.core.JmsTemplate;
import se.skl.prenumerationstjanst.PrenumerationstjanstApiApplication;
import se.skl.prenumerationstjanst.config.TestConfiguration;
import se.skl.prenumerationstjanst.dto.ApigwConsentDTO;
import se.skl.prenumerationstjanst.model.*;
import se.skl.prenumerationstjanst.repository.SubscriptionRepository;
import se.skl.prenumerationstjanst.repository.UserContextRepository;
import se.skl.prenumerationstjanst.repository.UserRepository;
import se.skl.prenumerationstjanst.service.*;
import se.skl.prenumerationstjanst.web.dto.SubscriptionActionRequestDTO;
import se.skl.prenumerationstjanst.web.dto.SubscriptionConfirmationDTO;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.options;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;

/**
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
@SpringApplicationConfiguration({
        PrenumerationstjanstApiApplication.class,
        SubscriptionControllerITest.TestConfig.class
})
public class SubscriptionControllerITest extends ControllerIntegrationTestSupport {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionControllerITest.class);
    private static final String CONFIRMATION_KEY = "confirmation-key";

    @Value("${pt.relation_subscription_age_limit}")
    private int relationAgeLimit;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserContextRepository userContextRepository;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private ApigwConsentService apigwConsentService;

    @Autowired
    private UrlService urlService;

    @Autowired
    private SubscriptionConfirmationService subscriptionConfirmationService;

    @Autowired
    private PreCheckService preCheckService;

    private ApigwConsentDTO userApigwConsentDTOForMMH;
    private ApigwConsentDTO userApigwConsentDTOForVH;
    private SubscriptionConfirmationDTO subscriptionConfirmationDTO = new SubscriptionConfirmationDTO();

    @Before
    public void setUp() throws Exception {
        super.setUp();
        //user
        userApigwConsentDTOForMMH = ApigwConsentDTO.create()
                .withScope("foo_scope")
                .withTokenValue("foo_token")
                .withExpirationDateTime(LocalDateTime.now());
        userApigwConsentDTOForVH = ApigwConsentDTO.create()
                .withScope("foo_scope_2")
                .withTokenValue("foo_token_2")
                .withExpirationDateTime(LocalDateTime.now().plusDays(1));
        Mockito.when(apigwConsentService.getConsent(eq(USER_CRN), eq(null), eq(TransferType.MATERNITY_MEDICAL_HISTORY)))
                .thenReturn(userApigwConsentDTOForMMH);

        Mockito.when(apigwConsentService.getConsent(eq(USER_CRN), eq(null), eq(TransferType.VACCINATION_HISTORY)))
                .thenReturn(userApigwConsentDTOForVH);

        Mockito.when(subscriptionConfirmationService.set(Mockito.any(SubscriptionConfirmationDTO.class), eq(SESSION_ID)))
                .thenReturn(CONFIRMATION_KEY);

        Mockito.when(subscriptionConfirmationService.get(eq(CONFIRMATION_KEY), eq(SESSION_ID)))
                .thenReturn(subscriptionConfirmationDTO);

        Mockito.when(preCheckService.preCheckAndExecuteCleanup(USER_CRN, USER_CRN))
                .thenReturn(Boolean.TRUE);
    }

    @After
    public void tearDown() throws Exception {
        Mockito.reset(apigwConsentService, subscriptionConfirmationService, preCheckService);
        userRepository.deleteAll();
        userContextRepository.deleteAll();
        subscriptionRepository.deleteAll();
    }

    @Test
    public void testCreateSubscriptionPreCheckFails() throws Exception {
        Mockito.when(preCheckService.preCheckAndExecuteCleanup(USER_CRN, USER_CRN)).thenReturn(Boolean.FALSE);
        //@formatter:off
        MockMvcResponse mockMvcResponse =
        given()
                .spec(getIdpUserRequestSpec())
                .contentType(ContentType.JSON)
                .body(createRequest(TransferType.MATERNITY_MEDICAL_HISTORY))
                .log().all()
        .when()
                .post("/user/subscriptions");
        ValidatableMockMvcResponse validatableMockMvcResponse = mockMvcResponse
        .then()
                .log().all()
                .body("subscriptionConfirmationKey", equalTo(null))
                .body("phkActionUrl", equalTo(urlService.getPhkAuthorizeStartUrl()))
                .statusCode(HttpStatus.OK.value());
        //@formatter:on
        assertEquals(0, subscriptionRepository.count());
    }

    @Test
    public void testCreateSubscriptionForExistingUser() throws Exception {
        Mockito.when(preCheckService.preCheckAndExecuteCleanup(USER_CRN, USER_CRN)).thenReturn(Boolean.TRUE);
        assertEquals(0, subscriptionRepository.count());
        User savedUser = userRepository.save(new User(USER_CRN, null));
        UserContext userContext = new UserContext(savedUser, USER_CRN, "phkRecordId");
        userContextRepository.save(userContext);
        //@formatter:off
        MockMvcResponse mockMvcResponse =
        given()
                .spec(getIdpUserRequestSpec())
                .contentType(ContentType.JSON)
                .body(createRequest(TransferType.MATERNITY_MEDICAL_HISTORY))
                .log().all()
        .when()
                .post("/user/subscriptions");
        ValidatableMockMvcResponse validatableMockMvcResponse = mockMvcResponse
        .then()
                .log().all()
                .body("subscriptionConfirmationKey", equalTo(CONFIRMATION_KEY))
                .body("phkActionUrl", equalTo(null))
                .statusCode(HttpStatus.OK.value());
        //@formatter:on
        assertEquals(1, subscriptionRepository.count());
        Subscription sub = subscriptionRepository.findAll().iterator().next();
        assertEquals(userApigwConsentDTOForMMH.getTokenValue(), sub.getApigwConsent().getTokenValue());
        assertEquals(userApigwConsentDTOForMMH.getScope(), sub.getApigwConsent().getScope());
        assertEquals(userApigwConsentDTOForMMH.getExpirationDateTime(), sub.getApigwConsent().getExpirationDateTime());
        assertEquals(savedUser.getId(), sub.getUserContext().getUser().getId());
    }

    @Test
    public void testCreateSubscriptionsForExistingUser() throws Exception {
        assertEquals(0, subscriptionRepository.count());
        User savedUser = userRepository.save(new User(USER_CRN, null));
        UserContext userContext = new UserContext(savedUser, USER_CRN, "phkRecordId");
        userContextRepository.save(userContext);
        //@formatter:off
        MockMvcResponse mockMvcResponse =
        given()
                .spec(getIdpUserRequestSpec())
                .contentType(ContentType.JSON)
                .body(createRequest(TransferType.MATERNITY_MEDICAL_HISTORY, TransferType.VACCINATION_HISTORY))
                .log().all()
        .when()
                .post("/user/subscriptions");
        ValidatableMockMvcResponse validatableMockMvcResponse = mockMvcResponse
        .then()
                .log().all()
                .body("subscriptionConfirmationKey", equalTo(CONFIRMATION_KEY))
                .body("phkActionUrl", equalTo(null))
                .statusCode(HttpStatus.OK.value());
        //@formatter:on
        assertEquals(2, subscriptionRepository.count());
        Iterator<Subscription> subIter = subscriptionRepository.findAll().iterator();
        while(subIter.hasNext()) {
            Subscription sub = subIter.next();
            if (sub.getApprovedTransferType() == TransferType.MATERNITY_MEDICAL_HISTORY) {
                assertEquals(userApigwConsentDTOForMMH.getTokenValue(), sub.getApigwConsent().getTokenValue());
                assertEquals(userApigwConsentDTOForMMH.getScope(), sub.getApigwConsent().getScope());
                assertEquals(userApigwConsentDTOForMMH.getExpirationDateTime(),
                        sub.getApigwConsent().getExpirationDateTime());
            } else if (sub.getApprovedTransferType() == TransferType.VACCINATION_HISTORY) {
                assertEquals(userApigwConsentDTOForVH.getTokenValue(), sub.getApigwConsent().getTokenValue());
                assertEquals(userApigwConsentDTOForVH.getScope(), sub.getApigwConsent().getScope());
                assertEquals(userApigwConsentDTOForVH.getExpirationDateTime(),
                        sub.getApigwConsent().getExpirationDateTime());
            }
            assertEquals(savedUser.getId(), sub.getUserContext().getUser().getId());
        }
    }

    @Test
    public void testCreateSubscriptionWithUnknownTransferType() throws Exception {
        userRepository.save(
                new User(USER_CRN, null));
        SubscriptionActionRequestDTO request = createRequest(null);
        request.setTransferTypes(Collections.singleton("unknown"));
        //@formatter:off
        given()
                .spec(getIdpUserRequestSpec())
                .body(request)
                .contentType(ContentType.JSON)
                .log().all()
        .when()
                .post("/user/subscriptions")
        .then()
                .log().all()
                .body("errorCode", equalTo("SUBSCRIPTION_CREATION_FAILED"))
                .statusCode(HttpStatus.BAD_REQUEST.value());
        //@formatter:on
        assertEquals(0, subscriptionRepository.count());
    }

    @Test
    public void testCreateSubscriptionEmptyTransferTypeSubmitted() throws Exception {
        userRepository.save(
            new User(USER_CRN, null));
        //@formatter:off
        given()
                .spec(getIdpUserRequestSpec())
                .body(createRequest(null))
                .contentType(ContentType.JSON)
                .log().all()
        .when()
                .post("/user/subscriptions")
        .then()
                .log().all()
                .body("errorCode", equalTo("VALIDATION_FAILED"))
                .statusCode(HttpStatus.BAD_REQUEST.value());
        //@formatter:on
        assertEquals(0, subscriptionRepository.count());
    }

    @Test
    public void testGetSubscriptions() throws Exception {
        User savedUser = userRepository.save(new User(USER_CRN, null));
        UserContext userContext = new UserContext(savedUser, USER_CRN, "phkRecordId");
        userContext = userContextRepository.save(userContext);
        Subscription subscription = new Subscription(userContext, TransferType.MATERNITY_MEDICAL_HISTORY);
        ApigwConsent apigwConsent = new ApigwConsent();
        apigwConsent.setTokenValue("mep");
        apigwConsent.setExpirationDateTime(LocalDateTime.now());
        apigwConsent.setScope("mep");
        subscription.setApigwConsent(apigwConsent);
        Subscription savedSubscription = subscriptionRepository.save(subscription);
        //@formatter:off
        given()
                .spec(getIdpUserRequestSpec())
                .log().all()
        .when()
                .get("/user/subscriptions")
        .then()
                .log().all()
                .body("$", hasSize(1))
                .body("get(0).approvedTransferType", equalTo(TransferType.MATERNITY_MEDICAL_HISTORY.name()))
                .body("get(0).id", equalTo(savedSubscription.getId()))
                .statusCode(HttpStatus.OK.value());
        //@formatter:on
    }

    @Test
    public void testGetSubscriptionsAllAreIncomplete() throws Exception {
        User savedUser = userRepository.save(new User(USER_CRN, null));
        UserContext userContext = new UserContext(savedUser, USER_CRN, "phkRecordId");
        userContext = userContextRepository.save(userContext);
        subscriptionRepository.save(new Subscription(userContext, TransferType.MATERNITY_MEDICAL_HISTORY));
        //@formatter:off
        given()
                .spec(getIdpUserRequestSpec())
                .log().all()
        .when()
                .get("/user/subscriptions")
        .then()
                .log().all()
                .body("$", hasSize(1))
                .body("get(0).complete", equalTo(false))
                .statusCode(HttpStatus.OK.value());
        //@formatter:on
    }

    @Test
    public void testDeleteSubscription() throws Exception {
        User savedUser = userRepository.save(new User(USER_CRN, null));
        UserContext userContext = new UserContext(savedUser, USER_CRN, "phkRecordId");
        userContext = userContextRepository.save(userContext);
        Subscription subscription = new Subscription(userContext, TransferType.MATERNITY_MEDICAL_HISTORY);
        Subscription savedSubscription = subscriptionRepository.save(subscription);
        //@formatter:off
        given()
                .spec(getIdpUserRequestSpec())
                .contentType(ContentType.JSON)
                .body(createRequest(TransferType.MATERNITY_MEDICAL_HISTORY))
                .log().all()
        .when()
                .post("/user/subscriptions/delete")
        .then()
                .log().all()
                .body("subscriptionConfirmationKey", equalTo(CONFIRMATION_KEY))
                .body("phkActionUrl", equalTo(null))
                .statusCode(HttpStatus.OK.value());
        //@formatter:on
        assertEquals(1, subscriptionRepository.count());
        assertTrue(subscriptionRepository.findOne(savedSubscription.getId()).isScheduledForRemoval());
    }

    @Test
    public void testDeleteSubscriptionUnknownSubscription() throws Exception {
        User savedUser = userRepository.save(new User(USER_CRN, null));
        UserContext userContext = new UserContext(savedUser, USER_CRN, "phkRecordId");
        userContext = userContextRepository.save(userContext);
        //@formatter:off
        given()
                .spec(getIdpUserRequestSpec())
                .contentType(ContentType.JSON)
                .body(createRequest(TransferType.MATERNITY_MEDICAL_HISTORY))
                .log().all()
        .when()
                .post("/user/subscriptions/delete")
        .then()
                .log().all()
                .body("subscriptionConfirmationKey", equalTo(CONFIRMATION_KEY))
                .body("phkActionUrl", equalTo(null))
                .statusCode(HttpStatus.OK.value());
        //@formatter:on
        assertEquals(0, subscriptionRepository.count());
        ArgumentCaptor<SubscriptionConfirmationDTO> subscriptionConfirmationDTOArgumentCaptor = ArgumentCaptor
                .forClass(SubscriptionConfirmationDTO.class);
        Mockito.verify(subscriptionConfirmationService).set(subscriptionConfirmationDTOArgumentCaptor.capture(),
                anyString());
        SubscriptionConfirmationDTO value = subscriptionConfirmationDTOArgumentCaptor.getValue();
        assertTrue(value.getSuccess().isEmpty());
        assertEquals(1, value.getFail().size());
        assertEquals(TransferType.MATERNITY_MEDICAL_HISTORY.name(), value.getFail().get(0));
    }

    private SubscriptionActionRequestDTO createRequest(TransferType... transferTypes) {
        if (transferTypes == null) {
            return new SubscriptionActionRequestDTO();
        }
        Set<String> transferTypesString = Arrays.stream(transferTypes).map(Enum::name).collect(Collectors.toSet());
        SubscriptionActionRequestDTO requestDTO = new SubscriptionActionRequestDTO();
        requestDTO.setTransferTypes(transferTypesString);
        return requestDTO;
    }

    @TestConfiguration
    @Configuration
    public static class TestConfig {

        @Bean
        @Primary
        public ApigwConsentService apigwConsentService() {
            return Mockito.mock(ApigwConsentService.class);
        }

        @Bean
        @Primary
        public JmsTemplate mockJmsTemplate() {
            return Mockito.mock(JmsTemplate.class);
        }

        @Bean
        @Primary
        public SubscriptionConfirmationService subscriptionConfirmationService() {
            return Mockito.mock(SubscriptionConfirmationService.class);
        }

        @Bean
        @Primary
        public PreCheckService preCheckService() {
            return Mockito.mock(PreCheckService.class);
        }
    }
}