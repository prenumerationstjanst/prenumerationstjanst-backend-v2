package se.skl.prenumerationstjanst.web.controller;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;
import se.skl.prenumerationstjanst.PrenumerationstjanstApiApplication;
import se.skl.prenumerationstjanst.model.TransferType;

import java.util.Arrays;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;

/**
 * @author Martin Samuelsson
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(PrenumerationstjanstApiApplication.class)
@WebAppConfiguration
@DirtiesContext
@ActiveProfiles({"test"})
@IntegrationTest("server.port:0")
public class TransferTypeControllerITest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp() throws Exception {
        RestAssuredMockMvc.webAppContextSetup(webApplicationContext);
    }

    @Test
    public void testGetTransferTypes() throws Exception {
        //@formatter:off
        given()
                .log().all()
        .when()
                .get("/transfertypes")
        .then()
                .log().all()
                .body("$", hasSize(TransferType.values().length))
                .body("$", hasItems(Arrays.asList(TransferType.values()).stream().map(Enum::name).toArray()))
                .contentType(ContentType.JSON)
                .statusCode(200);
        //@formatter:on
    }
}