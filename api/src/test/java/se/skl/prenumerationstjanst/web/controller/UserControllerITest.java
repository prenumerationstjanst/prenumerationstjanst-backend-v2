package se.skl.prenumerationstjanst.web.controller;

import com.jayway.restassured.http.ContentType;
import org.junit.After;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import se.skl.prenumerationstjanst.PrenumerationstjanstApiApplication;
import se.skl.prenumerationstjanst.config.TestConfiguration;
import se.skl.prenumerationstjanst.dto.RecordDTO;
import se.skl.prenumerationstjanst.model.User;
import se.skl.prenumerationstjanst.repository.UserRepository;
import se.skl.prenumerationstjanst.service.CitizenInformationService;
import se.skl.prenumerationstjanst.service.PhkInfoService;

import java.util.Collections;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;

/**
 * @author Martin Samuelsson
 */
@SpringApplicationConfiguration({
        PrenumerationstjanstApiApplication.class,
        UserControllerITest.TestConfig.class
})
public class UserControllerITest extends ControllerIntegrationTestSupport {

    private static final Logger logger = LoggerFactory.getLogger(UserControllerITest.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PhkInfoService phkInfoService;

    @After
    public void tearDown() throws Exception {
        userRepository.deleteAll();
    }

    @Test
    public void testGetUser_NotPhkAuthorized() throws Exception {
        //@formatter:off
        given()
                .spec(getIdpUserRequestSpec())
                .log().all()
        .when()
                .get("/user/profile")
        .then()
                .log().all()
                .body("phkAuthorized", equalTo(false))
                .body("crn", not(equalTo(USER_CRN)))
                .body("name", equalTo(USER_NAME))
                .contentType(ContentType.JSON)
                .statusCode(200);
        //@formatter:on
    }

    @Test
    public void testGetUser_PhkAuthorized() throws Exception {
        User user = new User(USER_CRN, "pID");
        userRepository.save(user);
        Mockito.when(phkInfoService.checkPhkPersonId(eq("pID")))
                .thenReturn(PhkInfoService.PHK_PERSON_STATUS.ACTIVE);
        assertEquals(1, userRepository.count());
        //@formatter:off
        given()
                .spec(getIdpUserRequestSpec())
                .log().all()
        .when()
                .get("/user/profile")
        .then()
                .log().all()
                .body("phkAuthorized", equalTo(true))
                .body("crn", not(equalTo(USER_CRN)))
                .body("name", equalTo(USER_NAME))
                .contentType(ContentType.JSON)
                .statusCode(200);
        //@formatter:on
    }

    @TestConfiguration
    @Configuration
    public static class TestConfig {

        @Bean
        @Primary
        public CitizenInformationService citizenInformationService() {
            CitizenInformationService mock = Mockito.mock(CitizenInformationService.class);
            Mockito.when(mock.getCitizenName(eq(USER_CRN))).thenReturn(USER_NAME);
            return mock;
        }
    }
}