package se.skl.prenumerationstjanst.web.security;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import se.skl.prenumerationstjanst.service.SecurityContextService;
import se.skl.prenumerationstjanst.web.security.auth.SimpleCitizen;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Mockito.when;

/**
 * @author Martin Samuelsson
 */
@RunWith(MockitoJUnitRunner.class)
public class AgeRestrictionFilterTest {

    @Mock
    private SecurityContextService securityContextService;

    @InjectMocks
    private AgeRestrictionFilter ageRestrictionFilter;

    @Before
    public void setUp() throws Exception {
        ReflectionTestUtils.setField(ageRestrictionFilter, "minRequiredAge", 18);
    }

    @Test
    public void doFilterInternalOverAgeRequired() throws Exception {
        when(securityContextService.getPerformingCitizen()).thenReturn(createSimpleCitizen(19));
        ageRestrictionFilter.doFilterInternal(
                Mockito.mock(HttpServletRequest.class),
                Mockito.mock(HttpServletResponse.class),
                Mockito.mock(FilterChain.class));
    }

    @Test
    public void doFilterInternalSameAsMinAgeRequired() throws Exception {
        when(securityContextService.getPerformingCitizen()).thenReturn(createSimpleCitizen(18));
        ageRestrictionFilter.doFilterInternal(
                Mockito.mock(HttpServletRequest.class),
                Mockito.mock(HttpServletResponse.class),
                Mockito.mock(FilterChain.class));
    }

    @Test(expected = AgeRestrictionFilter.AgeRestrictionException.class)
    public void doFilterInternalBadAge() throws Exception {
        when(securityContextService.getPerformingCitizen()).thenReturn(createSimpleCitizen(17));
        ageRestrictionFilter.doFilterInternal(
                Mockito.mock(HttpServletRequest.class),
                Mockito.mock(HttpServletResponse.class),
                Mockito.mock(FilterChain.class));
    }

    private SimpleCitizen createSimpleCitizen(int age) {
        return new SimpleCitizen("191212121212", "name", "gender", "19121212", age);
    }

}