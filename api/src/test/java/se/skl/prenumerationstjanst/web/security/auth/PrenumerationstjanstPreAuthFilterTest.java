package se.skl.prenumerationstjanst.web.security.auth;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.test.util.ReflectionTestUtils;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Martin Samuelsson
 */
@RunWith(MockitoJUnitRunner.class)
public class PrenumerationstjanstPreAuthFilterTest {

    private static final Logger logger = LoggerFactory.getLogger(PrenumerationstjanstPreAuthFilterTest.class);

    private static final String SSN_HEADER = "ssnHeader";
    private static final String CN_HEADER = "scnHeader";
    private static final String AGE_HEADER = "ageHeader";
    private static final String DOB_HEADER = "dobHeader";
    private static final String GENDER_HEADER = "genHeader";

    private static final String CRN = "191212121212";
    private static final String NAME = "Sune Suspekt";
    private static final String DOB = "19121212";
    private static final int AGE = 30;
    private static final String GENDER = "M";

    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private AuthenticationDetailsSource authenticationDetailsSource;

    @InjectMocks
    private PrenumerationstjanstPreAuthFilter prenumerationstjanstPreAuthFilter;

    private HttpServletRequest httpServletRequest;

    @Before
    public void setUp() throws Exception {
        ReflectionTestUtils.setField(prenumerationstjanstPreAuthFilter, "subjectSerialNumberHeader", SSN_HEADER);
        ReflectionTestUtils.setField(prenumerationstjanstPreAuthFilter, "subjectCommonNameHeader", CN_HEADER);
        ReflectionTestUtils.setField(prenumerationstjanstPreAuthFilter, "ageHeader", AGE_HEADER);
        ReflectionTestUtils.setField(prenumerationstjanstPreAuthFilter, "dateOfBirthHeader", DOB_HEADER);
        ReflectionTestUtils.setField(prenumerationstjanstPreAuthFilter, "genderHeader", GENDER_HEADER);
        prenumerationstjanstPreAuthFilter.initFilterBean();
        httpServletRequest = mock(HttpServletRequest.class);
    }

    @Test
    public void getPreAuthenticatedPrincipal() throws Exception {
        when(httpServletRequest.getHeader(SSN_HEADER)).thenReturn(CRN);
        when(httpServletRequest.getHeader(CN_HEADER)).thenReturn(NAME);
        when(httpServletRequest.getIntHeader(AGE_HEADER)).thenReturn(AGE);
        when(httpServletRequest.getHeader(DOB_HEADER)).thenReturn(DOB);
        when(httpServletRequest.getHeader(GENDER_HEADER)).thenReturn(GENDER);
        CitizenContext citizenContext = (CitizenContext) prenumerationstjanstPreAuthFilter
                .getPreAuthenticatedPrincipal(httpServletRequest);
        logger.info("{}", citizenContext);
        SimpleCitizen performingCitizen = citizenContext.getPerformingCitizen();
        assertEquals(performingCitizen.getCrn(), CRN);
        assertEquals(performingCitizen.getName(), NAME);
        assertEquals(performingCitizen.getAge(), AGE);
        assertEquals(performingCitizen.getDateOfBirth(), DOB);
        assertEquals(performingCitizen.getGender(), GENDER);
        assertSame(citizenContext.getPerformingCitizen(), citizenContext.getRepresentingCitizen());
    }

}