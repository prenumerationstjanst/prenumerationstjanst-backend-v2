package se.skl.prenumerationstjanst;

/**
 * @author Martin Samuelsson
 */
public class PrenumerationstjanstConstants {

    private static final String DLQ_PREFIX = "DLQ.";
    private static final String QUEUE_PREFIX = "pt.";

    private static final String INITIAL_TRANSFERS_FOR_SUBSCRIPTION_QUEUE_SUFFIX = "initialTransfersForSubscription";
    public static final String INITIAL_TRANSFERS_FOR_SUBSCRIPTION_QUEUE = QUEUE_PREFIX + INITIAL_TRANSFERS_FOR_SUBSCRIPTION_QUEUE_SUFFIX;
    public static final String INITIAL_TRANSFERS_FOR_SUBSCRIPTION_QUEUE_DLQ = DLQ_PREFIX + INITIAL_TRANSFERS_FOR_SUBSCRIPTION_QUEUE;

    private static final String START_TRANSFER_FOR_SINGLE_SOURCE_SYSTEM_QUEUE_SUFFIX =
            "startTransferForSingleSourceSystem";
    public static final String START_TRANSFER_FOR_SINGLE_SOURCE_SYSTEM_QUEUE = QUEUE_PREFIX +
            START_TRANSFER_FOR_SINGLE_SOURCE_SYSTEM_QUEUE_SUFFIX;
    public static final String START_TRANSFER_FOR_SINGLE_SOURCE_SYSTEM_QUEUE_DLQ = DLQ_PREFIX +
            START_TRANSFER_FOR_SINGLE_SOURCE_SYSTEM_QUEUE;

    private static final String FETCH_AGGREGATED_QUEUE_SUFFIX = "fetchAggregatedQueue";
    public static final String FETCH_AGGREGATED_QUEUE = QUEUE_PREFIX + FETCH_AGGREGATED_QUEUE_SUFFIX;
    public static final String FETCH_AGGREGATED_QUEUE_DLQ = DLQ_PREFIX + FETCH_AGGREGATED_QUEUE;

    private static final String FETCH_SINGLE_SOURCE_SYSTEM_QUEUE_SUFFIX = "fetchSingleSourceSystemQueue";
    public static final String FETCH_SINGLE_SOURCE_SYSTEM_QUEUE = QUEUE_PREFIX + FETCH_SINGLE_SOURCE_SYSTEM_QUEUE_SUFFIX;
    public static final String FETCH_SINGLE_SOURCE_SYSTEM_QUEUE_DLQ = DLQ_PREFIX + FETCH_SINGLE_SOURCE_SYSTEM_QUEUE;

    private static final String PUSH_QUEUE_SUFFIX = "pushQueue";
    public static final String PUSH_QUEUE = QUEUE_PREFIX + PUSH_QUEUE_SUFFIX;
    public static final String PUSH_QUEUE_DLQ = DLQ_PREFIX + PUSH_QUEUE;

    private static final String MONITORING_QUEUE_SUFFIX = "monitoringQueue";
    public static final String MONITORING_QUEUE = QUEUE_PREFIX + MONITORING_QUEUE_SUFFIX;
    public static final String MONITORING_QUEUE_DLQ = DLQ_PREFIX + MONITORING_QUEUE;

    private static final String TRANSFORMATION_QUEUE_SUFFIX = "transformationQueue";
    public static final String TRANSFORMATION_QUEUE = QUEUE_PREFIX + TRANSFORMATION_QUEUE_SUFFIX;
    public static final String TRANSFORMATION_QUEUE_DLQ = DLQ_PREFIX + TRANSFORMATION_QUEUE;

    private static final String NOTIFICATION_QUEUE_SUFFIX = "notificationQueue";
    public static final String NOTIFICATION_QUEUE = QUEUE_PREFIX + NOTIFICATION_QUEUE_SUFFIX;
    public static final String NOTIFICATION_QUEUE_DLQ = DLQ_PREFIX + NOTIFICATION_QUEUE;

    private static final String TRANSFER_MONITORING_QUEUE_SUFFIX = "transferMonitoringQueue";
    public static final String TRANSFER_MONITORING_QUEUE = QUEUE_PREFIX + TRANSFER_MONITORING_QUEUE_SUFFIX;
    public static final String TRANSFER_MONITORING_QUEUE_DLQ = DLQ_PREFIX + TRANSFER_MONITORING_QUEUE;

    public static final String PHK_CLIENT_STATE_SESSION_KEY = "phkClientState";
    public static final String PHK_WCTOKEN_SESSION_KEY = "phkWcToken";
    public static final String PHK_RECORDS_SESSION_KEY = "phkCachedRecords";
    public static final String PHK_PERSON_ID_SESSION_KEY = "phkPersonId";
    public static final String SUBSCRIPTION_ID_SESSION_KEY = "subscriptionId";
    public static final String TRANSFER_TYPES_SESSION_KEY = "transferTypes";

    public static final String MDC_PROCESS_ID_KEY = "pt_processId";
    public static final String MDC_ACTIVITY_ID_KEY = "pt_activityId";
    public static final String MDC_PREVIOUS_ACTIVITY_ID_KEY = "pt_previousActivityId";
    public static final String MDC_CLIENT_CORRELATION_ID_KEY = "pt_clientCorrelationId";
    public static final String MDC_SUBSCRIPTION_ID_KEY = "pt_subscriptionId";
    public static final String MDC_TRANSFER_JOB_ID_KEY = "pt_transferJobId";
    public static final String MDC_FETCH_JOB_ID_KEY = "pt_fetchJobId";
    public static final String MDC_TRANSFORMATION_JOB_ID_KEY = "pt_transformationJobId";
    public static final String MDC_PUSH_JOB_ID_KEY = "pt_pushJobId";
    public static final String MDC_TRANSFER_TYPE_KEY = "pt_transferType";
    public static final String MDC_TRANSFER_ORGIN_KEY = "pt_transferOrigin";
    public static final String MDC_SOURCE_SYSTEM_HSA_ID_KEY = "pt_sourceSystemHsaId";
    public static final String MDC_START_DATE_TIME_KEY = "pt_startDateTime";
    public static final String MDC_END_DATE_TIME_KEY = "pt_endDateTime";

    public static final String CLIENT_CORRELATION_ID_HEADER_KEY = "X-PT-CLIENT-CORRELATION-ID";
}
