package se.skl.prenumerationstjanst.chbase;

import com.chbase.Connection;

import java.net.URI;

/**
 * Re-implementation of com.chbase.ConnectionFactory
 *
 * @author Martin Samuelsson
 */
public interface CHBaseConnectionFactory {
    /**
     * Gets the connection for the specified app id.  All connections
     * from this factory use the same private key.
     *
     * @param appId the app id
     *
     * @return the connection
     */
    Connection getConnection(String appId);

    /**
     * Gets a connection using the default app id.
     *
     * @return the connection
     */
    Connection getConnection();

    /**
     * Gets a connection using the specified url for
     * HealthVault platform's endpoint.
     *
     * @param platform uri
     * @return the connection
     */
    Connection getConnection(String appId, URI platform);

}
