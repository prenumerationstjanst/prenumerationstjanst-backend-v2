package se.skl.prenumerationstjanst.chbase;

import com.chbase.methods.jaxb.SimpleRequestTemplate;

/**
 * Factory for creating chbase request templates to ease testing of application code
 *
 * @author Martin Samuelsson
 */
public interface CHBaseRequestTemplateFactory {
    SimpleRequestTemplate getJaxbSimpleRequestTemplate();
    SimpleRequestTemplate getJaxbSimpleRequestTemplate(String personId, String recordId);
}
