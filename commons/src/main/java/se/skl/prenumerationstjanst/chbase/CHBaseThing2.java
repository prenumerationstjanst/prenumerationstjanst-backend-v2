package se.skl.prenumerationstjanst.chbase;

import com.chbase.thing.oxm.jaxb.thing.*;

import java.util.List;

/**
 * Extension of com.chbase.thing.oxm.jaxb.thing.Thing2 to enable access to the 'common' property of the
 * first DataXml record which is otherwise hidden
 *
 * @author Martin Samuelsson
 */
public class CHBaseThing2 extends Thing2 {

    /**
     * Sets the Common element on the first DataXml object
     * if no DataXml objects are found, the List is initalized and an empty DataXml is added before setting Common
     * @param common
     */
    public void setCommon(Common common) {
        List<DataXml> dataXmlList = getDataXml();
        if(dataXmlList.size() == 0) {
            DataXml dataXml = new DataXml();
            dataXmlList.add(dataXml);
        }
        dataXmlList.get(0).setCommon(common);
    }

    /**
     * @return Common from the first DataXml item if present, otherwise null
     */
    public Common getCommon() {
        if (getDataXml().size() > 0) { //never null
            return getDataXml().get(0).getCommon();
        }
        return null;
    }
}
