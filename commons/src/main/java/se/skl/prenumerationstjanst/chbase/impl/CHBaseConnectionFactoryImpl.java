package se.skl.prenumerationstjanst.chbase.impl;

import com.chbase.*;
import org.springframework.beans.factory.annotation.Value;
import se.skl.prenumerationstjanst.chbase.CHBaseConnectionFactory;

import java.net.URI;
import java.net.URL;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Non-static re-implementation of com.chbase.ConnectionFactory better suited
 * for use in a Spring environment
 *
 * @author Martin Samuelsson
 */
public class CHBaseConnectionFactoryImpl implements CHBaseConnectionFactory {

    @Value("${chbase.app_id}")
    private String defaultAppId;

    @Value("${chbase.hv_uri}")
    private URI defaultHvUri;

    @Value("${chbase.keystore.resource}")
    private String keystoreResource;

    @Value("${chbase.keystore.alias}")
    private String keystoreAlias;

    @Value("${chbase.keystore.password}")
    private String keystorePassword;

    @Value("${chbase.transport.timeout.connect}")
    private int connectionTimeout;

    @Value("${chbase.transport.timeout.read}")
    private int readTimeout;

    private Map<String, ApplicationAuthenticator> authenticators =
            new ConcurrentHashMap<>();

    @Override
    public Connection getConnection() {
        return getConnection(defaultAppId);
    }

    @Override
    public Connection getConnection(String appId) {
        return getConnection(appId, defaultHvUri);
    }

    @Override
    public Connection getConnection(String appId, URI platform) {
        Connection connection = new Connection();
        try {
            connection.setTransport(getTransport(platform));
            connection.setAuthenticator(getAuthenticator(appId));
            connection.authenticate();
            return connection;
        } catch (Exception e) {
            throw new HVException(e);
        }
    }

    /**
     * see com.chbase.ConnectionFactory#getAuthenticator(java.lang.String)
     */
    private Authenticator getAuthenticator(String appId) {
        return Optional
                .ofNullable(authenticators.get(appId))
                .orElseGet(() -> {
                    PrivateKeyStore keyStore = new CHBasePrivateKeyStoreImpl(
                            keystoreResource, keystoreAlias, keystorePassword);
                    ApplicationAuthenticator authenticator = new ApplicationAuthenticator();
                    authenticator.setSharedSecretGenerator(new DefaultSharedSecret());
                    authenticator.setAppId(appId);
                    authenticator.setKeyStore(keyStore);
                    authenticators.put(appId, authenticator);
                    return authenticator;
                });
    }

    /**
     * see com.chbase.ConnectionFactory#getTransport(java.net.URI)
     */
    private Transport getTransport(URI platform) throws Exception {
        URL url = platform.toURL();
        URLConnectionTransport transport = new URLConnectionTransport();
        transport.setConnectionTimeout(connectionTimeout);
        transport.setReadTimeout(readTimeout);
        transport.setUrl(url);
        return transport;
    }
}
