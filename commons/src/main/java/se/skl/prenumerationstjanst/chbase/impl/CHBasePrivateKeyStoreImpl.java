package se.skl.prenumerationstjanst.chbase.impl;

import com.chbase.HVSystemException;
import com.chbase.PrivateKeyStore;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.util.Enumeration;
import java.util.Optional;

/**
 * Re-implementation of com.chbase.DefaultPrivateKeyStore to work with KeyStore:s not
 * on classpath
 *
 * @author Martin Samuelsson
 */
public class CHBasePrivateKeyStoreImpl implements PrivateKeyStore {

    private static final Logger logger = LoggerFactory.getLogger(CHBasePrivateKeyStoreImpl.class);

    private PrivateKey privateKey;
    private String thumbprint;

    public CHBasePrivateKeyStoreImpl(String keystoreResource, String keystoreAlias, String keystorePassword) {
        init(keystoreResource, keystoreAlias, keystorePassword);
    }

    @Override
    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    @Override
    public String getThumbprint() {
        return thumbprint;
    }

    @SuppressWarnings("Duplicates")
    private void init(String keystoreResource, String keyAlias, String keystorePassword) {
        InputStream inputStream;
        try {
            try {
                inputStream = new ClassPathResource(keystoreResource).getInputStream();
                logger.debug("found {} on classpath", keystoreResource);
            } catch (IOException e) {
                logger.warn("could not load {} from classpath, will try filesystem", keystoreResource);
                try {
                    inputStream = new FileSystemResource(keystoreResource).getInputStream();
                    logger.debug("found {} on filesystem", keystoreResource);
                } catch (IOException e1) {
                    logger.warn("could not load {} from classpath or filesystem, failing...", keystoreResource);
                    throw e1;
                }
            }
            KeyStore keyStore = KeyStore.getInstance("JKS");
            keyStore.load(inputStream, keystorePassword.toCharArray());
            Enumeration<String> aliases = keyStore.aliases();
            while(aliases.hasMoreElements()) {
                String s = aliases.nextElement();
                logger.debug("alias: {}", s);
            }
            this.privateKey = (PrivateKey) Optional
                    .of(keyStore.getKey(keyAlias, keystorePassword.toCharArray()))
                    .get();

            Certificate certificate = Optional
                    .of(keyStore.getCertificate(keyAlias))
                    .get();

            MessageDigest messageDigest = MessageDigest.getInstance("SHA1");
            messageDigest.update(certificate.getEncoded());
            this.thumbprint = new String(Hex.encodeHex(messageDigest.digest()));
        } catch (Exception e) {
            throw new HVSystemException("Could not get private key information", e);
        }
    }
}
