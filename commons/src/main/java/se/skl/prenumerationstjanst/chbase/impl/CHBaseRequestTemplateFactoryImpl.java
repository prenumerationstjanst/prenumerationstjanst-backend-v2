package se.skl.prenumerationstjanst.chbase.impl;

import com.chbase.methods.jaxb.SimpleRequestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import se.skl.prenumerationstjanst.chbase.CHBaseConnectionFactory;
import se.skl.prenumerationstjanst.chbase.CHBaseRequestTemplateFactory;

/**
 * @author Martin Samuelsson
 */
public class CHBaseRequestTemplateFactoryImpl implements CHBaseRequestTemplateFactory {

    @Autowired
    private CHBaseConnectionFactory connectionFactory;

    @Override
    public SimpleRequestTemplate getJaxbSimpleRequestTemplate() {
        return new SimpleRequestTemplate(connectionFactory.getConnection());
    }

    @Override
    public SimpleRequestTemplate getJaxbSimpleRequestTemplate(String personId, String recordId) {
        return new SimpleRequestTemplate(connectionFactory.getConnection(), personId, recordId);
    }
}
