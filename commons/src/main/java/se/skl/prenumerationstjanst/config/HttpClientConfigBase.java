package se.skl.prenumerationstjanst.config;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContextBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import se.skl.prenumerationstjanst.httpclient.CorrelationIdRequestInterceptor;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Martin Samuelsson
 */
public class HttpClientConfigBase {

    private static final Logger logger = LoggerFactory.getLogger(HttpClientConfigBase.class);

    @Value("${pt.apigw.keystore.resource}")
    private String keystoreResource;

    @Value("${pt.apigw.keystore.password}")
    private String keystorePassword;

    @Value("${pt.apigw.keystore.type}")
    private String keystoreType;

    @Value("${pt.apigw.truststore.resource}")
    private String truststoreResource;

    @Value("${pt.apigw.truststore.password}")
    private String truststorePassword;

    @Value("${pt.apigw.truststore.type}")
    private String truststoreType;

    protected ClientHttpRequestFactory createHttpBasicRequestFactory(String host, int port, final String username,
                                                                   final String password) {
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory() {
            @Override
            protected HttpContext createHttpContext(HttpMethod httpMethod, URI uri) {
                BasicAuthCache basicAuthCache = new BasicAuthCache();
                basicAuthCache.put(new HttpHost(host, port), new BasicScheme());
                HttpClientContext httpClientContext = HttpClientContext.create();
                httpClientContext.setAuthCache(basicAuthCache);
                BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();
                credentialsProvider.setCredentials(AuthScope.ANY,
                        new UsernamePasswordCredentials(username, password));
                httpClientContext.setCredentialsProvider(credentialsProvider);
                return httpClientContext;
            }
        };
        CloseableHttpClient httpClient = HttpClientBuilder.create()
                .build();

        factory.setHttpClient(httpClient);
        return factory;
    }

    protected ClientHttpRequestFactory createX509RequestFactory() throws Exception {
        KeyStore keyStore = loadKeystore(keystoreResource, keystorePassword, keystoreType);
        KeyStore trustStore = loadKeystore(truststoreResource, truststorePassword, truststoreType);
        SSLContext sslContext = new SSLContextBuilder()
                .loadTrustMaterial(trustStore, null)
                .loadKeyMaterial(keyStore, keystorePassword.toCharArray()).build();
        SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(sslContext);

        CloseableHttpClient httpClient = HttpClientBuilder.create()
                .setSSLSocketFactory(sslConnectionSocketFactory)
                .build();
        return new HttpComponentsClientHttpRequestFactory(httpClient);
    }

    @SuppressWarnings("Duplicates")
    protected KeyStore loadKeystore(String keystoreResource, String keystorePassword, String keystoreType) throws
            KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
        KeyStore keyStore = KeyStore.getInstance(keystoreType);
        InputStream inputStream;
        try {
            inputStream = new ClassPathResource(keystoreResource).getInputStream();
            logger.debug("found {} on classpath", keystoreResource);
        } catch (IOException e) {
            logger.warn("could not load {} from classpath, will try filesystem", keystoreResource);
            try {
                inputStream = new FileSystemResource(keystoreResource).getInputStream();
                logger.debug("found {} on filesystem", keystoreResource);
            } catch (IOException e1) {
                logger.warn("could not load {} from classpath or filesystem, failing...", keystoreResource);
                throw e1;
            }
        }
        keyStore.load(inputStream, keystorePassword.toCharArray());
        return keyStore;
    }

    protected void addCorrelationIdInterceptorForApigw(RestTemplate restTemplate) {
        List<ClientHttpRequestInterceptor> clientHttpRequestInterceptors =
                Optional.ofNullable(restTemplate.getInterceptors())
                        .orElse(new ArrayList<>());
        clientHttpRequestInterceptors.add(new CorrelationIdRequestInterceptor("X-APIGW-CLIENT-CORRELATION-ID"));
        restTemplate.setInterceptors(clientHttpRequestInterceptors);
    }
}
