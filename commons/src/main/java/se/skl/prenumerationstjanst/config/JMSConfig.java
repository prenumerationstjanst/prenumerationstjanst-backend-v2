package se.skl.prenumerationstjanst.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.RedeliveryPolicy;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.util.ErrorHandler;
import se.skl.prenumerationstjanst.jms.DefaultJmsErrorHandler;
import se.skl.prenumerationstjanst.jms.IdAddingMessageConverter;

import javax.jms.ConnectionFactory;
import java.util.Arrays;

/**
 * Default JMS Config
 *
 * @author Martin Samuelsson
 */
@Configuration
public class JMSConfig {

    private static final Logger logger = LoggerFactory.getLogger(JMSConfig.class);

    @Value("${pt.activemq.broker-url}")
    private String brokerUrl;

    @Value("${pt.activemq.username}")
    private String username;

    @Value("${pt.activemq.password}")
    private String password;

    @Value("${pt.activemq.maximum_redeliveries}")
    private int redeliveryPolicyMaximumRedeliveries;

    @Value("${pt.activemq.redelivery_delay}")
    private long redeliveryPolicyRedeliveryDelay;

    @Value("${pt.activemq.use_exponential_backoff}")
    private boolean redeliveryPolicyUseExponentialBackoff;

    @Value("${pt.activemq.backoff_multiplier}")
    private double redeliveryPolicyBackoffMultiplier;

    /**
     * The default JMS ConnectionFactory for the application
     *
     * Will be picked up by the framework and used in the auto
     * configured JmsTemplate
     */
    @Bean
    @ConditionalOnMissingBean(value = ConnectionFactory.class)
    public ConnectionFactory jmsConnectionFactory() {
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
        connectionFactory.setBrokerURL(brokerUrl);
        connectionFactory.setUserName(username);
        connectionFactory.setPassword(password);
        // we do not want redeliveries to block other incoming deliveries
        // until all possible redeliveries for a given message has been completed
        connectionFactory.setNonBlockingRedelivery(true);
        connectionFactory.setRedeliveryPolicy(redeliveryPolicy());
        connectionFactory.setTrustedPackages(Arrays.asList(
                "java",
                "se.skl.prenumerationstjanst",
                "org.apigw.engagementindex"));
        pooledConnectionFactory.setConnectionFactory(connectionFactory);
        return pooledConnectionFactory;
    }

    /**
     * ActiveMQ RedeliverPolicy configured by properties
     */
    private RedeliveryPolicy redeliveryPolicy() {
        RedeliveryPolicy redeliveryPolicy = new RedeliveryPolicy();
        redeliveryPolicy.setMaximumRedeliveries(redeliveryPolicyMaximumRedeliveries);
        redeliveryPolicy.setInitialRedeliveryDelay(redeliveryPolicyRedeliveryDelay);
        redeliveryPolicy.setRedeliveryDelay(redeliveryPolicyRedeliveryDelay);
        redeliveryPolicy.setUseExponentialBackOff(redeliveryPolicyUseExponentialBackoff);
        redeliveryPolicy.setBackOffMultiplier(redeliveryPolicyBackoffMultiplier);
        return redeliveryPolicy;
    }

    @Bean
    @ConditionalOnMissingBean(name = "jmsErrorHandler")
    public ErrorHandler jmsErrorHandler() {
        return new DefaultJmsErrorHandler();
    }

    @Bean
    public IdAddingMessageConverter idAddingMessageConverter() {
        return new IdAddingMessageConverter();
    }

    /**
     * To prevent circular dependencies, keep DefaultJmsListenerContainerFactory
     * in a separate @Configuration class
     *
     * The ConnectionFactory and the ErrorHandler might be overridden by another profile or configuration
     * so we can not link to the @Bean declared method
     *
     */
    @Configuration
    public static class JmsListenerContainerFactoryConfig {

        private static final Logger logger = LoggerFactory.getLogger(JmsListenerContainerFactoryConfig.class);

        @Autowired
        private ConnectionFactory connectionFactory;

        @Autowired
        private ErrorHandler jmsErrorHandler;

        @Autowired
        private IdAddingMessageConverter idAddingMessageConverter;

        /**
         * DefaultJmsListenerContainerFactory with enabled transactions
         */
        @Bean
        @ConditionalOnMissingBean
        public DefaultJmsListenerContainerFactory jmsListenerContainerFactory() {
            DefaultJmsListenerContainerFactory listenerContainerFactory = new DefaultJmsListenerContainerFactory();
            listenerContainerFactory.setConnectionFactory(connectionFactory);
            listenerContainerFactory.setErrorHandler(jmsErrorHandler);
            listenerContainerFactory.setSessionTransacted(true);
            listenerContainerFactory.setMessageConverter(idAddingMessageConverter);
            return listenerContainerFactory;
        }

        /**
         * @return JmsTemplate with custom message converter
         */
        @Bean
        public JmsTemplate jmsTemplate() {
            JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory);
            jmsTemplate.setMessageConverter(idAddingMessageConverter);
            return jmsTemplate;
        }

        @Bean
        public JmsTemplate highPriorityJmsTemplate() {
            JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory);
            jmsTemplate.setExplicitQosEnabled(true);
            jmsTemplate.setPriority(9);
            jmsTemplate.setMessageConverter(idAddingMessageConverter);
            return jmsTemplate;
        }

    }

}
