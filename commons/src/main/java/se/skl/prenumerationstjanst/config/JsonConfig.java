package se.skl.prenumerationstjanst.config;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.cfg.PackageVersion;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import se.skl.prenumerationstjanst.json.PrenumerationstjanstLocalDateDeserializer;
import se.skl.prenumerationstjanst.json.PrenumerationstjanstLocalDateSerializer;

import java.time.LocalDateTime;

/**
 * @author Martin Samuelsson
 */
@Configuration
public class JsonConfig {

    /**
     * App specific jackson module to add custom LocalDate serializer/deserializer
     *
     * Spring boot auto-registration.
     *
     * @returns
     */
    @Bean
    public Module prenumerationstjanstJsonModule() {
        return new PrenumerationstjanstJsonModule();
    }

    public static class PrenumerationstjanstJsonModule extends SimpleModule {
        public PrenumerationstjanstJsonModule() {
            super(PackageVersion.VERSION);
            this.addSerializer(LocalDateTime.class, new PrenumerationstjanstLocalDateSerializer());
            this.addDeserializer(LocalDateTime.class, new PrenumerationstjanstLocalDateDeserializer());
        }
    }
}
