package se.skl.prenumerationstjanst.config;

/**
 * Marker annotation to exclude @Configuration classes created for tests
 * from the global component scan
 *
 * @author Martin Samuelsson
 */
public @interface TestConfiguration {
}
