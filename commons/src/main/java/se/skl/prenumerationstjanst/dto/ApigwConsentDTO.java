package se.skl.prenumerationstjanst.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author Martin Samuelsson
 */
public class ApigwConsentDTO implements Serializable {
    private final String tokenValue;
    private final LocalDateTime expirationDateTime;
    private final String scope;
    private final SubscriptionDTO subscription;

    private ApigwConsentDTO() {
        this.tokenValue = null;
        this.expirationDateTime = null;
        this.scope = null;
        this.subscription = null;
    }

    public ApigwConsentDTO(String tokenValue, LocalDateTime expirationDateTime, String scope, SubscriptionDTO subscription) {
        this.tokenValue = tokenValue;
        this.expirationDateTime = expirationDateTime != null ? LocalDateTime.from(expirationDateTime) : null;
        this.scope = scope;
        this.subscription = subscription;
    }

    public static ApigwConsentDTO create() {
        return new ApigwConsentDTO();
    }

    public ApigwConsentDTO withTokenValue(String tokenValue) {
        return new ApigwConsentDTO(tokenValue, this.expirationDateTime, this.scope, this.subscription);
    }

    public ApigwConsentDTO withExpirationDateTime(LocalDateTime expirationDateTime) {
        return new ApigwConsentDTO(this.tokenValue, expirationDateTime, this.scope, this.subscription);
    }

    public ApigwConsentDTO withScope(String scope) {
        return new ApigwConsentDTO(this.tokenValue, this.expirationDateTime, scope, this.subscription);
    }

    public ApigwConsentDTO withSubscription(SubscriptionDTO subscription) {
        return new ApigwConsentDTO(this.tokenValue, this.expirationDateTime, this.scope, subscription);
    }

    public String getTokenValue() {
        return tokenValue;
    }

    public LocalDateTime getExpirationDateTime() {
        return expirationDateTime;
    }

    public String getScope() {
        return scope;
    }

    public SubscriptionDTO getSubscription() {
        return subscription;
    }

    @Override
    public String toString() {
        return "ApigwConsentDTO{" +
                "tokenValue='" + tokenValue + '\'' +
                ", expirationDateTime=" + expirationDateTime +
                ", scope='" + scope + '\'' +
                ", subscription=" + subscription +
                '}';
    }
}
