package se.skl.prenumerationstjanst.dto;

import se.skl.prenumerationstjanst.model.JobStatus;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Used for passing around FetchJob info in a detached fashion
 *
 * @author Martin Samuelsson
 */
public class FetchJobDTO implements Serializable {

    private final String id;
    private final TransferJobDTO transferJob;
    private final String apigwToken;
    private final JobStatus status;
    private final LocalDateTime fetchFinishTime;

    private FetchJobDTO() {
        this.id = null;
        this.transferJob = null;
        this.apigwToken = null;
        this.status = null;
        this.fetchFinishTime = null;
    }

    public FetchJobDTO(String id, TransferJobDTO transferJob, String apigwToken, JobStatus status,
                       LocalDateTime fetchFinishTime) {
        this.id = id;
        this.transferJob = transferJob;
        this.apigwToken = apigwToken;
        this.status = status;
        this.fetchFinishTime = fetchFinishTime;
    }

    public static FetchJobDTO create() {
        return new FetchJobDTO();
    }

    public FetchJobDTO withId(String id) {
        return new FetchJobDTO(id, this.transferJob, this.apigwToken, this.status, this.fetchFinishTime);
    }

    public FetchJobDTO withTransferJob(TransferJobDTO transferJob) {
        return new FetchJobDTO(this.id, transferJob, this.apigwToken, this.status, this.fetchFinishTime);
    }

    public FetchJobDTO withApigwToken(String apigwToken) {
        return new FetchJobDTO(this.id, this.transferJob, apigwToken, this.status, this.fetchFinishTime);
    }

    public FetchJobDTO withStatus(JobStatus status) {
        return new FetchJobDTO(this.id, this.transferJob, this.apigwToken, status, this.fetchFinishTime);
    }

    public FetchJobDTO withFetchFinishTime(LocalDateTime fetchFinishTime) {
        return new FetchJobDTO(this.id, this.transferJob, this.apigwToken, this.status, fetchFinishTime);
    }

    public String getId() {
        return id;
    }

    public TransferJobDTO getTransferJob() {
        return transferJob;
    }

    public String getApigwToken() {
        return apigwToken;
    }

    public JobStatus getStatus() {
        return status;
    }

    public LocalDateTime getFetchFinishTime() {
        return fetchFinishTime;
    }

    @Override
    public String toString() {
        return "FetchJobDTO{" +
                "id='" + id + '\'' +
                ", transferJob=" + transferJob +
                ", apigwToken='" + apigwToken + '\'' +
                ", status=" + status +
                ", fetchFinishTime=" + fetchFinishTime +
                '}';
    }
}
