package se.skl.prenumerationstjanst.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author Martin Samuelsson
 */
public class JobStateDTO implements Serializable {
    private final LocalDateTime dateTime;
    private final String jobStatus;

    public JobStateDTO(LocalDateTime dateTime, String jobStatus) {
        this.dateTime = dateTime;
        this.jobStatus = jobStatus;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    @Override
    public String toString() {
        return "JobStateDTO{" +
                "dateTime=" + dateTime +
                ", jobStatus='" + jobStatus + '\'' +
                '}';
    }
}
