package se.skl.prenumerationstjanst.dto;

import java.io.Serializable;

/**
 * @author Martin Samuelsson
 */
public class MonitoredTransferEventDTO implements Serializable {
    private static final long serialVersionUID = -2251751014325938258L;

    private final Serializable monitoredObject;
    private final String monitoredObjectType;
    private final String jobStatus;

    public MonitoredTransferEventDTO(Serializable monitoredObject, String jobStatus) {
        this.monitoredObject = monitoredObject;
        this.monitoredObjectType = monitoredObject.getClass().getName();
        this.jobStatus = jobStatus;
    }

    public Serializable getMonitoredObject() {
        return monitoredObject;
    }

    public String getMonitoredObjectType() {
        return monitoredObjectType;
    }

    public String getJobStatus() {
        return jobStatus;
    }
}
