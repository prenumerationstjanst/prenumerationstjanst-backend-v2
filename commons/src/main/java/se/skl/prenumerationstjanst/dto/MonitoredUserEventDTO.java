package se.skl.prenumerationstjanst.dto;

import se.skl.prenumerationstjanst.model.monitoring.MonitoredUserEventType;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Martin Samuelsson
 */
public class MonitoredUserEventDTO implements Serializable {

    private static final long serialVersionUID = -3712965706795401353L;

    private final String userCrn;
    private final String userIpAddress;
    private final String correlationId;
    private final MonitoredUserEventType eventType;
    private final Map<String, String> eventDetails;

    public MonitoredUserEventDTO(String userCrn, String userIpAddress, String correlationId, MonitoredUserEventType
            eventType, Map<String, String> eventDetails) {
        this.userCrn = userCrn;
        this.userIpAddress = userIpAddress;
        this.correlationId = correlationId;
        this.eventType = eventType;
        this.eventDetails = eventDetails;
    }

    public String getUserCrn() {
        return userCrn;
    }

    public String getUserIpAddress() {
        return userIpAddress;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public MonitoredUserEventType getEventType() {
        return eventType;
    }

    public Map<String, String> getEventDetails() {
        return eventDetails;
    }

    @Override
    public String toString() {
        return "MonitoredUserEvent{" +
                "userCrn='" + userCrn + '\'' +
                ", userIpAddress='" + userIpAddress + '\'' +
                ", correlationId='" + correlationId + '\'' +
                ", eventType=" + eventType +
                ", eventDetails=" + eventDetails +
                '}';
    }
}
