package se.skl.prenumerationstjanst.dto;

import java.io.Serializable;

/**
 * @author Martin Samuelsson
 */
public class PhkConsentDTO implements Serializable {
    private final String personId;
    private final String recordId;
    private final String recordName;

    private PhkConsentDTO() {
        this.personId = null;
        this.recordId = null;
        this.recordName = null;
    }

    public PhkConsentDTO(String personId, String recordId, String recordName) {
        this.personId = personId;
        this.recordId = recordId;
        this.recordName = recordName;
    }

    public static PhkConsentDTO create() {
        return new PhkConsentDTO();
    }

    public PhkConsentDTO withPersonId(String personId) {
        return new PhkConsentDTO(personId, this.recordId, this.recordName);
    }

    public PhkConsentDTO withRecordId(String recordId) {
        return new PhkConsentDTO(this.personId, recordId, this.recordName);
    }

    public PhkConsentDTO withRecordName(String recordName) {
        return new PhkConsentDTO(this.personId, this.recordId, recordName);
    }

    public String getPersonId() {
        return personId;
    }

    public String getRecordId() {
        return recordId;
    }

    public String getRecordName() {
        return recordName;
    }

    @Override
    public String toString() {
        return "PhkConsentDTO{" +
                "personId='" + personId + '\'' +
                ", recordId='" + recordId + '\'' +
                ", recordName='" + recordName + '\'' +
                '}';
    }
}
