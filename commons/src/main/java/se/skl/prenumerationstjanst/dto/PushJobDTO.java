package se.skl.prenumerationstjanst.dto;

import se.skl.prenumerationstjanst.model.JobStatus;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

/**
 * Used for passing around PushJob info in a detached fashion
 *
 * @author Martin Samuelsson
 */
public class PushJobDTO implements Serializable {

    private final String id;
    private final TransferJobDTO transferJob;
    private final String phkPersonId;
    private final String phkRecordId;
    private final String value;
    private final List<String> relatedValues;
    private final JobStatus status;
    private final String sourceId;
    private final int sourceVersion;
    private final LocalDateTime pushFinishTime;
    /**
     * free text to indicate the type of data that was (or was not) pushed
     * to the recipient system
     *
     * should only be available if status = FINISHED or DUPLICATE
     */
    private final String typeOfData;

    /**
     * id of a 'parent' PushJob (if applicable) to indicate a relation
     *
     * should only be available if status = FINISHED
     */
    private final String relatedTo;

    private PushJobDTO() {
        this.id = null;
        this.transferJob = null;
        this.phkPersonId = null;
        this.phkRecordId = null;
        this.value = null;
        this.relatedValues = Collections.unmodifiableList(Collections.emptyList());
        this.status = null;
        this.sourceId = null;
        this.sourceVersion = 0;
        this.pushFinishTime = null;
        this.typeOfData = null;
        this.relatedTo = null;

    }

    public PushJobDTO(String id, TransferJobDTO transferJob, String phkPersonId, String phkRecordId, String value, List<String> relatedValues, JobStatus status,
                      String sourceId, int sourceVersion, LocalDateTime pushFinishTime, String relatedTo, String typeOfData) {
        this.id = id;
        this.transferJob = transferJob;
        this.phkPersonId = phkPersonId;
        this.phkRecordId = phkRecordId;
        this.value = value;
        this.relatedValues = relatedValues != null
                ? Collections.unmodifiableList(relatedValues)
                : Collections.unmodifiableList(Collections.emptyList());
        this.status = status;
        this.sourceId = sourceId;
        this.sourceVersion = sourceVersion;
        this.pushFinishTime = pushFinishTime != null
                ? LocalDateTime.from(pushFinishTime)
                : null;
        this.typeOfData = typeOfData;
        this.relatedTo = relatedTo;

    }

    public static PushJobDTO create() {
        return new PushJobDTO();
    }

    public PushJobDTO withId(String id) {
        return new PushJobDTO(id, this.transferJob, this.phkPersonId, this.phkRecordId, this.value, this.relatedValues, this.status, this.sourceId,
                this.sourceVersion, this.pushFinishTime, this.relatedTo, this.typeOfData);
    }

    public PushJobDTO withTransferJob(TransferJobDTO transferJob) {
        return new PushJobDTO(this.id, transferJob, this.phkPersonId, this.phkRecordId, this.value, this.relatedValues, this.status, this.sourceId,
                this.sourceVersion, this.pushFinishTime, this.relatedTo, this.typeOfData);
    }

    public PushJobDTO withPhkPersonId(String phkPersonId) {
        return new PushJobDTO(this.id, this.transferJob, phkPersonId, this.phkRecordId, this.value, this.relatedValues, this.status, this.sourceId,
                this.sourceVersion, this.pushFinishTime, this.relatedTo, this.typeOfData);
    }

    public PushJobDTO withPhkRecordId(String phkRecordId) {
        return new PushJobDTO(this.id, this.transferJob, this.phkPersonId, phkRecordId, this.value, this.relatedValues, this.status, this.sourceId,
                this.sourceVersion, this.pushFinishTime, this.relatedTo, this.typeOfData);
    }

    public PushJobDTO withValue(String value) {
        return new PushJobDTO(this.id, this.transferJob, this.phkPersonId, this.phkRecordId, value, this.relatedValues, this.status, this.sourceId,
                this.sourceVersion, this.pushFinishTime, this.relatedTo, this.typeOfData);
    }

    public PushJobDTO withRelatedValues(List<String> relatedValues) {
        return new PushJobDTO(this.id, this.transferJob, this.phkPersonId, this.phkRecordId, this.value, relatedValues, this.status, this.sourceId,
                this.sourceVersion, this.pushFinishTime, this.relatedTo, this.typeOfData);
    }

    public PushJobDTO withStatus(JobStatus status) {
        return new PushJobDTO(this.id, this.transferJob, this.phkPersonId, this.phkRecordId, this.value, this.relatedValues, status, this.sourceId,
                this.sourceVersion, this.pushFinishTime, this.relatedTo, this.typeOfData);
    }

    public PushJobDTO withSourceId(String sourceId) {
        return new PushJobDTO(this.id, this.transferJob, this.phkPersonId, this.phkRecordId, this.value, this.relatedValues, this.status, sourceId,
                this.sourceVersion, this.pushFinishTime, this.relatedTo, this.typeOfData);
    }

    public PushJobDTO withSourceVersion(int sourceVersion) {
        return new PushJobDTO(this.id, this.transferJob, this.phkPersonId, this.phkRecordId, this.value, this.relatedValues, this.status, this.sourceId,
                sourceVersion, this.pushFinishTime, this.relatedTo, this.typeOfData);
    }

    public PushJobDTO withPushFinishTime(LocalDateTime pushFinishTime) {
        return new PushJobDTO(this.id, this.transferJob, this.phkPersonId, this.phkRecordId, this.value, this.relatedValues, this.status, this.sourceId,
                this.sourceVersion, pushFinishTime, this.relatedTo, this.typeOfData);
    }

    public PushJobDTO withTypeOfData(String typeOfData) {
        return new PushJobDTO(this.id, this.transferJob, this.phkPersonId, this.phkRecordId, this.value, this.relatedValues, this.status, this.sourceId,
                this.sourceVersion, this.pushFinishTime, this.relatedTo, typeOfData);
    }

    public PushJobDTO withRelatedTo(String relatedTo) {
        return new PushJobDTO(this.id, this.transferJob, this.phkPersonId, this.phkRecordId, this.value, this.relatedValues, this.status, this.sourceId,
                this.sourceVersion, this.pushFinishTime, relatedTo, this.typeOfData);
    }

    public String getId() {
        return id;
    }

    public TransferJobDTO getTransferJob() {
        return transferJob;
    }

    public String getPhkPersonId() {
        return phkPersonId;
    }

    public String getPhkRecordId() {
        return phkRecordId;
    }

    public String getValue() {
        return value;
    }

    public List<String> getRelatedValues() {
        return relatedValues;
    }

    public JobStatus getStatus() {
        return status;
    }

    public String getSourceId() {
        return sourceId;
    }

    public int getSourceVersion() {
        return sourceVersion;
    }

    public LocalDateTime getPushFinishTime() {
        return pushFinishTime;
    }

    public String getTypeOfData() {
        return typeOfData;
    }

    public String getRelatedTo() {
        return relatedTo;
    }

    @Override
    public String toString() {
        return "PushJobDTO{" +
                "id='" + id + '\'' +
                ", transferJob=" + transferJob +
                ", phkPersonId='" + phkPersonId + '\'' +
                ", phkRecordId='" + phkRecordId + '\'' +
                ", value='" + value + '\'' +
                ", relatedValues=" + relatedValues +
                ", status=" + status +
                ", sourceId='" + sourceId + '\'' +
                ", sourceVersion=" + sourceVersion +
                ", pushFinishTime=" + pushFinishTime +
                ", typeOfData='" + typeOfData + '\'' +
                ", relatedTo='" + relatedTo + '\'' +
                '}';
    }
}
