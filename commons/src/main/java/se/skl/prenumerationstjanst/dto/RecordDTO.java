package se.skl.prenumerationstjanst.dto;

import java.io.Serializable;

/**
 * @author Martin Samuelsson
 */
public class RecordDTO implements Serializable {
    private final String personId;
    private final String recordId;
    private final String recordName;

    public RecordDTO(String recordId, String personId, String recordName) {
        this.recordId = recordId;
        this.personId = personId;
        this.recordName = recordName;
    }

    public String getRecordId() {
        return recordId;
    }

    public String getPersonId() {
        return personId;
    }

    public String getRecordName() {
        return recordName;
    }

    @Override
    public String toString() {
        return "RecordDTO{" +
                "recordId='" + recordId + '\'' +
                ", personId='" + personId + '\'' +
                ", recordName='" + recordName + '\'' +
                '}';
    }
}
