package se.skl.prenumerationstjanst.dto;

import java.io.Serializable;

/**
 * @author Martin Samuelsson
 */
public class RelationDTO implements Serializable {
    private final String name;
    private final String crn;
    private final int age;

    private RelationDTO() {
        this.name = null;
        this.crn = null;
        this.age = 0;
    }

    public RelationDTO(String name, String crn, int age) {
        this.name = name;
        this.crn = crn;
        this.age = age;
    }

    public static RelationDTO create() {
        return new RelationDTO();
    }

    public RelationDTO withName(String name) {
        return new RelationDTO(name, this.crn, this.age);
    }

    public RelationDTO withCrn(String crn) {
        return new RelationDTO(this.name, crn, this.age);
    }

    public RelationDTO withAge(int age) {
        return new RelationDTO(this.name, this.crn, age);
    }

    public String getName() {
        return name;
    }

    public String getCrn() {
        return crn;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "RelationDTO{" +
                "name='" + name + '\'' +
                ", crn='" + crn + '\'' +
                ", age=" + age +
                '}';
    }
}
