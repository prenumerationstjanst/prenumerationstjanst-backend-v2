package se.skl.prenumerationstjanst.dto;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * @author Martin Samuelsson
 */
public class SubscriptionDTO implements Serializable {

    private final String id;
    private final UserContextDTO userContext;
    private final String approvedTransferType;
    private final List<TransferJobDTO> transferJobs;
    private final ApigwConsentDTO apigwConsent;

    private SubscriptionDTO() {
        this.id = null;
        this.userContext = null;
        this.approvedTransferType = null;
        this.transferJobs = null;
        this.apigwConsent = null;
    }

    public SubscriptionDTO(String id, UserContextDTO userContext, String approvedTransferType,
                           List<TransferJobDTO> transferJobs, ApigwConsentDTO apigwConsent) {
        this.id = id;
        this.userContext = userContext;
        this.approvedTransferType = approvedTransferType;
        this.transferJobs = transferJobs != null ? Collections.unmodifiableList(transferJobs) : null;
        this.apigwConsent = apigwConsent;
    }

    public static SubscriptionDTO create() {
        return new SubscriptionDTO();
    }

    public SubscriptionDTO withId(String id) {
        return new SubscriptionDTO(id, this.userContext, this.approvedTransferType, this.transferJobs, this.apigwConsent);
    }

    public SubscriptionDTO withUserContext(UserContextDTO userContext) {
        return new SubscriptionDTO(this.id, userContext, this.approvedTransferType, this.transferJobs, this.apigwConsent);
    }

    public SubscriptionDTO withApprovedTransferType(String approvedTransferType) {
        return new SubscriptionDTO(this.id, this.userContext, approvedTransferType, this.transferJobs, this.apigwConsent);
    }

    public SubscriptionDTO withTransferJobs(List<TransferJobDTO> transferJobs) {
        return new SubscriptionDTO(this.id, this.userContext, this.approvedTransferType, transferJobs, this.apigwConsent);
    }

    public SubscriptionDTO withApigwConsent(ApigwConsentDTO apigwConsent) {
        return new SubscriptionDTO(this.id, this.userContext, this.approvedTransferType, this.transferJobs, apigwConsent);
    }

    public String getId() {
        return id;
    }

    public UserContextDTO getUserContext() {
        return userContext;
    }

    public String getApprovedTransferType() {
        return approvedTransferType;
    }

    public List<TransferJobDTO> getTransferJobs() {
        return transferJobs;
    }

    public ApigwConsentDTO getApigwConsent() {
        return apigwConsent;
    }

    @Override
    public String toString() {
        return "SubscriptionDTO{" +
                "id='" + id + '\'' +
                ", userContext=" + userContext +
                ", approvedTransferType=" + approvedTransferType +
                ", transferJobs=" + transferJobs +
                ", apigwConsent=" + apigwConsent +
                '}';
    }
}
