package se.skl.prenumerationstjanst.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

/**
 * @author Martin Samuelsson
 */
public class TransferJobDTO implements Serializable {

    private final String id;
    private final String representingCrn;
    private final String userCrn;
    private final String subscriptionId;
    private final String transferType;
    private final LocalDateTime startDateTime;
    private final LocalDateTime endDateTime;
    private final String sourceSystemHsaId;
    private final FetchJobDTO fetchJob;
    private final List<PushJobDTO> pushJobs;
    private final String origin;

    private TransferJobDTO() {
        this.id = null;
        this.userCrn = null;
        this.representingCrn = null;
        this.subscriptionId = null;
        this.transferType = null;
        this.startDateTime = null;
        this.endDateTime = null;
        this.sourceSystemHsaId = null;
        this.fetchJob = null;
        this.pushJobs = Collections.unmodifiableList(Collections.emptyList());
        this.origin = null;
    }

    public TransferJobDTO(String id, String userCrn, String representingCrn, String subscriptionId, String transferType, LocalDateTime startDateTime,
                          LocalDateTime endDateTime, String sourceSystemHsaId, FetchJobDTO fetchJob,
                          List<PushJobDTO> pushJobs, String origin) {
        this.userCrn = userCrn;
        this.representingCrn = representingCrn;
        this.subscriptionId = subscriptionId;
        this.id = id;
        this.transferType = transferType;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.sourceSystemHsaId = sourceSystemHsaId;
        this.fetchJob = fetchJob;
        this.pushJobs = Collections.unmodifiableList(pushJobs);
        this.origin = origin;
    }

    public static TransferJobDTO create() {
        return new TransferJobDTO();
    }

    public TransferJobDTO withId(String id) {
        return new TransferJobDTO(id, this.userCrn, this.representingCrn, this.subscriptionId, this.transferType, this.startDateTime,
                this.endDateTime, this.sourceSystemHsaId, this.fetchJob, this.pushJobs, this.origin);
    }

    public TransferJobDTO withUserCrn(String userCrn) {
        return new TransferJobDTO(this.id, userCrn, this.representingCrn, this.subscriptionId, this.transferType, this.startDateTime,
                this.endDateTime, this.sourceSystemHsaId, this.fetchJob, this.pushJobs, this.origin);
    }

    public TransferJobDTO withRepresentingCrn(String representingCrn) {
        return new TransferJobDTO(this.id, this.userCrn, representingCrn, this.subscriptionId, this.transferType, this.startDateTime,
                this.endDateTime, this.sourceSystemHsaId, this.fetchJob, this.pushJobs, this.origin);
    }

    public TransferJobDTO withSubscriptionId(String subscriptionId) {
        return new TransferJobDTO(this.id, this.userCrn, this.representingCrn, subscriptionId, this.transferType, this.startDateTime,
                this.endDateTime, this.sourceSystemHsaId, this.fetchJob, this.pushJobs, this.origin);
    }

    public TransferJobDTO withTransferType(String transferType) {
        return new TransferJobDTO(this.id, this.userCrn, this.representingCrn, this.subscriptionId, transferType, this.startDateTime,
                this.endDateTime, this.sourceSystemHsaId, this.fetchJob, this.pushJobs, this.origin);
    }

    public TransferJobDTO withStartDateTime(LocalDateTime startDateTime) {
        return new TransferJobDTO(this.id, this.userCrn, this.representingCrn, this.subscriptionId, this.transferType, startDateTime,
                this.endDateTime, this.sourceSystemHsaId, this.fetchJob, this.pushJobs, this.origin);
    }

    public TransferJobDTO withEndDateTime(LocalDateTime endDateTime) {
        return new TransferJobDTO(this.id, this.userCrn, this.representingCrn, this.subscriptionId, this.transferType, this.startDateTime,
                endDateTime, this.sourceSystemHsaId, this.fetchJob, this.pushJobs, this.origin);
    }

    public TransferJobDTO withSourceSystemHsaId(String sourceSystemHsaId) {
        return new TransferJobDTO(this.id, this.userCrn, this.representingCrn, this.subscriptionId, this.transferType, this.startDateTime,
                this.endDateTime, sourceSystemHsaId, this.fetchJob, this.pushJobs, this.origin);
    }

    public TransferJobDTO withFetchJob(FetchJobDTO fetchJob) {
        return new TransferJobDTO(this.id, this.userCrn, this.representingCrn, this.subscriptionId, this.transferType, this.startDateTime,
                this.endDateTime, this.sourceSystemHsaId, fetchJob, this.pushJobs, this.origin);
    }

    public TransferJobDTO withPushJobs(List<PushJobDTO> pushJobs) {
        return new TransferJobDTO(this.id, this.userCrn, this.representingCrn, this.subscriptionId, this.transferType, this.startDateTime,
                this.endDateTime, this.sourceSystemHsaId, this.fetchJob, pushJobs, this.origin);
    }

    public TransferJobDTO withOrigin(String origin) {
        return new TransferJobDTO(this.id, this.userCrn, this.representingCrn, this.subscriptionId, this.transferType, this.startDateTime,
                this.endDateTime, this.sourceSystemHsaId, this.fetchJob, this.pushJobs, origin);
    }

    public String getId() {
        return id;
    }

    public String getRepresentingCrn() {
        return representingCrn;
    }

    public String getUserCrn() {
        return userCrn;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public String getTransferType() {
        return transferType;
    }

    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }

    public LocalDateTime getEndDateTime() {
        return endDateTime;
    }

    public String getSourceSystemHsaId() {
        return sourceSystemHsaId;
    }

    public FetchJobDTO getFetchJob() {
        return fetchJob;
    }

    public List<PushJobDTO> getPushJobs() {
        return pushJobs;
    }

    public String getOrigin() {
        return origin;
    }

    @Override
    public String toString() {
        return "TransferJobDTO{" +
                "id='" + id + '\'' +
                ", representingCrn='" + representingCrn + '\'' +
                ", userCrn='" + userCrn + '\'' +
                ", subscriptionId='" + subscriptionId + '\'' +
                ", transferType='" + transferType + '\'' +
                ", startDateTime=" + startDateTime +
                ", endDateTime=" + endDateTime +
                ", sourceSystemHsaId='" + sourceSystemHsaId + '\'' +
                ", fetchJob=" + fetchJob +
                ", pushJobs=" + pushJobs +
                ", origin='" + origin + '\'' +
                '}';
    }
}
