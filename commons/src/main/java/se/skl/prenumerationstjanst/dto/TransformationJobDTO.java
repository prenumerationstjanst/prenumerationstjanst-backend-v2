package se.skl.prenumerationstjanst.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * @author Martin Samuelsson
 */
public class TransformationJobDTO implements Serializable {

    private final String id;
    private final TransferJobDTO transferJob;
    private final String source;
    private final LocalDateTime sourceFetchTime;
    private final int sourceVersion;

    public TransformationJobDTO(String id, TransferJobDTO transferJob, String source, LocalDateTime sourceFetchTime, int sourceVersion) {
        this.id = id;
        this.transferJob = transferJob;
        this.source = source;
        this.sourceFetchTime = sourceFetchTime != null ? LocalDateTime.from(sourceFetchTime) : null;
        this.sourceVersion = sourceVersion;
    }

    public String getId() {
        return id;
    }

    public TransferJobDTO getTransferJob() {
        return transferJob;
    }

    public String getSource() {
        return source;
    }

    public LocalDateTime getSourceFetchTime() {
        return sourceFetchTime;
    }

    public int getSourceVersion() {
        return sourceVersion;
    }

    public TransformationJobDTO withId(String id) {
        return new TransformationJobDTO(id, this.transferJob, this.source, this.sourceFetchTime, this.sourceVersion);
    }

    public TransformationJobDTO withTransferJob(TransferJobDTO transferJob) {
        return new TransformationJobDTO(this.id, transferJob, this.source, this.sourceFetchTime, this.sourceVersion);
    }

    public TransformationJobDTO withSource(String source) {
        return new TransformationJobDTO(this.id, this.transferJob, source, this.sourceFetchTime, this.sourceVersion);
    }

    public TransformationJobDTO withSourceFetchTime(LocalDateTime sourceFetchTime) {
        return new TransformationJobDTO(this.id, this.transferJob, this.source, sourceFetchTime, this.sourceVersion);
    }

    public TransformationJobDTO withSourceVersion(int sourceVersion) {
        return new TransformationJobDTO(this.id, this.transferJob, this.source, this.sourceFetchTime, sourceVersion);
    }

    @Override
    public String toString() {
        return "TransformationJobDTO{" +
                "id='" + id + '\'' +
                ", transferJob=" + transferJob +
                ", source='" + source + '\'' +
                ", sourceFetchTime=" + sourceFetchTime +
                ", sourceVersion=" + sourceVersion +
                '}';
    }
}
