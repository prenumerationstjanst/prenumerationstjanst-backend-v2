package se.skl.prenumerationstjanst.dto;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * @author Martin Samuelsson
 */
public class UserContextDTO implements Serializable {
    private final UserDTO user;
    private final String representingCrn;
    private final List<SubscriptionDTO> subscriptions;
    private final String phkRecordId;

    private UserContextDTO() {
        this.user = null;
        this.representingCrn = null;
        this.subscriptions = null;
        this.phkRecordId = null;
    }

    public UserContextDTO(UserDTO user, String representingCrn, List<SubscriptionDTO> subscriptions,
                          String phkRecordId) {
        this.user = user;
        this.representingCrn = representingCrn;
        this.subscriptions = subscriptions != null
                ? Collections.unmodifiableList(subscriptions)
                : Collections.emptyList();
        this.phkRecordId = phkRecordId;
    }

    public static UserContextDTO create() {
        return new UserContextDTO();
    }

    public UserContextDTO withUser(UserDTO user) {
        return new UserContextDTO(user, this.representingCrn, this.subscriptions, this.phkRecordId);
    }

    public UserContextDTO withRepresentingCrn(String representingCrn) {
        return new UserContextDTO(this.user, representingCrn, this.subscriptions, this.phkRecordId);
    }

    public UserContextDTO withSubscriptions(List<SubscriptionDTO> subscriptions) {
        return new UserContextDTO(this.user, this.representingCrn, subscriptions, this.phkRecordId);
    }

    public UserContextDTO withPhkRecordId(String phkRecordId) {
        return new UserContextDTO(this.user, this.representingCrn, subscriptions, phkRecordId);
    }

    public UserDTO getUser() {
        return user;
    }

    public String getRepresentingCrn() {
        return representingCrn;
    }

    public List<SubscriptionDTO> getSubscriptions() {
        return subscriptions;
    }

    public String getPhkRecordId() {
        return phkRecordId;
    }

    @Override
    public String toString() {
        return "UserContextDTO{" +
                "user=" + user +
                ", representingCrn='[NOT LOGGED]'" +
                ", subscriptions=" + subscriptions +
                ", phkRecordId=" + phkRecordId +
                '}';
    }
}
