package se.skl.prenumerationstjanst.dto;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * @author Martin Samuelsson
 */
public class UserDTO implements Serializable {

    private final Long id;
    private final String crn;
    private final String phkPersonId;
    private final List<UserContextDTO> userContexts;

    private UserDTO() {
        this.id = null;
        this.crn = null;
        this.phkPersonId = null;
        this.userContexts = null;
    }

    public UserDTO(Long id, String crn, String phkPersonId, List<UserContextDTO> userContexts) {
        this.id = id;
        this.crn = crn;
        this.phkPersonId = phkPersonId;
        this.userContexts = userContexts != null
                ? Collections.unmodifiableList(userContexts)
                : Collections.emptyList();
    }

    public static UserDTO create() {
        return new UserDTO();
    }

    public UserDTO withId(Long id) {
        return new UserDTO(id, this.crn, this.phkPersonId, this.userContexts);
    }

    public UserDTO withCrn(String crn) {
        return new UserDTO(this.id, crn, this.phkPersonId, this.userContexts);
    }

    public UserDTO withPhkPersonId(String phkPersonId) {
        return new UserDTO(this.id, this.crn, phkPersonId, this.userContexts);
    }

    public UserDTO withUserContexts(List<UserContextDTO> userContexts) {
        return new UserDTO(this.id, this.crn, this.phkPersonId, userContexts);
    }

    public Long getId() {
        return id;
    }

    public String getCrn() {
        return crn;
    }

    public String getPhkPersonId() {
        return phkPersonId;
    }

    public List<UserContextDTO> getUserContexts() {
        return userContexts;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", crn='[NOT LOGGED]'" +
                ", phkPersonId=" + phkPersonId +
                ", userContexts=" + userContexts +
                '}';
    }
}
