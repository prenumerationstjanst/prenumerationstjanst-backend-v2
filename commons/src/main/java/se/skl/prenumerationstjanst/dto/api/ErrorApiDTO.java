package se.skl.prenumerationstjanst.dto.api;

/**
 * @author Martin Samuelsson
 */
public class ErrorApiDTO {
    private final String errorCode;
    private final String errorMsg;

    public ErrorApiDTO(String errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}
