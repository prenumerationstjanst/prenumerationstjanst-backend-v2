package se.skl.prenumerationstjanst.dto.api;

/**
 * @author Martin Samuelsson
 */
public class RecordApiDTO {
    private final String recordId;
    private final String recordName;

    public RecordApiDTO(String recordId, String recordName) {
        this.recordId = recordId;
        this.recordName = recordName;
    }

    public String getRecordId() {
        return recordId;
    }

    public String getRecordName() {
        return recordName;
    }

    @Override
    public String toString() {
        return "RecordApiDTO{" +
                "recordId='" + recordId + '\'' +
                ", recordName='" + recordName + '\'' +
                '}';
    }
}
