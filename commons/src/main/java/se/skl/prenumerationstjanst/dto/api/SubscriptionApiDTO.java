package se.skl.prenumerationstjanst.dto.api;

import java.time.LocalDateTime;

/**
 * @author Martin Samuelsson
 */
public class SubscriptionApiDTO {

    private final String id;
    private final String approvedTransferType;
    private final LocalDateTime healthcareExpiration;
    private final LocalDateTime healthAccountExpiration;
    private final LocalDateTime expiration;
    private final String displayName;
    private final boolean complete;

    private SubscriptionApiDTO() {
        this.id = null;
        this.approvedTransferType = null;
        this.healthcareExpiration = null;
        this.healthAccountExpiration = null;
        this.expiration = null;
        this.displayName = null;
        this.complete = false;
    }

    private SubscriptionApiDTO(String id, String approvedTransferType, LocalDateTime healthcareExpiration, LocalDateTime healthAccountExpiration, String displayName, boolean complete) {
        this.id = id;
        this.approvedTransferType = approvedTransferType;
        this.healthcareExpiration =  healthcareExpiration != null ? LocalDateTime.from(healthcareExpiration) : null;
        this.healthAccountExpiration = healthAccountExpiration != null ? LocalDateTime.from(healthAccountExpiration) : null;
        this.displayName = displayName;
        this.complete = complete;
        LocalDateTime expiration = null;
        if (this.healthcareExpiration != null) {
            if (this.healthAccountExpiration != null) {
                expiration = this.healthcareExpiration.isBefore(this.healthAccountExpiration)
                        ? this.healthcareExpiration
                        : this.healthAccountExpiration;
            } else {
                expiration = this.healthcareExpiration;
            }
        } else {
            expiration = this.healthAccountExpiration;
        }
        this.expiration = expiration;
    }

    public static SubscriptionApiDTO create() {
        return new SubscriptionApiDTO();
    }

    public SubscriptionApiDTO withId(String id) {
        return new SubscriptionApiDTO(id, this.approvedTransferType, this.healthcareExpiration, this.healthAccountExpiration, this.displayName, this.complete);
    }

    public SubscriptionApiDTO withApprovedTransferType(String approvedTransferType) {
        return new SubscriptionApiDTO(this.id, approvedTransferType, this.healthcareExpiration, this.healthAccountExpiration, this.displayName, this.complete);
    }

    public SubscriptionApiDTO withHealthcareExpiration(LocalDateTime healthcareExpiration) {
        return new SubscriptionApiDTO(this.id, this.approvedTransferType, healthcareExpiration, this.healthAccountExpiration, this.displayName, this.complete);
    }

    public SubscriptionApiDTO withHealthAccountExpiration(LocalDateTime healthAccountExpiration) {
        return new SubscriptionApiDTO(this.id, this.approvedTransferType, this.healthcareExpiration, healthAccountExpiration, this.displayName, this.complete);
    }

    public SubscriptionApiDTO withDisplayName(String displayName) {
        return new SubscriptionApiDTO(this.id, this.approvedTransferType, this.healthcareExpiration, this.healthAccountExpiration, displayName, this.complete);
    }

    public SubscriptionApiDTO withComplete(boolean complete) {
        return new SubscriptionApiDTO(this.id, this.approvedTransferType, this.healthcareExpiration, this.healthAccountExpiration, this.displayName, complete);
    }

    public String getId() {
        return id;
    }

    public String getApprovedTransferType() {
        return approvedTransferType;
    }

    public LocalDateTime getHealthcareExpiration() {
        return healthcareExpiration;
    }

    public LocalDateTime getHealthAccountExpiration() {
        return healthAccountExpiration;
    }

    public LocalDateTime getExpiration() {
        return expiration;
    }

    public String getDisplayName() {
        return displayName;
    }

    public boolean isComplete() {
        return complete;
    }
}
