package se.skl.prenumerationstjanst.dto.api;

/**
 * @author Martin Samuelsson
 */
public class UserProfileApiDTO {
    private final String crn;
    private final String name;
    private final boolean phkAuthorized; //TODO: Boolean / enum (null = undecided)

    private UserProfileApiDTO() {
        this.crn = null;
        this.name = null;
        this.phkAuthorized = false;
    }

    private UserProfileApiDTO(String crn, String name, boolean phkAuthorized) {
        this.crn = crn;
        this.name = name;
        this.phkAuthorized = phkAuthorized;
    }

    public static UserProfileApiDTO create() {
        return new UserProfileApiDTO();
    }

    public UserProfileApiDTO withCrn(String crn) {
        return new UserProfileApiDTO(crn, this.name, this.phkAuthorized);
    }

    public UserProfileApiDTO withName(String name) {
        return new UserProfileApiDTO(this.crn, name, this.phkAuthorized);
    }

    public UserProfileApiDTO withPhkAuthorized(boolean phkAuthorized) {
        return new UserProfileApiDTO(this.crn, this.name, phkAuthorized);
    }

    public String getCrn() {
        return crn;
    }

    public String getName() {
        return name;
    }

    public boolean isPhkAuthorized() {
        return phkAuthorized;
    }

    @Override
    public String toString() {
        return "ProfileApiDTO{" +
                "crn='" + crn + '\'' +
                ", name='" + name + '\'' +
                ", phkAuthorized=" + phkAuthorized +
                '}';
    }
}
