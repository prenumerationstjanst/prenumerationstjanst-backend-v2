package se.skl.prenumerationstjanst.dto.api;

import java.util.List;

/**
 * @author Martin Samuelsson
 */
public class ValidationErrorApiDTO extends ErrorApiDTO {

    private final List<FieldError> fieldErrors;

    public ValidationErrorApiDTO(String errorCode, String errorMsg, List<FieldError> fieldErrors) {
        super(errorCode, errorMsg);
        this.fieldErrors = fieldErrors;
    }

    public List<FieldError> getFieldErrors() {
        return fieldErrors;
    }

    public static class FieldError {

        private final String field;
        private final String errorCode;
        private final Object rejectedValue;

        public FieldError(String field, String errorCode, Object rejectedValue) {
            this.field = field;
            this.errorCode = errorCode;
            this.rejectedValue = rejectedValue;
        }

        public String getField() {
            return field;
        }

        public String getErrorCode() {
            return errorCode;
        }

        public Object getRejectedValue() {
            return rejectedValue;
        }

    }
}
