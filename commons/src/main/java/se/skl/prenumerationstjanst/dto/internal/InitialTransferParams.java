package se.skl.prenumerationstjanst.dto.internal;

import java.io.Serializable;
import java.util.Set;

/**
 * @author Martin Samuelsson
 */
public class InitialTransferParams implements Serializable {
    private static final long serialVersionUID = 0L;
    private final String subscriptionId;
    private final String transferType;

    public InitialTransferParams(String subscriptionId, String transferType) {
        this.subscriptionId = subscriptionId;
        this.transferType = transferType;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public String getTransferType() {
        return transferType;
    }

    @Override
    public String toString() {
        return "InitialTransferParams{" +
                "subscriptionId='" + subscriptionId + '\'' +
                ", transferType=" + transferType +
                '}';
    }
}
