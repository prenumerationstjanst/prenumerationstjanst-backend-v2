package se.skl.prenumerationstjanst.dto.internal;

import se.skl.prenumerationstjanst.model.TransferJob;
import se.skl.prenumerationstjanst.model.TransferType;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author Martin Samuelsson
 */
public class SingleSourceSystemTransferParams implements Serializable {
    private static final long serialVersionUID = 0L;
    private final String subscriptionId;
    private final TransferType transferType;
    private final LocalDateTime startDateTime;
    private final LocalDateTime endDateTime;
    private final String sourceSystemHsaId;
    private final TransferJob.Origin origin;

    public SingleSourceSystemTransferParams(String subscriptionId, TransferType transferType, LocalDateTime
            startDateTime, LocalDateTime endDateTime, String sourceSystemHsaId, TransferJob.Origin origin) {
        this.subscriptionId = subscriptionId;
        this.transferType = transferType;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.sourceSystemHsaId = sourceSystemHsaId;
        this.origin = origin;

    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public TransferType getTransferType() {
        return transferType;
    }

    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }

    public LocalDateTime getEndDateTime() {
        return endDateTime;
    }

    public String getSourceSystemHsaId() {
        return sourceSystemHsaId;
    }

    public TransferJob.Origin getOrigin() {
        return origin;
    }

    @Override
    public String toString() {
        return "SingleSourceSystemTransferParams{" +
                "subscriptionId='" + subscriptionId + '\'' +
                ", transferType=" + transferType +
                ", startDateTime=" + startDateTime +
                ", endDateTime=" + endDateTime +
                ", sourceSystemHsaId='" + sourceSystemHsaId + '\'' +
                ", origin=" + origin +
                '}';
    }
}
