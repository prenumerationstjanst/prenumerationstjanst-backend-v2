package se.skl.prenumerationstjanst.exception.subscription;

/**
 * @author Martin Samuelsson
 */
public class SubscriptionCreationFailedException extends RuntimeException {

    private static final long serialVersionUID = -5749299431168586017L;

    /**
     *
     * @param message Reason for the creation error. Might be visible to the end user.
     */
    public SubscriptionCreationFailedException(String message) {
        super(message);
    }
}
