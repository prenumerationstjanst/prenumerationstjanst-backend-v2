package se.skl.prenumerationstjanst.exception.subscription;

/**
 * @author Martin Samuelsson
 */
public class SubscriptionDeletionFailedException extends RuntimeException {
    private static final long serialVersionUID = 1294212181144611508L;

    /**
     *
     * @param message Reason for the delete error. Might be visible to the end user.
     */
    public SubscriptionDeletionFailedException(String message) {
        super(message);
    }
}
