package se.skl.prenumerationstjanst.exception.subscription;

/**
 * @author Martin Samuelsson
 */
public class SubscriptionNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -9089456014969053518L;

    public SubscriptionNotFoundException(String subscriptionId) {
        super("subscription with id " + subscriptionId + " not found");
    }

    public SubscriptionNotFoundException(String performingCrn, String representingCrn, String transferType) {
        super("subscription for performingCrn[" + performingCrn + "], " +
                "representingCrn[" + representingCrn + "], " +
                "transferType[" + transferType + "] not found");
    }
}
