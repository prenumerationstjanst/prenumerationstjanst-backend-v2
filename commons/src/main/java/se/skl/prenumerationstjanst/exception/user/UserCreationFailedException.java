package se.skl.prenumerationstjanst.exception.user;

/**
 * @author Martin Samuelsson
 */
public class UserCreationFailedException extends RuntimeException {
    private static final long serialVersionUID = -6986409649124656645L;

    public UserCreationFailedException(String message) {
        super(message);
    }
}
