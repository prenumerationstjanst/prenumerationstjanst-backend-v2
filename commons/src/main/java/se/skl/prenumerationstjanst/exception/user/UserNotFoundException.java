package se.skl.prenumerationstjanst.exception.user;

/**
 * @author Martin Samuelsson
 */
public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException() {
    }

    public UserNotFoundException(String message) {
        super(message);
    }
}
