package se.skl.prenumerationstjanst.httpclient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import se.skl.prenumerationstjanst.PrenumerationstjanstConstants;

import java.io.IOException;

/**
 * ClientHttpRequestInterceptor for RestTemplate to add our correlationId
 * as HTTP header on outgoing requests
 *
 * @author Martin Samuelsson
 */
public class CorrelationIdRequestInterceptor implements ClientHttpRequestInterceptor {

    private static final Logger logger = LoggerFactory.getLogger(CorrelationIdRequestInterceptor.class);

    private final String headerName;

    /**
     *
     * @param headerName the name of the outgoing HTTP header
     */
    public CorrelationIdRequestInterceptor(String headerName) {
        this.headerName = headerName;
    }

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        String correlationId = MDC.get(PrenumerationstjanstConstants.MDC_ACTIVITY_ID_KEY);
        if (correlationId != null) {
            logger.trace("Adding correlationId to outgoing request");
            request.getHeaders().add(headerName, correlationId);
        } else {
            logger.trace("No correlationId found");
        }
        return execution.execute(request, body);

    }
}
