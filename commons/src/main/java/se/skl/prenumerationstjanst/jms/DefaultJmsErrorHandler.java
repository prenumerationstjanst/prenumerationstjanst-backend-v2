package se.skl.prenumerationstjanst.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ErrorHandler;

/**
 * Simple logging error handler
 *
 * @author Martin Samuelsson
 */
public class DefaultJmsErrorHandler implements ErrorHandler {

    private static final Logger logger = LoggerFactory.getLogger(DefaultJmsErrorHandler.class);

    @Override
    public void handleError(Throwable t) {
        if (t.getCause() != null) {
            logger.error("unknown JMS error of type {} with cause {}: {}", t.getClass().getName(),
                    t.getCause().getClass().getName(), t.getMessage(), t);
        } else {
            logger.error("unknown JMS error of type {}: {}", t.getClass().getName(), t.getMessage(), t);
        }
    }
}
