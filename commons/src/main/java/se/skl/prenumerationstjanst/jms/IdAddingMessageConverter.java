package se.skl.prenumerationstjanst.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.SimpleMessageConverter;
import se.skl.prenumerationstjanst.PrenumerationstjanstConstants;
import se.skl.prenumerationstjanst.UuidGenerator;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

/**
 * JMS message converter for propagating eventId:s via JMS headers
 *
 * fromMessage will also add a new activityId
 *
 * example:
 * ===============================
 * jms msg step     #1  #2  #3  #4
 * ===============================
 * processId        p1  p1  p1  p1
 * activityId       a1  a2  a3  a4
 * prevActivityId   --  a1  a2  a3
 * -------------------------------
 *
 * SimpleMessageConverter does the conversion
 *
 *  @author Martin Samuelsson
 */
public class IdAddingMessageConverter extends SimpleMessageConverter {

    private static final Logger logger = LoggerFactory.getLogger(IdAddingMessageConverter.class);

    @Autowired
    private UuidGenerator activityIdGenerator;

    @Override
    public Message toMessage(Object object, Session session) throws JMSException, MessageConversionException {
        logger.trace("toMessage");
        Message message = super.toMessage(object, session);
        if (MDC.get(PrenumerationstjanstConstants.MDC_PROCESS_ID_KEY) != null) {
            message.setStringProperty(PrenumerationstjanstConstants.MDC_PROCESS_ID_KEY, MDC.get(PrenumerationstjanstConstants.MDC_PROCESS_ID_KEY));
            logger.trace("set {} on Message", PrenumerationstjanstConstants.MDC_PROCESS_ID_KEY);
        }
        if (MDC.get(PrenumerationstjanstConstants.MDC_ACTIVITY_ID_KEY) != null) {
            message.setStringProperty(PrenumerationstjanstConstants.MDC_ACTIVITY_ID_KEY, MDC.get(PrenumerationstjanstConstants.MDC_ACTIVITY_ID_KEY));
            logger.trace("set {} on Message", PrenumerationstjanstConstants.MDC_ACTIVITY_ID_KEY);
        }
        return message;
    }

    @Override
    public Object fromMessage(Message message) throws JMSException, MessageConversionException {
        logger.trace("fromMessage");
        String processId = message.getStringProperty(PrenumerationstjanstConstants.MDC_PROCESS_ID_KEY);
        if (processId != null) {
            MDC.put(PrenumerationstjanstConstants.MDC_PROCESS_ID_KEY, processId);
            logger.trace("put {} in MDC", PrenumerationstjanstConstants.MDC_PROCESS_ID_KEY);
        }

        String prevActivityId = message.getStringProperty(PrenumerationstjanstConstants.MDC_ACTIVITY_ID_KEY);
        if (prevActivityId != null) {
            MDC.put(PrenumerationstjanstConstants.MDC_PREVIOUS_ACTIVITY_ID_KEY, prevActivityId);
            logger.trace("put {} in MDC", PrenumerationstjanstConstants.MDC_PREVIOUS_ACTIVITY_ID_KEY);
        }

        MDC.put(PrenumerationstjanstConstants.MDC_ACTIVITY_ID_KEY, activityIdGenerator.generateSanitizedId());
        logger.trace("put {} in MDC", PrenumerationstjanstConstants.MDC_ACTIVITY_ID_KEY);
        return super.fromMessage(message);
    }
}
