package se.skl.prenumerationstjanst.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * App specific deserializer since LocalDateTime does not contain TZ data
 * and jackson-datatype-jsr310 refuses to work with millis since epochs
 *
 * @author Martin Samuelsson
 */
public class PrenumerationstjanstLocalDateDeserializer extends JsonDeserializer<LocalDateTime> {

    private static final Logger logger = LoggerFactory.getLogger(PrenumerationstjanstLocalDateDeserializer.class);

    @Override
    public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        logger.debug("de-serializing LocalDateTime using timezone {}", ZoneId.systemDefault());
        if (jsonParser.getCurrentToken() == JsonToken.VALUE_NUMBER_INT) {
            return LocalDateTime.ofInstant(Instant.ofEpochMilli(jsonParser.getLongValue()), ZoneId.systemDefault());
        } else {
            throw deserializationContext.mappingException("Expected type integer");
        }
    }
}
