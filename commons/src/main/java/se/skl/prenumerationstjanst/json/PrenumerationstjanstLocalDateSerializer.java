package se.skl.prenumerationstjanst.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * App specific serializer since LocalDateTime does not contain TZ data
 * and jackson-datatype-jsr310 refuses to work with millis since epochs
 *
 * @author Martin Samuelsson
 */
public class PrenumerationstjanstLocalDateSerializer extends JsonSerializer<LocalDateTime> {
    private static final Logger logger = LoggerFactory.getLogger(PrenumerationstjanstLocalDateSerializer.class);
    @Override
    public void serialize(LocalDateTime localDateTime, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        logger.debug("serializing LocalDateTime using timezone {}", ZoneId.systemDefault());
        jsonGenerator.writeNumber(localDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
    }
}
