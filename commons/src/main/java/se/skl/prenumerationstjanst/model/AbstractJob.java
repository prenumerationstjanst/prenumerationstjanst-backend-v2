package se.skl.prenumerationstjanst.model;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Abstract superclass for 'Job' entities
 * Generates UUID id on first save and handles time stamping
 *
 * @author Martin Samuelsson
 */
@MappedSuperclass
public abstract class AbstractJob extends AbstractAudited {

    @Id
    private String id;

    @PrePersist
    public void prePersist() {
        this.id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }
}
