package se.skl.prenumerationstjanst.model;

import javax.persistence.*;

/**
 * @author Martin Samuelsson
 */
@MappedSuperclass
public class AbstractJobState extends AbstractAudited {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private JobStatus status;

    protected AbstractJobState() {
    }

    public AbstractJobState(JobStatus status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public JobStatus getStatus() {
        return status;
    }

    public void setStatus(JobStatus status) {
        this.status = status;
    }
}
