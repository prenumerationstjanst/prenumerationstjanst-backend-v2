package se.skl.prenumerationstjanst.model;

import javax.persistence.Embeddable;
import java.time.LocalDateTime;

/**
 * @author Martin Samuelsson
 */
@Embeddable
public class ApigwConsent {
    private String tokenValue;
    private LocalDateTime expirationDateTime;
    private String scope;

    public String getTokenValue() {
        return tokenValue;
    }

    public void setTokenValue(String tokenValue) {
        this.tokenValue = tokenValue;
    }

    public LocalDateTime getExpirationDateTime() {
        return expirationDateTime;
    }

    public void setExpirationDateTime(LocalDateTime expirationDateTime) {
        this.expirationDateTime = expirationDateTime;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }
}
