package se.skl.prenumerationstjanst.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Symbolizes a job to fetch data from the Healthcare backend
 *
 * @author Martin Samuelsson
 */
@Entity
public class FetchJob extends AbstractJob {

    @OneToOne
    private TransferJob transferJob;

    private String apigwToken;

    @Column(nullable = false, updatable = true)
    @Enumerated(EnumType.STRING)
    private JobStatus status;

    //need the eager fetch for cascading to work properly
    @OneToMany(mappedBy = "fetchJob", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<FetchJobState> jobStates;

    private LocalDateTime fetchFinishTime;

    private FetchJob() {
        this(null, null);
    }

    public FetchJob(TransferJob transferJob, String apigwToken) {
        this.status = JobStatus.CREATED;
        this.transferJob = transferJob;
        this.apigwToken = apigwToken;
        this.jobStates = new ArrayList<>();
        this.jobStates.add(new FetchJobState(this, JobStatus.CREATED));
    }

    public TransferJob getTransferJob() {
        return transferJob;
    }

    public void setTransferJob(TransferJob transferJob) {
        this.transferJob = transferJob;
    }

    public String getApigwToken() {
        return apigwToken;
    }

    public void setApigwToken(String apigwToken) {
        this.apigwToken = apigwToken;
    }

    public JobStatus getStatus() {
        return status;
    }

    public void setStatus(JobStatus status) {
        this.status = status;
    }

    public LocalDateTime getFetchFinishTime() {
        return fetchFinishTime;
    }

    public void setFetchFinishTime(LocalDateTime fetchFinishTime) {
        this.fetchFinishTime = fetchFinishTime;
    }

    public List<FetchJobState> getJobStates() {
        return jobStates;
    }

    public void setJobStates(List<FetchJobState> jobStates) {
        this.jobStates = jobStates;
    }
}
