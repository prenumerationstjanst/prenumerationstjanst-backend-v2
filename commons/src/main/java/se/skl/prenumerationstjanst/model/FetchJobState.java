package se.skl.prenumerationstjanst.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * @author Martin Samuelsson
 */
@Entity
public class FetchJobState extends AbstractJobState {

    @ManyToOne
    @JoinColumn(name = "fetchJobId")
    private FetchJob fetchJob;

    private FetchJobState() {
    }

    public FetchJobState(FetchJob fetchJob, JobStatus status) {
        super(status);
        this.fetchJob = fetchJob;
    }

    public FetchJob getFetchJob() {
        return fetchJob;
    }

    public void setFetchJob(FetchJob fetchJob) {
        this.fetchJob = fetchJob;
    }
}
