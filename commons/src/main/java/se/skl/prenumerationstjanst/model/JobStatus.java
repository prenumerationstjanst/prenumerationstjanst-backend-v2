package se.skl.prenumerationstjanst.model;

/**
 * Represents job statuses for Fetch/Push
 *
 * CREATED:
 * The job is newly created and has not been processed yet
 *
 * RUNNING:
 * The job is currently running
 *
 * FINISHED:
 * The job has finished successfully
 *
 * FAILED:
 * The job is in failed state but is placed in re-delivery
 *
 * FAILED_ALL_RETRIES:
 * The job has finished unsuccessfully due to none of the process attempts was successful
 *
 * FAILED_UNAUTHORIZED
 * The job has finished unsuccessfully due to authorization issues
 *
 * DUPLICATE:
 * The job was determined to be a duplicate of another job and therefore not performed
 * (currently only applicable for PushJob)
 *
 *
 *
 * @author Martin Samuelsson
 */
public enum JobStatus {
    CREATED,
    RUNNING,
    FINISHED,
    FAILED,
    FAILED_ALL_RETRIES,
    FAILED_UNAUTHORIZED,
    DUPLICATE
}
