package se.skl.prenumerationstjanst.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Simple String key-value entity
 *
 * @author Martin Samuelsson
 */
@Entity
@IdClass(KeyValue.KeyValuePK.class)
public class KeyValue extends AbstractAudited {

    @Id
    @Column(name = "_key") //key is reserved in mysql
    private String key;

    @Id
    private String sessionId;

    @Column(length = 1024)
    private String value;

    @Version
    private int version;

    private KeyValue() {
    }

    public KeyValue(String key, String sessionId, String value) {
        this.key = key;
        this.sessionId = sessionId;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getSessionId() {
        return sessionId;
    }

    public String getValue() {
        return value;
    }

    public int getVersion() {
        return version;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static class KeyValuePK implements Serializable {

        protected String key;

        protected String sessionId;

        private KeyValuePK() {
        }

        public KeyValuePK(String key, String sessionId) {
            this.key = key;
            this.sessionId = sessionId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof KeyValuePK)) return false;

            KeyValuePK that = (KeyValuePK) o;

            if (!key.equals(that.key)) return false;
            return sessionId.equals(that.sessionId);

        }

        @Override
        public int hashCode() {
            int result = key.hashCode();
            result = 31 * result + sessionId.hashCode();
            return result;
        }
    }

}
