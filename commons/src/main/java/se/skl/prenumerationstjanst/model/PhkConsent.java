package se.skl.prenumerationstjanst.model;

import javax.persistence.Embeddable;

/**
 * @author Martin Samuelsson
 */
@Embeddable
public class PhkConsent {
    private String personId;
    private String recordId;

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }
}
