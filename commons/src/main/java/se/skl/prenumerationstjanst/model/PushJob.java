package se.skl.prenumerationstjanst.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Symbolizes a job to push data to the 'Personal Health Account'
 *
 * @author Martin Samuelsson
 */
@Entity
public class PushJob extends AbstractJob {

    @ManyToOne
    private TransferJob transferJob;

    private String phkPersonId;

    private String phkRecordId;

    @Column(nullable = false, updatable = true)
    @Enumerated(EnumType.STRING)
    private JobStatus status;

    @Transient
    private String value;

    @Transient
    private List<String> relatedValues;

    private String sourceId;
    private int sourceVersion;

    //need the eager fetch for cascading to work properly
    @OneToMany(mappedBy = "pushJob", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<PushJobState> jobStates;

    private LocalDateTime pushFinishTime;

    /**
     * free text to indicate the type of data that was (or was not) pushed
     * to the recipient system
     *
     * should only be available if status = FINISHED or DUPLICATE
     */
    private String typeOfData;

    /**
     * id of a 'parent' PushJob (if applicable) to indicate a relation
     *
     * should only be available if status = FINISHED
     */
    private String relatedTo;

    private PushJob() {
        this(null, null, null);
    }

    public PushJob(TransferJob transferJob, String phkPersonId, String phkRecordId) {
        this.status = JobStatus.CREATED;
        this.transferJob = transferJob;
        this.phkPersonId = phkPersonId;
        this.phkRecordId = phkRecordId;
        this.jobStates = new ArrayList<>();
        this.jobStates.add(new PushJobState(this, JobStatus.CREATED));
    }

    public TransferJob getTransferJob() {
        return transferJob;
    }

    public void setTransferJob(TransferJob transferJob) {
        this.transferJob = transferJob;
    }

    public String getPhkPersonId() {
        return phkPersonId;
    }

    public void setPhkPersonId(String phkPersonId) {
        this.phkPersonId = phkPersonId;
    }

    public String getPhkRecordId() {
        return phkRecordId;
    }

    public void setPhkRecordId(String phkRecordId) {
        this.phkRecordId = phkRecordId;
    }

    public JobStatus getStatus() {
        return status;
    }

    public void setStatus(JobStatus status) {
        this.status = status;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<String> getRelatedValues() {
        return relatedValues;
    }

    public void setRelatedValues(List<String> relatedValues) {
        this.relatedValues = relatedValues;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public int getSourceVersion() {
        return sourceVersion;
    }

    public void setSourceVersion(int sourceVersion) {
        this.sourceVersion = sourceVersion;
    }

    public LocalDateTime getPushFinishTime() {
        return pushFinishTime;
    }

    public void setPushFinishTime(LocalDateTime pushFinishTime) {
        this.pushFinishTime = pushFinishTime;
    }

    public String getRelatedTo() {
        return relatedTo;
    }

    public void setRelatedTo(String relatedTo) {
        this.relatedTo = relatedTo;
    }

    public String getTypeOfData() {
        return typeOfData;
    }

    public void setTypeOfData(String typeOfData) {
        this.typeOfData = typeOfData;
    }

    public List<PushJobState> getJobStates() {
        return jobStates;
    }

    public void setJobStates(List<PushJobState> jobStates) {
        this.jobStates = jobStates;
    }
}
