package se.skl.prenumerationstjanst.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * @author Martin Samuelsson
 */
@Entity
public class PushJobState extends AbstractJobState {

    @ManyToOne
    @JoinColumn(name = "pushJobId")
    private PushJob pushJob;

    private PushJobState() {
    }

    public PushJobState(PushJob pushJob, JobStatus status) {
        super(status);
        this.pushJob = pushJob;
    }

    public PushJob getPushJob() {
        return pushJob;
    }

    public void setPushJob(PushJob pushJob) {
        this.pushJob = pushJob;
    }
}
