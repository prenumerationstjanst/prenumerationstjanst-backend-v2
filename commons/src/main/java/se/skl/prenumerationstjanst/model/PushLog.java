package se.skl.prenumerationstjanst.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * TODO: relation to Subscription?
 * @author Martin Samuelsson
 */
@Entity
public class PushLog extends AbstractAudited {

    @Id
    private String id;

    @Column(nullable = false, updatable = false)
    private String sourceId;

    @Column(nullable = false, updatable = false)
    private LocalDateTime sourceFetchTime;

    @Column(nullable = false, updatable = false)
    private int sourceVersion;

    @Column(nullable = false, updatable = false)
    private String hash;

    @ManyToOne(optional = false)
    private Subscription subscription;

    public PushLog() {
    }

    public PushLog(Subscription subscription, String sourceId, LocalDateTime sourceFetchTime, int sourceVersion,
                   String hash) {
        this.subscription = subscription;
        this.sourceId = sourceId;
        this.sourceFetchTime = sourceFetchTime;
        this.sourceVersion = sourceVersion;
        this.hash = hash;
    }

    @PrePersist
    public void prePersist() {
        this.id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public LocalDateTime getSourceFetchTime() {
        return sourceFetchTime;
    }

    public void setSourceFetchTime(LocalDateTime sourceFetchTime) {
        this.sourceFetchTime = sourceFetchTime;
    }

    public int getSourceVersion() {
        return sourceVersion;
    }

    public void setSourceVersion(int sourceVersion) {
        this.sourceVersion = sourceVersion;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
}
