package se.skl.prenumerationstjanst.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * A Subscription specifies a number of health care info types
 * approved for transfer by the user (context), and keeps track of transfer jobs
 *
 * @author Martin Samuelsson
 */
@Entity
public class Subscription extends AbstractAudited {

    @Id
    private String id;

    @ManyToOne
    private UserContext userContext;

    @Column(nullable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private TransferType approvedTransferType;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "tokenValue", column = @Column(name = "apigwConsentTokenValue")),
            @AttributeOverride(name = "expirationDateTime", column = @Column(name = "apigwConsentExpirationDateTime")),
            @AttributeOverride(name = "scope", column = @Column(name = "apigwConsentScope"))
    })
    private ApigwConsent apigwConsent;

    private boolean scheduledForRemoval = false;

    @OneToMany(mappedBy = "subscription", cascade = CascadeType.REMOVE)
    private List<PushLog> pushLogs;

    @PrePersist
    public void prePersist() {
        id = UUID.randomUUID().toString();
    }

    public Subscription() {
    }

    public Subscription(UserContext userContext, TransferType approvedTransferType) {
        this.userContext = userContext;
        this.approvedTransferType = approvedTransferType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserContext getUserContext() {
        return userContext;
    }

    public void setUserContext(UserContext userContext) {
        this.userContext = userContext;
    }

    public TransferType getApprovedTransferType() {
        return approvedTransferType;
    }

    public void setApprovedTransferType(TransferType approvedTransferType) {
        this.approvedTransferType = approvedTransferType;
    }

    public ApigwConsent getApigwConsent() {
        return apigwConsent;
    }

    public void setApigwConsent(ApigwConsent apigwConsent) {
        this.apigwConsent = apigwConsent;
    }

    public boolean isScheduledForRemoval() {
        return scheduledForRemoval;
    }

    public void setScheduledForRemoval(boolean scheduledForRemoval) {
        this.scheduledForRemoval = scheduledForRemoval;
    }

    public List<PushLog> getPushLogs() {
        if (pushLogs == null) {
            pushLogs = new ArrayList<>();
        }
        return pushLogs;
    }

    public void setPushLogs(List<PushLog> pushLogs) {
        this.pushLogs = pushLogs;
    }
}
