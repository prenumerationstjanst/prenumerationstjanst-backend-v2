package se.skl.prenumerationstjanst.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * A TransferJob specifies what should be fetched from the health care system
 * and pushed to the 'Personal Health Account'
 *
 * @author Martin Samuelsson
 */
@Entity
public class TransferJob extends AbstractJob {

    private String userCrn; //TODO: not null

    private String representingCrn; //TODO: not null

    private String subscriptionId;

    @Column(nullable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private TransferType transferType;

    @Column(nullable = false, updatable = false)
    private LocalDateTime startDateTime;

    @Column(nullable = false, updatable = false)
    private LocalDateTime endDateTime;

    private String sourceSystemHsaId;

    @OneToOne(mappedBy = "transferJob", cascade = CascadeType.REMOVE)
    private FetchJob fetchJob;

    @OneToMany(mappedBy = "transferJob", cascade = CascadeType.REMOVE)
    private List<TransformationJob> transformationJobs;

    @OneToMany(mappedBy = "transferJob", cascade = CascadeType.REMOVE)
    private List<PushJob> pushJobs;

    @Column(nullable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private Origin origin;

    private TransferJob() {
    }

    public TransferJob(String userCrn, String representingCrn, String subscriptionId, TransferType transferType,
                       LocalDateTime startDateTime, LocalDateTime endDateTime, Origin origin) {
        this(userCrn, representingCrn, subscriptionId, transferType, startDateTime, endDateTime, null, origin);
    }

    public TransferJob(String userCrn, String representingCrn, String subscriptionId, TransferType transferType,
                       LocalDateTime startDateTime, LocalDateTime endDateTime,
                       String sourceSystemHsaId, Origin origin) {
        this.userCrn = userCrn;
        this.representingCrn = representingCrn;
        this.subscriptionId = subscriptionId;
        this.transferType = transferType;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.sourceSystemHsaId = sourceSystemHsaId;
        this.origin = origin;
    }

    public String getUserCrn() {
        return userCrn;
    }

    public void setUserCrn(String userCrn) {
        this.userCrn = userCrn;
    }

    public String getRepresentingCrn() {
        return representingCrn;
    }

    public void setRepresentingCrn(String representingCrn) {
        this.representingCrn = representingCrn;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public TransferType getTransferType() {
        return transferType;
    }

    public void setTransferType(TransferType transferType) {
        this.transferType = transferType;
    }

    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(LocalDateTime startDateTime) {
        this.startDateTime = startDateTime;
    }

    public LocalDateTime getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(LocalDateTime endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getSourceSystemHsaId() {
        return sourceSystemHsaId;
    }

    public void setSourceSystemHsaId(String sourceSystemHsaId) {
        this.sourceSystemHsaId = sourceSystemHsaId;
    }

    public FetchJob getFetchJob() {
        return fetchJob;
    }

    public void setFetchJob(FetchJob fetchJob) {
        this.fetchJob = fetchJob;
    }

    public List<TransformationJob> getTransformationJobs() {
        if (this.transformationJobs == null) {
            this.transformationJobs = new ArrayList<>();
        }
        return transformationJobs;
    }

    public void setTransformationJobs(List<TransformationJob> transformationJobs) {
        this.transformationJobs = transformationJobs;
    }

    public List<PushJob> getPushJobs() {
        if (this.pushJobs == null) {
            this.pushJobs = new ArrayList<>();
        }
        return pushJobs;
    }

    public void setPushJobs(List<PushJob> pushJobs) {
        this.pushJobs = pushJobs;
    }

    public Origin getOrigin() {
        return origin;
    }

    public void setOrigin(Origin origin) {
        this.origin = origin;
    }

    /**
     * Origin describes the origin of this particular TransferJob
     *
     * INITIAL:
     * Part of the initial set of TransferJobs for a new Subscription
     *
     * EI_NOTIFICATION:
     * Created based on a received Engagement Index notification
     *
     * FAILED_SOURCE_SYSTEM:
     * Created based on another TransferJob where the FetchJob obtained
     * information about a non-responding source system
     *
     */
    public enum Origin {
        INITIAL,
        EI_NOTIFICATION,
        FAILED_SOURCE_SYSTEM
    }

}
