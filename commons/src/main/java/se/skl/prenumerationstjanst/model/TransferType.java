package se.skl.prenumerationstjanst.model;

/**
 * Enum representing info types currently handled by the application
 *
 * @author Martin Samuelsson
 */
public enum TransferType {
    MATERNITY_MEDICAL_HISTORY("MATERNITY_MEDICAL_HISTORY_READ"),
    VACCINATION_HISTORY("VACCINATION_HISTORY_READ"),
    LABORATORY_ORDER_OUTCOME("LABORATORY_ORDER_OUTCOME_READ"),
    CARE_DOCUMENTATION("CARE_DOCUMENTATION_READ"),
    CARE_CONTACTS("CARE_CONTACTS_READ"),
    DIAGNOSIS("DIAGNOSIS_READ");

    private final String scope;

    TransferType(final String scope) {
        this.scope = scope;
    }

    public String getScope() {
        return scope;
    }

    public static TransferType forScope(String scope) { //FIXME: scope is not unique atm
        for (TransferType transferType : TransferType.values()) {
            if (transferType.getScope().equals(scope)) {
                return transferType;
            }
        }
        return null;
    }

}
