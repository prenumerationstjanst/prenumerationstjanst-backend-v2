package se.skl.prenumerationstjanst.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Martin Samuelsson
 */
@Entity
public class TransformationJob extends AbstractJob {

    @ManyToOne
    private TransferJob transferJob;

    @Transient
    private String source;

    private LocalDateTime sourceFetchTime;

    private int sourceVersion;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private JobStatus status;

    //need the eager fetch for cascading to work properly
    @OneToMany(mappedBy = "transformationJob", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<TransformationJobState> jobStates;

    private TransformationJob() {
    }

    public TransformationJob(TransferJob transferJob, String source, LocalDateTime sourceFetchTime, int sourceVersion) {
        this.status = JobStatus.CREATED;
        this.transferJob = transferJob;
        this.source = source;
        this.sourceFetchTime = sourceFetchTime;
        this.sourceVersion = sourceVersion;
        this.jobStates = new ArrayList<>();
        this.jobStates.add(new TransformationJobState(this, JobStatus.CREATED));
    }

    public TransferJob getTransferJob() {
        return transferJob;
    }

    public void setTransferJob(TransferJob transferJob) {
        this.transferJob = transferJob;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public LocalDateTime getSourceFetchTime() {
        return sourceFetchTime;
    }

    public void setSourceFetchTime(LocalDateTime sourceFetchTime) {
        this.sourceFetchTime = sourceFetchTime;
    }

    public int getSourceVersion() {
        return sourceVersion;
    }

    public void setSourceVersion(int sourceVersion) {
        this.sourceVersion = sourceVersion;
    }

    public JobStatus getStatus() {
        return status;
    }

    public void setStatus(JobStatus status) {
        this.status = status;
    }

    public List<TransformationJobState> getJobStates() {
        if (this.jobStates == null) {
            this.jobStates = new ArrayList<>();
        }
        return jobStates;
    }

    public void setJobStates(List<TransformationJobState> jobStates) {
        this.jobStates = jobStates;
    }
}
