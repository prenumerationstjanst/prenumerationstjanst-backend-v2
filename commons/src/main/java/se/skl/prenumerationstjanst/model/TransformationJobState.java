package se.skl.prenumerationstjanst.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * @author Martin Samuelsson
 */
@Entity
public class TransformationJobState extends AbstractJobState {

    @ManyToOne
    @JoinColumn(name = "transformationJobId")
    private TransformationJob transformationJob;

    private TransformationJobState() {
    }

    public TransformationJobState(TransformationJob transformationJob, JobStatus status) {
        super(status);
        this.transformationJob = transformationJob;
    }

    public TransformationJob getTransformationJob() {
        return transformationJob;
    }

    public void setTransformationJob(TransformationJob transformationJob) {
        this.transformationJob = transformationJob;
    }
}
