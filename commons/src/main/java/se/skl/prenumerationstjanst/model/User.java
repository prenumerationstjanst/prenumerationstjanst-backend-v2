package se.skl.prenumerationstjanst.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Martin Samuelsson
 */
@Entity
public class User extends AbstractAudited {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    private String crn;

    @Column(unique = true, nullable = false)
    private String phkPersonId;

    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE)
    private List<UserContext> userContexts;

    private User() {
    }

    public User(String crn, String phkPersonId) {
        this.crn = crn;
        this.phkPersonId = phkPersonId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCrn() {
        return crn;
    }

    public void setCrn(String crn) {
        this.crn = crn;
    }

    public String getPhkPersonId() {
        return phkPersonId;
    }

    public void setPhkPersonId(String phkPersonId) {
        this.phkPersonId = phkPersonId;
    }

    public List<UserContext> getUserContexts() {
        return userContexts != null ? userContexts : new ArrayList<>();
    }

    public void setUserContexts(List<UserContext> userContexts) {
        this.userContexts = userContexts;
    }
}
