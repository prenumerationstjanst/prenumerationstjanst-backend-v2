package se.skl.prenumerationstjanst.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Martin Samuelsson
 */
@Entity
public class UserContext extends AbstractAudited {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String representingCrn;

    @OneToMany(mappedBy = "userContext", cascade = CascadeType.REMOVE)
    private List<Subscription> subscriptions;

    @ManyToOne(optional = false)
    private User user;

    @Column(unique = true, nullable = false)
    private String phkRecordId;

    private UserContext() {
    }

    public UserContext(User user, String representingCrn, String phkRecordId) {
        this.user = user;
        this.representingCrn = representingCrn;
        this.phkRecordId = phkRecordId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRepresentingCrn() {
        return representingCrn;
    }

    public void setRepresentingCrn(String representingCrn) {
        this.representingCrn = representingCrn;
    }

    public List<Subscription> getSubscriptions() {
        return subscriptions != null ? subscriptions : new ArrayList<>();
    }

    public void setSubscriptions(List<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPhkRecordId() {
        return phkRecordId;
    }

    public void setPhkRecordId(String phkRecordId) {
        this.phkRecordId = phkRecordId;
    }
}
