package se.skl.prenumerationstjanst.model.monitoring;

import se.skl.prenumerationstjanst.model.AbstractAudited;

import javax.persistence.*;

/**
 * @author Martin Samuelsson
 */
@Entity
public class MonitoredUserEvent extends AbstractAudited {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(updatable = false)
    private String userCrn;

    @Column(updatable = false)
    private String userIpAddress;

    @Column(updatable = false)
    private String correlationId;

    @Column(updatable = false)
    @Enumerated(EnumType.STRING)
    private MonitoredUserEventType eventType;

    @Column(updatable = false)
    @Lob
    private String eventDetails;

    private MonitoredUserEvent() {
    }

    public MonitoredUserEvent(String userCrn, String userIpAddress, String correlationId, MonitoredUserEventType
            eventType, String eventDetails) {
        this.userCrn = userCrn;
        this.userIpAddress = userIpAddress;
        this.correlationId = correlationId;
        this.eventType = eventType;
        this.eventDetails = eventDetails;
    }

    public Long getId() {
        return id;
    }

    public String getUserCrn() {
        return userCrn;
    }

    public String getUserIpAddress() {
        return userIpAddress;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public MonitoredUserEventType getEventType() {
        return eventType;
    }

    public String getEventDetails() {
        return eventDetails;
    }
}
