package se.skl.prenumerationstjanst.model.monitoring;

/**
 * Types of monitored user events
 *
 * @author Martin Samuelsson
 */
public enum MonitoredUserEventType {
    USER_CREATES_ACCOUNT,
    USER_ACCEPTS_TERMS_OF_SERVICE,
    USER_REMOVES_ACCOUNT,
    USER_CREATES_SUBSCRIPTION,
    USER_UPDATES_SUBSCRIPTION,
    USER_REMOVES_SUBSCRIPTION,
    SYSTEM_REMOVES_INCOMPLETE_SUBSCRIPTION
}
