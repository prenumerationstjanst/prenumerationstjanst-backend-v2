package se.skl.prenumerationstjanst.repository;

import org.springframework.data.repository.CrudRepository;
import se.skl.prenumerationstjanst.model.FetchJob;

/**
 * @author Martin Samuelsson
 */
public interface FetchJobRepository extends CrudRepository<FetchJob, String> {
}
