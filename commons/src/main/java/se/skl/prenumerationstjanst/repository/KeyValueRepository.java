package se.skl.prenumerationstjanst.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import se.skl.prenumerationstjanst.model.KeyValue;

import java.time.LocalDateTime;

/**
 * @author Martin Samuelsson
 */
public interface KeyValueRepository extends CrudRepository<KeyValue, KeyValue.KeyValuePK> {

    @Query("SELECT kv FROM KeyValue kv WHERE kv.key = :key AND kv.sessionId = :sessionId")
    KeyValue findByKey(@Param("key") String key, @Param("sessionId") String sessionId);

    @Modifying
    @Transactional
    @Query("DELETE from KeyValue kv WHERE kv.lastModified < :lastModifiedBefore")
    void deleteOldCachedValues(@Param("lastModifiedBefore") LocalDateTime lastModifiedBefore);

}
