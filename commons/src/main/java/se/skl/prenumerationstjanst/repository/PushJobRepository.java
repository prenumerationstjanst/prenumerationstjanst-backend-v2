package se.skl.prenumerationstjanst.repository;

import org.springframework.data.repository.CrudRepository;
import se.skl.prenumerationstjanst.model.PushJob;

/**
 * @author Martin Samuelsson
 */
public interface PushJobRepository extends CrudRepository<PushJob, String> {
}
