package se.skl.prenumerationstjanst.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import se.skl.prenumerationstjanst.model.PushLog;

import java.time.LocalDateTime;

/**
 * @author Martin Samuelsson
 */
public interface PushLogRepository extends CrudRepository<PushLog, String> {

    @Query("SELECT CASE WHEN COUNT(pl) > 0 THEN true ELSE false END FROM PushLog pl WHERE " +
            "pl.subscription.id = :subid AND pl.sourceId = :souid AND pl.sourceVersion = :souver AND " +
            "((pl.sourceFetchTime > :sft) OR (pl.sourceFetchTime <= :sft AND pl.hash = :hash))")
    boolean sameOrNewerPushed(@Param("subid") String subscriptionId, @Param("sft") LocalDateTime sourceFetchTime,
                              @Param("souid") String sourceId, @Param("souver") int sourceVersion,
                              @Param("hash") String hash);
}
