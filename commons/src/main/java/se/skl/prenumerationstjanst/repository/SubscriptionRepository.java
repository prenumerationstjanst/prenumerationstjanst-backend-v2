package se.skl.prenumerationstjanst.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import se.skl.prenumerationstjanst.model.Subscription;
import se.skl.prenumerationstjanst.model.TransferType;
import se.skl.prenumerationstjanst.model.User;
import se.skl.prenumerationstjanst.model.UserContext;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Martin Samuelsson
 */
public interface SubscriptionRepository extends CrudRepository<Subscription, String> {
    List<Subscription> findByUserContextAndScheduledForRemovalFalse(UserContext userContext);

    @Query("SELECT s FROM Subscription s " +
            "   WHERE s.approvedTransferType = :transferType " +
            "       AND s.scheduledForRemoval = false " +
            "       AND s.userContext.user.crn = :performingCrn " +
            "       AND s.userContext.representingCrn = :representingCrn")
    Subscription findForUserContextByTransferType(@Param("performingCrn") String performingCrn,
                           @Param("representingCrn") String representingCrn,
                           @Param("transferType") TransferType transferType);

    Subscription findByApigwConsentTokenValueAndScheduledForRemovalFalse(String tokenValue);

    @Query("SELECT s from Subscription s WHERE s.created < :createdBefore AND s.apigwConsent IS null")
    List<Subscription> findByCreatedBeforeAndApigwConsentIsNull(@Param("createdBefore") LocalDateTime createdBefore);

    @Modifying
    @Transactional
    void deleteByScheduledForRemovalTrueAndLastModifiedBefore(LocalDateTime lastModifiedBefore);

    Long countByScheduledForRemovalTrueAndLastModifiedBefore(LocalDateTime lastModifiedBefore);
}
