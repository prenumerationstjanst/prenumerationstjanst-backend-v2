package se.skl.prenumerationstjanst.repository;

import org.springframework.data.repository.CrudRepository;
import se.skl.prenumerationstjanst.model.TransferJob;

/**
 * @author Martin Samuelsson
 */
public interface TransferJobRepository extends CrudRepository<TransferJob, String> {
}
