package se.skl.prenumerationstjanst.repository;

import org.springframework.data.repository.CrudRepository;
import se.skl.prenumerationstjanst.model.TransformationJob;

/**
 * @author Martin Samuelsson
 */
public interface TransformationJobRepository extends CrudRepository<TransformationJob, String> {
}
