package se.skl.prenumerationstjanst.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import se.skl.prenumerationstjanst.model.UserContext;

/**
 * @author Martin Samuelsson
 */
public interface UserContextRepository extends CrudRepository<UserContext, Long> {
    @Query("SELECT uc FROM UserContext uc WHERE uc.user.crn = :userCrn AND uc.representingCrn = :representingCrn")
    UserContext findByUserCrnAndRepresentingCrn(@Param("userCrn") String userCrn,
                                                @Param("representingCrn") String representingCrn);

    @Query("SELECT CASE WHEN COUNT(uc) > 0 THEN true ELSE false END FROM UserContext uc " +
            "WHERE uc.user.crn = :performingCrn AND uc.representingCrn = :representingCrn")
    boolean exists(@Param("performingCrn") String performingCrn, @Param("representingCrn") String representingCrn);
}
