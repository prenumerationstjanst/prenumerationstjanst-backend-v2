package se.skl.prenumerationstjanst.repository;

import org.springframework.data.repository.CrudRepository;
import se.skl.prenumerationstjanst.model.User;

/**
 * @author Martin Samuelsson
 */
public interface UserRepository extends CrudRepository<User, Long> {
    User findByCrn(String crn);
}
