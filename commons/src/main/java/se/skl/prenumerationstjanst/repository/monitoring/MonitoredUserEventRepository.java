package se.skl.prenumerationstjanst.repository.monitoring;

import org.springframework.data.repository.PagingAndSortingRepository;
import se.skl.prenumerationstjanst.model.monitoring.MonitoredUserEvent;

/**
 * @author Martin Samuelsson
 */
public interface MonitoredUserEventRepository extends PagingAndSortingRepository<MonitoredUserEvent, Long>{
}
