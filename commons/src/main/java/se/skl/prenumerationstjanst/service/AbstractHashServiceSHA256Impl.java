package se.skl.prenumerationstjanst.service;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

/**
 * Abstract HashService implementation that uses SHA-256 algorithm
 *
 * Subclasses should implement getHashableString(String input)
 *
 * @author Martin Samuelsson
 */
public abstract class AbstractHashServiceSHA256Impl implements HashService {

    private static final Logger logger = LoggerFactory.getLogger(AbstractHashServiceSHA256Impl.class);

    /**
     * @param input input String
     * @return hexadecimal String representation of a SHA-256 hash of the input String
     */
    @Override
    public String calculateHash(String input) {
        String hashable = getHashableString(input);
        if (hashable == null || hashable.trim().isEmpty()) {
            logger.error("Could not get hashable String from input, randomizing");
            hashable = UUID.randomUUID().toString();
        }
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(hashable.getBytes());
            return Hex.encodeHexString(md.digest());
        } catch (NoSuchAlgorithmException e) { //TODO: exception
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Sub-class method that determines the String that should be hashed
     *
     * Must be indempotent.
     *
     * @param input the original input
     * @return a String representation of the original input suitable for hashing
     */
    protected abstract String getHashableString(String input);
}
