package se.skl.prenumerationstjanst.service;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonSetter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import se.skl.prenumerationstjanst.dto.ApigwConsentDTO;
import se.skl.prenumerationstjanst.model.TransferType;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Martin Samuelsson
 */
public class ApigwConsentService {

    private static final Logger logger = LoggerFactory.getLogger(ApigwConsentService.class);

    @Value("${pt.apigw.auth_server.base_uri}")
    private String baseUri;

    @Value("${pt.apigw.auth_server.trusted_create_token_path}")
    private String trustedCreateTokenPath;

    @Value("${pt.apigw.auth_server.trusted_revoke_token_path}")
    private String trustedRevokeTokenPath;

    @Autowired
    @Qualifier(value = "authServerRestTemplate")
    protected RestTemplate authServerRestTemplate;

    /**
     *
     * @param crn The crn of the current user
     * @param childCrn If applicable, the crn of the users child, otherwise null
     * @param transferType The transfer types the consent should grant access to
     *
     * @return a DTO representing the consent created in APIGW. Note that the scope attribute of
     * the returned DTO might not contain all of the requested transfer types. This happens when we as an APIGW client
     * does not have the correct permissions set up in APIGW. Use the parseApigwScope-method.
     */
    public ApigwConsentDTO getConsent(String crn, String childCrn, TransferType transferType) {
        logger.debug("getConsent(crn={}, childCrn={}, transferTypes={})", crn, childCrn, transferType);
        URI requestURI;
        try {
            requestURI = new URI(baseUri + trustedCreateTokenPath);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e); //TODO: exception
        }
        //APIGW requires application/x-www-form-urlencoded
        //FormHttpMessageConverter will be used by default since we provide a MultiValueMap
        try {
            ApigwAccessToken apigwAccessToken = authServerRestTemplate.postForObject(
                    requestURI, createConsentRequest(crn, childCrn, transferType), ApigwAccessToken.class);
            return toApigwConsentDTO(apigwAccessToken);
        } catch (RuntimeException e) {
            logger.error("Encountered exception while calling APIGW", e);
            throw e; //TODO: exception
        }
    }

    /**
     * Deletes a consent (APIGW Authorization grant) in APIGW
     * @param tokenValue value representing the APIGW token
     */
    //TODO: make async via activemq
    public void deleteConsent(String tokenValue) {
        logger.debug("deleteConsent(tokenValue={})", tokenValue);
        URI requestURI;
        try {
            requestURI = new URI(baseUri + trustedRevokeTokenPath);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        try {
            authServerRestTemplate.postForObject(
                    requestURI, deleteConsentRequest(tokenValue), Object.class);
        } catch (HttpClientErrorException e) {
            logger.warn("Caught HttpClientErrorException while trying to revoke token={} in APIGW", tokenValue, e);
        }
    }

    private MultiValueMap<String, String> deleteConsentRequest(String tokenValue) {
        MultiValueMap<String, String> consentRequest = new LinkedMultiValueMap<>();
        consentRequest.add("tokenValue", tokenValue);
        return consentRequest;
    }

    private MultiValueMap<String, String> createConsentRequest(String crn, String childCrn, TransferType transferType) {
        MultiValueMap<String, String> consentRequest = new LinkedMultiValueMap<>();
        if (childCrn == null) {
            consentRequest.add("citizen", crn);
        } else {
            consentRequest.add("citizen", childCrn);
            consentRequest.add("legalGuardian", crn);
        }
        consentRequest.add("scope", transferType.getScope());
        return consentRequest;
    }

    private ApigwConsentDTO toApigwConsentDTO(ApigwAccessToken apigwAccessToken) {
        return ApigwConsentDTO.create()
                .withTokenValue(apigwAccessToken.getAccessToken())
                .withScope(apigwAccessToken.getScope())
                .withExpirationDateTime(apigwAccessToken.getExpirationDateTime());
    }

    /*
    {access_token=d48f3e2d-7faf-49b2-acd3-6f8381722b39, token_type=Bearer, expires_in=31535999, scope=CLINICALPROCESS_HEALTHCOND_ACTOUTCOME_READ, issue_date=1447335523792}
     */
    protected static class ApigwAccessToken {
        private String accessToken;
        private LocalDateTime expirationDateTime;
        private String scope;
        private Long issueDate; //used to calculate expirationDateTime
        private Long expiresIn; //used to calculate expirationDateTime

        public String getAccessToken() {
            return accessToken;
        }

        @JsonSetter("access_token")
        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public LocalDateTime getExpirationDateTime() {
            return expirationDateTime;
        }

        public void setExpirationDateTime(LocalDateTime expirationDateTime) {
            this.expirationDateTime = expirationDateTime;
        }

        public String getScope() {
            return scope;
        }

        public void setScope(String scope) {
            this.scope = scope;
        }

        @JsonAnySetter
        public void setUnhandled(String name, Object value) {
            if ("expires_in".equals(name)) { //Integer or Long (seconds)
                logger.info("value: {}", value);
                if (value instanceof Integer) {
                    this.expiresIn = (long) (int) value;
                } else {
                    this.expiresIn = (Long) value;
                }
            }
            if("issue_date".equals(name)) {
                this.issueDate = (Long) value;
            }
            if (issueDate != null && expiresIn != null) {
                expirationDateTime = LocalDateTime
                        .ofInstant(Instant.ofEpochMilli(issueDate), ZoneId.systemDefault())
                        .plusSeconds(expiresIn);
            }
        }

        @Override
        public String toString() {
            return "ApigwAccessToken{" +
                    "accessToken='" + accessToken + '\'' +
                    ", expirationDateTime=" + expirationDateTime +
                    ", scope='" + scope + '\'' +
                    '}';
        }
    }
}
