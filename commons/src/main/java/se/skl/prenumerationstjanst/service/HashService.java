package se.skl.prenumerationstjanst.service;

/**
 * Service interface for calculating reproducible one-way hashes for Strings.
 *
 * May be used with additional logic to determine if a PushJob
 * should be carried out or not by comparing the hash values.
 *
 * Must be indempotent.
 *
 * @author Martin Samuelsson
 */
public interface HashService {
    /**
     * Calculates a reproducible one-way hash from a String.
     *
     * Must be indempotent.
     *
     * @param input input String
     * @return one-way hash
     */
    String calculateHash(String input);
}
