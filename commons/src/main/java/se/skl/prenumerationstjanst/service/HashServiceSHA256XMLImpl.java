package se.skl.prenumerationstjanst.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.XMLEvent;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;

/**
 * XML-aware HashService implementation.
 *
 * To prevent different XML formatting from interfering with the result hash,
 * this implementation extracts only the character data from the input XML
 *
 * Data will be extracted in order from start of the document to the end and
 * returned as a concatenated String
 *
 * @author Martin Samuelsson
 */
public class HashServiceSHA256XMLImpl extends AbstractHashServiceSHA256Impl {

    private static final Logger logger = LoggerFactory.getLogger(HashServiceSHA256XMLImpl.class);

    @Override
    protected String getHashableString(String input) {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new ByteArrayInputStream(input
                    .getBytes(StandardCharsets.UTF_8)));
            StringBuilder stringBuilder = new StringBuilder();
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    Iterator attributes = xmlEvent.asStartElement().getAttributes();
                    while (attributes.hasNext()) {
                        String data = ((Attribute) attributes.next()).getValue();
                        stringBuilder.append(data);
                    }
                }
                if (xmlEvent.isCharacters()) {
                    String data = xmlEvent.asCharacters().getData().trim();
                    stringBuilder.append(data);
                }
            }
            return stringBuilder.toString();
        } catch (XMLStreamException e) {
            logger.error("Could not extract hashable String, input might not we well-formatted XML", e);
        }
        return null;
    }
}
