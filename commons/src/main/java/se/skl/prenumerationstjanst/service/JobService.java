package se.skl.prenumerationstjanst.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import se.skl.prenumerationstjanst.dto.FetchJobDTO;
import se.skl.prenumerationstjanst.dto.PushJobDTO;
import se.skl.prenumerationstjanst.dto.TransferJobDTO;
import se.skl.prenumerationstjanst.dto.TransformationJobDTO;
import se.skl.prenumerationstjanst.model.*;
import se.skl.prenumerationstjanst.repository.*;

import java.time.LocalDateTime;

/**
 * Service class with the purpose to create and update
 * the various Job:s
 *
 * @author Martin Samuelsson
 */
public class JobService {

    private static final Logger logger = LoggerFactory.getLogger(JobService.class);

    @Autowired
    private TransferJobRepository transferJobRepository;

    @Autowired
    private FetchJobRepository fetchJobRepository;

    @Autowired
    private PushJobRepository pushJobRepository;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private TransformationJobRepository transformationJobRepository;

    @Autowired
    private TransformationService transformationService;

    @Transactional
    public TransferJobDTO createTransferJob(String subscriptionId, TransferType transferType,
                                            LocalDateTime startDateTime, LocalDateTime endDateTime,
                                            String sourceSystemHsaId, TransferJob.Origin origin) {
        Subscription subscription = subscriptionRepository.findOne(subscriptionId);
        String userCrn = subscription.getUserContext().getUser().getCrn();
        String representingCrn = subscription.getUserContext().getRepresentingCrn();
        TransferJob transferJob = transferJobRepository.save(new TransferJob(userCrn, representingCrn, subscriptionId,
                transferType, startDateTime, endDateTime, sourceSystemHsaId, origin));
        return transformationService.toDTO(transferJob);
    }

    @Transactional(readOnly = true)
    public TransferJobDTO getTransferJob(String id) {
        return transformationService.toDTO(transferJobRepository.findOne(id));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public FetchJobDTO createFetchJob(String transferJobId) {
        TransferJob transferJob = transferJobRepository.findOne(transferJobId);
        Subscription sub = subscriptionRepository.findOne(transferJob.getSubscriptionId());
        FetchJob fetchJob = fetchJobRepository.save(new FetchJob(transferJob, sub.getApigwConsent().getTokenValue()));
        transferJob.setFetchJob(fetchJob);
        transferJobRepository.save(transferJob);
        return transformationService.toDTO(fetchJob);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public FetchJobDTO updateFetchJobStatus(String id, JobStatus status) {
        FetchJob fetchJob = fetchJobRepository.findOne(id);
        fetchJob.setStatus(status);
        fetchJob.getJobStates().add(new FetchJobState(fetchJob, status));
        return transformationService.toDTO(fetchJobRepository.save(fetchJob));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public FetchJobDTO finishFetchJob(String id, LocalDateTime fetchFinishTime) {
        FetchJob fetchJob = fetchJobRepository.findOne(id);
        fetchJob.setStatus(JobStatus.FINISHED);
        fetchJob.setFetchFinishTime(fetchFinishTime);
        fetchJob.getJobStates().add(new FetchJobState(fetchJob, JobStatus.FINISHED));
        FetchJob save = fetchJobRepository.save(fetchJob);
        return transformationService.toDTO(save);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public TransformationJobDTO createTransformationJob(String transferJobId, String source, LocalDateTime sourceFetchTime,
                                                        int sourceVersion) {
        TransferJob transferJob = transferJobRepository.findOne(transferJobId);
        TransformationJob transformationJob = transformationJobRepository.save(new TransformationJob(transferJob, source,
                sourceFetchTime, sourceVersion));
        transferJob.getTransformationJobs().add(transformationJob);
        transferJobRepository.save(transferJob);
        return transformationService.toDTO(transformationJob);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public TransformationJobDTO updateTransformationJobStatus(String id, JobStatus jobStatus) {
        TransformationJob transformationJob = transformationJobRepository.findOne(id);
        transformationJob.setStatus(jobStatus);
        transformationJob.getJobStates().add(new TransformationJobState(transformationJob, jobStatus));
        return transformationService.toDTO(transformationJob);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public TransformationJobDTO finishTransformationJob(String id) {
        TransformationJob transformationJob = transformationJobRepository.findOne(id);
        transformationJob.setStatus(JobStatus.FINISHED);
        transformationJob.getJobStates().add(new TransformationJobState(transformationJob, JobStatus.FINISHED));
        return transformationService.toDTO(transformationJob);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public PushJobDTO createPushJob(String transferJobId) {
        TransferJob transferJob = transferJobRepository.findOne(transferJobId);
        Subscription sub = subscriptionRepository.findOne(transferJob.getSubscriptionId());
        String phkPersonId = sub.getUserContext().getUser().getPhkPersonId();
        String phkRecordId = sub.getUserContext().getPhkRecordId();
        PushJob pushJob = pushJobRepository.save(new PushJob(transferJob, phkPersonId, phkRecordId));
        transferJob.getPushJobs().add(pushJob);
        transferJobRepository.save(transferJob);
        return transformationService.toDTO(pushJob);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public PushJobDTO updatePushJobStatus(String id, JobStatus status) {
        PushJob pushJob = pushJobRepository.findOne(id);
        pushJob.setStatus(status);
        pushJob.getJobStates().add(new PushJobState(pushJob, status));
        return transformationService.toDTO(pushJobRepository.save(pushJob));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public PushJobDTO finishPushJob(String id, String sourceId, int sourceVersion,
                                    LocalDateTime pushFinishTime, JobStatus status,
                                    String typeOfData, String relatedTo) {
        PushJob pushJob = pushJobRepository.findOne(id);
        pushJob.setStatus(status);
        pushJob.setSourceId(sourceId);
        pushJob.setSourceVersion(sourceVersion);
        pushJob.setPushFinishTime(pushFinishTime);
        pushJob.setTypeOfData(typeOfData);
        pushJob.setRelatedTo(relatedTo);
        pushJob.getJobStates().add(new PushJobState(pushJob, status));
        return transformationService.toDTO(pushJobRepository.save(pushJob));
    }

}
