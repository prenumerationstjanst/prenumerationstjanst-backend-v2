package se.skl.prenumerationstjanst.service;

import com.chbase.methods.jaxb.SimpleRequestTemplate;
import com.chbase.methods.jaxb.getpersoninfo.request.GetPersonInfoRequest;
import com.chbase.methods.jaxb.getpersoninfo.response.GetPersonInfoResponse;
import com.chbase.methods.jaxb.removeapplicationrecordauthorization.request.RemoveApplicationRecordAuthorizationRequest;
import com.chbase.thing.oxm.jaxb.types.PersonInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.skl.prenumerationstjanst.chbase.CHBaseRequestTemplateFactory;
import se.skl.prenumerationstjanst.dto.PhkConsentDTO;

/**
 * @author Martin Samuelsson
 */
public class PhkConsentService {

    private static final Logger logger = LoggerFactory.getLogger(PhkConsentService.class);

    @Autowired
    private CHBaseRequestTemplateFactory chBaseRequestTemplateFactory;

    public PhkConsentDTO getPhkConsent(String authToken) {
        SimpleRequestTemplate simpleRequestTemplate = chBaseRequestTemplateFactory.getJaxbSimpleRequestTemplate();
        GetPersonInfoRequest getPersonInfoRequest = new GetPersonInfoRequest();
        simpleRequestTemplate.setUserAuthToken(authToken);
        try {
            GetPersonInfoResponse getPersonInfoResponse = (GetPersonInfoResponse) simpleRequestTemplate.makeRequest(getPersonInfoRequest);
            PersonInfo personInfo = getPersonInfoResponse.getPersonInfo();
            String selectedRecordId = personInfo.getSelectedRecordId();
            String recordName = personInfo.getRecord().get(0).getValue();
            PhkConsentDTO phkConsentDTO = PhkConsentDTO.create()
                    .withPersonId(personInfo.getPersonId())
                    .withRecordId(selectedRecordId)
                    .withRecordName(recordName);
            logger.debug("fetched PhkConsent: {}", phkConsentDTO);
            return phkConsentDTO;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e); //TODO: exception
        }
    }

    //TODO: make async
    public void deletePhkConsent(String personId, String recordId) {
        logger.debug("deletePhkConsent personId[{}], String recordId[{}]", personId, recordId);
        SimpleRequestTemplate simpleRequestTemplate = chBaseRequestTemplateFactory.getJaxbSimpleRequestTemplate
                (personId, recordId);
        RemoveApplicationRecordAuthorizationRequest removeApplicationRecordAuthorizationRequest =
                new RemoveApplicationRecordAuthorizationRequest();
        try {
            simpleRequestTemplate.makeRequest(removeApplicationRecordAuthorizationRequest);
        } catch (Exception e) {
            logger.error("Could not remove authorization from record {}", recordId, e);
        }
    }
}
