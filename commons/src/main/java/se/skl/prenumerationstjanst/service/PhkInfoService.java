package se.skl.prenumerationstjanst.service;

import se.skl.prenumerationstjanst.dto.RecordDTO;

/**
 * @author Martin Samuelsson
 */
public interface PhkInfoService {
    RecordDTO getRecordForAuthToken(String authToken);
    PHK_PERSON_STATUS checkPhkPersonId(String personId);
    String getShellRedirectUrl();


    enum PHK_PERSON_STATUS {
        ACTIVE,
        INACTIVE,
        UNDECIDED
    }
}
