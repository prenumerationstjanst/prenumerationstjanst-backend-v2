package se.skl.prenumerationstjanst.service;

import com.chbase.HVRequestException;
import com.chbase.StatusCode;
import com.chbase.methods.jaxb.SimpleRequestTemplate;
import com.chbase.methods.jaxb.getpersoninfo.request.GetPersonInfoRequest;
import com.chbase.methods.jaxb.getpersoninfo.response.GetPersonInfoResponse;
import com.chbase.methods.jaxb.getservicedefinition2.request.GetServiceDefinition2Request;
import com.chbase.methods.jaxb.getservicedefinition2.response.GetServiceDefinition2Response;
import com.chbase.thing.oxm.jaxb.types.PersonInfo;
import com.chbase.thing.oxm.jaxb.types.Record;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import se.skl.prenumerationstjanst.chbase.CHBaseRequestTemplateFactory;
import se.skl.prenumerationstjanst.dto.RecordDTO;

/**
 * @author Martin Samuelsson
 */
public class PhkInfoServiceImpl implements PhkInfoService {

    private static final Logger logger = LoggerFactory.getLogger(PhkInfoServiceImpl.class);

    @Autowired
    private CHBaseRequestTemplateFactory chBaseRequestTemplateFactory;

    @Override
    public RecordDTO getRecordForAuthToken(String authToken) {
        SimpleRequestTemplate simpleRequestTemplate = chBaseRequestTemplateFactory.getJaxbSimpleRequestTemplate();
        simpleRequestTemplate.setUserAuthToken(authToken);
        try {
            PersonInfo personInfo = getPersonInfo(simpleRequestTemplate);
            return new RecordDTO(personInfo.getSelectedRecordId(),
                    personInfo.getPersonId(),
                    personInfo.getRecord().stream()
                        .filter(record -> record.getId().equals(personInfo.getSelectedRecordId()))
                        .map(Record::getDisplayName)
                        .findFirst()
                        .orElse(null));
        } catch (Exception e) {
            //TODO: exception
            logger.error("Could not get record for authToken[{}]", authToken, e);
            return null;
        }
    }

    @Override
    public PHK_PERSON_STATUS checkPhkPersonId(String personId) {
        SimpleRequestTemplate simpleRequestTemplate = chBaseRequestTemplateFactory.getJaxbSimpleRequestTemplate();
        simpleRequestTemplate.setPersonId(personId);
        try {
            getPersonInfo(simpleRequestTemplate);
            return PHK_PERSON_STATUS.ACTIVE;
        } catch (HVRequestException e) {
            if (e.getCode() == StatusCode.INVALID_PERSON) {
                return PHK_PERSON_STATUS.INACTIVE;
            } else {
                logger.error("caught HVRequestException while trying to decide status of personId={} in PHK (exception code={})", personId, e.getCode(), e);
            }
        } catch (Exception e) {
            logger.error("caught exception while trying to decide status of personId={} in PHK", personId, e);
        }
        return PHK_PERSON_STATUS.UNDECIDED;
    }

    private PersonInfo getPersonInfo(SimpleRequestTemplate simpleRequestTemplate) throws Exception {
        GetPersonInfoResponse getPersonInfoResponse = (GetPersonInfoResponse)
                simpleRequestTemplate.makeRequest(new GetPersonInfoRequest());
        return getPersonInfoResponse.getPersonInfo();

    }

    @Override
    public String getShellRedirectUrl() {
        SimpleRequestTemplate simpleRequestTemplate = chBaseRequestTemplateFactory.getJaxbSimpleRequestTemplate();
        try {
            GetServiceDefinition2Response serviceDefinition = (GetServiceDefinition2Response)
                    simpleRequestTemplate.makeRequest(new GetServiceDefinition2Request());
            if (serviceDefinition.getShell() != null) {
                return serviceDefinition.getShell().getRedirectUrl();
            }
        } catch (Exception e) {
            logger.error("Could not get CHBase service definition", e);
        }
        return null;
    }

}
