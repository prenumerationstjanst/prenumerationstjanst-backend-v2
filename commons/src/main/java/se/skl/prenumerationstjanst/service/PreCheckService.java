package se.skl.prenumerationstjanst.service;

import org.springframework.transaction.annotation.Transactional;

/**
 * @author Martin Samuelsson
 */
public interface PreCheckService {
    @Transactional(readOnly = true)
    Boolean preCheck(String performingCrn, String representingCrn);

    @Transactional
    Boolean preCheckAndExecuteCleanup(String performingCitizenCrn, String representingCitizenCrn);
}
