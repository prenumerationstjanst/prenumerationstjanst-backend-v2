package se.skl.prenumerationstjanst.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import se.skl.prenumerationstjanst.dto.SubscriptionDTO;
import se.skl.prenumerationstjanst.dto.UserContextDTO;
import se.skl.prenumerationstjanst.dto.UserDTO;
import se.skl.prenumerationstjanst.model.TransferType;

import java.util.List;

/**
 * @author Martin Samuelsson
 */
public class PreCheckServiceImpl implements PreCheckService {

    private static final Logger logger = LoggerFactory.getLogger(PreCheckServiceImpl.class);

    @Autowired
    private UserService userService;

    @Autowired
    private PhkInfoService phkInfoService;

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private UserContextService userContextService;

    @Override
    @Transactional(readOnly = true)
    public Boolean preCheck(String performingCrn, String representingCrn) {
        UserDTO user = userService.getUser(performingCrn);
        if (user == null) {
            logger.debug("User not found, preCheck fails");
            return false;
        }
        UserContextDTO userContext = userContextService.getUserContext(performingCrn,
                representingCrn);
        if (userContext == null) {
            logger.debug("UserContext not found, preCheck fails");
            return false;
        }
        Boolean retVal;
        switch (phkInfoService.checkPhkPersonId(user.getPhkPersonId())) {
            case ACTIVE:
                retVal = Boolean.TRUE;
                break;
            case INACTIVE:
                retVal = Boolean.FALSE;
                break;
            case UNDECIDED:
                retVal = null;
                break;
            default:
                retVal = null;
        }
        if (retVal == Boolean.FALSE) {
            logger.debug("phkPersonId={} is no longer active in PHK, preCheck fails", user.getPhkPersonId());
        }
        return retVal;
    }

    @Override
    @Transactional
    public Boolean preCheckAndExecuteCleanup(String performingCitizenCrn, String representingCitizenCrn) {
        Boolean preCheckResult = preCheck(performingCitizenCrn, representingCitizenCrn);
        if (!preCheckResult) {
            List<SubscriptionDTO> subscriptionsForUserContext = subscriptionService
                    .getSubscriptionsForUserContext(performingCitizenCrn, representingCitizenCrn);
            subscriptionsForUserContext.forEach(subscriptionDTO -> {
                logger.debug("'removing' {} subscription for {}:{}", subscriptionDTO.getApprovedTransferType(),
                        performingCitizenCrn, representingCitizenCrn);
                subscriptionService.removeSubscription(performingCitizenCrn, representingCitizenCrn,
                        TransferType.valueOf(subscriptionDTO.getApprovedTransferType()));
            });
        }
        return preCheckResult;
    }
}
