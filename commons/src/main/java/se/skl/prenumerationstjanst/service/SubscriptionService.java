package se.skl.prenumerationstjanst.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import se.skl.prenumerationstjanst.dto.ApigwConsentDTO;
import se.skl.prenumerationstjanst.dto.SubscriptionDTO;
import se.skl.prenumerationstjanst.exception.subscription.SubscriptionCreationFailedException;
import se.skl.prenumerationstjanst.exception.subscription.SubscriptionNotFoundException;
import se.skl.prenumerationstjanst.exception.user.UserNotFoundException;
import se.skl.prenumerationstjanst.model.ApigwConsent;
import se.skl.prenumerationstjanst.model.Subscription;
import se.skl.prenumerationstjanst.model.TransferType;
import se.skl.prenumerationstjanst.model.UserContext;
import se.skl.prenumerationstjanst.repository.SubscriptionRepository;
import se.skl.prenumerationstjanst.repository.UserContextRepository;
import se.skl.prenumerationstjanst.service.monitoring.SubscriptionMonitoringService;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service class that handles Subscriptions
 *
 * @author Martin Samuelsson
 */
public class SubscriptionService {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionService.class);

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private UserContextRepository userContextRepository;

    @Autowired
    private TransformationService transformationService;

    @Autowired
    private ApigwConsentService apigwConsentService;

    @Autowired
    private SubscriptionMonitoringService subscriptionMonitoringService;

    @Transactional
    public SubscriptionDTO createSubscription(String userCrn, String subscriptionCrn, TransferType approvedTransferType) {
        UserContext userContext = userContextRepository.findByUserCrnAndRepresentingCrn(userCrn, subscriptionCrn);
        if (userContext != null) {
            if (canCreateSubscription(userCrn, subscriptionCrn, approvedTransferType)) {
                Subscription subscription = new Subscription(userContext, approvedTransferType);
                subscriptionMonitoringService.monitorCreateSubscription(subscription.getUserContext().getUser().getCrn(), null, null,
                        subscription.getId(), subscription.getUserContext().getRepresentingCrn(), subscription.getApprovedTransferType());

                String childCrn = null;
                if (!userCrn.equals(subscriptionCrn)) {
                    childCrn = subscriptionCrn;
                }
                ApigwConsentDTO apigwConsentDTO = apigwConsentService.getConsent(userCrn, childCrn, approvedTransferType);
                addApigwConsentToSubscription(subscription, apigwConsentDTO);

                subscriptionMonitoringService.monitorUpdateSubscription(subscription.getUserContext().getUser().getCrn(), null, null,
                        subscription.getId(), subscription.getUserContext().getRepresentingCrn(),
                        SubscriptionMonitoringService.UpdateType.HEALTHCARE_EXPIRATION, null);

                return transformationService.toDTO(subscriptionRepository.save(subscription));
            } else { //a subscription owned by the current user for subscriptionCrn with one or more of the wanted transfer types already exists
                throw new SubscriptionCreationFailedException("a subscription with one or more of the requested " +
                        "transfer types already exists");
                //FIXME: if a previous creation attempt failed in the CHBase stage, the user can not create a subscription
                //for this transfer type at all. Need cleanup logic.
            }
        } else { //user not found
            //TODO: UserContextNotFoundException
            logger.error("userContext not found");
            throw new UserNotFoundException();
        }
    }

    @Transactional(readOnly = true)
    public List<SubscriptionDTO> getSubscriptionsForUserContext(String performingCrn, String representingCrn) {
        UserContext userContext = userContextRepository
                .findByUserCrnAndRepresentingCrn(performingCrn, representingCrn);
        return Optional.ofNullable(subscriptionRepository.findByUserContextAndScheduledForRemovalFalse(userContext))
                .orElseGet(Collections::emptyList)
                .stream()
                .map(sub -> transformationService.toDTO(sub, true, false))
                .collect(Collectors.toList());
    }

    @Transactional
    public void removeSubscription(String performingCrn, String representingCrn, TransferType transferType) {
        logger.debug("removeSubscription performingCrn={}, representingCrn={}, transferType={}", performingCrn, representingCrn, transferType);
        Subscription subscription = subscriptionRepository.findForUserContextByTransferType
                (performingCrn, representingCrn, transferType);
        if (subscription != null) {
            subscriptionMonitoringService.monitorRemoveSubscription(subscription.getUserContext().getUser().getCrn(), null, null,
                    subscription.getId(), subscription.getUserContext().getRepresentingCrn());
            if (subscription.getApigwConsent() != null) {
                apigwConsentService.deleteConsent(subscription.getApigwConsent().getTokenValue());
            }
            subscription.setScheduledForRemoval(true);
            subscriptionRepository.save(subscription);
        } else {
            throw new SubscriptionNotFoundException(performingCrn, representingCrn, transferType.name());
        }
    }

    @Transactional
    public SubscriptionDTO updateSubscriptionHealthcareExpiration(String id) {
        Subscription subscription = subscriptionRepository.findOne(id);
        if (subscription != null) {
            String userCrn = subscription.getUserContext().getUser().getCrn();
            String subCrn = subscription.getUserContext().getRepresentingCrn();
            String childCrn = null;
            if (!userCrn.equalsIgnoreCase(subCrn)) {
                childCrn = subCrn;
            }
            ApigwConsentDTO consent = apigwConsentService.getConsent(userCrn, childCrn,
                    subscription.getApprovedTransferType());
            //TODO: log difference
            addApigwConsentToSubscription(subscription, consent);
            subscriptionRepository.save(subscription);
            subscriptionMonitoringService.monitorUpdateSubscription(userCrn, null, null,
                    subscription.getId(), subCrn,
                    SubscriptionMonitoringService.UpdateType.HEALTHCARE_EXPIRATION, null);
            return transformationService.toDTO(subscription);
        } else {
            throw new SubscriptionNotFoundException(id);
        }
    }

    @Transactional
    public void removeSubscription(String id) {
        Subscription subscription = subscriptionRepository.findOne(id);
        if (subscription != null) {
            subscriptionMonitoringService.monitorRemoveSubscription(subscription.getUserContext().getUser().getCrn(), null, null,
                    subscription.getId(), subscription.getUserContext().getRepresentingCrn());
            if (subscription.getApigwConsent() != null) {
                apigwConsentService.deleteConsent(subscription.getApigwConsent().getTokenValue());
            }
            subscription.setScheduledForRemoval(true);
            subscriptionRepository.save(subscription);
        } else {
            throw new SubscriptionNotFoundException(id);
        }
    }

    /**
     * App internal subscription removal. All external connections (tokens)
     * should be handled before calling this method.
     *
     * Monitoring, if applicable, should be performed by caller.
     *
     * @param id id of the subscription
     */
    @Transactional
    public void internalRemoveSubscription(String id) {
        logger.debug("internalRemoveSubscription id={}", id);
        Subscription subscription = subscriptionRepository.findOne(id);
        if (subscription != null) {
            subscriptionRepository.delete(subscription);
        }
    }

    private void addApigwConsentToSubscription(Subscription subscription, ApigwConsentDTO apigwConsentDTO) {
        ApigwConsent apigwConsent = new ApigwConsent();
        apigwConsent.setTokenValue(apigwConsentDTO.getTokenValue());
        apigwConsent.setExpirationDateTime(apigwConsentDTO.getExpirationDateTime());
        apigwConsent.setScope(apigwConsentDTO.getScope());
        if (subscription.getApigwConsent() != null) {
            logger.debug("replacing old apigwConsent with new one");
        }
        subscription.setApigwConsent(apigwConsent);
    }

    /**
     * Returns an active (not scheduled for removal) subscription corresponding to a APIGW token value or null
     * if not found
     *
     * @param tokenValue
     * @return an active (not scheduled for removal) subscription corresponding to a APIGW token value or null if not found
     */
    public SubscriptionDTO findSubscriptionByApigwConsentToken(String tokenValue) {
        Subscription subscription = subscriptionRepository.findByApigwConsentTokenValueAndScheduledForRemovalFalse(tokenValue);
        if (subscription != null) {
            SubscriptionDTO subscriptionDTO = transformationService.toDTO(subscription, false, false);
            return subscriptionDTO;
        }
        return null;
    }

    /**
     * Determines if a subscription is active (exists and not scheduled for removal)
     *
     * @param id
     * @return true if a subscription exists for the given id and is not scheduled for removal, false otherwise
     */
    @Transactional(readOnly = true)
    public boolean isSubscriptionActive(String id) {
        Subscription subscription = subscriptionRepository.findOne(id);
        return subscription != null && !subscription.isScheduledForRemoval();
    }

    /**
     * Determines if it is possible to create a subscription based on performingCrn, representingCrn
     * and the requested TransferType
     *
     * Only one subscription for the unique combination of performingCrn, representingCrn and TransferType is allowed
     *
     * @param performingCrn performing citizen crn
     * @param representingCrn represented citizen crn
     * @param requestedTransferType requested TransferType
     * @return true if creation is allowed, false otherwise
     */
    private boolean canCreateSubscription(String performingCrn, String representingCrn, TransferType requestedTransferType) {
        Subscription sub = subscriptionRepository
                .findForUserContextByTransferType(performingCrn, representingCrn, requestedTransferType); //will also check for scheduledForRemoval=false
        return sub == null;
    }
}
