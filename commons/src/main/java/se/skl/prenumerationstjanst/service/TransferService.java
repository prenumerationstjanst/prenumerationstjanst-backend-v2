package se.skl.prenumerationstjanst.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import se.skl.prenumerationstjanst.dto.TransferJobDTO;
import se.skl.prenumerationstjanst.dto.internal.InitialTransferParams;
import se.skl.prenumerationstjanst.dto.internal.SingleSourceSystemTransferParams;
import se.skl.prenumerationstjanst.model.TransferJob;
import se.skl.prenumerationstjanst.model.TransferType;

import java.time.LocalDateTime;

import static se.skl.prenumerationstjanst.PrenumerationstjanstConstants.INITIAL_TRANSFERS_FOR_SUBSCRIPTION_QUEUE;
import static se.skl.prenumerationstjanst.PrenumerationstjanstConstants.START_TRANSFER_FOR_SINGLE_SOURCE_SYSTEM_QUEUE;

/**
 * Service class to asynchronously initiate TransferJob:s
 * by adding 'commands' to queues (will be picked up by TransferMDP)
 *
 * TODO: transactional?
 *
 * @author Martin Samuelsson
 */
public class TransferService {

    @Autowired
    private JmsTemplate jmsTemplate;

    public void startInitialTransfersForSubscription(String subscriptionId, String transferType) {
        jmsTemplate.convertAndSend(INITIAL_TRANSFERS_FOR_SUBSCRIPTION_QUEUE,
                new InitialTransferParams(
                        subscriptionId,
                        transferType)
        );
    }

    /**
     *
     * @param subscriptionId
     * @param transferType
     * @param documentTime
     * @param sourceSystemHsaId
     */
    public void startTransferForProcessNotification(String subscriptionId,
                                                    TransferType transferType,
                                                    LocalDateTime documentTime,
                                                    String sourceSystemHsaId) {
        jmsTemplate.convertAndSend(START_TRANSFER_FOR_SINGLE_SOURCE_SYSTEM_QUEUE,
                new SingleSourceSystemTransferParams(
                        subscriptionId,
                        transferType,
                        documentTime.toLocalDate().atStartOfDay(),
                        documentTime.toLocalDate().atTime(23, 59, 59),
                        sourceSystemHsaId,
                        TransferJob.Origin.EI_NOTIFICATION)
        );
    }

    public void startTransferForFailedSourceSystem(TransferJobDTO originalTransferJob, String sourceSystemHsaId) {
        jmsTemplate.convertAndSend(START_TRANSFER_FOR_SINGLE_SOURCE_SYSTEM_QUEUE,
                new SingleSourceSystemTransferParams(
                        originalTransferJob.getSubscriptionId(),
                        TransferType.valueOf(originalTransferJob.getTransferType()),
                        originalTransferJob.getStartDateTime(),
                        originalTransferJob.getEndDateTime(),
                        sourceSystemHsaId,
                        TransferJob.Origin.FAILED_SOURCE_SYSTEM
                ));
    }




}
