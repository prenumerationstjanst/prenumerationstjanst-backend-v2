package se.skl.prenumerationstjanst.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.skl.prenumerationstjanst.dto.*;
import se.skl.prenumerationstjanst.model.*;

import java.util.stream.Collectors;

/**
 * Service class to transform entities to their
 * respective DTO representations
 *
 * There are only one-way conversions i.e there
 * are no conversions from DTO to entity and all
 * operations on entities are expected to be handled
 * via Service methods.
 *
 * @author Martin Samuelsson
 */
public class TransformationService {

    private static final Logger logger = LoggerFactory.getLogger(TransformationService.class);

    public UserDTO toDTO(User user) {
        return toDTO(user, true);
    }

    private UserDTO toDTO(User user, boolean includeUserContext) {
        if (user == null) return null;
        UserDTO userDTO = UserDTO.create()
                .withId(user.getId())
                .withCrn(user.getCrn())
                .withPhkPersonId(user.getPhkPersonId());
        if (includeUserContext && user.getUserContexts() != null) {
            userDTO = userDTO.withUserContexts(
                    user.getUserContexts().stream()
                        .map(uc -> toDTO(uc, true, false))
                        .collect(Collectors.toList())
            );
        }
        return userDTO;
    }

    public UserContextDTO toDTO(UserContext userContext) {
        return toDTO(userContext, true, true);
    }

    private UserContextDTO toDTO(UserContext userContext, boolean includeSubscriptions, boolean includeUser) {
        if (userContext == null) return null;
        UserContextDTO userContextDTO = UserContextDTO.create()
                .withRepresentingCrn(userContext.getRepresentingCrn())
                .withPhkRecordId(userContext.getPhkRecordId());
        if (includeSubscriptions && userContext.getSubscriptions() != null) {
            userContextDTO = userContextDTO.withSubscriptions(
                    userContext.getSubscriptions().stream()
                        .map(sub -> toDTO(sub, true, false))
                        .collect(Collectors.toList())
            );
        }
        if (includeUser && userContext.getUser() != null) {
            userContextDTO = userContextDTO.withUser(toDTO(userContext.getUser(), false));
        }
        return userContextDTO;
    }

    /**
     * Subscription -> SubscriptionDTO transformation
     * with inclusion of underlying jobs
     *
     * @param subscription
     * @return SubscriptionDTO with child jobs included
     */
    public SubscriptionDTO toDTO(Subscription subscription) {
        return toDTO(subscription, true, true);
    }

    /**
     * Subscription -> SubscriptionDTO transformation
     * with the option to exclude child jobs, consents
     * and userContext to prevent circular references
     * circular references
     *
     * @param subscription
     * @param includeApigwConsent
     * @param includeUserContext
     * @return SubscriptionDTO
     */
    public SubscriptionDTO toDTO(Subscription subscription, boolean includeApigwConsent,
                                 boolean includeUserContext) {
        if (subscription == null) return null;
        SubscriptionDTO subscriptionDTO = SubscriptionDTO.create()
                .withId(subscription.getId())
                .withApprovedTransferType(subscription.getApprovedTransferType().name());
        if (includeApigwConsent) {
            subscriptionDTO = subscriptionDTO
                    .withApigwConsent(toDTO(subscription.getApigwConsent()));
        }
        if (includeUserContext) {
            subscriptionDTO = subscriptionDTO.withUserContext(toDTO(subscription.getUserContext(), false, true));
        }
        return subscriptionDTO;
    }

    public ApigwConsentDTO toDTO(ApigwConsent apigwConsent) {
        if (apigwConsent == null) return null;
        return ApigwConsentDTO.create()
                .withTokenValue(apigwConsent.getTokenValue())
                .withExpirationDateTime(apigwConsent.getExpirationDateTime());
    }

    /**
     * TransferJob -> TransferJobDTO transformation
     * with inclusion of child jobs and exclusion
     * of parent subscription
     *
     * @param transferJob
     * @return TransferJobDTO with child jobs and parent subscription included
     */
    public TransferJobDTO toDTO(TransferJob transferJob) {
        return toDTO(transferJob, true, true);
    }

    /**
     * TransferJob -> TransferJobDTO transformation
     * with the option to exclude child jobs and
     * and parent subscription to prevent circular
     * references
     *
     * @param transferJob
     * @param includeFetchJob
     * @param includePushJobs
     * @return TransferJobDTO
     */
    private TransferJobDTO toDTO(TransferJob transferJob, boolean includeFetchJob, boolean includePushJobs) {
        if (transferJob == null) return null;
        TransferJobDTO transferJobDTO = TransferJobDTO.create()
                .withId(transferJob.getId())
                .withUserCrn(transferJob.getUserCrn())
                .withRepresentingCrn(transferJob.getRepresentingCrn())
                .withSubscriptionId(transferJob.getSubscriptionId())
                .withTransferType(transferJob.getTransferType().name())
                .withStartDateTime(transferJob.getStartDateTime())
                .withEndDateTime(transferJob.getEndDateTime())
                .withSourceSystemHsaId(transferJob.getSourceSystemHsaId())
                .withOrigin(transferJob.getOrigin().name());
        if (includeFetchJob) {
            transferJobDTO = transferJobDTO.withFetchJob(toDTO(transferJob.getFetchJob(), false));
        }
        if (includePushJobs && transferJob.getPushJobs() != null) {
            transferJobDTO = transferJobDTO.withPushJobs(
                    transferJob.getPushJobs().stream()
                            .map(pushJob -> toDTO(pushJob, false)).collect(Collectors.toList())
            );
        }
        return transferJobDTO;
    }

    /**
     * FetchJob -> FetchJobDTO transformation with
     * inclusion of parent TransferJob(DTO)
     *
     * @param fetchJob
     * @return FetchJobDTO with included parent TransferJobDTO
     */
    public FetchJobDTO toDTO(FetchJob fetchJob) {
        return toDTO(fetchJob, true);
    }

    /**
     * FetchJob -> FetchJobDTO transformation
     * with the option to exclude parent TransferJob(DTO)
     * to prevent circular references
     *
     * @param fetchJob
     * @param includeTransferJob
     * @return FetchJobDTO
     */
    private FetchJobDTO toDTO(FetchJob fetchJob, boolean includeTransferJob) {
        if (fetchJob == null) return null;
        FetchJobDTO fetchJobDTO = FetchJobDTO.create()
                .withId(fetchJob.getId())
                .withApigwToken(fetchJob.getApigwToken())
                .withStatus(fetchJob.getStatus())
                .withFetchFinishTime(fetchJob.getFetchFinishTime());
        if (includeTransferJob) {
            fetchJobDTO = fetchJobDTO.withTransferJob(toDTO(fetchJob.getTransferJob(), false, true));
        }
        return fetchJobDTO;
    }

    /**
     * PushJob -> PushJobDTO transformation with
     * inclusion of parent TransferJob(DTO)
     *
     * @param pushJob
     * @return PushJobDTO with included parent TransferJob(DTO)
     */
    public PushJobDTO toDTO(PushJob pushJob) {
        return toDTO(pushJob, true);
    }

    /**
     * PushJob -> PushJobDTO transformation
     * with the option to exclude parent TransferJob(DTO)
     * to prevent circular references
     *
     * @param pushJob
     * @param includeTransferJob
     * @return PushJobDTO
     */
    private PushJobDTO toDTO(PushJob pushJob, boolean includeTransferJob) {
        if (pushJob == null) return null;
        PushJobDTO pushJobDTO = PushJobDTO.create()
                .withId(pushJob.getId())
                .withPhkPersonId(pushJob.getPhkPersonId())
                .withPhkRecordId(pushJob.getPhkRecordId())
                .withStatus(pushJob.getStatus())
                .withValue(pushJob.getValue())
                .withSourceId(pushJob.getSourceId())
                .withSourceVersion(pushJob.getSourceVersion())
                .withPushFinishTime(pushJob.getPushFinishTime())
                .withTypeOfData(pushJob.getTypeOfData())
                .withRelatedTo(pushJob.getRelatedTo());
        if (includeTransferJob) {
            pushJobDTO = pushJobDTO.withTransferJob(toDTO(pushJob.getTransferJob(), true, false));
        }
        return pushJobDTO;
    }

    /**
     * TransformationJob -> TransformationJobDTO transformation
     * with inclusion of parent TransferJob(DTO)
     *
     * @param transformationJob
     * @return TransformationJobDTO with included parent TransferJob(DTO)
     */
    public TransformationJobDTO toDTO(TransformationJob transformationJob) {
        return toDTO(transformationJob, true);
    }

    private TransformationJobDTO toDTO(TransformationJob transformationJob, boolean includeTransferJob) {
        if (transformationJob == null) return null;
        return new TransformationJobDTO(
                transformationJob.getId(),
                includeTransferJob
                ? toDTO(transformationJob.getTransferJob(), false, false)
                : null,
                transformationJob.getSource(),
                transformationJob.getSourceFetchTime(),
                transformationJob.getSourceVersion());
    }
}