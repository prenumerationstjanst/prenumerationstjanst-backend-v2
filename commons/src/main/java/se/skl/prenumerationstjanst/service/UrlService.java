package se.skl.prenumerationstjanst.service;

import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * @author Martin Samuelsson
 */
public class UrlService {

    private static final Logger logger = LoggerFactory.getLogger(UrlService.class);

    @Autowired
    private PhkInfoService phkInfoService;

    @Value("${pt.self.public.base_uri}")
    private String selfPublicBaseUri;

    @Value("${pt.self.service.base_uri}")
    private String selfServiceBaseUri;

    @Value("${pt.self.public.phk_authorize_start_path}")
    private String phkAuthorizeStartPath;

    @Value("${pt.self.public.phk_authorize_callback_path}")
    private String phkAuthorizeCallbackPath;

    @Value("${pt.self.public.apigw_notification_path}")
    private String selfApigwNotificationPath;

    @Value("${pt.phk.chbase.public.base_uri}")
    private String phkCHBaseBaseShellRedirectUri;

    @Value("${chbase.app_id}")
    private String phkCHBaseAppId;

    public String getPhkAuthorizeStartUrl() {
        return getSelfPublicUrl(phkAuthorizeStartPath, null);
    }

    /**
     * Will try to fetch shell redirect URL from CHBase service definition and
     * replace our configured value if they differ
     */
    @PostConstruct
    public void postConstruct() {
        logger.info("default phkCHBaseBaseShellRedirectUri: {}", phkCHBaseBaseShellRedirectUri);
        String shellRedirectUrl = null;
        try {
            shellRedirectUrl = phkInfoService.getShellRedirectUrl();
        } catch (RuntimeException e) {
            logger.error("Could not get shellRedirectUrl during startup", e);
        }
        if (shellRedirectUrl != null && !shellRedirectUrl.equalsIgnoreCase(phkCHBaseBaseShellRedirectUri)) {
            logger.info("replacing default phkCHBaseBaseShellRedirectUri with: {}", shellRedirectUrl);
            phkCHBaseBaseShellRedirectUri = shellRedirectUrl;
        }
    }

    //CHBase web connectivity - https://www.chbasedeveloperportal.com/chbase/en-us/docs/develop/dn783271
    //Shell redirect interface - https://www.chbasedeveloperportal.com/chbase/en-us/docs/develop/dn783307

    /**
     * Generate URL to PHK CHBase 'AUTH' target.
     *
     * This URL allows the user to login to PHK and we will get a token in the callback that allows us
     * fetch all records that the user has _previously_ granted us access to
     *
     * @param state
     * @return
     */
    public String getPhkRemoteAuthUrl(String state) {
        return getPhkRemoteUrl(state, "AUTH");
    }

    public String getNotificationPath() {
        return getSelfServiceUrl(selfApigwNotificationPath, null);
    }

    private String getPhkRemoteUrl(String state, String target) {
        try {
            URIBuilder targetQsBuilder = new URIBuilder("");
            targetQsBuilder.addParameter("appid", phkCHBaseAppId);
            targetQsBuilder.addParameter("actionqs", state);
            if (phkAuthorizeCallbackPath != null && !phkAuthorizeCallbackPath.isEmpty()) {
                logger.warn("adding redirect parameter. Can only be done in a non production env according to CHBase docs");
                targetQsBuilder.addParameter("redirect", getPhkAuthorizeCallbackPath());
            }
            String targetqs = targetQsBuilder.build().getQuery();
            return new URIBuilder(phkCHBaseBaseShellRedirectUri)
                    .addParameter("target", target)
                    .addParameter("targetqs", targetqs).build().toString();
        } catch (URISyntaxException e) {
            logger.error("could not build PHK remote authorize URL");
            throw new RuntimeException(e);
        }
    }

    private String getPhkAuthorizeCallbackPath() {
        return getSelfPublicUrl(phkAuthorizeCallbackPath, null);
    }

    private String getSelfPublicUrl(String path, Map<String, String> parameters) {
        try {
            URIBuilder uriBuilder = new URIBuilder(selfPublicBaseUri);
            if (path != null) {
                String pathToAdd = path.startsWith("/") ? path : "/" + path;
                uriBuilder.setPath(pathToAdd);
            }
            if (parameters != null) {
                parameters.entrySet()
                        .forEach(entry -> uriBuilder.addParameter(entry.getKey(), entry.getValue()));
            }
            return uriBuilder.build().toString();
        } catch (URISyntaxException e) {
            logger.error("Could not build selfPublicUrl from path: {} with parameters: {} due to exception: {}",
                    path, parameters, e);
            throw new RuntimeException(e);
        }
    }

    private String getSelfServiceUrl(String path, Map<String, String> parameters) {
        try {
            URIBuilder uriBuilder = new URIBuilder(selfServiceBaseUri);
            if (path != null) {
                String pathToAdd = path.startsWith("/") ? path : "/" + path;
                uriBuilder.setPath(pathToAdd);
            }
            if (parameters != null) {
                parameters.entrySet()
                        .forEach(entry -> uriBuilder.addParameter(entry.getKey(), entry.getValue()));
            }
            return uriBuilder.build().toString();
        } catch (URISyntaxException e) {
            logger.error("Could not build selfServiceUrl from path: {} with parameters: {} due to exception: {}",
                    path, parameters, e);
            throw new RuntimeException(e);
        }
    }
}
