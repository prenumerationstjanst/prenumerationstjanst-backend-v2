package se.skl.prenumerationstjanst.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se.skl.prenumerationstjanst.dto.UserContextDTO;
import se.skl.prenumerationstjanst.exception.user.UserNotFoundException;
import se.skl.prenumerationstjanst.model.Subscription;
import se.skl.prenumerationstjanst.model.User;
import se.skl.prenumerationstjanst.model.UserContext;
import se.skl.prenumerationstjanst.repository.UserContextRepository;
import se.skl.prenumerationstjanst.repository.UserRepository;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Martin Samuelsson
 */
@Transactional
public class UserContextService {

    private static final Logger logger = LoggerFactory.getLogger(UserContextService.class);

    @Autowired
    private UserContextRepository userContextRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TransformationService transformationService;

    @Transactional(readOnly = true)
    public UserContextDTO getUserContext(String performingCrn, String representingCrn) {
        logger.debug("getUserContext(performingCrn[{}], representingCrn[{}])", performingCrn, representingCrn);
        UserContext userContext = userContextRepository
                .findByUserCrnAndRepresentingCrn(performingCrn, representingCrn);
        if (userContext == null) {
            return null;
        }
        //remove subs scheduled for removal
        if (userContext.getSubscriptions() != null) {
            List<Subscription> subs = userContext.getSubscriptions().stream()
                    .filter(sub -> !sub.isScheduledForRemoval())
                    .collect(Collectors.toList());
            userContext.setSubscriptions(subs);

        }
        return transformationService.toDTO(userContext);
    }

    public UserContextDTO createUserContext(String performingCrn, String representingCrn, String phkRecordId) {
        logger.debug("createUserContext(performingCrn[{}], representingCrn[{}], phkRecordId[{}])",
                performingCrn, representingCrn, phkRecordId);
        UserContext userContext = userContextRepository
                .findByUserCrnAndRepresentingCrn(performingCrn, representingCrn);
        if (userContext == null) {
            User user = userRepository.findByCrn(performingCrn);
            if (user != null) {
                userContext = new UserContext(user, representingCrn, phkRecordId);
                userContext = userContextRepository.save(userContext);
            } else {
                logger.error("can not create UserContext when no matching user exists");
                throw new UserNotFoundException();
            }
        }
        return transformationService.toDTO(userContext);
    }

    public UserContextDTO updateUserContext(String performingCrn, String representingCrn, String newPhkRecordId) {
        logger.debug("updateUserContext(performingCrn[{}], representingCrn[{}], newPhkRecordId[{}])",
                performingCrn, representingCrn, newPhkRecordId);
        UserContext userContext = userContextRepository
                .findByUserCrnAndRepresentingCrn(performingCrn, representingCrn);
        if (userContext != null) {
            userContext.setPhkRecordId(newPhkRecordId);
        } else {
            //TODO: exception
            logger.warn("userContext does not exist");
        }
        return transformationService.toDTO(userContext);
    }

    public void removeUserContext(String performingCrn, String representingCrn) {
        logger.debug("removeUserContext(performingCrn[{}], representingCrn[{}], newPhkRecordId[{}])",
                performingCrn, representingCrn);
        UserContext userContext = userContextRepository
                .findByUserCrnAndRepresentingCrn(performingCrn, representingCrn);
        if (userContext != null) {
            userContextRepository.delete(userContext);
        } else {
            //TODO: exception
            logger.warn("userContext does not exist");
        }
    }
}
