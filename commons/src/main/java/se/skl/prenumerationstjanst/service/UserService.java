package se.skl.prenumerationstjanst.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import se.skl.prenumerationstjanst.dto.UserDTO;
import se.skl.prenumerationstjanst.exception.user.UserCreationFailedException;
import se.skl.prenumerationstjanst.exception.user.UserNotFoundException;
import se.skl.prenumerationstjanst.model.User;
import se.skl.prenumerationstjanst.repository.UserRepository;
import se.skl.prenumerationstjanst.service.monitoring.UserMonitoringService;

/**
 * @author Martin Samuelsson
 */
public class UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TransformationService transformationService;

    @Autowired
    private UserMonitoringService userMonitoringService;

    /**
     * Gets user with userContexts removed
     * @param crn
     * @return
     */
    @Transactional(readOnly = true)
    public UserDTO getUser(String crn) {
        User user = userRepository.findByCrn(crn);
        if (user == null) {
            return null;
        }
        user.setUserContexts(null);
        return transformationService.toDTO(user);
    }

    @Transactional
    public UserDTO createUser(String crn, String phkPersonId) {
        User user = userRepository.findByCrn(crn);
        if (user == null) {
            user = userRepository.save(new User(crn, phkPersonId));
            userMonitoringService.monitorCreateUser(crn, null, null);
        } else {
            logger.warn("createUser: user already exists");
            throw new UserCreationFailedException("user already exists");
        }
        return transformationService.toDTO(user);
    }

    @Transactional
    public UserDTO updateUser(String crn, String newPhkPersonId) {
        User user = userRepository.findByCrn(crn);
        if (user != null) {
            user.setPhkPersonId(newPhkPersonId);
            user = userRepository.save(user);
            //TODO: monitor?
            return transformationService.toDTO(user);
        } else {
            throw new UserNotFoundException(crn);
        }
    }
}
