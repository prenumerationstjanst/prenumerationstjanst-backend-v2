package se.skl.prenumerationstjanst.service.mock;

import se.skl.prenumerationstjanst.dto.RecordDTO;
import se.skl.prenumerationstjanst.service.PhkInfoService;

/**
 * @author Martin Samuelsson
 */
public class PhkInfoServiceMockImpl implements PhkInfoService {

    @Override
    public RecordDTO getRecordForAuthToken(String authToken) {
        return new RecordDTO("dummy-record-id", "dummy-person-id", "dummy-record-name");
    }

    @Override
    public PHK_PERSON_STATUS checkPhkPersonId(String personId) {
        return PHK_PERSON_STATUS.ACTIVE;
    }

    @Override
    public String getShellRedirectUrl() {
        return null;
    }

}
