package se.skl.prenumerationstjanst.service.monitoring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import se.skl.prenumerationstjanst.dto.MonitoredUserEventDTO;
import se.skl.prenumerationstjanst.model.monitoring.MonitoredUserEventType;

import java.util.Map;

import static se.skl.prenumerationstjanst.PrenumerationstjanstConstants.MONITORING_QUEUE;

/**
 * Implementation of MonitoringService that will place a MonitoredUserEventDTO on a queue
 *
 * @author Martin Samuelsson
 */
public class ActiveMQMonitoringService implements MonitoringService {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Override
    public void monitorUserEvent(String userCrn, String userIpAddress, String correlationId, MonitoredUserEventType eventType,
                                 Map<String, String> eventDetails) {
        jmsTemplate.convertAndSend(MONITORING_QUEUE,
                new MonitoredUserEventDTO(userCrn, userIpAddress, correlationId, eventType, eventDetails));
    }
}
