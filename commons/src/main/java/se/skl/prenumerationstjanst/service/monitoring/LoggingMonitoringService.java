package se.skl.prenumerationstjanst.service.monitoring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import se.skl.prenumerationstjanst.model.monitoring.MonitoredUserEventType;

import java.util.Map;

/**
 * Logging implementation of MonitoringService, not for production use
 *
 * @author Martin Samuelsson
 */
public class LoggingMonitoringService implements MonitoringService {

    private static final Logger logger = LoggerFactory.getLogger(LoggingMonitoringService.class);

    @Override
    @Transactional
    public void monitorUserEvent(String userCrn, String userIpAddress, String correlationId, MonitoredUserEventType
            eventType, Map<String, String> eventDetails) {
        logger.debug("\n#\n# monitorUserEvent\n#\n# userCrn[{}]\n# userIpAddress[{}]\n# correlationId[{}]\n# " +
                "eventType[{}]\n# eventDetails[{}]\n#", userCrn, userIpAddress, correlationId, eventType, eventDetails);
    }
}
