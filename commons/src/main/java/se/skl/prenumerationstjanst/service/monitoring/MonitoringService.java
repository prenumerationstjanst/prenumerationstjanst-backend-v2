package se.skl.prenumerationstjanst.service.monitoring;

import se.skl.prenumerationstjanst.model.monitoring.MonitoredUserEventType;

import java.util.Map;

/**
 * @author Martin Samuelsson
 */
public interface MonitoringService {

    void monitorUserEvent(String userCrn, String userIpAddress, String correlationId, MonitoredUserEventType eventType,
                          Map<String, String> eventDetails);

}
