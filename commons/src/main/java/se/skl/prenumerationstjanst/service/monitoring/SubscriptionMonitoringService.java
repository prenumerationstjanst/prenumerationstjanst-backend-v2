package se.skl.prenumerationstjanst.service.monitoring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import se.skl.prenumerationstjanst.model.TransferType;
import se.skl.prenumerationstjanst.model.monitoring.MonitoredUserEventType;

import java.util.HashMap;
import java.util.Map;

/**
 * Subscription aware monitoring service. Responsible for filling the eventDetails map with
 * sensible subscription related data before calling MonitoringService.
 *
 * Used for monitoring subscription related events.
 *
 * The events monitored here are not considered critical. Exceptions will be caught and logged.
 *
 * @author Martin Samuelsson
 */
public class SubscriptionMonitoringService {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionMonitoringService.class);

    public enum UpdateType {
        TRANSFER_TYPES,
        HEALTHCARE_EXPIRATION,
        HEALTH_ACCOUNT_EXPIRATION
    }

    @Autowired
    private MonitoringService monitoringService;

    public void monitorCreateSubscription(String userCrn, String userIpAddress, String correlationId, String
            subscriptionId, String subscriptionCrn, TransferType transferType) {
        Map<String, String> eventDetails = new HashMap<>();
        eventDetails.put("subscriptionId", subscriptionId);
        eventDetails.put("subscriptionCrn", subscriptionCrn);
        eventDetails.put("transferType", transferType != null ? transferType.name() : null);
        try {
            monitoringService.monitorUserEvent(userCrn, userIpAddress, correlationId, MonitoredUserEventType
                    .USER_CREATES_SUBSCRIPTION, eventDetails);
        } catch (RuntimeException e) {
            logger.error("Unable to monitor event of type {}", MonitoredUserEventType.USER_CREATES_SUBSCRIPTION, e);
        }
    }

    public void monitorUpdateSubscription(String userCrn, String userIpAddress, String correlationId,
                                          String subscriptionId, String subscriptionCrn, UpdateType updateType,
                                          TransferType transferType) {
        Map<String, String> eventDetails = new HashMap<>();
        eventDetails.put("subscriptionId", subscriptionId);
        eventDetails.put("subscriptionCrn", subscriptionCrn);
        eventDetails.put("transferType", transferType != null ? transferType.name() : null);
        eventDetails.put("updateType", updateType.name());
        try {
            monitoringService.monitorUserEvent(userCrn, userIpAddress, correlationId, MonitoredUserEventType
                    .USER_UPDATES_SUBSCRIPTION, eventDetails);
        } catch (RuntimeException e) {
            logger.error("Unable to monitor event of type {}", MonitoredUserEventType.USER_UPDATES_SUBSCRIPTION, e);
        }
    }

    public void monitorRemoveSubscription(String userCrn, String userIpAddress, String correlationId,
                                          String subscriptionId, String subscriptionCrn) {
        Map<String, String> eventDetails = new HashMap<>();
        eventDetails.put("subscriptionId", subscriptionId);
        eventDetails.put("subscriptionCrn", subscriptionCrn);
        try {
            monitoringService.monitorUserEvent(userCrn, userIpAddress, correlationId, MonitoredUserEventType
                    .USER_REMOVES_SUBSCRIPTION, eventDetails);
        } catch (RuntimeException e) {
            logger.error("Unable to monitor event of type {}", MonitoredUserEventType.USER_REMOVES_SUBSCRIPTION, e);
        }
    }

    public void monitorRemoveIncompleteSubscription(String userCrn, String correlationId, String subscriptionId,
                                                    String subscriptionCrn) {
        Map<String, String> eventDetails = new HashMap<>();
        eventDetails.put("subscriptionId", subscriptionId);
        eventDetails.put("subscriptionCrn", subscriptionCrn);
        try {
            monitoringService.monitorUserEvent(userCrn, null, correlationId, MonitoredUserEventType
                    .SYSTEM_REMOVES_INCOMPLETE_SUBSCRIPTION, eventDetails);
        } catch (RuntimeException e) {
            logger.error("Unable to monitor event of type {}",
                    MonitoredUserEventType.SYSTEM_REMOVES_INCOMPLETE_SUBSCRIPTION, e);
        }
    }
}
