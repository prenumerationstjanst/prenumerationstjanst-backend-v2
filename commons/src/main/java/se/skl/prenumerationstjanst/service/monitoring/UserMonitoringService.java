package se.skl.prenumerationstjanst.service.monitoring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.skl.prenumerationstjanst.model.monitoring.MonitoredUserEventType;

import java.util.HashMap;
import java.util.Map;


/**
 * User aware monitoring service. Responsible for filling the eventDetails map with
 * sensible user related data before calling MonitoringService.
 *
 * Used for monitoring user related events.
 *
 * @author Martin Samuelsson
 */
public class UserMonitoringService {

    @Autowired
    private MonitoringService monitoringService;

    public void monitorCreateUser(String userCrn, String userIpAddress, String correlationId) {
        monitoringService.monitorUserEvent(userCrn, userIpAddress, correlationId,
                MonitoredUserEventType.USER_CREATES_ACCOUNT, null);
    }

    public void monitorAcceptTermsOfService(String userCrn, String userIpAddress, String correlationId, Long tosId,
                                            String tosText) {
        Map<String, String> eventDetails = new HashMap<>();
        eventDetails.put("termsOfServiceId", "" + tosId);
        eventDetails.put("termsOfServiceText", tosText);
        monitoringService.monitorUserEvent(userCrn, userIpAddress, correlationId,
                MonitoredUserEventType.USER_ACCEPTS_TERMS_OF_SERVICE, eventDetails);

    }

    public void monitorRemoveUser(String userCrn, String userIpAddress, String correlationId) {
        monitoringService.monitorUserEvent(userCrn, userIpAddress, correlationId,
                MonitoredUserEventType.USER_REMOVES_ACCOUNT, null);
    }
}
