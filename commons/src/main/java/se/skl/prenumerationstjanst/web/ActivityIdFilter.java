package se.skl.prenumerationstjanst.web;

import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import se.skl.prenumerationstjanst.PrenumerationstjanstConstants;
import se.skl.prenumerationstjanst.UuidGenerator;

import javax.servlet.*;
import java.io.IOException;

/**
 * Filter to generate correlationId and apply it to the MDC
 *
 * @author Martin Samuelsson
 */
public class ActivityIdFilter implements Filter {

    @Autowired
    private UuidGenerator activityIdGenerator;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String activityId = activityIdGenerator.generateSanitizedId();
        MDC.put(PrenumerationstjanstConstants.MDC_ACTIVITY_ID_KEY, activityId);
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
