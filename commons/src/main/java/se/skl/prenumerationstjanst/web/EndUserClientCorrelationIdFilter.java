package se.skl.prenumerationstjanst.web;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.MDC;
import se.skl.prenumerationstjanst.PrenumerationstjanstConstants;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter to generate an end user friendly correlationId and apply it to the MDC
 * as well as set it as a response http header for use in the UI
 *
 * @author Martin Samuelsson
 */
public class EndUserClientCorrelationIdFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        //short version suitable for end users (not guaranteed to be unique, but high enough probability for our case)
        String clientCorrelationId = RandomStringUtils.randomAlphanumeric(4) + "-" + RandomStringUtils.randomAlphanumeric(4);
        MDC.put(PrenumerationstjanstConstants.MDC_CLIENT_CORRELATION_ID_KEY, clientCorrelationId);
        httpServletResponse.setHeader(PrenumerationstjanstConstants.CLIENT_CORRELATION_ID_HEADER_KEY, clientCorrelationId);
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
