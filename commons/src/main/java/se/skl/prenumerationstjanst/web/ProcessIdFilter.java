package se.skl.prenumerationstjanst.web;

import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import se.skl.prenumerationstjanst.PrenumerationstjanstConstants;
import se.skl.prenumerationstjanst.UuidGenerator;

import javax.servlet.*;
import java.io.IOException;

/**
 * Filter to generate eventId and apply it to the MDC
 *
 * @author Martin Samuelsson
 */
public class ProcessIdFilter implements Filter {

    @Autowired
    UuidGenerator processIdGenerator;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String processId = processIdGenerator.generateSanitizedId();
        MDC.put(PrenumerationstjanstConstants.MDC_PROCESS_ID_KEY, processId);
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
