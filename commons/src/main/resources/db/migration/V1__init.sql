create table fetch_job (
    id varchar(255) not null,
    created timestamp,
    last_modified timestamp,
    fetch_finish_time datetime,
    status varchar(255) not null,
    transfer_job varchar(255),
    apigw_token varchar(255),
    primary key (id)
) ENGINE=InnoDB;

create table key_value (
    _key varchar(255) not null,
    session_id varchar(255) not null,
    created timestamp,
    last_modified timestamp,
    value varchar(1024),
    version integer not null,
    primary key (_key, session_id)
) ENGINE=InnoDB;

create table monitored_user_event (
    id bigint not null auto_increment,
    created timestamp,
    last_modified timestamp,
    correlation_id varchar(255),
    event_details longtext,
    event_type varchar(255),
    user_crn varchar(255),
    user_ip_address varchar(255),
    primary key (id)
) ENGINE=InnoDB;

create table push_job (
    id varchar(255) not null,
    created timestamp,
    last_modified timestamp,
    push_finish_time datetime,
    related_to varchar(255),
    source_id varchar(255),
    source_version integer not null,
    status varchar(255) not null,
    type_of_data varchar(255),
    transfer_job varchar(255),
    phk_person_id varchar(255),
    phk_record_id varchar(255),
    primary key (id)
) ENGINE=InnoDB;

create table transformation_job (
    id varchar(255) not null,
    created timestamp,
    last_modified timestamp,
    source_fetch_time datetime,
    source_version integer not null,
    status varchar(255) not null,
    transfer_job varchar(255),
    primary key (id)
) ENGINE=InnoDB;

create table subscription (
    id varchar(255) not null,
    created timestamp,
    last_modified timestamp,
    apigw_consent_expiration_date_time datetime,
    apigw_consent_scope varchar(255),
    apigw_consent_token_value varchar(255),
    scheduled_for_removal bit not null,
    user_context bigint,
    approved_transfer_type varchar(255) not null,
    primary key (id)
) ENGINE=InnoDB;

create table transfer_job (
    id varchar(255) not null,
    created timestamp,
    last_modified timestamp,
    end_date_time datetime not null,
    origin varchar(255) not null,
    source_system_hsa_id varchar(255),
    start_date_time datetime not null,
    user_crn varchar(255),
    representing_crn varchar(255),
    subscription_id varchar(255),
    transfer_type varchar(255) not null,
    primary key (id)
) ENGINE=InnoDB;

create table user (
    id bigint not null auto_increment,
    created timestamp,
    last_modified timestamp,
    crn varchar(255) not null,
    phk_person_id varchar(255) not null,
    primary key (id)
) ENGINE=InnoDB;

create table user_context (
    id bigint not null auto_increment,
    created timestamp,
    last_modified timestamp,
    representing_crn varchar(255) not null,
    phk_record_id varchar(255) not null,
    user bigint,
    primary key (id)
) ENGINE=InnoDB;

create table push_log (
    id varchar(255) not null,
    created timestamp,
    last_modified timestamp,
    source_id varchar(255) not null,
    source_fetch_time datetime not null,
    source_version integer not null,
    hash varchar(255) not null,
    subscription varchar(255) not null,
    primary key (id)
) ENGINE=InnoDB;

create table fetch_job_state (
    id bigint not null auto_increment,
    created timestamp,
    last_modified timestamp,
    status varchar(255) not null,
    fetch_job_id varchar(255),
    primary key (id)
) ENGINE=InnoDB;

alter table fetch_job_state
    add constraint FK_fetchjob
foreign key (fetch_job_id)
references fetch_job (id);

create table push_job_state (
    id bigint not null auto_increment,
    created timestamp,
    last_modified timestamp,
    status varchar(255) not null,
    push_job_id varchar(255),
    primary key (id)
) ENGINE=InnoDB;

alter table push_job_state
    add constraint FK_pushjob
foreign key (push_job_id)
references push_job (id);

create table transformation_job_state (
    id bigint not null auto_increment,
    created timestamp,
    last_modified timestamp,
    status varchar(255) not null,
    transformation_job_id varchar(255),
    primary key (id)
) ENGINE=InnoDB;

alter table transformation_job_state
    add constraint FK_transformationjob
foreign key (transformation_job_id)
references transformation_job (id);

alter table user
    add constraint UK_ggsg83wq8dv49dml39aj7rrcs  unique (crn);

alter table user
    add constraint UK_ggsg64wq8dv49dml39aj7absw  unique (phk_person_id);

alter table user_context
    add constraint UK_ggsg83wq8dv49dml39aj7rems  unique (phk_record_id);

alter table fetch_job
    add constraint FK_gaejwew6w6hckbq5co2imt4is
foreign key (transfer_job)
references transfer_job (id);

alter table push_job
    add constraint FK_9c1hnntpyvah7heka0oui6ywb
foreign key (transfer_job)
references transfer_job (id);

alter table transformation_job
    add constraint FK_transferjob
foreign key (transfer_job)
references transfer_job (id);

alter table subscription
    add constraint FK_1etar9rug3fp5apqccc1h76ti
foreign key (user_context)
references user_context (id);

alter table user_context
    add constraint FK_1etar9rd5h9qx4kqccc1h76ti
foreign key (user)
references user (id);
