package se.skl.prenumerationstjanst;

import org.slf4j.MDC;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.context.WebApplicationContext;
import se.skl.prenumerationstjanst.config.TestConfiguration;

@SpringBootApplication
//exclude classes marked with @TestConfiguration
@ComponentScan(excludeFilters = @ComponentScan.Filter(TestConfiguration.class))
@EnableScheduling
@EnableJms
@EnableCaching
@EnableJpaAuditing
@EnableAsync
public class PrenumerationstjanstITestApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(PrenumerationstjanstITestApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(PrenumerationstjanstITestApplication.class);
    }

    @Override
    protected WebApplicationContext run(SpringApplication application) {
        MDC.put(PrenumerationstjanstConstants.MDC_PROCESS_ID_KEY, processIdGenerator().generateSanitizedId());
        MDC.put(PrenumerationstjanstConstants.MDC_ACTIVITY_ID_KEY, activityIdGenerator().generateSanitizedId());
        return super.run(application);
    }

    @Bean
    public UuidGenerator processIdGenerator() {
        return new UuidGenerator("PID-" + UuidGenerator.getHostName());
    }

    @Bean
    public UuidGenerator activityIdGenerator() {
        return new UuidGenerator("AID-" + UuidGenerator.getHostName());
    }

}
