package se.skl.prenumerationstjanst.httpclient;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.MDC;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.mock.http.client.MockClientHttpRequest;
import se.skl.prenumerationstjanst.PrenumerationstjanstConstants;

import static org.junit.Assert.*;

/**
 * @author Martin Samuelsson
 */
public class CorrelationIdRequestInterceptorTest {

    private static final String HEADER_NAME = "mep";
    private static final String CORRELATION_ID = "corrId";

    private CorrelationIdRequestInterceptor interceptor;

    @Before
    public void setUp() throws Exception {
        interceptor = new CorrelationIdRequestInterceptor(HEADER_NAME);
    }

    @Test
    public void intercept() throws Exception {
        MDC.put(PrenumerationstjanstConstants.MDC_ACTIVITY_ID_KEY, CORRELATION_ID);
        HttpRequest request = new MockClientHttpRequest();
        assertNull(request.getHeaders().get(HEADER_NAME));
        interceptor.intercept(request, null, Mockito.mock(ClientHttpRequestExecution.class));
        assertNotNull(request.getHeaders().get(HEADER_NAME));
    }

    @Test
    public void interceptNoCorrelationIdSet() throws Exception {
        HttpRequest request = new MockClientHttpRequest();
        assertNull(request.getHeaders().get(HEADER_NAME));
        interceptor.intercept(request, null, Mockito.mock(ClientHttpRequestExecution.class));
        assertNull(request.getHeaders().get(HEADER_NAME));
    }

}