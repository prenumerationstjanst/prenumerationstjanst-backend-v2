package se.skl.prenumerationstjanst.jms;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import se.skl.prenumerationstjanst.PrenumerationstjanstConstants;
import se.skl.prenumerationstjanst.UuidGenerator;

import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static se.skl.prenumerationstjanst.PrenumerationstjanstConstants.*;

/**
 * @author Martin Samuelsson
 */
@RunWith(MockitoJUnitRunner.class)
public class IdAddingMessageConverterTest {

    private static final Logger logger = LoggerFactory.getLogger(IdAddingMessageConverterTest.class);

    public static final String DUMMY_TEXT = "";
    public static final String PROCESS_ID = "processId";
    public static final String PREVIOUS_ACTIVITY_ID = "previousActivityId";
    public static final String ACTIVITY_ID = "activityId";
    public static final String GENERATED_ACTIVITY_ID = "generatedActivityId";

    @Mock
    private UuidGenerator uuidGenerator;

    @Mock
    private Session session;

    @Mock
    private TextMessage textMessage;

    @InjectMocks
    private IdAddingMessageConverter idAddingMessageConverter;

    @Before
    public void setUp() throws Exception {
        MDC.clear();
        when(session.createTextMessage(DUMMY_TEXT))
                .thenReturn(textMessage);
    }

    @After
    public void tearDown() throws Exception {
        MDC.clear();
    }

    @Test
    public void toMessage() throws Exception {
        MDC.put(MDC_PROCESS_ID_KEY, PROCESS_ID);
        MDC.put(PrenumerationstjanstConstants.MDC_PREVIOUS_ACTIVITY_ID_KEY, PREVIOUS_ACTIVITY_ID);
        MDC.put(PrenumerationstjanstConstants.MDC_ACTIVITY_ID_KEY, ACTIVITY_ID);
        Message message = idAddingMessageConverter.toMessage(DUMMY_TEXT, session);
        assertSame(textMessage, message);
        verify(textMessage).setStringProperty(eq(MDC_PROCESS_ID_KEY), eq(PROCESS_ID));
        verify(textMessage).setStringProperty(eq(MDC_ACTIVITY_ID_KEY), eq(ACTIVITY_ID));
        verifyNoMoreInteractions(textMessage); //do not want previous activity id in toMessage
    }

    @Test
    public void fromMessage() throws Exception {
        when(uuidGenerator.generateSanitizedId()).thenReturn(GENERATED_ACTIVITY_ID);
        when(textMessage.getStringProperty(eq(MDC_PROCESS_ID_KEY))).thenReturn(PROCESS_ID);
        when(textMessage.getStringProperty(eq(MDC_ACTIVITY_ID_KEY))).thenReturn(ACTIVITY_ID);
        idAddingMessageConverter.fromMessage(textMessage);
        assertEquals(PROCESS_ID, MDC.get(MDC_PROCESS_ID_KEY));
        assertEquals(ACTIVITY_ID, MDC.get(MDC_PREVIOUS_ACTIVITY_ID_KEY));
        assertEquals(GENERATED_ACTIVITY_ID, MDC.get(MDC_ACTIVITY_ID_KEY));
    }
 
}