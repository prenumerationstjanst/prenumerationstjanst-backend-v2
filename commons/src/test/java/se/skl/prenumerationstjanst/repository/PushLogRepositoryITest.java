package se.skl.prenumerationstjanst.repository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import se.skl.prenumerationstjanst.PrenumerationstjanstITestApplication;
import se.skl.prenumerationstjanst.model.PushLog;
import se.skl.prenumerationstjanst.model.Subscription;
import se.skl.prenumerationstjanst.model.TransferType;

import java.time.LocalDateTime;

import static org.junit.Assert.*;

/**
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = PrenumerationstjanstITestApplication.class)
@DirtiesContext
@ActiveProfiles("test")
@Transactional
public class PushLogRepositoryITest {

    @Autowired
    private PushLogRepository pushLogRepository;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    public static final String SOURCE_ID = "sourceId";
    public static final int SOURCE_VERSION = 1;
    public static final LocalDateTime SOURCE_FETCH_TIME = LocalDateTime.now();
    public static final String HASH = "hash";

    @Before
    public void setUp() throws Exception {
        subscriptionRepository.deleteAll();
        pushLogRepository.deleteAll();
    }

    @After
    public void destroy() throws Exception {
        subscriptionRepository.deleteAll();
        pushLogRepository.deleteAll();
    }

    @Test
    public void testSameOrNewerPushed_SameAll() throws Exception {
        assertEquals(0, pushLogRepository.count());
        String subscriptionId = createAndInsertSubscription().getId();
        createAndInsertPushLog(subscriptionId, HASH, SOURCE_FETCH_TIME, SOURCE_ID, SOURCE_VERSION);
        assertEquals(1, pushLogRepository.count());
        assertTrue(pushLogRepository.sameOrNewerPushed(subscriptionId, SOURCE_FETCH_TIME, SOURCE_ID, SOURCE_VERSION, HASH));
    }

    @Test
    public void testSameOrNewerPushed_OlderSourceFetchTime() throws Exception {
        assertEquals(0, pushLogRepository.count());
        String subscriptionId = createAndInsertSubscription().getId();
        createAndInsertPushLog(subscriptionId, HASH, SOURCE_FETCH_TIME, SOURCE_ID, SOURCE_VERSION);
        assertEquals(1, pushLogRepository.count());
        assertTrue(pushLogRepository.sameOrNewerPushed(subscriptionId, SOURCE_FETCH_TIME.minusSeconds(1), SOURCE_ID,
                SOURCE_VERSION, HASH));
    }

    @Test
    public void testSameOrNewerPushed_OlderSourceFetchTimeNewHash() throws Exception {
        assertEquals(0, pushLogRepository.count());
        String subscriptionId = createAndInsertSubscription().getId();
        createAndInsertPushLog(subscriptionId, HASH, SOURCE_FETCH_TIME, SOURCE_ID, SOURCE_VERSION);
        assertEquals(1, pushLogRepository.count());
        assertTrue(pushLogRepository.sameOrNewerPushed(subscriptionId, SOURCE_FETCH_TIME.minusSeconds(1), SOURCE_ID,
                SOURCE_VERSION, HASH + "mep"));
    }

    @Test
    public void testSameOrNewerPushed_NewerSourceFetchTimeSameHash() throws Exception {
        assertEquals(0, pushLogRepository.count());
        String subscriptionId = createAndInsertSubscription().getId();
        createAndInsertPushLog(subscriptionId, HASH, SOURCE_FETCH_TIME, SOURCE_ID, SOURCE_VERSION);
        assertEquals(1, pushLogRepository.count());
        assertTrue(pushLogRepository.sameOrNewerPushed(subscriptionId, SOURCE_FETCH_TIME.plusSeconds(1), SOURCE_ID,
                SOURCE_VERSION, HASH));
    }

    @Test
    public void testSameOrNewerPushed_NewerSourceFetchTimeNewHash() throws Exception {
        assertEquals(0, pushLogRepository.count());
        String subscriptionId = createAndInsertSubscription().getId();
        createAndInsertPushLog(subscriptionId, HASH, SOURCE_FETCH_TIME, SOURCE_ID, SOURCE_VERSION);
        assertEquals(1, pushLogRepository.count());
        assertFalse(pushLogRepository.sameOrNewerPushed(subscriptionId, SOURCE_FETCH_TIME.plusSeconds(1), SOURCE_ID,
                SOURCE_VERSION, HASH + "mep"));
    }

    @Test
    public void testSameOrNewerPushed_SameSourceFetchTimeNewHash() throws Exception {
        assertEquals(0, pushLogRepository.count());
        String subscriptionId = createAndInsertSubscription().getId();
        createAndInsertPushLog(subscriptionId, HASH, SOURCE_FETCH_TIME, SOURCE_ID, SOURCE_VERSION);
        assertEquals(1, pushLogRepository.count());
        assertFalse(pushLogRepository.sameOrNewerPushed(subscriptionId, SOURCE_FETCH_TIME, SOURCE_ID, SOURCE_VERSION,
                HASH + "mep"));
    }

    @Test
    public void testSameOrNewerPushed_SameSubscriptionSameSourceFetchTimeOtherSourceId() throws Exception {
        assertEquals(0, pushLogRepository.count());
        String subscriptionId = createAndInsertSubscription().getId();
        createAndInsertPushLog(subscriptionId, HASH, SOURCE_FETCH_TIME, SOURCE_ID, SOURCE_VERSION);
        assertEquals(1, pushLogRepository.count());
        assertFalse(pushLogRepository.sameOrNewerPushed(subscriptionId, SOURCE_FETCH_TIME, SOURCE_ID + "mep", SOURCE_VERSION, HASH));
    }

    @Test
    public void testSameOrNewerPushed_OtherSubscriptionSameSourceFetchTimeSameSourceId() throws Exception {
        assertEquals(0, pushLogRepository.count());
        String subscriptionId = createAndInsertSubscription().getId();
        createAndInsertPushLog(subscriptionId, HASH, SOURCE_FETCH_TIME, SOURCE_ID, SOURCE_VERSION);
        assertEquals(1, pushLogRepository.count());
        assertFalse(pushLogRepository.sameOrNewerPushed(subscriptionId + "mep", SOURCE_FETCH_TIME, SOURCE_ID, SOURCE_VERSION, HASH));
    }

    private Subscription createAndInsertSubscription() {
        return subscriptionRepository.save(new Subscription(null, TransferType.VACCINATION_HISTORY));
    }

    private PushLog createAndInsertPushLog(String subscriptionId, String hash, LocalDateTime sourceFetchTime, String sourceId,
                                           int sourceVersion) {
        Subscription sub = subscriptionRepository.findOne(subscriptionId);
        PushLog pushLog = pushLogRepository.save(new PushLog(sub, sourceId, sourceFetchTime, sourceVersion, hash));
        sub.getPushLogs().add(pushLog);
        subscriptionRepository.save(sub);
        return pushLog;
    }
}
