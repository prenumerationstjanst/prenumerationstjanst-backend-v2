package se.skl.prenumerationstjanst.repository;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import se.skl.prenumerationstjanst.PrenumerationstjanstITestApplication;
import se.skl.prenumerationstjanst.model.*;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

/**
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = PrenumerationstjanstITestApplication.class)
@DirtiesContext
@ActiveProfiles("test")
public class SubscriptionRepositoryITest {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionRepositoryITest.class);
    public static final String USER_CRN = "191212121212";
    private static final TransferType DEFAULT_TRANSFER_TYPE = TransferType.VACCINATION_HISTORY;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserContextRepository userContextRepository;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Before
    public void setUp() throws Exception {
        userRepository.deleteAll();
        userContextRepository.deleteAll();
        subscriptionRepository.deleteAll();
    }

    @After
    public void tearDown() throws Exception {
        subscriptionRepository.deleteAll();
        userContextRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    public void findByUser() throws Exception {

    }

    @Test
    public void findByUserAndScheduledForRemovalFalse() throws Exception {
        assertEquals(0, subscriptionRepository.count());
        createAndSaveInitialSubscription(); //scheduledForRemoval false by default
        UserContext userContext = userContextRepository.findByUserCrnAndRepresentingCrn(USER_CRN, USER_CRN);
        Assert.assertNotNull(userContext);
        assertEquals(1, subscriptionRepository.count());
        List<Subscription> subscriptions = subscriptionRepository
                .findByUserContextAndScheduledForRemovalFalse(userContext);
        Assert.assertEquals(1, subscriptions.size());
    }

    @Test
    public void findByUserAndScheduledForRemovalFalse_ScheduledForRemovalIsTrue() throws Exception {
        assertEquals(0, subscriptionRepository.count());
        Subscription subscription = createAndSaveInitialSubscription();
        subscription.setScheduledForRemoval(true);
        subscriptionRepository.save(subscription);
        UserContext userContext = userContextRepository.findByUserCrnAndRepresentingCrn(USER_CRN, USER_CRN);
        Assert.assertNotNull(userContext);
        assertEquals(1, subscriptionRepository.count());
        List<Subscription> subscriptions = subscriptionRepository
                .findByUserContextAndScheduledForRemovalFalse(userContext);
        Assert.assertEquals(0, subscriptions.size());
    }

    @Test
    public void findByApigwConsentTokenValueAndScheduledForRemovalFalse() throws Exception {
        Subscription subscription = createAndSaveInitialSubscription();
        ApigwConsent apigwConsent = new ApigwConsent();
        apigwConsent.setTokenValue("mep");
        subscription.setApigwConsent(apigwConsent);
        subscriptionRepository.save(subscription);
        assertEquals(1, subscriptionRepository.count());
        Subscription subWithApigwToken =
                subscriptionRepository.findByApigwConsentTokenValueAndScheduledForRemovalFalse("mep");
        Assert.assertNotNull(subWithApigwToken);
    }

    @Test
    public void findByApigwConsentTokenValueAndScheduledForRemovalFalse_ScheduledForRemovalIsTrue() throws Exception {
        Subscription subscription = createAndSaveInitialSubscription();
        ApigwConsent apigwConsent = new ApigwConsent();
        apigwConsent.setTokenValue("mep");
        subscription.setApigwConsent(apigwConsent);
        subscription.setScheduledForRemoval(true);
        subscriptionRepository.save(subscription);
        assertEquals(1, subscriptionRepository.count());
        Subscription subWithApigwToken =
                subscriptionRepository.findByApigwConsentTokenValueAndScheduledForRemovalFalse("mep");
        assertNull(subWithApigwToken);
    }

    @Test
    public void findByCreatedBeforeAndApigwConsentIsNull() throws Exception {
        Subscription subscription = createAndSaveInitialSubscription();
        LocalDateTime created = subscription.getCreated();
        assertEquals(1, subscriptionRepository.count());
        List<Subscription> subs = subscriptionRepository
                .findByCreatedBeforeAndApigwConsentIsNull(created.plusSeconds(1));
        Assert.assertEquals(1, subs.size());
    }

    @Test
    public void findByCreatedBeforeAndApigwConsentIsNull_WithNoConsentButBadDate() throws Exception {
        Subscription subscription = createAndSaveInitialSubscription();
        subscriptionRepository.save(subscription);
        LocalDateTime created = subscription.getCreated();
        assertEquals(1, subscriptionRepository.count());
        List<Subscription> subs = subscriptionRepository
                .findByCreatedBeforeAndApigwConsentIsNull(created.minusSeconds(1));
        Assert.assertEquals(0, subs.size());
    }

    @Test
    public void findByCreatedBeforeAndApigwConsentIsNull_WithApigwConsentAndOkDate() throws Exception {
        Subscription subscription = createAndSaveInitialSubscription();
        ApigwConsent apigwConsent = new ApigwConsent();
        apigwConsent.setScope("mep");
        subscription.setApigwConsent(apigwConsent);
        subscriptionRepository.save(subscription);
        LocalDateTime created = subscription.getCreated();
        assertEquals(1, subscriptionRepository.count());
        List<Subscription> subs = subscriptionRepository.findByCreatedBeforeAndApigwConsentIsNull(created.plusSeconds(1));
        Assert.assertEquals(0, subs.size());
    }

    @Test
    public void findByCreatedBeforeAndApigwConsentIsNull_WithApigwConsentButBadDate() throws Exception {
        Subscription subscription = createAndSaveInitialSubscription();
        ApigwConsent apigwConsent = new ApigwConsent();
        apigwConsent.setScope("mep");
        subscription.setApigwConsent(apigwConsent);
        subscriptionRepository.save(subscription);
        LocalDateTime created = subscription.getCreated();
        assertEquals(1, subscriptionRepository.count());
        List<Subscription> subs = subscriptionRepository
                .findByCreatedBeforeAndApigwConsentIsNull(created.minusSeconds(1));
        Assert.assertEquals(0, subs.size());
    }

    @Test
    public void deleteByScheduledForRemovalTrueAndLastModifiedBefore_OKAgeButNotScheduledForRemoval() throws Exception {
        Subscription subscription = createAndSaveInitialSubscription();
        LocalDateTime lastModified = subscription.getLastModified();
        assertEquals(1, subscriptionRepository.count());
        subscriptionRepository.deleteByScheduledForRemovalTrueAndLastModifiedBefore(lastModified.plusSeconds(1));
        assertEquals(1, subscriptionRepository.count()); //should _not_ be removed
    }

    @Test
    public void deleteByScheduledForRemovalTrueAndLastModifiedBefore_BadAgeAndScheduledForRemoval() throws Exception {
        Subscription subscription = createAndSaveInitialSubscription();
        subscription.setScheduledForRemoval(true);
        subscription = subscriptionRepository.save(subscription);
        LocalDateTime lastModified = subscription.getLastModified();
        assertEquals(1, subscriptionRepository.count());
        subscriptionRepository.deleteByScheduledForRemovalTrueAndLastModifiedBefore(lastModified.minusSeconds(1));
        assertEquals(1, subscriptionRepository.count()); //should _not_ be removed
    }

    @Test
    public void deleteByScheduledForRemovalTrueAndLastModifiedBefore_OKAgeAndScheduledForRemoval() throws Exception {
        Subscription subscription = createAndSaveInitialSubscription();
        subscription.setScheduledForRemoval(true);
        subscription = subscriptionRepository.save(subscription);
        LocalDateTime lastModified = subscription.getLastModified();
        assertEquals(1, subscriptionRepository.count());
        subscriptionRepository.deleteByScheduledForRemovalTrueAndLastModifiedBefore(lastModified.plusSeconds(1));
        assertEquals(0, subscriptionRepository.count()); //should be removed
    }

    @Test
    public void countByScheduledForRemovalTrueAndLastModifiedBefore_OKAgeButNotScheduledForRemoval() throws Exception {
        Subscription subscription = createAndSaveInitialSubscription();
        LocalDateTime lastModified = subscription.getLastModified();
        assertEquals(1, subscriptionRepository.count());
        assertEquals((Long)0L,
                subscriptionRepository.countByScheduledForRemovalTrueAndLastModifiedBefore(lastModified.plusSeconds(1)));
    }

    @Test
    public void countByScheduledForRemovalTrueAndLastModifiedBefore_BadAgeAndScheduledForRemoval() throws Exception {
        Subscription subscription = createAndSaveInitialSubscription();
        subscription.setScheduledForRemoval(true);
        subscription = subscriptionRepository.save(subscription);
        LocalDateTime lastModified = subscription.getLastModified();
        assertEquals(1, subscriptionRepository.count());
        assertEquals((Long)0L,
                subscriptionRepository.countByScheduledForRemovalTrueAndLastModifiedBefore(lastModified.minusSeconds(1)));
    }

    @Test
    public void countByScheduledForRemovalTrueAndLastModifiedBefore_OKAgeAndScheduledForRemoval() throws Exception {
        Subscription subscription = createAndSaveInitialSubscription();
        subscription.setScheduledForRemoval(true);
        subscription = subscriptionRepository.save(subscription);
        LocalDateTime lastModified = subscription.getLastModified();
        assertEquals(1, subscriptionRepository.count());
        assertEquals((Long)1L,
                subscriptionRepository.countByScheduledForRemovalTrueAndLastModifiedBefore(lastModified.plusSeconds(1)));
    }

    @Test
    public void findForUserContextByTransferType() throws Exception {
        Subscription initialSubscription = createAndSaveInitialSubscription();
        Subscription found = subscriptionRepository.findForUserContextByTransferType(USER_CRN,
                USER_CRN, DEFAULT_TRANSFER_TYPE);
        logger.debug("{} vs {}", initialSubscription.getId(), found.getId());
        assertEquals(initialSubscription.getId(), found.getId());
    }

    @Test
    public void findForUserContextByTransferType_BadTransferType() throws Exception {
        createAndSaveInitialSubscription();
        assertNull(subscriptionRepository.findForUserContextByTransferType(USER_CRN,
                USER_CRN, TransferType.MATERNITY_MEDICAL_HISTORY));
    }

    @Test
    public void findForUserContextByTransferType_ScheduledForRemovalShouldNotBeReturned() throws Exception {
        Subscription initialSubscription = createAndSaveInitialSubscription();
        initialSubscription.setScheduledForRemoval(true);
        subscriptionRepository.save(initialSubscription);
        assertNull(subscriptionRepository.findForUserContextByTransferType(USER_CRN,
                USER_CRN, DEFAULT_TRANSFER_TYPE));
    }

    @Test
    public void findForUserContextByTransferType_UnknownPerformingCrn() throws Exception {
        createAndSaveInitialSubscription();
        assertNull(subscriptionRepository.findForUserContextByTransferType("101010101010",
                USER_CRN, DEFAULT_TRANSFER_TYPE));
    }

    @Test
    public void findForUserContextByTransferType_UnknownRepresentingCrn() throws Exception {
        createAndSaveInitialSubscription();
        assertNull(subscriptionRepository.findForUserContextByTransferType(USER_CRN,
                "101010101010", DEFAULT_TRANSFER_TYPE));
    }



    private Subscription createAndSaveInitialSubscription() {
        UserContext uc = new UserContext(userRepository.save(new User(USER_CRN, "phkPersonId")), USER_CRN, "phkRecordId");
        uc = userContextRepository.save(uc);
        Subscription sub = new Subscription(uc, DEFAULT_TRANSFER_TYPE);
        sub.setUserContext(uc);
        return subscriptionRepository.save(sub);
    }
}