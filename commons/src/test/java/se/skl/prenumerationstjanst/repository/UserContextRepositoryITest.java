package se.skl.prenumerationstjanst.repository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import se.skl.prenumerationstjanst.PrenumerationstjanstITestApplication;
import se.skl.prenumerationstjanst.model.User;
import se.skl.prenumerationstjanst.model.UserContext;

import static org.junit.Assert.*;

/**
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = PrenumerationstjanstITestApplication.class)
@DirtiesContext
@ActiveProfiles("test")
public class UserContextRepositoryITest {

    private static final String PERFORMING_CRN = "191212121212";
    private static final String PHK_PERSON_ID = "phkPersonId";
    private static final String REPRESENTING_CRN = "191212121212";
    private static final String PHK_RECORD_ID = "phkRecordId";
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserContextRepository userContextRepository;

    private UserContext savedUserContext;

    @Before
    public void setUp() throws Exception {
        userRepository.deleteAll();
        userContextRepository.deleteAll();
        savedUserContext = userContextRepository
                .save(new UserContext(userRepository.save(new User(PERFORMING_CRN, PHK_PERSON_ID)),
                REPRESENTING_CRN, PHK_RECORD_ID));
    }

    @After
    public void tearDown() throws Exception {
        userRepository.deleteAll();
        userContextRepository.deleteAll();
    }

    @Test
    public void findByUserCrnAndRepresentingCrn() throws Exception {
        UserContext userContext = userContextRepository
                .findByUserCrnAndRepresentingCrn(PERFORMING_CRN, REPRESENTING_CRN);
        assertNotNull(userContext);
        assertEquals(REPRESENTING_CRN, userContext.getRepresentingCrn());
        assertNotNull(userContext.getUser());
        assertEquals(PERFORMING_CRN, userContext.getUser().getCrn());
    }

    @Test
    public void findByUserCrnAndRepresentingCrnNotFound() throws Exception {
        UserContext userContext = userContextRepository
                .findByUserCrnAndRepresentingCrn(PERFORMING_CRN, "1");
        assertNull(userContext);
    }

    @Test
    public void testExists() throws Exception {
        assertTrue(userContextRepository.exists(PERFORMING_CRN, REPRESENTING_CRN));
    }

    @Test
    public void testExistsFalse() throws Exception {
        assertFalse(userContextRepository.exists(PERFORMING_CRN, "1"));
    }

}