package se.skl.prenumerationstjanst.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.AssertionErrors;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;
import se.skl.prenumerationstjanst.config.TestConfiguration;
import se.skl.prenumerationstjanst.dto.ApigwConsentDTO;
import se.skl.prenumerationstjanst.model.TransferType;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

/**
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApigwConsentServiceTest.TestConfig.class)
public class ApigwConsentServiceTest {

    private static final Logger logger = LoggerFactory.getLogger(ApigwConsentServiceTest.class);
    public static final String BASE_URI = "http://mep";
    public static final String TRUSTED_CREATE_TOKEN_PATH = "/tct";
    public static final String TRUSTED_REVOKE_TOKEN_PATH = "/trt";
    public static final String USER_CRN = "userCrn";
    public static final String CHILD_CRN = "childCrn";
    public static final String TOKEN_VALUE = "d48f3e2d-7faf-49b2-acd3-6f8381722b39";

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ApigwConsentService apigwConsentService;

    private MockRestServiceServer mockServer;

    @Before
    public void setUp() throws Exception {
        ReflectionTestUtils.setField(apigwConsentService, "baseUri", BASE_URI);
        ReflectionTestUtils.setField(apigwConsentService, "trustedCreateTokenPath", TRUSTED_CREATE_TOKEN_PATH);
        ReflectionTestUtils.setField(apigwConsentService, "trustedRevokeTokenPath", TRUSTED_REVOKE_TOKEN_PATH);
        mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    @Test
    public void testGetConsent_userOnly() throws Exception {
        int expiresIn = 60; //seconds
        long nowMillis = System.currentTimeMillis();
        mockServer.expect(requestTo(BASE_URI + TRUSTED_CREATE_TOKEN_PATH))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentType(MediaType.APPLICATION_FORM_URLENCODED))
                //no cleaner way to get form data
                .andExpect(request -> {
                    String body = ((MockHttpOutputMessage) request).getBodyAsString();
                    List<String> params = Arrays.asList(body.split("&"));
                    AssertionErrors.assertEquals("expected 2 parameters", 2, params.size());
                    AssertionErrors.assertTrue("expected body to contain citizen=userCrn",
                            params.contains("citizen=userCrn"));
                    AssertionErrors.assertTrue("expected body to contain scope=" +
                            TransferType.MATERNITY_MEDICAL_HISTORY.getScope(),
                            params.contains("scope=" + TransferType.MATERNITY_MEDICAL_HISTORY.getScope()));
                })
                .andRespond(
                        withSuccess("{\"access_token\": \"" + TOKEN_VALUE + "\", " +
                                "\"token_type\": \"Bearer\", " +
                                "\"expires_in\": " + expiresIn + ", " +
                                "\"scope\": \"" + TransferType.MATERNITY_MEDICAL_HISTORY.getScope() + "\", " +
                                "\"issue_date\": " + nowMillis + "}", MediaType.APPLICATION_JSON));

        ApigwConsentDTO response = apigwConsentService.getConsent(USER_CRN, null, TransferType.MATERNITY_MEDICAL_HISTORY);
        assertEquals(TransferType.MATERNITY_MEDICAL_HISTORY.getScope(), response.getScope());
        assertEquals(TOKEN_VALUE, response.getTokenValue());
        LocalDateTime expectedTokenExpiration = LocalDateTime
                .ofInstant(Instant.ofEpochMilli(nowMillis), ZoneId.systemDefault())
                .plusSeconds(expiresIn);
        assertEquals(expectedTokenExpiration, response.getExpirationDateTime());
        mockServer.verify();
    }

    @Test
    public void testGetConsent_userOnly_longExpiresIn() throws Exception {
        Long expiresIn = (long) Integer.MAX_VALUE + 1; //seconds
        long nowMillis = System.currentTimeMillis();
        mockServer.expect(requestTo(BASE_URI + TRUSTED_CREATE_TOKEN_PATH))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentType(MediaType.APPLICATION_FORM_URLENCODED))
                //no cleaner way to get form data
                .andExpect(request -> {
                    String body = ((MockHttpOutputMessage) request).getBodyAsString();
                    List<String> params = Arrays.asList(body.split("&"));
                    AssertionErrors.assertEquals("expected 2 parameters", 2, params.size());
                    AssertionErrors.assertTrue("expected body to contain citizen=userCrn",
                            params.contains("citizen=userCrn"));
                    AssertionErrors.assertTrue("expected body to contain scope=" +
                                    TransferType.MATERNITY_MEDICAL_HISTORY.getScope(),
                            params.contains("scope=" + TransferType.MATERNITY_MEDICAL_HISTORY.getScope()));
                })
                .andRespond(
                        withSuccess("{\"access_token\": \"" + TOKEN_VALUE + "\", " +
                                "\"token_type\": \"Bearer\", " +
                                "\"expires_in\": " + expiresIn + ", " +
                                "\"scope\": \"" + TransferType.MATERNITY_MEDICAL_HISTORY.getScope() + "\", " +
                                "\"issue_date\": " + nowMillis + "}", MediaType.APPLICATION_JSON));

        ApigwConsentDTO response = apigwConsentService.getConsent(USER_CRN, null, TransferType.MATERNITY_MEDICAL_HISTORY);
        assertEquals(TransferType.MATERNITY_MEDICAL_HISTORY.getScope(), response.getScope());
        assertEquals(TOKEN_VALUE, response.getTokenValue());
        LocalDateTime expectedTokenExpiration = LocalDateTime
                .ofInstant(Instant.ofEpochMilli(nowMillis), ZoneId.systemDefault())
                .plusSeconds(expiresIn);
        assertEquals(expectedTokenExpiration, response.getExpirationDateTime());
        mockServer.verify();
    }

    @Test
    public void testGetConsent_userAndChild() throws Exception {
        int expiresIn = 60; //seconds
        long nowMillis = System.currentTimeMillis();
        mockServer.expect(requestTo(BASE_URI + TRUSTED_CREATE_TOKEN_PATH))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentType(MediaType.APPLICATION_FORM_URLENCODED))
                //no cleaner way to get form data
                .andExpect(request -> {
                    String body = ((MockHttpOutputMessage) request).getBodyAsString();
                    List<String> params = Arrays.asList(body.split("&"));
                    AssertionErrors.assertEquals("expected 3 parameters", 3, params.size());
                    AssertionErrors.assertTrue("expected body to contain citizen=" + CHILD_CRN,
                            params.contains("citizen=" + CHILD_CRN));
                    AssertionErrors.assertTrue("expected body to contain legalGuardian=" + USER_CRN,
                            params.contains("legalGuardian=" + USER_CRN));
                    AssertionErrors.assertTrue("expected body to contain scope=" + TransferType.MATERNITY_MEDICAL_HISTORY.getScope(),
                            params.contains("scope=" + TransferType.MATERNITY_MEDICAL_HISTORY.getScope()));
                })
                .andRespond(
                        withSuccess("{\"access_token\": \"" + TOKEN_VALUE + "\", " +
                                "\"token_type\": \"Bearer\", " +
                                "\"expires_in\": " + expiresIn + ", " +
                                "\"scope\": \"" + TransferType.MATERNITY_MEDICAL_HISTORY.getScope() + "\", " +
                                "\"issue_date\": " + nowMillis + "}", MediaType.APPLICATION_JSON));

        ApigwConsentDTO response = apigwConsentService.getConsent(USER_CRN, CHILD_CRN, TransferType.MATERNITY_MEDICAL_HISTORY);
        assertEquals(TransferType.MATERNITY_MEDICAL_HISTORY.getScope(), response.getScope());
        assertEquals(TOKEN_VALUE, response.getTokenValue());
        LocalDateTime expectedTokenExpiration = LocalDateTime
                .ofInstant(Instant.ofEpochMilli(nowMillis), ZoneId.systemDefault())
                .plusSeconds(expiresIn);
        assertEquals(expectedTokenExpiration, response.getExpirationDateTime());
        mockServer.verify();
    }

    @Test
    public void testDeleteConsent() throws Exception {
        mockServer.expect(requestTo(BASE_URI + TRUSTED_REVOKE_TOKEN_PATH))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().contentType(MediaType.APPLICATION_FORM_URLENCODED))
                //no cleaner way to get form data
                .andExpect(request -> {
                    String body = ((MockHttpOutputMessage) request).getBodyAsString();
                    logger.debug("delete body: {}", body);
                    List<String> params = Arrays.asList(body.split("&"));
                    AssertionErrors.assertEquals("expected 1 parameter", 1, params.size());
                    AssertionErrors.assertTrue("expected body to contain tokenValue=" + TOKEN_VALUE,
                            params.contains("tokenValue=" + TOKEN_VALUE));
                })
                .andRespond(withSuccess());
        apigwConsentService.deleteConsent(TOKEN_VALUE);
        mockServer.verify();
    }

    @Configuration
    @TestConfiguration
    public static class TestConfig {

        @Bean(name = "authServerRestTemplate")
        @Primary
        public RestTemplate restTemplate() {
            return new RestTemplate();
        }

        @Bean
        public ApigwConsentService apigwConsentService() {
            return new ApigwConsentService();
        }
    }
}