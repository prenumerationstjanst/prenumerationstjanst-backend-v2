package se.skl.prenumerationstjanst.service;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.*;

/**
 * @author Martin Samuelsson
 */
public class HashServiceSHA256XMLImplTest {

    private static final Logger logger = LoggerFactory.getLogger(HashServiceSHA256XMLImplTest.class);

    private HashService hashService;

    private HashService controlHashService;

    @Before
    public void setUp() throws Exception {
        hashService = new HashServiceSHA256XMLImpl();
        controlHashService = new AbstractHashServiceSHA256Impl() {
            @Override
            protected String getHashableString(String input) {
                return input;
            }
        };
    }

    @Test
    public void generateHashFromPlainXml() throws Exception {
        String control = controlHashService.calculateHash("foobarbaz");
        String input = "<root><e>foo</e><e>bar</e><e>baz</e></root>";
        String hash = hashService.calculateHash(input);
        assertNotNull(hash);
        assertFalse(hash.trim().isEmpty());
        assertEquals(control, hash);
    }

    @Test
    public void generateHashFromFormattedXml() throws Exception {
        String control = controlHashService.calculateHash("foobarbaz");
        String input = "<root>\n\t<e>foo</e>\n\t<e>bar</e>\n\t<e>baz</e>\n</root>";
        String hash = hashService.calculateHash(input);
        assertNotNull(hash);
        assertFalse(hash.trim().isEmpty());
        assertEquals(control, hash);
    }

    @Test
    public void generateHashFromXmlWithAttribute() throws Exception {
        String control = controlHashService.calculateHash("foobarbaz");
        String input = "<root>\n\t<e>foo</e>\n\t<e mepmep=\"bar\">baz</e>\n</root>";
        String hash = hashService.calculateHash(input);
        assertNotNull(hash);
        assertFalse(hash.trim().isEmpty());
        assertEquals(control, hash);
    }

    /**
     * Test non XML. Result should be a hashed randomized String
     * @throws Exception
     */
    @Test
    public void generateHashPlainString() throws Exception {
        String input = "mepmep";
        String hash1 = hashService.calculateHash(input);
        assertNotNull(hash1);
        assertFalse(hash1.trim().isEmpty());
        logger.debug("hash1: {}", hash1);
        String hash2 = hashService.calculateHash(input);
        assertNotNull(hash2);
        assertFalse(hash2.trim().isEmpty());
        logger.debug("hash2: {}", hash2);
        assertNotEquals(hash1, hash2);
    }

}