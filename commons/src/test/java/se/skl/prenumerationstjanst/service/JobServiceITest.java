package se.skl.prenumerationstjanst.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import se.skl.prenumerationstjanst.PrenumerationstjanstITestApplication;
import se.skl.prenumerationstjanst.dto.FetchJobDTO;
import se.skl.prenumerationstjanst.dto.PushJobDTO;
import se.skl.prenumerationstjanst.dto.TransferJobDTO;
import se.skl.prenumerationstjanst.dto.TransformationJobDTO;
import se.skl.prenumerationstjanst.model.*;
import se.skl.prenumerationstjanst.repository.*;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = PrenumerationstjanstITestApplication.class)
@DirtiesContext
@ActiveProfiles("test")
public class JobServiceITest {

    private static final Logger logger = LoggerFactory.getLogger(JobServiceITest.class);
    private static final String TOKEN_VALUE = "tval";

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private TransferJobRepository transferJobRepository;

    @Autowired
    private FetchJobRepository fetchJobRepository;

    @Autowired
    private PushJobRepository pushJobRepository;

    @Autowired
    private TransformationJobRepository transformationJobRepository;

    @Autowired
    private UserContextRepository userContextRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JobService jobService;

    private String baseSubscriptionId;

    @Before
    public void setUp() throws Exception {
        userContextRepository.deleteAll();
        userRepository.deleteAll();
        subscriptionRepository.deleteAll();
        fetchJobRepository.deleteAll();
        transformationJobRepository.deleteAll();
        pushJobRepository.deleteAll();
        transferJobRepository.deleteAll();
        baseSubscriptionId = insertBaseSubscription();
        assert subscriptionRepository.count() == 1;
    }

    @After
    public void tearDown() throws Exception {
        userContextRepository.deleteAll();
        userRepository.deleteAll();
        subscriptionRepository.deleteAll();
        transformationJobRepository.deleteAll();
        pushJobRepository.deleteAll();
        transferJobRepository.deleteAll();
        fetchJobRepository.deleteAll();
    }

    @Test
    public void createTransferJob() throws Exception {
        Subscription subscription = subscriptionRepository.findOne(baseSubscriptionId);
        LocalDateTime startDateTime = LocalDateTime.now();
        LocalDateTime endDateTime = LocalDateTime.now().plusDays(1);
        TransferJobDTO transferJobDTO = jobService.createTransferJob(baseSubscriptionId, TransferType.VACCINATION_HISTORY,
                startDateTime, endDateTime, null, TransferJob.Origin.INITIAL);
        assertEquals(1, transferJobRepository.count());
        TransferJob transferJob = transferJobRepository.findOne(transferJobDTO.getId());
        assertTransferJob(transferJobDTO, transferJob);
        assertEquals(subscription.getUserContext().getUser().getCrn(), transferJobDTO.getUserCrn());
        assertEquals(subscription.getUserContext().getRepresentingCrn(), transferJobDTO.getRepresentingCrn());
        assertEquals(startDateTime, transferJobDTO.getStartDateTime());
        assertEquals(endDateTime, transferJobDTO.getEndDateTime());
        assertEquals(TransferType.VACCINATION_HISTORY.name(), transferJobDTO.getTransferType());
        assertEquals(TransferJob.Origin.INITIAL.name(), transferJobDTO.getOrigin());
    }

    @Test
    public void getTransferJob() throws Exception {
        String tjId = insertBaseTransferJob(baseSubscriptionId);
        TransferJob transferJob = transferJobRepository.findOne(tjId);
        TransferJobDTO transferJobDTO = jobService.getTransferJob(tjId);
        assertTransferJob(transferJobDTO, transferJob);
    }

    private void assertTransferJob(TransferJobDTO transferJobDTO, TransferJob transferJob) {
        assertEquals(transferJobDTO.getSubscriptionId(), transferJob.getSubscriptionId());
        assertEquals(transferJobDTO.getUserCrn(), transferJob.getUserCrn());
        assertEquals(transferJobDTO.getRepresentingCrn(), transferJob.getRepresentingCrn());
        assertEquals(transferJobDTO.getSourceSystemHsaId(), transferJob.getSourceSystemHsaId());
        assertEquals(transferJobDTO.getStartDateTime(), transferJob.getStartDateTime());
        assertEquals(transferJobDTO.getEndDateTime(), transferJob.getEndDateTime());
        assertEquals(transferJobDTO.getTransferType(), transferJob.getTransferType().name());
        assertEquals(transferJobDTO.getOrigin(), transferJob.getOrigin().name());
    }

    @Test
    public void createFetchJob() throws Exception {
        String tjId = insertBaseTransferJob(baseSubscriptionId);
        assertEquals(1, transferJobRepository.count());
        FetchJobDTO fetchJobDTO = jobService.createFetchJob(tjId);
        FetchJob fetchJob = fetchJobRepository.findOne(fetchJobDTO.getId());
        assertEquals(1, fetchJob.getJobStates().size());
        FetchJobState fetchJobState = fetchJob.getJobStates().get(0);
        assertNotNull(fetchJobState.getCreated());
        assertEquals(fetchJobState.getCreated(), fetchJobState.getLastModified());
        assertEquals(JobStatus.CREATED, fetchJobState.getStatus());
    }

    @Test
    public void updateFetchJobStatus() throws Exception {
        String fetchJobId = insertBaseFetchJob(baseSubscriptionId);
        FetchJob fetchJob = fetchJobRepository.findOne(fetchJobId);
        assertEquals(1, fetchJob.getJobStates().size());
        assertEquals(JobStatus.CREATED, fetchJob.getJobStates().get(0).getStatus());
        jobService.updateFetchJobStatus(fetchJobId, JobStatus.FAILED);
        fetchJob = fetchJobRepository.findOne(fetchJobId);
        assertEquals(2, fetchJob.getJobStates().size());
        assertEquals(JobStatus.FAILED, fetchJob.getJobStates().get(1).getStatus());
    }

    @Test
    public void finishFetchJob() throws Exception {
        String fetchJobId = insertBaseFetchJob(baseSubscriptionId);
        FetchJob fetchJob = fetchJobRepository.findOne(fetchJobId);
        assertEquals(1, fetchJob.getJobStates().size());
        assertEquals(JobStatus.CREATED, fetchJob.getJobStates().get(0).getStatus());
        jobService.finishFetchJob(fetchJobId, LocalDateTime.now());
        fetchJob = fetchJobRepository.findOne(fetchJobId);
        assertEquals(2, fetchJob.getJobStates().size());
        assertEquals(JobStatus.FINISHED, fetchJob.getJobStates().get(1).getStatus());
    }

    @Test
    public void createTransformationJob() throws Exception {
        String tjId = insertBaseTransferJob(baseSubscriptionId);
        TransformationJobDTO transformationJobDTO = jobService.createTransformationJob(tjId, "source", LocalDateTime.now(), 1);
        assertEquals("source", transformationJobDTO.getSource());
        assertEquals(1, transformationJobRepository.count());
        TransformationJob transformationJob = transformationJobRepository.findOne(transformationJobDTO.getId());
        assertEquals(1, transformationJob.getJobStates().size());
        TransformationJobState transformationJobState = transformationJob.getJobStates().get(0);
        assertNotNull(transformationJobState.getCreated());
        assertEquals(transformationJobState.getCreated(), transformationJobState.getLastModified());
        assertEquals(JobStatus.CREATED, transformationJobState.getStatus());
    }

    @Test
    public void updateTransformationJobStatus() throws Exception {
        String transformationJobId = insertBaseTransformationJob(baseSubscriptionId);
        TransformationJob transformationJob = transformationJobRepository.findOne(transformationJobId);
        assertEquals(1, transformationJob.getJobStates().size());
        assertEquals(JobStatus.CREATED, transformationJob.getJobStates().get(0).getStatus());
        jobService.updateTransformationJobStatus(transformationJobId, JobStatus.RUNNING);
        transformationJob = transformationJobRepository.findOne(transformationJobId);
        assertEquals(2, transformationJob.getJobStates().size());
        assertEquals(JobStatus.RUNNING, transformationJob.getJobStates().get(1).getStatus());
    }

    @Test
    public void finishTransformationJob() throws Exception {
        String transformationJobId = insertBaseTransformationJob(baseSubscriptionId);
        TransformationJob transformationJob = transformationJobRepository.findOne(transformationJobId);
        assertEquals(1, transformationJob.getJobStates().size());
        assertEquals(JobStatus.CREATED, transformationJob.getJobStates().get(0).getStatus());
        jobService.finishTransformationJob(transformationJobId);
        transformationJob = transformationJobRepository.findOne(transformationJobId);
        assertEquals(2, transformationJob.getJobStates().size());
        assertEquals(JobStatus.FINISHED, transformationJob.getJobStates().get(1).getStatus());
    }

    @Test
    public void createPushJob() throws Exception {
        String tjId = insertBaseTransferJob(baseSubscriptionId);
        assertEquals(1, transferJobRepository.count());
        PushJobDTO pushJobDTO = jobService.createPushJob(tjId);
        PushJob pushJob = pushJobRepository.findOne(pushJobDTO.getId());
        assertEquals(1, pushJob.getJobStates().size());
        PushJobState pushJobState = pushJob.getJobStates().get(0);
        assertNotNull(pushJobState.getCreated());
        assertEquals(pushJobState.getCreated(), pushJobState.getLastModified());
        assertEquals(JobStatus.CREATED, pushJobState.getStatus());
    }

    @Test
    public void updatePushJobStatus() throws Exception {
        String pushJobId = insertBasePushJob(baseSubscriptionId);
        PushJob pushJob = pushJobRepository.findOne(pushJobId);
        assertEquals(1, pushJob.getJobStates().size());
        assertEquals(JobStatus.CREATED, pushJob.getJobStates().get(0).getStatus());
        jobService.updatePushJobStatus(pushJobId, JobStatus.DUPLICATE);
        pushJob = pushJobRepository.findOne(pushJobId);
        assertEquals(2, pushJob.getJobStates().size());
        assertEquals(JobStatus.DUPLICATE, pushJob.getJobStates().get(1).getStatus());
    }

    @Test
    public void finishPushJob() throws Exception {
        String pushJobId = insertBasePushJob(baseSubscriptionId);
        PushJob pushJob = pushJobRepository.findOne(pushJobId);
        assertEquals(1, pushJob.getJobStates().size());
        assertEquals(JobStatus.CREATED, pushJob.getJobStates().get(0).getStatus());
        jobService.finishPushJob(pushJobId, "sourceId", 1, LocalDateTime.now(), JobStatus.FINISHED, "typeOfData", null);
        pushJob = pushJobRepository.findOne(pushJobId);
        assertEquals(2, pushJob.getJobStates().size());
        assertEquals(JobStatus.FINISHED, pushJob.getJobStates().get(1).getStatus());
    }

    @Test
    public void pushJobMultiBall() throws Exception {
        String tjId = insertBaseTransferJob(baseSubscriptionId);
        assertEquals(1, transferJobRepository.count());
        PushJobDTO pushJobDTO = jobService.createPushJob(tjId);
        PushJob pushJob = pushJobRepository.findOne(pushJobDTO.getId());
        assertEquals(1, pushJob.getJobStates().size());
        PushJobState pushJobState = pushJob.getJobStates().get(0);
        assertNotNull(pushJobState.getCreated());
        assertEquals(pushJobState.getCreated(), pushJobState.getLastModified());
        jobService.updatePushJobStatus(pushJobDTO.getId(), JobStatus.RUNNING);
        pushJob = pushJobRepository.findOne(pushJobDTO.getId());
        assertEquals(2, pushJob.getJobStates().size());
        jobService.updatePushJobStatus(pushJobDTO.getId(), JobStatus.FAILED);
        pushJob = pushJobRepository.findOne(pushJobDTO.getId());
        assertEquals(3, pushJob.getJobStates().size());
        jobService.updatePushJobStatus(pushJobDTO.getId(), JobStatus.RUNNING);
        pushJob = pushJobRepository.findOne(pushJobDTO.getId());
        assertEquals(4, pushJob.getJobStates().size());
        jobService.finishPushJob(pushJobDTO.getId(), "sourceId", 1, LocalDateTime.now(), JobStatus.FINISHED, "typeOfData", null);
        pushJob = pushJobRepository.findOne(pushJobDTO.getId());
        assertEquals(5, pushJob.getJobStates().size());
        assertJobState(pushJob.getJobStates().get(0), JobStatus.CREATED);
        assertJobState(pushJob.getJobStates().get(1), JobStatus.RUNNING);
        assertJobState(pushJob.getJobStates().get(2), JobStatus.FAILED);
        assertJobState(pushJob.getJobStates().get(3), JobStatus.RUNNING);
        assertJobState(pushJob.getJobStates().get(4), JobStatus.FINISHED);
    }

    private void assertJobState(AbstractJobState jobState, JobStatus jobStatus) {
        assertEquals(jobStatus, jobState.getStatus());
        assertEquals(jobState.getLastModified(), jobState.getCreated());
    }

    private String insertBaseSubscription() {
        User user = userRepository.save(new User("crn", "phkPersonId"));
        UserContext userC = userContextRepository.save(new UserContext(user, "representingCrn", "phkRecordId"));
        Subscription subscription = new Subscription(userC, TransferType.VACCINATION_HISTORY);
        ApigwConsent apigwConsent = new ApigwConsent();
        apigwConsent.setTokenValue(TOKEN_VALUE);
        subscription.setApigwConsent(apigwConsent);
        return subscriptionRepository.save(subscription).getId();
    }

    private String insertBaseTransferJob(String subscriptionId) {
        TransferJob transferJob = new TransferJob("", "", subscriptionId, TransferType.VACCINATION_HISTORY,
                LocalDateTime.now(),
                LocalDateTime.now(), TransferJob.Origin.INITIAL);
        return transferJobRepository.save(transferJob).getId();
    }

    private String insertBaseFetchJob(String subscriptionId) {
        TransferJob transferJob = transferJobRepository.findOne(insertBaseTransferJob(subscriptionId));
        return fetchJobRepository.save(new FetchJob(transferJob, "apigwToken")).getId();
    }

    private String insertBasePushJob(String subscriptionId) {
        TransferJob transferJob = transferJobRepository.findOne(insertBaseTransferJob(subscriptionId));
        return pushJobRepository.save(new PushJob(transferJob, "phkPersonId", "pgkRecordId")).getId();
    }

    private String insertBaseTransformationJob(String subscriptionId) {
        TransferJob transferJob = transferJobRepository.findOne(insertBaseTransferJob(subscriptionId));
        return transformationJobRepository.save(new TransformationJob(transferJob, "source", LocalDateTime.now(), 1))
                .getId();
    }



}