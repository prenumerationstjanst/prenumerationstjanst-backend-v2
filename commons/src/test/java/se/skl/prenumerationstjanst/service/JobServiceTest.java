package se.skl.prenumerationstjanst.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import se.skl.prenumerationstjanst.dto.FetchJobDTO;
import se.skl.prenumerationstjanst.dto.PushJobDTO;
import se.skl.prenumerationstjanst.dto.TransferJobDTO;
import se.skl.prenumerationstjanst.model.*;
import se.skl.prenumerationstjanst.repository.FetchJobRepository;
import se.skl.prenumerationstjanst.repository.PushJobRepository;
import se.skl.prenumerationstjanst.repository.SubscriptionRepository;
import se.skl.prenumerationstjanst.repository.TransferJobRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

/**
 * @author Martin Samuelsson
 */
@RunWith(MockitoJUnitRunner.class)
public class JobServiceTest {

    @Mock
    private TransferJobRepository transferJobRepository;

    @Mock
    private FetchJobRepository fetchJobRepository;

    @Mock
    private PushJobRepository pushJobRepository;

    @Mock
    private SubscriptionRepository subscriptionRepository;

    @Mock
    private TransformationService transformationService;

    @InjectMocks
    private JobService jobService;

    private Subscription mockSubscription;

    @Before
    public void setUp() {
        reset(
                transferJobRepository,
                fetchJobRepository,
                pushJobRepository,
                subscriptionRepository,
                transformationService
        );
        mockSubscription = mockSubscription();
        when(subscriptionRepository.findOne(anyString())).thenReturn(mockSubscription);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testCreateTransferJob() throws Exception {
        TransferJob savedTransferJob = mock(TransferJob.class);
        TransferJobDTO expectedTransferJobDTO = TransferJobDTO.create();
        when(transferJobRepository.save(any(TransferJob.class))).thenReturn(savedTransferJob);
        when(transformationService.toDTO(any(TransferJob.class))).thenReturn(expectedTransferJobDTO);
        TransferJobDTO transferJobDTO = jobService.createTransferJob(
                "subId",
                TransferType.MATERNITY_MEDICAL_HISTORY,
                LocalDateTime.MIN,
                LocalDateTime.MAX,
                null,
                TransferJob.Origin.INITIAL);
        assertSame(expectedTransferJobDTO,  transferJobDTO);
        verify(subscriptionRepository).findOne(eq("subId"));
        verify(transferJobRepository).save(any(TransferJob.class));
        verify(transformationService).toDTO(any(TransferJob.class));
        verifyNoMoreInteractions(subscriptionRepository, transferJobRepository, transformationService);
    }

    @Test
    public void testGetTransferJob() throws Exception {
        TransferJob transferJob = mock(TransferJob.class);
        TransferJobDTO expectedTransferJobDTO = TransferJobDTO.create();
        when(transferJobRepository.findOne(eq("tjId"))).thenReturn(transferJob);
        when(transformationService.toDTO(transferJob)).thenReturn(expectedTransferJobDTO);
        TransferJobDTO transferJobDTO = jobService.getTransferJob("tjId");
        assertSame(expectedTransferJobDTO, transferJobDTO);
        verify(transferJobRepository).findOne(eq("tjId"));
        verify(transformationService).toDTO(transferJob);
        verifyNoMoreInteractions(transferJobRepository, transformationService);
    }

    @Test
    public void testCreateFetchJob() throws Exception {
        TransferJob transferJob = mock(TransferJob.class);
        FetchJob savedFetchJob = mock(FetchJob.class);
        FetchJobDTO expectedFetchJobDTO = mock(FetchJobDTO.class);
        when(transferJobRepository.findOne(eq("tjId"))).thenReturn(transferJob);
        when(fetchJobRepository.save(any(FetchJob.class))).thenReturn(savedFetchJob);
        when(transformationService.toDTO(savedFetchJob)).thenReturn(expectedFetchJobDTO);
        FetchJobDTO fetchJobDTO = jobService.createFetchJob("tjId");

        assertSame(expectedFetchJobDTO, fetchJobDTO);
        verify(transferJob).setFetchJob(savedFetchJob);
        verify(transferJobRepository).findOne(eq("tjId"));
        verify(transferJobRepository).save(transferJob);
        verify(fetchJobRepository).save(any(FetchJob.class));
        verify(transformationService).toDTO(savedFetchJob);
        verifyNoMoreInteractions(transferJobRepository, fetchJobRepository, transformationService);
    }

    @Test
    public void testUpdateFetchJobStatus() throws Exception {
        FetchJob fetchJob = mock(FetchJob.class);
        FetchJobDTO expectedFetchJobDTO = mock(FetchJobDTO.class);
        when(fetchJobRepository.findOne(eq("fjId"))).thenReturn(fetchJob);
        when(fetchJobRepository.save(fetchJob)).thenReturn(fetchJob);
        List<FetchJobState> fetchJobStates = Mockito.spy(new ArrayList<>());
        when(fetchJob.getJobStates()).thenReturn(fetchJobStates);
        when(transformationService.toDTO(fetchJob)).thenReturn(expectedFetchJobDTO);
        FetchJobDTO fetchJobDTO = jobService.updateFetchJobStatus("fjId", JobStatus.RUNNING);
        assertSame(expectedFetchJobDTO, fetchJobDTO);
        verify(fetchJobRepository).findOne(eq("fjId"));
        verify(fetchJobRepository).save(fetchJob);
        verify(fetchJob).setStatus(JobStatus.RUNNING);
        verify(transformationService).toDTO(fetchJob);
        ArgumentCaptor<FetchJobState> fetchJobStateArgumentCaptor = ArgumentCaptor.forClass(FetchJobState.class);
        verify(fetchJobStates).add(fetchJobStateArgumentCaptor.capture());
        assertEquals(JobStatus.RUNNING, fetchJobStateArgumentCaptor.getValue().getStatus());
        verifyNoMoreInteractions(fetchJobRepository, transformationService);
    }

    @Test
    public void testFinishFetchJob() throws Exception {
        FetchJob fetchJob = mock(FetchJob.class);
        FetchJobDTO expectedFetchJobDTO = mock(FetchJobDTO.class);
        when(fetchJobRepository.findOne(eq("fjId"))).thenReturn(fetchJob);
        when(fetchJobRepository.save(fetchJob)).thenReturn(fetchJob);
        List<FetchJobState> fetchJobStates = Mockito.spy(new ArrayList<>());
        when(fetchJob.getJobStates()).thenReturn(fetchJobStates);
        when(transformationService.toDTO(fetchJob)).thenReturn(expectedFetchJobDTO);
        LocalDateTime fetchFinishTime = LocalDateTime.now();
        FetchJobDTO fetchJobDTO = jobService.finishFetchJob("fjId", fetchFinishTime);
        assertSame(expectedFetchJobDTO, fetchJobDTO);
        verify(fetchJobRepository).findOne(eq("fjId"));
        verify(fetchJobRepository).save(fetchJob);
        verify(fetchJob).setStatus(JobStatus.FINISHED);
        verify(transformationService).toDTO(fetchJob);
        ArgumentCaptor<FetchJobState> fetchJobStateArgumentCaptor = ArgumentCaptor.forClass(FetchJobState.class);
        verify(fetchJobStates).add(fetchJobStateArgumentCaptor.capture());
        assertEquals(JobStatus.FINISHED, fetchJobStateArgumentCaptor.getValue().getStatus());
        verifyNoMoreInteractions(fetchJobRepository, transformationService);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testCreatePushJob() throws Exception {
        TransferJob transferJob = mock(TransferJob.class);
        PushJob savedPushJob = mock(PushJob.class);
        PushJobDTO expectedPushJobDTO = mock(PushJobDTO.class);
        List pushJobs = mock(List.class);
        when(transferJobRepository.findOne(eq("tjId"))).thenReturn(transferJob);
        when(transferJob.getPushJobs()).thenReturn(pushJobs);
        when(pushJobRepository.save(any(PushJob.class))).thenReturn(savedPushJob);
        when(transformationService.toDTO(savedPushJob)).thenReturn(expectedPushJobDTO);
        PushJobDTO pushJobDTO = jobService.createPushJob("tjId");
        assertSame(expectedPushJobDTO, pushJobDTO);
        verify(transferJobRepository).findOne(eq("tjId"));
        verify(pushJobRepository).save(any(PushJob.class));
        verify(transferJob).getPushJobs();
        verify(pushJobs).add(savedPushJob);
        verify(transferJobRepository).save(transferJob);
        verify(transformationService).toDTO(savedPushJob);
        verifyNoMoreInteractions(transferJobRepository, pushJobRepository, transformationService);
    }

    @Test
    public void testUpdatePushJobStatus() throws Exception {
        PushJob pushJob = mock(PushJob.class);
        PushJobDTO expectedPushJob = mock(PushJobDTO.class);
        List<PushJobState> pushJobStates = Mockito.spy(new ArrayList<>());
        when(pushJob.getJobStates()).thenReturn(pushJobStates);
        when(pushJobRepository.findOne(eq("pjId"))).thenReturn(pushJob);
        when(pushJobRepository.save(pushJob)).thenReturn(pushJob);
        when(transformationService.toDTO(pushJob)).thenReturn(expectedPushJob);
        PushJobDTO pushJobDTO = jobService.updatePushJobStatus("pjId", JobStatus.RUNNING);
        assertSame(expectedPushJob, pushJobDTO);
        verify(pushJobRepository).findOne(eq("pjId"));
        verify(pushJobRepository).save(pushJob);
        verify(pushJob).setStatus(JobStatus.RUNNING);
        ArgumentCaptor<PushJobState> pushJobStateArgumentCaptor = ArgumentCaptor.forClass(PushJobState.class);
        verify(pushJobStates).add(pushJobStateArgumentCaptor.capture());
        assertEquals(JobStatus.RUNNING, pushJobStateArgumentCaptor.getValue().getStatus());
        verify(transformationService).toDTO(pushJob);
        verifyNoMoreInteractions(pushJobRepository, transformationService);
    }

    @Test
    public void testFinishPushJob() throws Exception {
        PushJob pushJob = mock(PushJob.class);
        PushJobDTO expectedPushJob = mock(PushJobDTO.class);
        List<PushJobState> pushJobStates = Mockito.spy(new ArrayList<>());
        when(pushJob.getJobStates()).thenReturn(pushJobStates);
        when(pushJobRepository.findOne(eq("pjId"))).thenReturn(pushJob);
        when(pushJobRepository.save(pushJob)).thenReturn(pushJob);
        when(transformationService.toDTO(pushJob)).thenReturn(expectedPushJob);
        PushJobDTO pushJobDTO = jobService.finishPushJob("pjId", "sourceId", 1, LocalDateTime.now(),
                JobStatus.FINISHED, "typeOfData", null);
        assertSame(expectedPushJob, pushJobDTO);
        verify(pushJobRepository).findOne(eq("pjId"));
        verify(pushJobRepository).save(pushJob);
        verify(pushJob).setStatus(JobStatus.FINISHED);
        ArgumentCaptor<PushJobState> pushJobStateArgumentCaptor = ArgumentCaptor.forClass(PushJobState.class);
        verify(pushJobStates).add(pushJobStateArgumentCaptor.capture());
        assertEquals(JobStatus.FINISHED, pushJobStateArgumentCaptor.getValue().getStatus());
        verify(transformationService).toDTO(pushJob);
        verifyNoMoreInteractions(pushJobRepository, transformationService);
    }

    private Subscription mockSubscription() {
        Subscription subscription = new Subscription(new UserContext(new User("userCrn", "phkPersonId"),
                "representingCrn", "phkRecordId"),
                TransferType.VACCINATION_HISTORY);
        ApigwConsent apigwConsent = new ApigwConsent();
        apigwConsent.setTokenValue("tokenValue");
        subscription.setApigwConsent(apigwConsent);
        return subscription;
    }
}