package se.skl.prenumerationstjanst.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import se.skl.prenumerationstjanst.dto.SubscriptionDTO;
import se.skl.prenumerationstjanst.dto.UserContextDTO;
import se.skl.prenumerationstjanst.dto.UserDTO;
import se.skl.prenumerationstjanst.model.TransferType;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;

/**
 * @author Martin Samuelsson
 */
@RunWith(MockitoJUnitRunner.class)
public class PreCheckServiceImplTest {

    private static final String PERFORMING_CRN = "191212121212";
    private static final String REPRESENTING_CRN = "191312121212";
    private static final String PHK_PERSON_ID = "phkPersonId";
    private static final String PHK_RECORD_ID = "phkRecordId";
    @Mock
    private UserService userService;

    @Mock
    private PhkInfoService phkInfoService;

    @Mock
    private SubscriptionService subscriptionService;

    @Mock
    private UserContextService userContextService;

    @InjectMocks
    private PreCheckServiceImpl preCheckService;

    private UserDTO userDTO;

    private UserContextDTO userContextDTO;

    @Before
    public void setUp() throws Exception {
        userDTO = new UserDTO(0L, PERFORMING_CRN, PHK_PERSON_ID, null);
        userContextDTO = new UserContextDTO(userDTO, REPRESENTING_CRN, null, PHK_RECORD_ID);
    }

    @Test
    public void preCheck_noUserFound() throws Exception {
        Mockito.when(userService.getUser(PERFORMING_CRN)).thenReturn(null);
        assertEquals(Boolean.FALSE, preCheckService.preCheck(PERFORMING_CRN, REPRESENTING_CRN));
        Mockito.verifyNoMoreInteractions(subscriptionService);
    }

    @Test
    public void preCheck_noUserContextFound() throws Exception {
        Mockito.when(userService.getUser(PERFORMING_CRN)).thenReturn(userDTO);
        Mockito.when(userContextService.getUserContext(PERFORMING_CRN, REPRESENTING_CRN)).thenReturn(null);
        assertEquals(Boolean.FALSE, preCheckService.preCheck(PERFORMING_CRN, REPRESENTING_CRN));
        Mockito.verifyNoMoreInteractions(subscriptionService);
    }

    @Test
    public void preCheck_personIdNotActive() throws Exception {
        Mockito.when(userService.getUser(PERFORMING_CRN)).thenReturn(userDTO);
        Mockito.when(userContextService.getUserContext(PERFORMING_CRN, REPRESENTING_CRN)).thenReturn(userContextDTO);
        Mockito.when(phkInfoService.checkPhkPersonId(PHK_PERSON_ID)).thenReturn(PhkInfoService.PHK_PERSON_STATUS.INACTIVE);
        assertEquals(Boolean.FALSE, preCheckService.preCheck(PERFORMING_CRN, REPRESENTING_CRN));
        Mockito.verifyNoMoreInteractions(subscriptionService);
    }

    @Test
    public void preCheck_ok() throws Exception {
        Mockito.when(userService.getUser(PERFORMING_CRN)).thenReturn(userDTO);
        Mockito.when(userContextService.getUserContext(PERFORMING_CRN, REPRESENTING_CRN)).thenReturn(userContextDTO);
        Mockito.when(phkInfoService.checkPhkPersonId(PHK_PERSON_ID)).thenReturn(PhkInfoService.PHK_PERSON_STATUS.ACTIVE);
        assertEquals(Boolean.TRUE, preCheckService.preCheck(PERFORMING_CRN, REPRESENTING_CRN));
        Mockito.verifyNoMoreInteractions(subscriptionService);
    }

    @Test
    public void preCheckAndExecuteCleanup() throws Exception {
        Mockito.when(userService.getUser(PERFORMING_CRN)).thenReturn(null);
        List<SubscriptionDTO> subscriptionDTOs = new ArrayList<>();
        subscriptionDTOs.add(new SubscriptionDTO("a", null, TransferType.VACCINATION_HISTORY.name(), null, null));
        subscriptionDTOs.add(new SubscriptionDTO("a", null, TransferType.VACCINATION_HISTORY.name(), null, null));
        Mockito.when(subscriptionService.getSubscriptionsForUserContext(PERFORMING_CRN, REPRESENTING_CRN))
                .thenReturn(subscriptionDTOs);
        assertEquals(Boolean.FALSE, preCheckService.preCheckAndExecuteCleanup(PERFORMING_CRN, REPRESENTING_CRN));
        Mockito.verify(subscriptionService, times(2)).removeSubscription(PERFORMING_CRN, REPRESENTING_CRN,
                TransferType.VACCINATION_HISTORY);
    }

}