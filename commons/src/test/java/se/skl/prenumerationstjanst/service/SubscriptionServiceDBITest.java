package se.skl.prenumerationstjanst.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import se.skl.prenumerationstjanst.PrenumerationstjanstITestApplication;
import se.skl.prenumerationstjanst.config.TestConfiguration;
import se.skl.prenumerationstjanst.dto.ApigwConsentDTO;
import se.skl.prenumerationstjanst.dto.SubscriptionDTO;
import se.skl.prenumerationstjanst.exception.subscription.SubscriptionCreationFailedException;
import se.skl.prenumerationstjanst.exception.subscription.SubscriptionNotFoundException;
import se.skl.prenumerationstjanst.model.*;
import se.skl.prenumerationstjanst.repository.PushLogRepository;
import se.skl.prenumerationstjanst.repository.SubscriptionRepository;
import se.skl.prenumerationstjanst.repository.UserContextRepository;
import se.skl.prenumerationstjanst.repository.UserRepository;

import java.time.LocalDateTime;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/**
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {
        PrenumerationstjanstITestApplication.class,
        SubscriptionServiceDBITest.TestConfig.class
})
@DirtiesContext
@ActiveProfiles("test")
public class SubscriptionServiceDBITest {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionServiceDBITest.class);

    public static final String USER_CRN = "191212121212";
    public static final String CHILD_CRN = "191301010101";
    public static final String APIGW_TOKEN_VALUE = "newToken";
    public static final TransferType TRANSFER_TYPE = TransferType.VACCINATION_HISTORY;
    public static final String APIGW_SCOPE = TRANSFER_TYPE.getScope();

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserContextRepository userContextRepository;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private PushLogRepository pushLogRepository;

    @Autowired
    private ApigwConsentService apigwConsentService;

    private ApigwConsentDTO apigwConsentDTO;

    private LocalDateTime now = LocalDateTime.now();

    @Before
    public void setup() throws Exception {
        logger.debug("Mocked ApigwConsentService: {}", Mockito.mockingDetails(apigwConsentService).isMock());
        apigwConsentDTO = ApigwConsentDTO.create()
                .withTokenValue(APIGW_TOKEN_VALUE)
                .withScope(APIGW_SCOPE)
                .withExpirationDateTime(now);
        when(apigwConsentService.getConsent(anyString(), anyString(), any(TransferType.class)))
                .thenReturn(apigwConsentDTO);
        subscriptionRepository.deleteAll();
        userRepository.deleteAll();
        userContextRepository.deleteAll();
        UserContext userContext = new UserContext(userRepository.save(new User(USER_CRN, "phkPid")),
                USER_CRN, "phkRid");
        userContextRepository.save(userContext);
    }

    @After
    public void tearDown() throws Exception {
        subscriptionRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    public void testCreateSubscriptionForUser() throws Exception {
        assertEquals(0, subscriptionRepository.count());
        SubscriptionDTO subscriptionDTO = subscriptionService.createSubscription(USER_CRN, USER_CRN, TRANSFER_TYPE);
        Subscription subscription = subscriptionRepository.findOne(subscriptionDTO.getId());
        assertEquals(TRANSFER_TYPE, subscription.getApprovedTransferType());
        assertEquals(USER_CRN, subscription.getUserContext().getRepresentingCrn());
        ApigwConsent apigwConsent = subscription.getApigwConsent();
        assertNotNull(apigwConsent);
        assertEquals(APIGW_SCOPE, apigwConsent.getScope());
        assertEquals(APIGW_TOKEN_VALUE, apigwConsent.getTokenValue());
        assertEquals(1, subscriptionRepository.count());
    }

    @Test
    public void testCreateSubscriptionDuplicate() throws Exception {
        assertEquals(0, subscriptionRepository.count());
        subscriptionService.createSubscription(USER_CRN, USER_CRN, TRANSFER_TYPE);
        assertEquals(1, subscriptionRepository.count());
        boolean exceptionThrown = false;
        try {
            subscriptionService.createSubscription(USER_CRN, USER_CRN, TRANSFER_TYPE);
        } catch (SubscriptionCreationFailedException e) {
            exceptionThrown = true;
        }
        assertEquals(1, subscriptionRepository.count());
        assertTrue(exceptionThrown);
    }

    @Test
    public void testRemoveSubscription() {
        Subscription savedSub = subscriptionRepository.save(getSubscription());
        assertFalse(savedSub.isScheduledForRemoval());
        String subId = savedSub.getId();
        assertEquals(1, subscriptionRepository.count());
        subscriptionService.removeSubscription(subId);
        Subscription sub = subscriptionRepository.findOne(subId);
        assertTrue(sub.isScheduledForRemoval());
        assertEquals(1, subscriptionRepository.count());
    }

    @Test(expected = SubscriptionNotFoundException.class)
    public void testRemoveSubscription_SubNotFound() {
        subscriptionService.removeSubscription("mep");
    }

    @Test
    public void testRemoveSubscriptionUC() {
        Subscription savedSub = subscriptionRepository.save(getSubscription());
        assertFalse(savedSub.isScheduledForRemoval());
        assertEquals(1, subscriptionRepository.count());
        subscriptionService.removeSubscription(USER_CRN, USER_CRN, TRANSFER_TYPE);
        Subscription afterRemoval = subscriptionRepository.findOne(savedSub.getId());
        assertTrue(afterRemoval.isScheduledForRemoval());
        assertEquals(1, subscriptionRepository.count());
    }

    @Test(expected = SubscriptionNotFoundException.class)
    public void testRemoveSubscriptionUC_NotFound() {
        Subscription savedSub = subscriptionRepository.save(getSubscription());
        assertFalse(savedSub.isScheduledForRemoval());
        assertEquals(1, subscriptionRepository.count());
        subscriptionService.removeSubscription(USER_CRN, "101010101010", TRANSFER_TYPE);
    }

    @Test
    public void testInternalRemoveSubscription_Cascade() {
        Subscription subscription = subscriptionRepository.save(getSubscription());
        PushLog pushLog = pushLogRepository.save(new PushLog(subscription, "", LocalDateTime.now(), 1, "hash"));
        subscription.getPushLogs().add(pushLog);
        String subId = subscription.getId();
        assertEquals(1, subscriptionRepository.count());
        assertEquals(1, pushLogRepository.count());
        subscriptionService.internalRemoveSubscription(subId);
        assertEquals(0, subscriptionRepository.count());
        assertEquals(0, pushLogRepository.count());
    }

    @Test
    public void testIsSubscriptionActive() throws Exception {
        Subscription subscription = getSubscription();
        String subId = subscriptionRepository.save(subscription).getId();
        assertEquals(1, subscriptionRepository.count());
        assertTrue(subscriptionService.isSubscriptionActive(subId));
    }

    @Test
    public void testIsSubscriptionActive_NotFound() throws Exception {
        assertEquals(0, subscriptionRepository.count());
        assertFalse(subscriptionService.isSubscriptionActive("mep"));
    }

    private void assertConsent(ApigwConsent consent) {
        assertEquals(apigwConsentDTO.getExpirationDateTime(), consent.getExpirationDateTime());
        assertEquals(apigwConsentDTO.getTokenValue(), consent.getTokenValue());
        assertEquals(apigwConsentDTO.getScope(), consent.getScope());
    }

    private Subscription getSubscription() {
        Subscription subscription = new Subscription();
        subscription.setUserContext(userContextRepository.findByUserCrnAndRepresentingCrn(USER_CRN, USER_CRN));
        ApigwConsent apigwConsent = new ApigwConsent();
        apigwConsent.setTokenValue("token");
        apigwConsent.setScope("scope");
        apigwConsent.setExpirationDateTime(now);
        subscription.setApigwConsent(apigwConsent);
        subscription.setApprovedTransferType(TRANSFER_TYPE);
        return subscription;
    }

    @TestConfiguration
    @Configuration
    public static class TestConfig {
        @Bean
        @Primary
        public ApigwConsentService apigwConsentService() {
            return Mockito.mock(ApigwConsentService.class);
        }
    }
}
