package se.skl.prenumerationstjanst.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.UncategorizedJmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import se.skl.prenumerationstjanst.PrenumerationstjanstITestApplication;
import se.skl.prenumerationstjanst.config.TestConfiguration;
import se.skl.prenumerationstjanst.dto.ApigwConsentDTO;
import se.skl.prenumerationstjanst.dto.MonitoredUserEventDTO;
import se.skl.prenumerationstjanst.model.*;
import se.skl.prenumerationstjanst.repository.SubscriptionRepository;
import se.skl.prenumerationstjanst.repository.UserContextRepository;
import se.skl.prenumerationstjanst.repository.UserRepository;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

/**
 * Test that makes sure runtime errors while monitoring does not affect normal application flow
 *
 * @author Martin Samuelsson
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {
        PrenumerationstjanstITestApplication.class,
        SubscriptionServiceFailedMonitoringITest.TestConfig.class
})
@DirtiesContext
@ActiveProfiles("test")
public class SubscriptionServiceFailedMonitoringITest {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionServiceFailedMonitoringITest.class);

    public static final String USER_CRN = "191212121212";
    public static final LocalDateTime ORIGINAL_TOKEN_EXPIRATION = LocalDateTime.now().minusMonths(1);
    public static final LocalDateTime NEW_TOKEN_EXPIRATION = LocalDateTime.now();

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private UserContextRepository userContextRepository;

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private ApigwConsentService apigwConsentService;

    @Before
    public void setUp() throws Exception {
        UserContext userContext = new UserContext(userRepository.save(new User(USER_CRN, "phkPersonId")),
                USER_CRN, "phkRecordId");
        userContextRepository.save(userContext);
        Subscription subscription = new Subscription(userContext, TransferType.MATERNITY_MEDICAL_HISTORY);
        ApigwConsent apigwConsent = new ApigwConsent();
        apigwConsent.setScope("scope");
        apigwConsent.setExpirationDateTime(ORIGINAL_TOKEN_EXPIRATION);
        apigwConsent.setTokenValue("token");
        subscription.setApigwConsent(apigwConsent);
        subscriptionRepository.save(subscription);
        doThrow(new UncategorizedJmsException("mepmep")).when(jmsTemplate).convertAndSend(anyString(), any
                (MonitoredUserEventDTO.class));
        when(apigwConsentService.getConsent(anyString(), anyString(), any(TransferType.class)))
                .thenReturn(ApigwConsentDTO.create()
                    .withScope("newScope").withTokenValue("newToken").withExpirationDateTime(NEW_TOKEN_EXPIRATION));
    }

    @After
    public void tearDown() throws Exception {
        subscriptionRepository.deleteAll();
        userRepository.deleteAll();
        userContextRepository.deleteAll();
    }

    @Test
    public void testUpdateSubscriptionHealthcareExpiration() throws Exception {
        assertEquals(1, subscriptionRepository.count());
        Subscription subscription = subscriptionRepository.findAll().iterator().next();
        assertEquals(ORIGINAL_TOKEN_EXPIRATION, subscription.getApigwConsent().getExpirationDateTime());
        String subscriptionId = subscription.getId();
        LocalDateTime now = LocalDateTime.now();
        subscriptionService.updateSubscriptionHealthcareExpiration(subscriptionId);
        subscription = subscriptionRepository.findOne(subscriptionId);
        assertEquals(NEW_TOKEN_EXPIRATION, subscription.getApigwConsent().getExpirationDateTime());
    }

    //TODO: phkConsent

    @Test
    public void testRemoveSubscription() throws Exception {

    }

    @TestConfiguration
    @Configuration
    public static class TestConfig {

        @Bean
        public JmsTemplate jmsTemplate() {
            return Mockito.mock(JmsTemplate.class);
        }

        @Bean
        public ApigwConsentService apigwConsentService() {
            return Mockito.mock(ApigwConsentService.class);
        }
    }
}