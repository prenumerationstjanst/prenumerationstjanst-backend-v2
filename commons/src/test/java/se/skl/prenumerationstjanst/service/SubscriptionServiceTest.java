package se.skl.prenumerationstjanst.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.skl.prenumerationstjanst.dto.ApigwConsentDTO;
import se.skl.prenumerationstjanst.dto.SubscriptionDTO;
import se.skl.prenumerationstjanst.exception.subscription.SubscriptionCreationFailedException;
import se.skl.prenumerationstjanst.exception.user.UserNotFoundException;
import se.skl.prenumerationstjanst.model.*;
import se.skl.prenumerationstjanst.repository.SubscriptionRepository;
import se.skl.prenumerationstjanst.repository.UserContextRepository;
import se.skl.prenumerationstjanst.repository.UserRepository;
import se.skl.prenumerationstjanst.service.monitoring.SubscriptionMonitoringService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

/**
 * @author Martin Samuelsson
 */
@RunWith(MockitoJUnitRunner.class)
public class SubscriptionServiceTest {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionServiceTest.class);

    public static final String USER_CRN = "userCrn";
    public static final String USER_NOT_FOUND_CRN = "userNotFoundCrn";

    @Mock
    private Subscription expectedSavedSubscription;

    @Mock
    private ApigwConsent expectedConsentForSavedSubscription;

    @Mock
    private SubscriptionDTO expectedReturnedSubscriptionDTO;

    @Mock
    private TransformationService transformationService;

    @Mock
    private ApigwConsentService apigwConsentService;

    @Mock
    private User user;

    @Mock
    private UserContext userContext;

    private List<Subscription> listOfUserContextSubscriptions;

    private ApigwConsentDTO expectedApigwConsentDTOForUser;

    @Mock
    private SubscriptionRepository subscriptionRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    UserContextRepository userContextRepository;

    @Mock
    private SubscriptionMonitoringService subscriptionMonitoringService;

    @InjectMocks
    private SubscriptionService subscriptionService;

    @Before
    public void setUp() {
        when(userRepository.findByCrn(USER_CRN)).thenReturn(user);
        when(userRepository.findByCrn(USER_NOT_FOUND_CRN)).thenReturn(null);
        when(userContextRepository.findByUserCrnAndRepresentingCrn(USER_CRN, USER_CRN)).thenReturn(userContext);
        when(expectedSavedSubscription.getApigwConsent()).thenReturn(expectedConsentForSavedSubscription);
        when(expectedSavedSubscription.getUserContext()).thenReturn(userContext);
        TransferType approved = TransferType.VACCINATION_HISTORY;
        when(expectedSavedSubscription.getApprovedTransferType()).thenReturn(approved);
        when(expectedConsentForSavedSubscription.getTokenValue()).thenReturn("mep");
        when(subscriptionRepository.save(any(Subscription.class))).thenReturn(expectedSavedSubscription);
        when(subscriptionRepository.findOne(anyString())).thenReturn(expectedSavedSubscription);
        when(subscriptionRepository.findForUserContextByTransferType(USER_CRN, USER_CRN, TransferType.MATERNITY_MEDICAL_HISTORY))
                .thenReturn(expectedSavedSubscription);
        when(transformationService.toDTO(any(Subscription.class))).thenReturn(expectedReturnedSubscriptionDTO);
        when(user.getCrn()).thenReturn(USER_CRN);
        listOfUserContextSubscriptions = new ArrayList<>();
        listOfUserContextSubscriptions.add(createSubscription(userContext, true, TransferType.MATERNITY_MEDICAL_HISTORY));
        when(userContext.getSubscriptions()).thenReturn(listOfUserContextSubscriptions);
        when(userContext.getRepresentingCrn()).thenReturn(USER_CRN);
        when(userContext.getUser()).thenReturn(user);
        expectedApigwConsentDTOForUser = createApigwConsentDTO("expected-for-user", LocalDateTime.MAX);
        when(apigwConsentService.getConsent(eq(USER_CRN), eq(null), any(TransferType.class))).thenReturn(expectedApigwConsentDTOForUser);
    }

    @Test
    public void testCreateSubscription_WithOKTransferType() throws Exception {
        TransferType approvedTransferType = TransferType.VACCINATION_HISTORY;
        assertEquals(expectedReturnedSubscriptionDTO, subscriptionService.createSubscription(USER_CRN, USER_CRN, approvedTransferType));
        verify(userContextRepository).findByUserCrnAndRepresentingCrn(USER_CRN, USER_CRN);
        verify(subscriptionRepository).findForUserContextByTransferType(USER_CRN, USER_CRN, approvedTransferType);
        ArgumentCaptor<Subscription> subscriptionArgumentCaptor = ArgumentCaptor.forClass(Subscription.class);
        verify(subscriptionRepository).save(subscriptionArgumentCaptor.capture());
        Subscription capturedSubscription = subscriptionArgumentCaptor.getValue();
        assertEquals(USER_CRN ,capturedSubscription.getUserContext().getRepresentingCrn());
        assertSame(user, capturedSubscription.getUserContext().getUser());
        assertEquals(approvedTransferType, capturedSubscription.getApprovedTransferType());
        verify(transformationService).toDTO(expectedSavedSubscription);
        verify(subscriptionMonitoringService).monitorCreateSubscription(anyString(), anyString(), anyString(),
                anyString(), anyString(), any(TransferType.class));
        verify(subscriptionMonitoringService).monitorUpdateSubscription(anyString(), anyString(), anyString(),
                anyString(), anyString(), eq(SubscriptionMonitoringService.UpdateType.HEALTHCARE_EXPIRATION), any(TransferType.class));
        verify(apigwConsentService).getConsent(eq(USER_CRN), eq(null), eq(TransferType.VACCINATION_HISTORY));
        verifyNoMoreInteractions(userRepository, subscriptionRepository, transformationService, apigwConsentService, subscriptionMonitoringService);
    }

    @Test
    public void testCreateSubscription_WithNotOKTransferType() throws Exception {
        TransferType approvedTransferType = TransferType.MATERNITY_MEDICAL_HISTORY;
        try {
            subscriptionService.createSubscription(USER_CRN, USER_CRN, approvedTransferType);
        } catch (RuntimeException e) {
            logger.error("exception", e);
            assertEquals(SubscriptionCreationFailedException.class, e.getClass());
        }
        verify(userContextRepository).findByUserCrnAndRepresentingCrn(USER_CRN, USER_CRN);
        verify(subscriptionRepository).findForUserContextByTransferType(USER_CRN, USER_CRN, approvedTransferType);
        verifyNoMoreInteractions(userRepository, subscriptionRepository, transformationService, apigwConsentService, subscriptionMonitoringService);
    }

    @Test
    public void testCreateSubscription_unknownUser() throws Exception {
        TransferType approvedTransferType = TransferType.VACCINATION_HISTORY;
        try {
            subscriptionService.createSubscription(USER_NOT_FOUND_CRN, USER_NOT_FOUND_CRN, approvedTransferType);
        } catch (RuntimeException e) {
            assertEquals(UserNotFoundException.class, e.getClass());
        }
        verify(userContextRepository).findByUserCrnAndRepresentingCrn(USER_NOT_FOUND_CRN, USER_NOT_FOUND_CRN);
        verifyNoMoreInteractions(userRepository, subscriptionRepository, transformationService, apigwConsentService, subscriptionMonitoringService);
    }

    @Test
    public void testRemoveSubscription() throws Exception {
        subscriptionService.removeSubscription("foo");
        verify(subscriptionRepository).findOne(eq("foo"));
        verify(expectedSavedSubscription).setScheduledForRemoval(eq(true));
        verify(subscriptionRepository).save(eq(expectedSavedSubscription));
        verify(apigwConsentService).deleteConsent(eq("mep"));
        verify(subscriptionMonitoringService).monitorRemoveSubscription(anyString(), anyString(), anyString(),
                anyString(), anyString());
        verifyNoMoreInteractions(subscriptionRepository, apigwConsentService, subscriptionMonitoringService);
    }

    @Test
    public void testInternalRemoveSubscription() throws Exception {
        subscriptionService.internalRemoveSubscription("foo");
        verify(subscriptionRepository).findOne(eq("foo"));
        verify(subscriptionRepository).delete(eq(expectedSavedSubscription));
        verifyNoMoreInteractions(subscriptionRepository);
    }

    private Subscription createSubscription(UserContext userContext, boolean addConsent, TransferType transferType) {
        Subscription subscription = new Subscription(userContext, transferType);
        String id = UUID.randomUUID().toString();
        logger.debug("createSubscription id[{}], transferType[{}]", id, userContext, transferType);
        subscription.setId(id);
        if (addConsent) {
            ApigwConsent apigwConsent = new ApigwConsent();
            apigwConsent.setScope("mep");
            apigwConsent.setTokenValue("mep");
            apigwConsent.setExpirationDateTime(LocalDateTime.now().plusDays(1));
            subscription.setApigwConsent(apigwConsent);
        }
        return subscription;
    }


    private ApigwConsentDTO createApigwConsentDTO(String tokenValue, LocalDateTime expirationDateTime) {
        return ApigwConsentDTO.create()
                .withTokenValue(tokenValue)
                .withExpirationDateTime(expirationDateTime);
    }
}