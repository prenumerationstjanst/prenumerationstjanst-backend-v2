package se.skl.prenumerationstjanst.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import se.skl.prenumerationstjanst.PrenumerationstjanstConstants;
import se.skl.prenumerationstjanst.dto.SubscriptionDTO;
import se.skl.prenumerationstjanst.dto.TransferJobDTO;
import se.skl.prenumerationstjanst.dto.internal.InitialTransferParams;
import se.skl.prenumerationstjanst.dto.internal.SingleSourceSystemTransferParams;
import se.skl.prenumerationstjanst.model.TransferJob;
import se.skl.prenumerationstjanst.model.TransferType;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static se.skl.prenumerationstjanst.PrenumerationstjanstConstants.START_TRANSFER_FOR_SINGLE_SOURCE_SYSTEM_QUEUE;

/**
 * @author Martin Samuelsson
 */
@RunWith(MockitoJUnitRunner.class)
public class TransferServiceTest {

    private static final Logger logger = LoggerFactory.getLogger(TransferServiceTest.class);

    @Mock
    private JmsTemplate jmsTemplate;

    @InjectMocks
    private TransferService transferService;

    @Test
    public void testStartInitialTransfersForSubscription() throws Exception {
        transferService.startInitialTransfersForSubscription("subId", TransferType.VACCINATION_HISTORY.name());

        ArgumentCaptor<InitialTransferParams> initialTransferParamsArgumentCaptor = ArgumentCaptor
                .forClass(InitialTransferParams.class);
        verify(jmsTemplate).convertAndSend(eq(PrenumerationstjanstConstants.INITIAL_TRANSFERS_FOR_SUBSCRIPTION_QUEUE), initialTransferParamsArgumentCaptor.capture());
        assertEquals("subId", initialTransferParamsArgumentCaptor.getValue().getSubscriptionId());
        assertEquals(TransferType.VACCINATION_HISTORY.name(), initialTransferParamsArgumentCaptor.getValue().getTransferType());
    }

    @Test
    public void testStartTransferForFailedSourceSystem() throws Exception {
        transferService.startTransferForFailedSourceSystem(TransferJobDTO.create()
                .withSubscriptionId(SubscriptionDTO.create().withId("subId").getId())
                .withTransferType(TransferType.MATERNITY_MEDICAL_HISTORY.name())
                .withStartDateTime(LocalDateTime.MIN)
                .withEndDateTime(LocalDateTime.MAX), "failedSourceSystem");
        ArgumentCaptor<SingleSourceSystemTransferParams> paramsArgumentCaptor =
                ArgumentCaptor.forClass(SingleSourceSystemTransferParams.class);
        verify(jmsTemplate).convertAndSend(eq(START_TRANSFER_FOR_SINGLE_SOURCE_SYSTEM_QUEUE),
                paramsArgumentCaptor.capture());
        SingleSourceSystemTransferParams params = paramsArgumentCaptor.getValue();
        assertEquals("subId", params.getSubscriptionId());
        assertEquals(TransferType.MATERNITY_MEDICAL_HISTORY, params.getTransferType());
        assertEquals(LocalDateTime.MIN, params.getStartDateTime());
        assertEquals(LocalDateTime.MAX, params.getEndDateTime());
        assertEquals("failedSourceSystem", params.getSourceSystemHsaId());
        assertEquals(TransferJob.Origin.FAILED_SOURCE_SYSTEM, params.getOrigin());
    }

    @Test
    public void testStartTransferForProcessNotification() throws Exception {
        LocalDateTime documentTime = LocalDateTime.now();
        LocalDateTime expectedStart = documentTime.toLocalDate().atStartOfDay();
        LocalDateTime expectedEnd = documentTime.toLocalDate().atTime(23, 59, 59);
        transferService.startTransferForProcessNotification("subId", TransferType.MATERNITY_MEDICAL_HISTORY,
                documentTime, "sourceSystem");
        ArgumentCaptor<SingleSourceSystemTransferParams> paramsArgumentCaptor =
                ArgumentCaptor.forClass(SingleSourceSystemTransferParams.class);
        verify(jmsTemplate).convertAndSend(eq(PrenumerationstjanstConstants.START_TRANSFER_FOR_SINGLE_SOURCE_SYSTEM_QUEUE),
                paramsArgumentCaptor.capture());
        SingleSourceSystemTransferParams params = paramsArgumentCaptor.getValue();
        assertEquals("subId", params.getSubscriptionId());
        assertEquals(TransferType.MATERNITY_MEDICAL_HISTORY, params.getTransferType());
        assertEquals(expectedStart, params.getStartDateTime());
        assertEquals(expectedEnd, params.getEndDateTime());
        assertEquals("sourceSystem", params.getSourceSystemHsaId());
        assertEquals(TransferJob.Origin.EI_NOTIFICATION, params.getOrigin());
    }
}