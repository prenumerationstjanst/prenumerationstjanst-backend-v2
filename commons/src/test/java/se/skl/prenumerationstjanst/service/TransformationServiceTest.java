package se.skl.prenumerationstjanst.service;

import org.junit.Assert;
import org.junit.Test;
import se.skl.prenumerationstjanst.dto.*;
import se.skl.prenumerationstjanst.model.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Martin Samuelsson
 */
public class TransformationServiceTest {

    private static final String USER_CRN = "userCrn";
    private static final String PHK_PERSON_ID = "phkPersonId";
    private static final String PHK_RECORD_ID = "phkRecordId";
    private static final String REPRESENTING_CRN = "191212121212";
    private static final String SUBSCRIPTION_ID = "sub-id";
    private static final String APIGW_TOKEN = "apigwToken";
    private TransformationService transformationService = new TransformationService();

    @Test
    public void testToDTO_UserWithNoUserContexts() throws Exception {
        User user = new User(USER_CRN, null);
        user.setId(1L);
        UserDTO userDTO = transformationService.toDTO(user);
        Assert.assertEquals(USER_CRN, userDTO.getCrn());
        Assert.assertEquals((Long)1L, userDTO.getId());
        Assert.assertEquals(0, userDTO.getUserContexts().size());
    }

    @Test
    public void testToDTO_UserWithTwoUserContextsWithSubscriptions() throws Exception {
        User user = new User(USER_CRN, null);
        user.setId(1L);
        List<UserContext> userContexts = new ArrayList<>();
        for (int i = 0; i < 2; i += 1) {
            List<Subscription> subscriptions = new ArrayList<>();
            UserContext userContext = createUserContext("19121212121" + i, "rId" + i, subscriptions, user);
            userContexts.add(userContext);
            subscriptions.add(createSubscription("sub1", userContext, TransferType.MATERNITY_MEDICAL_HISTORY));
            subscriptions.add(createSubscription("sub2", userContext, TransferType.VACCINATION_HISTORY));
        }
        user.setUserContexts(userContexts);
        UserDTO userDTO = transformationService.toDTO(user);
        Assert.assertEquals(USER_CRN, userDTO.getCrn());
        Assert.assertEquals((Long)1L, userDTO.getId());
        Assert.assertEquals(2, userDTO.getUserContexts().size());
        for (int i = 0; i < 2; i+= 1) {
            UserContextDTO userContextDTO = userDTO.getUserContexts().get(0);
            SubscriptionDTO subscriptionDTO1 = userContextDTO.getSubscriptions().get(0);
            Assert.assertEquals("sub1", subscriptionDTO1.getId());
            Assert.assertNotNull(subscriptionDTO1.getApprovedTransferType());
            assertTrue(subscriptionDTO1.getApprovedTransferType().contains(TransferType.MATERNITY_MEDICAL_HISTORY.name()));
            SubscriptionDTO subscriptionDTO2 = userContextDTO.getSubscriptions().get(1);
            Assert.assertEquals("sub2", subscriptionDTO2.getId());
            Assert.assertNotNull(subscriptionDTO2.getApprovedTransferType());
            assertTrue(subscriptionDTO2.getApprovedTransferType().contains(TransferType.VACCINATION_HISTORY.name()));
        }
    }

    @Test
    public void testToDTO_SubscriptionWithApigwConsent() throws Exception {
        UserContext userContext = createUserContext(REPRESENTING_CRN, PHK_RECORD_ID, null, new User(USER_CRN, PHK_PERSON_ID));
        Subscription subscription = createSubscription(SUBSCRIPTION_ID, userContext, TransferType.MATERNITY_MEDICAL_HISTORY);
        ApigwConsent apigwConsent = new ApigwConsent();
        apigwConsent.setTokenValue("token");
        apigwConsent.setExpirationDateTime(LocalDateTime.MAX);
        subscription.setApigwConsent(apigwConsent);
        SubscriptionDTO subscriptionDTO = transformationService.toDTO(subscription);
        assertEquals(subscription.getId(), subscriptionDTO.getId());
        assertEquals(subscription.getApprovedTransferType().name(), subscriptionDTO.getApprovedTransferType());
        Assert.assertEquals("token", subscriptionDTO.getApigwConsent().getTokenValue());
        assertEquals(LocalDateTime.MAX, subscription.getApigwConsent().getExpirationDateTime());
        assertNull(subscriptionDTO.getApigwConsent().getSubscription());
    }

    @Test
    public void testToDTO_TransferJobWithNoUnderlyingJobs() throws Exception {
        TransferJob transferJob = createTransferJob(null);
        transferJob.prePersist(); //simulate persisted TransferJob
        TransferJobDTO transferJobDTO = transformationService.toDTO(transferJob);
        assertEquals(transferJob.getId(), transferJobDTO.getId());
        assertNotNull(transferJobDTO.getUserCrn());
        assertNotNull(transferJobDTO.getRepresentingCrn());
        assertNotNull(transferJobDTO.getSubscriptionId());
        assertEquals(transferJob.getTransferType(), TransferType.valueOf(transferJobDTO.getTransferType()));
        assertEquals(transferJob.getSourceSystemHsaId(), transferJobDTO.getSourceSystemHsaId());
        assertEquals(transferJob.getStartDateTime(), transferJobDTO.getStartDateTime());
        assertEquals(transferJob.getEndDateTime(), transferJobDTO.getEndDateTime());
        assertNull(transferJobDTO.getFetchJob());
        Assert.assertEquals(0, transferJobDTO.getPushJobs().size());
    }

    @Test
    public void testToDTO_TransferJobWithUnderlyingJobs() throws Exception {
        TransferJob transferJob = createTransferJob(null);
        FetchJob fetchJob = new FetchJob(transferJob, null);
        fetchJob.prePersist(); //simulate persisted FetchJob
        transferJob.setFetchJob(fetchJob);
        transferJob.prePersist(); //simlate persisted TransferJob
        List<PushJob> pushJobs = new ArrayList<>();
        PushJob pushJob = new PushJob(transferJob, "phkPersonId", PHK_RECORD_ID);
        pushJob.prePersist(); //simulate persisted PushJob
        pushJobs.add(pushJob);
        transferJob.setPushJobs(pushJobs);
        TransferJobDTO transferJobDTO = transformationService.toDTO(transferJob);
        assertEquals(transferJob.getId(), transferJobDTO.getId());
        assertNotNull(transferJobDTO.getUserCrn());
        assertNotNull(transferJobDTO.getRepresentingCrn());
        assertNotNull(transferJobDTO.getSubscriptionId());
        assertEquals(transferJob.getTransferType(), TransferType.valueOf(transferJobDTO.getTransferType()));
        assertEquals(transferJob.getSourceSystemHsaId(), transferJobDTO.getSourceSystemHsaId());
        assertEquals(transferJob.getStartDateTime(), transferJobDTO.getStartDateTime());
        assertEquals(transferJob.getEndDateTime(), transferJobDTO.getEndDateTime());
        assertNotNull(transferJobDTO.getFetchJob());
        assertEquals(transferJob.getPushJobs().size(), transferJobDTO.getPushJobs().size());
    }

    @Test
    public void testToDTO_FetchJob() throws Exception {
        TransferJob transferJob = createTransferJob(null);
        FetchJob fetchJob = new FetchJob(transferJob, APIGW_TOKEN);
        fetchJob.setFetchFinishTime(LocalDateTime.now());
        List<PushJob> pushJobs = new ArrayList<>();
        PushJob pushJob = new PushJob(transferJob, PHK_PERSON_ID, PHK_RECORD_ID);
        pushJob.prePersist(); //simulate persisted PushJob
        pushJobs.add(pushJob);
        transferJob.setPushJobs(pushJobs);
        fetchJob.prePersist(); //simulate persisted
        FetchJobDTO fetchJobDTO = transformationService.toDTO(fetchJob);
        assertEquals(fetchJob.getId(), fetchJobDTO.getId());
        assertEquals(APIGW_TOKEN, fetchJobDTO.getApigwToken());
        assertEquals(fetchJob.getApigwToken(), fetchJobDTO.getApigwToken());
        assertEquals(fetchJob.getStatus(), fetchJobDTO.getStatus());
        assertEquals(fetchJob.getFetchFinishTime(), fetchJobDTO.getFetchFinishTime());
        assertNotNull(fetchJobDTO.getTransferJob());
        assertNotNull(fetchJobDTO.getTransferJob().getSubscriptionId());
        assertNull(fetchJobDTO.getTransferJob().getFetchJob());
        Assert.assertEquals(1, fetchJobDTO.getTransferJob().getPushJobs().size());
        PushJobDTO pushJobDTO = fetchJobDTO.getTransferJob().getPushJobs().get(0);
        assertNull(pushJobDTO.getTransferJob());
    }

    @Test
    public void testToDTO_PushJob() throws Exception {
        TransferJob transferJob = createTransferJob(null);
        transferJob.setFetchJob(new FetchJob(transferJob, null));
        PushJob pushJob = new PushJob(transferJob, PHK_PERSON_ID, PHK_RECORD_ID);
        pushJob.setSourceId("mep");
        pushJob.setSourceVersion(2);
        pushJob.prePersist(); //simlate persisted
        PushJobDTO pushJobDTO = transformationService.toDTO(pushJob);
        assertEquals(pushJob.getId(), pushJobDTO.getId());
        assertEquals(PHK_PERSON_ID, pushJobDTO.getPhkPersonId());
        assertEquals(PHK_RECORD_ID, pushJobDTO.getPhkRecordId());
        assertEquals(pushJob.getPhkPersonId(), pushJobDTO.getPhkPersonId());
        assertEquals(pushJob.getPhkRecordId(), pushJobDTO.getPhkRecordId());
        assertEquals(pushJob.getStatus(), pushJobDTO.getStatus());
        assertEquals(pushJob.getSourceId(), pushJobDTO.getSourceId());
        assertEquals(pushJob.getSourceVersion(), pushJobDTO.getSourceVersion());
        assertEquals(pushJob.getPhkPersonId(), pushJobDTO.getPhkPersonId());
        assertEquals(pushJob.getPhkRecordId(), pushJobDTO.getPhkRecordId());
        assertNotNull(pushJobDTO.getTransferJob());
        assertNotNull(pushJobDTO.getTransferJob().getSubscriptionId());
        assertNotNull(pushJobDTO.getTransferJob().getFetchJob());
        Assert.assertEquals(0, pushJobDTO.getTransferJob().getPushJobs().size());
    }

    private TransferJob createTransferJob(Subscription subscription) {
        UserContext userContext = createUserContext(REPRESENTING_CRN, PHK_RECORD_ID, null, new User(USER_CRN, PHK_PERSON_ID));
        Subscription s = subscription != null ? subscription :
                createSubscription("sub-id", userContext, TransferType.MATERNITY_MEDICAL_HISTORY);
        return new TransferJob(
                s.getUserContext().getUser().getCrn(),
                s.getUserContext().getRepresentingCrn(),
                s.getId(),
                TransferType.MATERNITY_MEDICAL_HISTORY,
                LocalDateTime.MIN,
                LocalDateTime.MAX,
                "ss-id",
                TransferJob.Origin.INITIAL
        );
    }

    private Subscription createSubscription(String id, UserContext userContext, TransferType transferType) {
        Subscription subscription = new Subscription(userContext, transferType);
        subscription.setId(id);
        return subscription;
    }

    private UserContext createUserContext(String representingCrn, String phkRecordId, List<Subscription> subscriptions,
                                          User user)  {
        UserContext userContext = new UserContext(user, representingCrn, phkRecordId);
        userContext.setSubscriptions(subscriptions);
        return userContext;
    }
}