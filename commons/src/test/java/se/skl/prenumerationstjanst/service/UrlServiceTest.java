package se.skl.prenumerationstjanst.service;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.util.ReflectionTestUtils;

import java.nio.charset.Charset;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

/**
 * @author Martin Samuelsson
 */
@RunWith(MockitoJUnitRunner.class)
public class UrlServiceTest {

    private static final Logger logger = LoggerFactory.getLogger(UrlServiceTest.class);
    public static final String SELF_PUBLIC_BASE_URI = "http://localhost";
    public static final String PHK_AUTH_START_PATH = "/m/e/p";
    public static final String PHK_AUTH_START_PATH_WO_STARTING_SLASH = "m/e/p";
    public static final String PHK_AUTH_CALLBACK_PATH = "/d/e/r/p";
    public static final String PHK_CLIENT_ID = "bar";
    public static final String STATE = "foo";
    public static final String PHK_CHBASE_BASE_URI = "http://phk/mepmep.aspx";

    private UrlService urlService;

    @Before
    public void setUp() throws Exception {
        urlService = new UrlService();
        ReflectionTestUtils.setField(urlService, "selfPublicBaseUri", SELF_PUBLIC_BASE_URI);
        ReflectionTestUtils.setField(urlService, "phkAuthorizeStartPath", PHK_AUTH_START_PATH);
        ReflectionTestUtils.setField(urlService, "phkAuthorizeCallbackPath", PHK_AUTH_CALLBACK_PATH);
        ReflectionTestUtils.setField(urlService, "phkCHBaseAppId", PHK_CLIENT_ID);
        ReflectionTestUtils.setField(urlService, "phkCHBaseBaseShellRedirectUri", PHK_CHBASE_BASE_URI);
    }

    @Test
    public void testGetPhkAuthorizeStartUrl() throws Exception {
        String phkAuthorizeStartUrl = urlService.getPhkAuthorizeStartUrl();
        logger.debug("phkAuthorizeStartUrl: {}", phkAuthorizeStartUrl);
        assertEquals(SELF_PUBLIC_BASE_URI + PHK_AUTH_START_PATH, phkAuthorizeStartUrl);
    }

    @Test
    public void testGetPhkAuthorizeStartUrlWithoutStartingSlash() throws Exception {
        ReflectionTestUtils.setField(urlService, "phkAuthorizeStartPath", PHK_AUTH_START_PATH_WO_STARTING_SLASH);
        String phkAuthorizeStartUrl = urlService.getPhkAuthorizeStartUrl();
        logger.debug("phkAuthorizeStartUrl: {}", phkAuthorizeStartUrl);
        assertEquals(SELF_PUBLIC_BASE_URI + "/" + PHK_AUTH_START_PATH_WO_STARTING_SLASH, phkAuthorizeStartUrl);
    }

    @Test
    public void testGetPhkRemoteAuthUrl() throws Exception {
        String phkRemoteUrl = urlService.getPhkRemoteAuthUrl(STATE);
        assertPhkRemoteUrl(phkRemoteUrl, "AUTH");
    }

    @Test
    public void testPostConstruct() throws Exception {
        final String[] o = {PHK_CHBASE_BASE_URI};
        PhkInfoService infoService = new PhkInfoServiceImpl() {
            @Override
            public String getShellRedirectUrl() {
                return o[0];
            }
        };
        ReflectionTestUtils.setField(urlService, "phkInfoService", infoService);

        urlService.postConstruct(); //should _not_ replace the default
        assertTrue(urlService.getPhkRemoteAuthUrl("mep").startsWith(PHK_CHBASE_BASE_URI));

        o[0] = null;
        urlService.postConstruct(); //should _not_ replace the default
        assertTrue(urlService.getPhkRemoteAuthUrl("mep").startsWith(PHK_CHBASE_BASE_URI));

        o[0] = "http://mep";
        urlService.postConstruct(); //should replace the default
        assertFalse(urlService.getPhkRemoteAuthUrl("mep").startsWith(PHK_CHBASE_BASE_URI));
        assertTrue(urlService.getPhkRemoteAuthUrl("mep").startsWith(o[0]));
    }

    private void assertPhkRemoteUrl(String phkRemoteUrl, String expectedTarget) {
        List<NameValuePair> parameters = URLEncodedUtils.parse(phkRemoteUrl.split("\\?")[1], Charset.forName("utf8"));
        assertTrue(parameters.stream().anyMatch(nvp -> nvp.getName().equals("target") && nvp.getValue().equals(expectedTarget)));
        assertTrue(parameters.stream().anyMatch(nvp -> nvp.getName().equals("targetqs")));
        NameValuePair targetqs = parameters.stream()
                .filter(nvp -> nvp.getName().equals("targetqs"))
                .findFirst()
                .get();
        List<NameValuePair> encodedParameters = URLEncodedUtils.parse(targetqs.getValue(), Charset.forName("utf8"));
        assertTrue(encodedParameters.stream()
                .anyMatch(nvp -> nvp.getName().equals("appid") && nvp.getValue().equals(PHK_CLIENT_ID)));
        assertTrue(encodedParameters.stream()
                .anyMatch(nvp -> nvp.getName().equals("actionqs") && nvp.getValue().equals(STATE)));
        logger.debug("phkRemoteUrl: {}", phkRemoteUrl);
    }
}