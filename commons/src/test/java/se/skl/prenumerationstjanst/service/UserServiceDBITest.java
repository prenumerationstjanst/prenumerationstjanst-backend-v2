package se.skl.prenumerationstjanst.service;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import se.skl.prenumerationstjanst.PrenumerationstjanstITestApplication;
import se.skl.prenumerationstjanst.dto.UserDTO;
import se.skl.prenumerationstjanst.exception.user.UserCreationFailedException;
import se.skl.prenumerationstjanst.model.User;
import se.skl.prenumerationstjanst.repository.SubscriptionRepository;
import se.skl.prenumerationstjanst.repository.UserContextRepository;
import se.skl.prenumerationstjanst.repository.UserRepository;

import static org.junit.Assert.assertEquals;

/**
 * @author Martin Samuelsson
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = PrenumerationstjanstITestApplication.class)
@DirtiesContext
@ActiveProfiles("test")
public class UserServiceDBITest {

    public static final String USER_CRN = "191212121212";
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserContextRepository userContextRepository;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private UserService userService;

    @Before
    public void setUp() throws Exception {
        userRepository.deleteAll();
        userContextRepository.deleteAll();
        subscriptionRepository.deleteAll();
    }

    @After
    public void tearDown() throws Exception {
        userRepository.deleteAll();
    }

    @Test
    public void testGetUser() throws Exception {
        assertEquals(0, userRepository.count());
        User user = new User(USER_CRN, "mep");
        userRepository.save(user);
        UserDTO userDTO = userService.getUser(USER_CRN);
        Assert.assertEquals(userDTO.getCrn(), USER_CRN);
    }

    @Test
    public void testCreateUser() throws Exception {
        assertEquals(0, userRepository.count());
        userService.createUser(USER_CRN, "mep");
        assertEquals(1, userRepository.count());
    }

    @Test(expected = UserCreationFailedException.class)
    public void testCreateUser_UserAlreadyExists() throws Exception {
        assertEquals(0, userRepository.count());
        User user = new User(USER_CRN, "mep");
        userRepository.save(user);
        assertEquals(1, userRepository.count());
        userService.createUser(USER_CRN, "mep2");
    }

}