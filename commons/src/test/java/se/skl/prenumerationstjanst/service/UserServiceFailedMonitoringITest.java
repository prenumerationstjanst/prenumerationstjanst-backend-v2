package se.skl.prenumerationstjanst.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.UncategorizedJmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import se.skl.prenumerationstjanst.PrenumerationstjanstITestApplication;
import se.skl.prenumerationstjanst.config.TestConfiguration;
import se.skl.prenumerationstjanst.dto.MonitoredUserEventDTO;
import se.skl.prenumerationstjanst.repository.UserRepository;
import se.skl.prenumerationstjanst.service.monitoring.ActiveMQMonitoringService;
import se.skl.prenumerationstjanst.service.monitoring.MonitoringService;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

/**
 * Test to make sure failed monitoring rolls back the transaction
 *
 * @author Martin Samuelsson
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {
        PrenumerationstjanstITestApplication.class,
        UserServiceFailedMonitoringITest.TestConfig.class
})
@DirtiesContext
@ActiveProfiles("test")
public class UserServiceFailedMonitoringITest {

    public static final String USER_CRN = "191212121212";
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private UserService userService;

    @Before
    public void setUp() throws Exception {
        userRepository.deleteAll();
        doThrow(new UncategorizedJmsException("mepmep")).when(jmsTemplate).convertAndSend(anyString(), any
                (MonitoredUserEventDTO.class));
    }

    @After
    public void tearDown() throws Exception {
        userRepository.deleteAll();
    }

    @Test
    public void testCreateUserMonitoringFailed() throws Exception {
        assertEquals(0, userRepository.count());
        try {
            userService.createUser(USER_CRN, "mep");
        } catch (UncategorizedJmsException e) {}
        assertEquals(0, userRepository.count());
        verify(jmsTemplate).convertAndSend(anyString(), any(MonitoredUserEventDTO.class));
    }

    @TestConfiguration
    @Configuration
    public static class TestConfig {

        @Bean
        public JmsTemplate jmsTemplate() {
            return Mockito.mock(JmsTemplate.class);
        }

        @Bean
        public MonitoringService monitoringService() {
            return new ActiveMQMonitoringService();
        }
    }
}