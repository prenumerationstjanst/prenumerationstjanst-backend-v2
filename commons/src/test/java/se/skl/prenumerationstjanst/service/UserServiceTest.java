package se.skl.prenumerationstjanst.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import se.skl.prenumerationstjanst.dto.UserDTO;
import se.skl.prenumerationstjanst.exception.user.UserCreationFailedException;
import se.skl.prenumerationstjanst.model.User;
import se.skl.prenumerationstjanst.model.UserContext;
import se.skl.prenumerationstjanst.repository.UserRepository;
import se.skl.prenumerationstjanst.service.monitoring.UserMonitoringService;

import java.util.ArrayList;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

/**
 * @author Martin Samuelsson
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    public static final String USER_FOUND_CRN = "foo";
    public static final String USER_NOT_FOUND_CRN = "bar";

    private User expectedUser;

    private User expectedNewUser;

    private UserDTO expectedUserDTO;

    private UserDTO expectedNewUserDTO;

    @Mock
    private UserRepository userRepository;

    @Mock
    private TransformationService transformationService;

    @Mock
    private UserMonitoringService userMonitoringService;

    @InjectMocks
    private UserService userService;

    @Before
    public void setUp() {
        expectedUser = new User(USER_FOUND_CRN, null);

        expectedUser.setUserContexts(new ArrayList<>());
        expectedUser.getUserContexts().add(new UserContext(expectedUser, USER_FOUND_CRN, USER_FOUND_CRN));
        expectedNewUser = new User(USER_NOT_FOUND_CRN, null);
        expectedUserDTO = UserDTO.create()
                .withCrn(USER_FOUND_CRN);
        expectedNewUserDTO = UserDTO.create()
                .withCrn(USER_NOT_FOUND_CRN);
        reset(userRepository, transformationService);
        when(userRepository.findByCrn(USER_FOUND_CRN)).thenReturn(expectedUser);
        when(userRepository.findByCrn(USER_NOT_FOUND_CRN)).thenReturn(null);
        when(userRepository.save(any(User.class))).thenReturn(expectedNewUser);
        when(transformationService.toDTO(expectedUser)).thenReturn(expectedUserDTO);
        when(transformationService.toDTO(expectedNewUser)).thenReturn(expectedNewUserDTO);
    }

    @Test
    public void testGetUser_found() throws Exception {
        UserDTO userDTO = userService.getUser(USER_FOUND_CRN);
        assertSame(expectedUserDTO, userDTO);
        verify(userRepository).findByCrn(eq(USER_FOUND_CRN));
        verify(transformationService).toDTO(expectedUser);
        verifyNoMoreInteractions(userRepository, transformationService);
    }

    @Test
    public void testGetUser_notFound() throws Exception {
        UserDTO userDTO = userService.getUser(USER_NOT_FOUND_CRN);
        assertNull(userDTO);
        verify(userRepository).findByCrn(eq(USER_NOT_FOUND_CRN));
        verifyNoMoreInteractions(userRepository, transformationService);
    }

    @Test(expected = UserCreationFailedException.class)
    public void testCreateUser_alreadyExists() throws Exception {
        userService.createUser(USER_FOUND_CRN, "mep");
    }

}