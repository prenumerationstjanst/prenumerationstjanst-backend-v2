package se.skl.prenumerationstjanst.service.monitoring;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jms.core.JmsTemplate;
import se.skl.prenumerationstjanst.PrenumerationstjanstConstants;
import se.skl.prenumerationstjanst.dto.MonitoredUserEventDTO;
import se.skl.prenumerationstjanst.model.monitoring.MonitoredUserEventType;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

/**
 * @author Martin Samuelsson
 */
@RunWith(MockitoJUnitRunner.class)
public class ActiveMQMonitoringServiceTest {

    @Mock
    private JmsTemplate jmsTemplate;

    @InjectMocks
    private ActiveMQMonitoringService activeMQMonitoringService;

    @Test
    public void testMonitorUserEvent() throws Exception {
        Map<String, String> eventDetails = new HashMap<>();
        eventDetails.put("foo", "bar");
        eventDetails.put("baz", "qux");
        activeMQMonitoringService.monitorUserEvent("191212121212", "127.0.0.1", "corrId", MonitoredUserEventType
                .USER_ACCEPTS_TERMS_OF_SERVICE, eventDetails);

        ArgumentCaptor<MonitoredUserEventDTO> monitoredUserEventDTOArgumentCaptor = ArgumentCaptor
                .forClass(MonitoredUserEventDTO.class);
        verify(jmsTemplate).convertAndSend(eq(PrenumerationstjanstConstants.MONITORING_QUEUE),
                monitoredUserEventDTOArgumentCaptor.capture());
        MonitoredUserEventDTO capturedValue = monitoredUserEventDTOArgumentCaptor.getValue();
        Assert.assertEquals("191212121212", capturedValue.getUserCrn());
        Assert.assertEquals("127.0.0.1", capturedValue.getUserIpAddress());
        Assert.assertEquals("corrId", capturedValue.getCorrelationId());
        assertEquals(MonitoredUserEventType.USER_ACCEPTS_TERMS_OF_SERVICE, capturedValue.getEventType());
        Assert.assertEquals(2, capturedValue.getEventDetails().size());
        Assert.assertEquals("bar", capturedValue.getEventDetails().get("foo"));
        Assert.assertEquals("qux", capturedValue.getEventDetails().get("baz"));
    }
}