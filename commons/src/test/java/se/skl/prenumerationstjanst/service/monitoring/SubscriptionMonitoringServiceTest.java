package se.skl.prenumerationstjanst.service.monitoring;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.skl.prenumerationstjanst.model.TransferType;
import se.skl.prenumerationstjanst.model.monitoring.MonitoredUserEventType;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * @author Martin Samuelsson
 */
@RunWith(MockitoJUnitRunner.class)
public class SubscriptionMonitoringServiceTest {

    public static final String USER_CRN = "191212121212";
    public static final String USER_IP_ADDRESS = "127.0.0.1";
    public static final String CORRELATION_ID = "corrId";
    public static final String SUBSCRIPTION_ID = "subId";
    public static final String SUBSCRIPTION_CRN = "191111111111";
    public static final TransferType TRANSFER_TYPE = TransferType.MATERNITY_MEDICAL_HISTORY;

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionMonitoringServiceTest.class);
    @Mock
    private MonitoringService monitoringService;

    @InjectMocks
    private SubscriptionMonitoringService subscriptionMonitoringService;

    @SuppressWarnings("unchecked")
    @Test
    public void testMonitorCreateSubscription() throws Exception {
        subscriptionMonitoringService.monitorCreateSubscription(USER_CRN, USER_IP_ADDRESS, CORRELATION_ID, SUBSCRIPTION_ID,
                SUBSCRIPTION_CRN, TRANSFER_TYPE);
        ArgumentCaptor<Map> mapArgumentCaptor = ArgumentCaptor.forClass(Map.class);
        verify(monitoringService).monitorUserEvent(eq(USER_CRN), eq(USER_IP_ADDRESS), eq(CORRELATION_ID), eq
                (MonitoredUserEventType.USER_CREATES_SUBSCRIPTION), mapArgumentCaptor.capture());
        Map<String, String> capturedDetails = mapArgumentCaptor.getValue();
        assertEquals(SUBSCRIPTION_ID, capturedDetails.get("subscriptionId"));
        assertEquals(SUBSCRIPTION_CRN, capturedDetails.get("subscriptionCrn"));
        assertEquals(TRANSFER_TYPE.name(), capturedDetails.get("transferType"));
        verifyNoMoreInteractions(monitoringService);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testMonitorUpdateSubscription() throws Exception {
        subscriptionMonitoringService.monitorUpdateSubscription(USER_CRN, USER_IP_ADDRESS, CORRELATION_ID,
                SUBSCRIPTION_ID, SUBSCRIPTION_CRN, SubscriptionMonitoringService.UpdateType.TRANSFER_TYPES,
                TRANSFER_TYPE);
        ArgumentCaptor<Map> mapArgumentCaptor = ArgumentCaptor.forClass(Map.class);
        verify(monitoringService).monitorUserEvent(eq(USER_CRN), eq(USER_IP_ADDRESS), eq(CORRELATION_ID), eq
                (MonitoredUserEventType.USER_UPDATES_SUBSCRIPTION), mapArgumentCaptor.capture());
        Map<String, String> capturedDetails = mapArgumentCaptor.getValue();
        assertEquals(SUBSCRIPTION_ID, capturedDetails.get("subscriptionId"));
        assertEquals(SUBSCRIPTION_CRN, capturedDetails.get("subscriptionCrn"));
        assertEquals(TRANSFER_TYPE.name(), capturedDetails.get("transferType"));
        verifyNoMoreInteractions(monitoringService);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testMonitorRemoveSubscription() throws Exception {
        subscriptionMonitoringService.monitorRemoveSubscription(USER_CRN, USER_IP_ADDRESS, CORRELATION_ID,
                SUBSCRIPTION_ID, SUBSCRIPTION_CRN);
        ArgumentCaptor<Map> mapArgumentCaptor = ArgumentCaptor.forClass(Map.class);
        verify(monitoringService).monitorUserEvent(eq(USER_CRN), eq(USER_IP_ADDRESS), eq(CORRELATION_ID), eq
                (MonitoredUserEventType.USER_REMOVES_SUBSCRIPTION), mapArgumentCaptor.capture());
        Map<String, String> capturedDetails = mapArgumentCaptor.getValue();
        assertEquals(SUBSCRIPTION_ID, capturedDetails.get("subscriptionId"));
        assertEquals(SUBSCRIPTION_CRN, capturedDetails.get("subscriptionCrn"));

    }
}