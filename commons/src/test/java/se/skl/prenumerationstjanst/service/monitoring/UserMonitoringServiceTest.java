package se.skl.prenumerationstjanst.service.monitoring;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import se.skl.prenumerationstjanst.model.monitoring.MonitoredUserEventType;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * @author Martin Samuelsson
 */
@RunWith(MockitoJUnitRunner.class)
public class UserMonitoringServiceTest {

    public static final String USER_CRN = "191212121212";
    public static final String USER_IP_ADDRESS = "127.0.0.1";
    public static final String CORRELATION_ID = "corrId";
    public static final long TOS_ID = 910L;
    public static final String TOS_TEXT = "One after 909";

    @Mock
    private MonitoringService monitoringService;

    @InjectMocks
    private UserMonitoringService userMonitoringService;

    @Test
    public void testMonitorCreateUser() throws Exception {
        userMonitoringService.monitorCreateUser(USER_CRN, USER_IP_ADDRESS, CORRELATION_ID);
        verify(monitoringService).monitorUserEvent(eq(USER_CRN), eq(USER_IP_ADDRESS), eq(CORRELATION_ID), eq
                (MonitoredUserEventType.USER_CREATES_ACCOUNT), eq(null));
        verifyNoMoreInteractions(monitoringService);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testMonitorAcceptTermsOfService() throws Exception {
        userMonitoringService.monitorAcceptTermsOfService(USER_CRN, USER_IP_ADDRESS, CORRELATION_ID, TOS_ID, TOS_TEXT);
        ArgumentCaptor<Map> mapArgumentCaptor = ArgumentCaptor.forClass(Map.class);
        verify(monitoringService).monitorUserEvent(eq(USER_CRN), eq(USER_IP_ADDRESS), eq(CORRELATION_ID),
                eq(MonitoredUserEventType.USER_ACCEPTS_TERMS_OF_SERVICE), mapArgumentCaptor.capture());
        Map<String, String> capturedEventDetails = mapArgumentCaptor.getValue();
        assertEquals(TOS_ID, Long.parseLong(capturedEventDetails.get("termsOfServiceId")));
        assertEquals(TOS_TEXT, capturedEventDetails.get("termsOfServiceText"));
        verifyNoMoreInteractions(monitoringService);
    }

    @Test
    public void testMonitorRemoveUser() throws Exception {
        userMonitoringService.monitorRemoveUser(USER_CRN, USER_IP_ADDRESS, CORRELATION_ID);
        verify(monitoringService).monitorUserEvent(eq(USER_CRN), eq(USER_IP_ADDRESS), eq(CORRELATION_ID), eq
                (MonitoredUserEventType.USER_REMOVES_ACCOUNT), eq(null));

    }
}