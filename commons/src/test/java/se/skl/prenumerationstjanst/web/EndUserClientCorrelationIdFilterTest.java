package se.skl.prenumerationstjanst.web;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.MDC;
import se.skl.prenumerationstjanst.PrenumerationstjanstConstants;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * @author Martin Samuelsson
 */
public class EndUserClientCorrelationIdFilterTest {

    private EndUserClientCorrelationIdFilter endUserClientCorrelationIdFilter;

    @Before
    public void setUp() throws Exception {
        MDC.clear();
        endUserClientCorrelationIdFilter = new EndUserClientCorrelationIdFilter();
    }

    @After
    public void cleanUp() throws Exception {
        MDC.clear();
    }

    @Test
    public void doFilter() throws Exception {
        assertNull(MDC.get(PrenumerationstjanstConstants.MDC_CLIENT_CORRELATION_ID_KEY));
        HttpServletResponse response = mock(HttpServletResponse.class);
        endUserClientCorrelationIdFilter
                .doFilter(mock(HttpServletRequest.class), response, mock(FilterChain.class));
        assertNotNull(MDC.get(PrenumerationstjanstConstants.MDC_CLIENT_CORRELATION_ID_KEY));
        verify(response).setHeader(PrenumerationstjanstConstants.CLIENT_CORRELATION_ID_HEADER_KEY,
                MDC.get(PrenumerationstjanstConstants.MDC_CLIENT_CORRELATION_ID_KEY));
    }

}