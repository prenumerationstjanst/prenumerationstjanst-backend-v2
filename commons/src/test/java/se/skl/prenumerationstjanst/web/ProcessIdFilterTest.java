package se.skl.prenumerationstjanst.web;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.MDC;
import se.skl.prenumerationstjanst.PrenumerationstjanstConstants;
import se.skl.prenumerationstjanst.UuidGenerator;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;

/**
 * @author Martin Samuelsson
 */
@RunWith(MockitoJUnitRunner.class)
public class ProcessIdFilterTest {

    private static final String UUID = "eventId";

    @Mock
    private UuidGenerator uuidGenerator;

    @InjectMocks
    private ProcessIdFilter processIdFilter;

    @Before
    public void setUp() throws Exception {
        MDC.clear();
        Mockito.when(uuidGenerator.generateSanitizedId()).thenReturn(UUID);
    }

    @After
    public void cleanUp() throws Exception {
        MDC.clear();
    }

    @Test
    public void doFilter() throws Exception {
        assertNull(MDC.get(PrenumerationstjanstConstants.MDC_PROCESS_ID_KEY));
        processIdFilter.doFilter(mock(HttpServletRequest.class), mock(HttpServletResponse.class), mock(FilterChain.class));
        assertEquals(UUID, MDC.get(PrenumerationstjanstConstants.MDC_PROCESS_ID_KEY));
    }

}