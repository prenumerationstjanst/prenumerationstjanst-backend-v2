package se.skl.prenumerationstjanst.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import se.skl.prenumerationstjanst.service.ApigwConsentService;
import se.skl.prenumerationstjanst.service.SubscriptionService;
import se.skl.prenumerationstjanst.service.TransformationService;
import se.skl.prenumerationstjanst.service.monitoring.SubscriptionMonitoringService;

/**
 * @author Martin Samuelsson
 */
@Configuration
public class IntsvcConfig {

    @Bean
    public ApigwConsentService apigwConsentService() {
        return new ApigwConsentService();
    }

    @Bean
    public SubscriptionService subscriptionService() {
        return new SubscriptionService();
    }

    @Bean
    public TransformationService transformationService() {
        return new TransformationService();
    }

    @Bean
    public SubscriptionMonitoringService subscriptionMonitoringService() {
        return new SubscriptionMonitoringService();
    }

}
