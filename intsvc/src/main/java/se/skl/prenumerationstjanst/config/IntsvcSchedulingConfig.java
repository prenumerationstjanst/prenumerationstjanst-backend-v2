package se.skl.prenumerationstjanst.config;

import com.google.common.util.concurrent.ForwardingExecutorService;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import se.skl.prenumerationstjanst.PrenumerationstjanstConstants;
import se.skl.prenumerationstjanst.UuidGenerator;

import java.util.concurrent.*;

/**
 * @author Martin Samuelsson
 */
@Configuration
public class IntsvcSchedulingConfig implements SchedulingConfigurer {

    @Autowired
    private UuidGenerator processIdGenerator;

    @Autowired
    private UuidGenerator activityIdGenerator;

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.setScheduler(new IdAddingScheduledExecutorService(Executors.newSingleThreadScheduledExecutor(), processIdGenerator, activityIdGenerator));
    }

    /**
     * ScheduledExecutorService wrapper that will add our eventId and correlationId to tasks
     * upon running
     */
    private static class IdAddingScheduledExecutorService extends ForwardingExecutorService implements ScheduledExecutorService {

        private final ExecutorService original;

        private final UuidGenerator processIdGenerator;

        private final UuidGenerator activityIdGenerator;

        public IdAddingScheduledExecutorService(ScheduledExecutorService original, UuidGenerator processIdGenerator, UuidGenerator activityIdGenerator) {
            this.original = original;
            this.processIdGenerator = processIdGenerator;
            this.activityIdGenerator = activityIdGenerator;
        }

        @Override
        protected ExecutorService delegate() {
            return original;
        }

        protected Runnable wrap(final Runnable runnable) {
            return () -> {
                MDC.clear();
                MDC.put(PrenumerationstjanstConstants.MDC_PROCESS_ID_KEY, processIdGenerator.generateSanitizedId());
                MDC.put(PrenumerationstjanstConstants.MDC_ACTIVITY_ID_KEY, activityIdGenerator.generateSanitizedId());
                try {
                    runnable.run();
                } finally {
                    MDC.clear();
                }
            };
        }

        @Override
        public ScheduledFuture<?> schedule(Runnable command, long delay, TimeUnit unit) {
            return ((ScheduledExecutorService)delegate()).schedule(wrap(command), delay, unit);
        }

        @Override
        public <V> ScheduledFuture<V> schedule(Callable<V> callable, long delay, TimeUnit unit) {
            throw new IllegalArgumentException("Callable not supported");
        }

        @Override
        public ScheduledFuture<?> scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit) {
            return ((ScheduledExecutorService)delegate()).scheduleAtFixedRate(wrap(command), initialDelay, period, unit);
        }

        @Override
        public ScheduledFuture<?> scheduleWithFixedDelay(Runnable command, long initialDelay, long delay, TimeUnit unit) {
            return ((ScheduledExecutorService)delegate()).scheduleWithFixedDelay(wrap(command), initialDelay, delay, unit);
        }
    }
}
