package se.skl.prenumerationstjanst.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import se.skl.prenumerationstjanst.model.Subscription;
import se.skl.prenumerationstjanst.repository.KeyValueRepository;
import se.skl.prenumerationstjanst.repository.SubscriptionRepository;
import se.skl.prenumerationstjanst.service.monitoring.SubscriptionMonitoringService;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

/**
 * Scheduled service that is responsible for various cleanup related tasks:
 *
 * - finds 'incomplete' subscriptions and deletes them
 * - deletes old KeyValue entries
 *
 * @author Martin Samuelsson
 */
@Component
public class HousekeepingService {

    private static final Logger logger = LoggerFactory.getLogger(HousekeepingService.class);

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private KeyValueRepository keyValueRepository;

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private SubscriptionMonitoringService subscriptionMonitoringService;

    @Autowired
    private ApigwConsentService apigwConsentService;

    @Value("${pt.cleanup.subscription_age}")
    private long incompleteSubscriptionMaxAge;

    @Value("${pt.cleanup.key_value_age}")
    private long maxKeyValueAge;

    @Scheduled(fixedRate = 60000)
    @Transactional
    public void cleanIncompleteSubscriptions() { //TODO: actual removal should be performed by SubscriptionService
        logger.debug("checking for incomplete subscriptions");
        List<Subscription> incompleteSubs = subscriptionRepository
                .findByCreatedBeforeAndApigwConsentIsNull(LocalDateTime.now().minus(incompleteSubscriptionMaxAge, ChronoUnit.MILLIS));
        incompleteSubs
                .forEach(subscription -> {
                    boolean apigwConsent = subscription.getApigwConsent() != null;
                    String userCrn = subscription.getUserContext() != null
                            && subscription.getUserContext().getUser() != null
                            ? subscription.getUserContext().getUser().getCrn()
                            : null;
                    String subscriptionId = subscription.getId();
                    String subscriptionCrn = subscription.getUserContext() != null
                            ? subscription.getUserContext().getRepresentingCrn()
                            : null;
                    LocalDateTime created = subscription.getCreated();
                    logger.debug("subscription {} created {}, apigwConsent: {}, will be removed due to incompleteness",
                            subscriptionId, created, apigwConsent);
                    if (subscription.getApigwConsent() != null) {
                        apigwConsentService.deleteConsent(subscription.getApigwConsent().getTokenValue());
                    }
                    subscriptionService.internalRemoveSubscription(subscriptionId);
                    subscriptionMonitoringService.monitorRemoveIncompleteSubscription(userCrn, null, subscriptionId,
                            subscriptionCrn);
                });
    }

    @Scheduled(fixedRate = 60000)
    @Transactional
    public void cleanSubscriptionsScheduledForRemoval() { //TODO: actual removal should be performed by SubscriptionService
        LocalDateTime olderThan = LocalDateTime.now().minusMinutes(1);
        Long count = subscriptionRepository.countByScheduledForRemovalTrueAndLastModifiedBefore(olderThan);
        if (count > 0) {
            logger.debug("{} subscription(s) scheduled for removal with lastModified older than {} will be removed",
                    count, olderThan);
            subscriptionRepository.deleteByScheduledForRemovalTrueAndLastModifiedBefore(olderThan);
        }
    }

    @Scheduled(fixedRate = 60000)
    @Transactional
    public void cleanOldCachedValues() {
        LocalDateTime olderThan = LocalDateTime.now().minus(maxKeyValueAge, ChronoUnit.MILLIS);
        logger.debug("deleting KeyValue:s older than {}", olderThan);
        keyValueRepository.deleteOldCachedValues(olderThan);
    }
}
