package se.skl.prenumerationstjanst.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import se.skl.prenumerationstjanst.dto.MonitoredUserEventDTO;
import se.skl.prenumerationstjanst.model.monitoring.MonitoredUserEvent;
import se.skl.prenumerationstjanst.repository.monitoring.MonitoredUserEventRepository;

import static se.skl.prenumerationstjanst.PrenumerationstjanstConstants.MONITORING_QUEUE;

/**
 * 'Message driven POJO' that receives MonitoredUserEventDTO:s from queue and
 * persists them to DB
 *
 * @author Martin Samuelsson
 */
@Component
public class MonitoringMDP {

    private static final Logger logger = LoggerFactory.getLogger(MonitoringMDP.class);

    @Autowired
    private MonitoredUserEventRepository monitoredUserEventRepository;

    @JmsListener(destination = MONITORING_QUEUE)
    public void processUserEvent(MonitoredUserEventDTO monitoredUserEventDTO) {
        logger.debug("monitoredUserEventDTO: {}", monitoredUserEventDTO);
        ObjectMapper objectMapper = new ObjectMapper();
        String eventDetails = null;
        try {
            eventDetails = objectMapper.writeValueAsString(monitoredUserEventDTO.getEventDetails());
        } catch (JsonProcessingException e) {
            logger.error("could not serialize eventDetails Map to String", e);
        }
        MonitoredUserEvent monitoredUserEvent = new MonitoredUserEvent(monitoredUserEventDTO.getUserCrn(),
                monitoredUserEventDTO.getUserIpAddress(),
                monitoredUserEventDTO.getCorrelationId(), monitoredUserEventDTO.getEventType(), eventDetails);

        monitoredUserEventRepository.save(monitoredUserEvent);

    }
}
