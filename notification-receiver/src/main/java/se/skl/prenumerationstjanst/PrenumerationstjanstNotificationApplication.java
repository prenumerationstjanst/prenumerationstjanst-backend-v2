package se.skl.prenumerationstjanst;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.context.WebApplicationContext;
import se.skl.prenumerationstjanst.config.TestConfiguration;
import se.skl.prenumerationstjanst.web.ActivityIdFilter;
import se.skl.prenumerationstjanst.web.ProcessIdFilter;

@SpringBootApplication
//exclude classes marked with @TestConfiguration
@ComponentScan(excludeFilters = @ComponentScan.Filter(TestConfiguration.class))
@EnableJms
@EnableJpaAuditing
@EnableAsync
public class PrenumerationstjanstNotificationApplication extends SpringBootServletInitializer {

    private static final Logger logger = LoggerFactory.getLogger(PrenumerationstjanstNotificationApplication.class);

    private static final int PROCESS_ID_FILTER_ORDER = SecurityProperties.DEFAULT_FILTER_ORDER - 30; //before Spring Security
    private static final int ACTIVITY_ID_FILTER_ORDER = SecurityProperties.DEFAULT_FILTER_ORDER - 20; //before Spring Security

    public static void main(String[] args) {
        SpringApplication.run(PrenumerationstjanstNotificationApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(PrenumerationstjanstNotificationApplication.class);
    }

    @Override
    protected WebApplicationContext run(SpringApplication application) {
        MDC.put(PrenumerationstjanstConstants.MDC_PROCESS_ID_KEY, processIdGenerator().generateSanitizedId());
        MDC.put(PrenumerationstjanstConstants.MDC_ACTIVITY_ID_KEY, activityIdGenerator().generateSanitizedId());
        return super.run(application);
    }

    @Bean
    public UuidGenerator processIdGenerator() {
        return new UuidGenerator("PID-" + UuidGenerator.getHostName());
    }

    @Bean
    public UuidGenerator activityIdGenerator() {
        return new UuidGenerator("AID-" + UuidGenerator.getHostName());
    }

    @Bean
    public ProcessIdFilter processIdFilter() {
        return new ProcessIdFilter();
    }

    @Bean
    public FilterRegistrationBean eventIdFilterRegistration() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(processIdFilter());
        filterRegistrationBean.setOrder(PROCESS_ID_FILTER_ORDER);
        return filterRegistrationBean;
    }

    @Bean
    public ActivityIdFilter activityIdFilter() {
        return new ActivityIdFilter();
    }

    @Bean
    public FilterRegistrationBean correlationIdFilterRegistration() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(activityIdFilter());
        filterRegistrationBean.setOrder(ACTIVITY_ID_FILTER_ORDER);
        return filterRegistrationBean;
    }

}
