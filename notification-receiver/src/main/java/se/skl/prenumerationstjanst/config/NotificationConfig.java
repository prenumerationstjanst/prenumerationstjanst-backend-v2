package se.skl.prenumerationstjanst.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import se.skl.prenumerationstjanst.service.*;
import se.skl.prenumerationstjanst.service.monitoring.SubscriptionMonitoringService;

/**
 * @author Martin Samuelsson
 */
@Configuration
public class NotificationConfig {

    @Bean
    public TransferService transferService() {
        return new TransferService();
    }

    @Bean
    public SubscriptionService subscriptionService() {
        return new SubscriptionService();
    }

    @Bean
    public ApigwConsentService apigwConsentService() {
        return new ApigwConsentService();
    }

    @Bean
    public TransformationService transformationService() {
        return new TransformationService();
    }

    @Bean
    public UrlService urlService() {
        return new UrlService();
    }

    @Bean
    public PhkInfoService phkInfoService() {
        return new PhkInfoServiceImpl();
    }

    @Bean
    public SubscriptionMonitoringService subscriptionMonitoringService() {
        return new SubscriptionMonitoringService();
    }
}
