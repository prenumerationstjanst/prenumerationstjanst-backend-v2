package se.skl.prenumerationstjanst.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.client.RestTemplate;

/**
 * @author Martin Samuelsson
 */
@Configuration
public class NotificationHttpClientConfig extends HttpClientConfigBase {

    private static final Logger logger = LoggerFactory.getLogger(NotificationHttpClientConfig.class);

    @Value("${pt.apigw.notification_server.host}")
    private String apigwNotificationServerHost;

    @Value("${pt.apigw.notification_server.port}")
    private int apigwNotificationServerPort;

    @Value("${pt.apigw.auth_server.host}")
    private String apigwAuthServerHost;

    @Value("${pt.apigw.auth_server.port}")
    private int apigwAuthServerPort;

    @Value("${pt.apigw.client_id}")
    private String apigwClientId;

    @Value("${pt.apigw.client_password}")
    private String apigwClientPassword;

    @Bean(name = "notificationServerRestTemplate")
    @ConditionalOnMissingBean(name = "notificationServerRestTemplate")
    public RestTemplate notificationServerRestTemplateX509() throws Exception {
        RestTemplate restTemplate = new RestTemplate(createX509RequestFactory());
        addCorrelationIdInterceptorForApigw(restTemplate);
        return restTemplate;
    }

    @Bean(name = "notificationServerRestTemplate")
    @Profile("apigw-client-basic")
    public RestTemplate notificationServerRestTemplateHttpBasic() throws Exception {
        logger.warn("creating notificationServerRestTemplate with HTTP Basic auth (_no_ X.509)");
        RestTemplate restTemplate = new RestTemplate(createHttpBasicRequestFactory(apigwNotificationServerHost,
                apigwNotificationServerPort,
                apigwClientId, apigwClientPassword));
        addCorrelationIdInterceptorForApigw(restTemplate);
        return restTemplate;
    }

    @Bean(name = "authServerRestTemplate")
    @ConditionalOnMissingBean(name = "authServerRestTemplate")
    public RestTemplate authServerRestTemplateX509() throws Exception {
        RestTemplate restTemplate = new RestTemplate(createX509RequestFactory());
        addCorrelationIdInterceptorForApigw(restTemplate);
        return restTemplate;
    }

    @Bean(name = "authServerRestTemplate")
    @Profile("apigw-client-basic")
    public RestTemplate authServerRestTemplateHttpBasic() throws Exception {
        logger.warn("creating authServerRestTemplate with HTTP Basic auth (_no_ X.509)");
        RestTemplate restTemplate = new RestTemplate(createHttpBasicRequestFactory(apigwAuthServerHost,
                apigwAuthServerPort,
                apigwClientId, apigwClientPassword));
        addCorrelationIdInterceptorForApigw(restTemplate);
        return restTemplate;
    }

}
