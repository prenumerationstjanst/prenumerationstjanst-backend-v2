package se.skl.prenumerationstjanst.config.dev;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import se.skl.prenumerationstjanst.service.monitoring.LoggingMonitoringService;
import se.skl.prenumerationstjanst.service.monitoring.MonitoringService;

/**
 * @author Martin Samuelsson
 */
@Configuration
@Profile({"dev"})
public class NotificiationDevConfig {

    @Bean
    @Profile("dev")
    public MonitoringService devMonitoringService() {
        return new LoggingMonitoringService();
    }
}
