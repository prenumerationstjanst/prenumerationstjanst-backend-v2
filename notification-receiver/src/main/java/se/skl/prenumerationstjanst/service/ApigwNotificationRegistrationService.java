package se.skl.prenumerationstjanst.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import se.skl.prenumerationstjanst.model.TransferType;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Scheduled service responsible for calling APIGW at regular intervals
 * to register our notification endpoint URL
 *
 * @author Martin Samuelsson
 */
@Component
public class ApigwNotificationRegistrationService {

    private static final Logger logger = LoggerFactory.getLogger(ApigwNotificationRegistrationService.class);

    private long lastSuccessfulRegistration = 0;

    @Autowired
    @Qualifier(value = "notificationServerRestTemplate")
    private RestTemplate notificationServerRestTemplate;

    @Autowired
    private UrlService urlService;

    @Value("${pt.apigw.notification_server.base_uri}")
    private String baseUri;

    @Value("${pt.apigw.notification_server.notification_registration_path}")
    private String notificationRegistrationPath;

    /**
     * Runs the registration on startup
     */
    @PostConstruct
    public void onStartup() {
        try {
            registerNotificationEndpoint();
        } catch (RuntimeException e) {
            logger.error("Caught exception on startup while trying to register our notification endpoint with APIGW", e);
        }
    }

    /**
     * Runs the registration every hour
     *
     * (needs to be performed once every 30 days, but we can do it as often as we like)
     */
    @Scheduled(cron = "0 0 0/1 1/1 * ?")
    public void registerNotificationEndpoint() {
        logger.debug("starting notification registration");
        if ((lastSuccessfulRegistration + 60000 * 60) < System.currentTimeMillis()) {
            for (TransferType transferType : TransferType.values()) {
                URI requestURI;
                try {
                    requestURI = new URI(baseUri + notificationRegistrationPath + "/" + transferType.getScope());
                    URI endpoint = UriComponentsBuilder.fromUri(requestURI)
                            .queryParam("endpoint", urlService.getNotificationPath())
                            .build()
                            .toUri();
                    Map map = notificationServerRestTemplate.postForObject(endpoint, null, Map.class);
                    logger.debug("notification registration response for {}: {}", transferType, map);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            }
            lastSuccessfulRegistration = System.currentTimeMillis();
        }
    }
}
