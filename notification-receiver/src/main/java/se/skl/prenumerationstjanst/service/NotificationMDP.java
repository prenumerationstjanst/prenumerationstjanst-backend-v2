package se.skl.prenumerationstjanst.service;

import org.apigw.engagementindex.NotificationType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import se.skl.prenumerationstjanst.dto.SubscriptionDTO;
import se.skl.prenumerationstjanst.model.TransferType;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static se.skl.prenumerationstjanst.PrenumerationstjanstConstants.NOTIFICATION_QUEUE;

/**
 * @author Martin Samuelsson
 */
@Component
public class NotificationMDP {

    private static final Logger logger = LoggerFactory.getLogger(NotificationMDP.class);

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private TransferService transferService;

    public static final DateTimeFormatter notificationDateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

    @JmsListener(destination = NOTIFICATION_QUEUE, concurrency = "1-10")
    public void handleNotification(NotificationType notification) {

        String token = notification.getToken();
        String scope = notification.getScope();
        String mostRecentContent = notification.getEngagement().getMostRecentContent();
        String sourceSystemHsaId = notification.getEngagement().getSourceSystem();

        SubscriptionDTO subscriptionDTO = subscriptionService.findSubscriptionByApigwConsentToken(token);
        TransferType transferType = TransferType.forScope(scope);

        if (subscriptionDTO == null) {
            logger.warn("Could not find subscription for APIGW token: {}, can not continue", token);
            return;
        } else if (transferType == null) {
            logger.warn("Could not find transferType for scope: {}, can not continue", scope);
            return;
        } else if (sourceSystemHsaId == null || sourceSystemHsaId.trim().isEmpty()) {
            logger.warn("Could not determine source system, can not continue");
            return;
        }

        LocalDateTime documentTime;

        try {
            documentTime = LocalDateTime.parse(mostRecentContent, notificationDateTimeFormatter);
        } catch (Exception e) {
            logger.error("could not parse mostRecentContent: {}, can not continue", mostRecentContent);
            return;
        }
        transferService.startTransferForProcessNotification(subscriptionDTO.getId(),
                transferType, documentTime, sourceSystemHsaId);
    }
}
