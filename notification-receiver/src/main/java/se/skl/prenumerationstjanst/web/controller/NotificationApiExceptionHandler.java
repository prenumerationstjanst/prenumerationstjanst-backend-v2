package se.skl.prenumerationstjanst.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import se.skl.prenumerationstjanst.dto.api.ErrorApiDTO;

/**
 * @author Martin Samuelsson
 */
@ControllerAdvice
public class NotificationApiExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(NotificationApiExceptionHandler.class);

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public ErrorApiDTO handleRuntimeException(RuntimeException e) {
        logger.warn("encountered {}", e.getClass().getSimpleName());
        return new ErrorApiDTO("INTERNAL_SERVER_ERROR", "internal server error");
    }
}
