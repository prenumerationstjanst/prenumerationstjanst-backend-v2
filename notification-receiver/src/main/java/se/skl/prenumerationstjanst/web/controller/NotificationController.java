package se.skl.prenumerationstjanst.web.controller;

import org.apigw.engagementindex.NotificationType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;

import static se.skl.prenumerationstjanst.PrenumerationstjanstConstants.NOTIFICATION_QUEUE;

/**
 * @author Martin Samuelsson
 */
@RestController
@RequestMapping(value = "/services/notification/process")
public class NotificationController {

    private static final Logger logger = LoggerFactory.getLogger(NotificationController.class);

    @Autowired
    private JmsTemplate jmsTemplate;

    public static DateTimeFormatter notificationDateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void receiveNotification(@RequestBody NotificationType notification) {
        jmsTemplate.convertAndSend(NOTIFICATION_QUEUE, notification);
    }
}
