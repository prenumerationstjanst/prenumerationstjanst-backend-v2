package se.skl.prenumerationstjanst;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import se.skl.prenumerationstjanst.service.ApigwNotificationRegistrationService;

/**
 * @author Martin Samuelsson
 */
@Configuration
@Profile({"test", "jmstest"})
public class NotificationIntegrationTestConfig {

    @Bean
    @Primary
    public ApigwNotificationRegistrationService apigwNotificationRegistrationService() {
        return Mockito.mock(ApigwNotificationRegistrationService.class);
    }
}
