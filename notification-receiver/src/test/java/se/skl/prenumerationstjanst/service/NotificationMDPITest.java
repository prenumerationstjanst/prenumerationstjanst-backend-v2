package se.skl.prenumerationstjanst.service;

import org.apigw.engagementindex.EngagementType;
import org.apigw.engagementindex.NotificationType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import se.skl.prenumerationstjanst.JmsTestConfig;
import se.skl.prenumerationstjanst.PrenumerationstjanstNotificationApplication;
import se.skl.prenumerationstjanst.model.ApigwConsent;
import se.skl.prenumerationstjanst.model.Subscription;
import se.skl.prenumerationstjanst.model.TransferType;
import se.skl.prenumerationstjanst.repository.SubscriptionRepository;

import java.time.LocalDateTime;
import java.util.Enumeration;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static se.skl.prenumerationstjanst.PrenumerationstjanstConstants.NOTIFICATION_QUEUE;
import static se.skl.prenumerationstjanst.PrenumerationstjanstConstants.NOTIFICATION_QUEUE_DLQ;
import static se.skl.prenumerationstjanst.service.NotificationMDP.notificationDateTimeFormatter;

/**
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {
        PrenumerationstjanstNotificationApplication.class,
        JmsTestConfig.class,
        NotificationMDPITest.NotificationMDPITestConfiguration.class

})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ActiveProfiles({"jmstest", "test"})
public class NotificationMDPITest {

    private static final Logger logger = LoggerFactory.getLogger(NotificationMDPITest.class);

    public static final String TOKEN_VALUE = "tokenValue";
    public static final String USER_CRN = "191212121212";
    public static final String SOURCE_SYSTEM_HSA_ID = "sourceSystemHsaId";

    @Autowired
    private TransferService transferService;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Before
    public void setUp() throws Exception {
        subscriptionRepository.deleteAll();
    }

    @After
    public void tearDown() throws Exception {
        subscriptionRepository.deleteAll();
    }

    @Test
    public void testHandleNotification_OK() throws Exception {
        Subscription subscription = new Subscription();
        subscription.setApprovedTransferType(TransferType.VACCINATION_HISTORY);
        ApigwConsent apigwConsent = new ApigwConsent();
        apigwConsent.setTokenValue(TOKEN_VALUE);
        subscription.setApigwConsent(apigwConsent);
        subscription = subscriptionRepository.save(subscription);

        NotificationType notification = new NotificationType();
        notification.setScope(TransferType.VACCINATION_HISTORY.getScope());
        notification.setToken(TOKEN_VALUE);
        EngagementType engagement = new EngagementType();
        engagement.setSourceSystem(SOURCE_SYSTEM_HSA_ID);
        LocalDateTime mostRecentContent = LocalDateTime.parse(LocalDateTime.now().format(notificationDateTimeFormatter),
                notificationDateTimeFormatter);
        engagement.setMostRecentContent(mostRecentContent.format(notificationDateTimeFormatter));
        notification.setEngagement(engagement);

        jmsTemplate.convertAndSend(NOTIFICATION_QUEUE, notification);

        Thread.sleep(1000);

        verify(transferService).startTransferForProcessNotification(eq(subscription.getId()),
                eq(TransferType.VACCINATION_HISTORY), eq(mostRecentContent), eq(SOURCE_SYSTEM_HSA_ID));

        assertQueueCount(NOTIFICATION_QUEUE, 0);
        assertQueueCount(NOTIFICATION_QUEUE_DLQ, 0);
    }

    @Test
    public void testReceiveNotification_NoSubFound() throws Exception {
        NotificationType notification = new NotificationType();
        notification.setScope(TransferType.VACCINATION_HISTORY.getScope());
        notification.setToken(TOKEN_VALUE);
        EngagementType engagement = new EngagementType();
        engagement.setSourceSystem(SOURCE_SYSTEM_HSA_ID);
        LocalDateTime mostRecentContent = LocalDateTime.parse(LocalDateTime.now().format(notificationDateTimeFormatter),
                notificationDateTimeFormatter);
        engagement.setMostRecentContent(mostRecentContent.format(notificationDateTimeFormatter));
        notification.setEngagement(engagement);

        jmsTemplate.convertAndSend(NOTIFICATION_QUEUE, notification);

        Thread.sleep(100);

        verifyNoMoreInteractions(transferService);

        assertQueueCount(NOTIFICATION_QUEUE, 0);
        assertQueueCount(NOTIFICATION_QUEUE_DLQ, 0);
    }

    @Test
    public void testReceiveNotification_MostRecentContentNull() throws Exception {
        Subscription subscription = new Subscription();
        subscription.setApprovedTransferType(TransferType.VACCINATION_HISTORY);
        ApigwConsent apigwConsent = new ApigwConsent();
        apigwConsent.setTokenValue(TOKEN_VALUE);
        subscription.setApigwConsent(apigwConsent);

        NotificationType notification = new NotificationType();
        notification.setScope(TransferType.VACCINATION_HISTORY.getScope());
        notification.setToken(TOKEN_VALUE);
        EngagementType engagement = new EngagementType();
        engagement.setSourceSystem(SOURCE_SYSTEM_HSA_ID);
        notification.setEngagement(engagement);

        jmsTemplate.convertAndSend(NOTIFICATION_QUEUE, notification);

        Thread.sleep(1000);

        verifyNoMoreInteractions(transferService);

        assertQueueCount(NOTIFICATION_QUEUE, 0);
        assertQueueCount(NOTIFICATION_QUEUE_DLQ, 0);
    }

    @Test
    public void testReceiveNotification_MostRecentContentBad() throws Exception {
        Subscription subscription = new Subscription();
        ApigwConsent apigwConsent = new ApigwConsent();
        apigwConsent.setTokenValue(TOKEN_VALUE);
        subscription.setApigwConsent(apigwConsent);

        NotificationType notification = new NotificationType();
        notification.setScope(TransferType.VACCINATION_HISTORY.getScope());
        notification.setToken(TOKEN_VALUE);
        EngagementType engagement = new EngagementType();
        engagement.setSourceSystem(SOURCE_SYSTEM_HSA_ID);
        engagement.setMostRecentContent("mep");
        notification.setEngagement(engagement);

        jmsTemplate.convertAndSend(NOTIFICATION_QUEUE, notification);

        Thread.sleep(1000);

        verifyNoMoreInteractions(transferService);

        assertQueueCount(NOTIFICATION_QUEUE, 0);
        assertQueueCount(NOTIFICATION_QUEUE_DLQ, 0);
    }

    @Test
    public void testReceiveNotification_ScopeBad() throws Exception {
        Subscription subscription = new Subscription();
        subscription.setApprovedTransferType(TransferType.VACCINATION_HISTORY);
        ApigwConsent apigwConsent = new ApigwConsent();
        apigwConsent.setTokenValue(TOKEN_VALUE);
        subscription.setApigwConsent(apigwConsent);

        NotificationType notification = new NotificationType();
        notification.setScope("mep");
        notification.setToken(TOKEN_VALUE);
        EngagementType engagement = new EngagementType();
        engagement.setSourceSystem(SOURCE_SYSTEM_HSA_ID);
        LocalDateTime mostRecentContent = LocalDateTime.parse(LocalDateTime.now().format(notificationDateTimeFormatter),
                notificationDateTimeFormatter);
        engagement.setMostRecentContent(mostRecentContent.format(notificationDateTimeFormatter));
        notification.setEngagement(engagement);

        jmsTemplate.convertAndSend(NOTIFICATION_QUEUE, notification);

        Thread.sleep(1000);

        verifyNoMoreInteractions(transferService);

        assertQueueCount(NOTIFICATION_QUEUE, 0);
        assertQueueCount(NOTIFICATION_QUEUE_DLQ, 0);
    }

    @Test
    public void testReceiveNotification_SourceSystemNull() throws Exception {
        Subscription subscription = new Subscription();
        subscription.setApprovedTransferType(TransferType.VACCINATION_HISTORY);
        ApigwConsent apigwConsent = new ApigwConsent();
        apigwConsent.setTokenValue(TOKEN_VALUE);
        subscription.setApigwConsent(apigwConsent);

        NotificationType notification = new NotificationType();
        notification.setScope(TransferType.VACCINATION_HISTORY.getScope());
        notification.setToken(TOKEN_VALUE);
        EngagementType engagement = new EngagementType();
        LocalDateTime mostRecentContent = LocalDateTime.parse(LocalDateTime.now().format(notificationDateTimeFormatter),
                notificationDateTimeFormatter);
        engagement.setMostRecentContent(mostRecentContent.format(notificationDateTimeFormatter));
        notification.setEngagement(engagement);

        jmsTemplate.convertAndSend(NOTIFICATION_QUEUE, notification);

        Thread.sleep(1000);

        verifyNoMoreInteractions(transferService);

        assertQueueCount(NOTIFICATION_QUEUE, 0);
        assertQueueCount(NOTIFICATION_QUEUE_DLQ, 0);
    }


    private void assertQueueCount(String queueName, int expectedMsgCount) {
        jmsTemplate.browse(queueName, (session, browser) -> {
            int msgCount = 0;
            Enumeration enumeration = browser.getEnumeration();
            while (enumeration.hasMoreElements()) {
                enumeration.nextElement();
                msgCount += 1;
            }
            logger.debug("{} msg count: {}", queueName, msgCount);
            assertEquals(expectedMsgCount, msgCount);
            return null;
        });
    }

    public static class NotificationMDPITestConfiguration {

        @Bean
        @Primary
        public TransferService transferService() {
            return Mockito.mock(TransferService.class);
        }

    }
}