package se.skl.prenumerationstjanst.web.controller;

import com.jayway.restassured.module.mockmvc.config.RestAssuredMockMvcConfig;
import org.apigw.engagementindex.EngagementType;
import org.apigw.engagementindex.NotificationType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;
import org.springframework.jms.UncategorizedJmsException;
import org.springframework.jms.core.JmsTemplate;
import se.skl.prenumerationstjanst.PrenumerationstjanstNotificationApplication;
import se.skl.prenumerationstjanst.config.TestConfiguration;
import se.skl.prenumerationstjanst.model.ApigwConsent;
import se.skl.prenumerationstjanst.model.Subscription;
import se.skl.prenumerationstjanst.model.TransferType;
import se.skl.prenumerationstjanst.repository.SubscriptionRepository;

import java.time.LocalDateTime;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.config;
import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
import static se.skl.prenumerationstjanst.PrenumerationstjanstConstants.NOTIFICATION_QUEUE;
import static se.skl.prenumerationstjanst.web.controller.NotificationController.notificationDateTimeFormatter;

/**
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
@SpringApplicationConfiguration({
        PrenumerationstjanstNotificationApplication.class,
        NotificationControllerITest.TestConfig.class
})
public class NotificationControllerITest extends ControllerIntegrationTestSupport {

    public static final String TOKEN_VALUE = "tokenValue";
    public static final String SOURCE_SYSTEM_HSA_ID = "sourceSystemHsaId";

    @Autowired
    private JmsTemplate jmsTemplate;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        Mockito.reset(jmsTemplate);
    }

    @After
    public void tearDown() throws Exception {
        Mockito.reset(jmsTemplate);
    }

    @Test
    public void testReceiveNotification_OK() throws Exception {
        NotificationType notification = new NotificationType();
        notification.setScope(TransferType.VACCINATION_HISTORY.getScope());
        notification.setToken(TOKEN_VALUE);
        EngagementType engagement = new EngagementType();
        engagement.setSourceSystem(SOURCE_SYSTEM_HSA_ID);
        LocalDateTime mostRecentContent = LocalDateTime.parse(LocalDateTime.now().format(notificationDateTimeFormatter),
                notificationDateTimeFormatter);
        engagement.setMostRecentContent(mostRecentContent.format(notificationDateTimeFormatter));
        notification.setEngagement(engagement);

        //@formatter:off
        given()
                .header("Content-Type", "application/json")
                .body(notification)
                .log().all()
        .when()
                .post("/services/notification/process")
        .then()
                .log().all()
                .statusCode(HttpStatus.OK.value());
        //@formatter:on
        verify(jmsTemplate).convertAndSend(eq(NOTIFICATION_QUEUE), any(NotificationType.class));
    }

    @Test
    public void testReceiveNotification_MostRecentContentNull() throws Exception {
        NotificationType notification = new NotificationType();
        notification.setScope(TransferType.VACCINATION_HISTORY.getScope());
        notification.setToken(TOKEN_VALUE);
        EngagementType engagement = new EngagementType();
        engagement.setSourceSystem(SOURCE_SYSTEM_HSA_ID);
        notification.setEngagement(engagement);

        //@formatter:off
        given()
                .header("Content-Type", "application/json")
                .body(notification)
                .log().all()
        .when()
                .post("/services/notification/process")
        .then()
                .log().all()
                .statusCode(HttpStatus.OK.value());
        //@formatter:on
        verify(jmsTemplate).convertAndSend(eq(NOTIFICATION_QUEUE), any(NotificationType.class));
    }

    @Test
    public void testReceiveNotification_MostRecentContentBad() throws Exception {
        NotificationType notification = new NotificationType();
        notification.setScope(TransferType.VACCINATION_HISTORY.getScope());
        notification.setToken(TOKEN_VALUE);
        EngagementType engagement = new EngagementType();
        engagement.setSourceSystem(SOURCE_SYSTEM_HSA_ID);
        engagement.setMostRecentContent("mep");
        notification.setEngagement(engagement);

        //@formatter:off
        given()
                .header("Content-Type", "application/json")
                .body(notification)
                .log().all()
        .when()
                .post("/services/notification/process")
        .then()
                .log().all()
                .statusCode(HttpStatus.OK.value());
        //@formatter:on
        verify(jmsTemplate).convertAndSend(eq(NOTIFICATION_QUEUE), any(NotificationType.class));
    }

    @Test
    public void testReceiveNotification_ScopeBad() throws Exception {
        NotificationType notification = new NotificationType();
        notification.setScope("mep");
        notification.setToken(TOKEN_VALUE);
        EngagementType engagement = new EngagementType();
        engagement.setSourceSystem(SOURCE_SYSTEM_HSA_ID);
        LocalDateTime mostRecentContent = LocalDateTime.parse(LocalDateTime.now().format(notificationDateTimeFormatter),
                notificationDateTimeFormatter);
        engagement.setMostRecentContent(mostRecentContent.format(notificationDateTimeFormatter));
        notification.setEngagement(engagement);

        //@formatter:off
        given()
                .header("Content-Type", "application/json")
                .body(notification)
                .log().all()
        .when()
                .post("/services/notification/process")
        .then()
                .log().all()
                .statusCode(HttpStatus.OK.value());
        //@formatter:on
        verify(jmsTemplate).convertAndSend(eq(NOTIFICATION_QUEUE), any(NotificationType.class));
    }

    @Test
    public void testReceiveNotification_SourceSystemNull() throws Exception {
        Subscription subscription = new Subscription();
        ApigwConsent apigwConsent = new ApigwConsent();
        apigwConsent.setTokenValue(TOKEN_VALUE);
        subscription.setApigwConsent(apigwConsent);

        NotificationType notification = new NotificationType();
        notification.setScope(TransferType.VACCINATION_HISTORY.getScope());
        notification.setToken(TOKEN_VALUE);
        EngagementType engagement = new EngagementType();
        LocalDateTime mostRecentContent = LocalDateTime.parse(LocalDateTime.now().format(notificationDateTimeFormatter),
                notificationDateTimeFormatter);
        engagement.setMostRecentContent(mostRecentContent.format(notificationDateTimeFormatter));
        notification.setEngagement(engagement);

        //@formatter:off
        given()
                .header("Content-Type", "application/json")
                .body(notification)
                .log().all()
        .when()
                .post("/services/notification/process")
        .then()
                .log().all()
                .statusCode(HttpStatus.OK.value());
        //@formatter:on
        verify(jmsTemplate).convertAndSend(eq(NOTIFICATION_QUEUE), any(NotificationType.class));
    }

    @Test
    public void testReceiveNotification_JmsException() throws Exception {
        Subscription subscription = new Subscription();
        ApigwConsent apigwConsent = new ApigwConsent();
        apigwConsent.setTokenValue(TOKEN_VALUE);
        subscription.setApigwConsent(apigwConsent);

        NotificationType notification = new NotificationType();
        notification.setScope(TransferType.VACCINATION_HISTORY.getScope());
        notification.setToken(TOKEN_VALUE);
        EngagementType engagement = new EngagementType();
        LocalDateTime mostRecentContent = LocalDateTime.parse(LocalDateTime.now().format(notificationDateTimeFormatter),
                notificationDateTimeFormatter);
        engagement.setMostRecentContent(mostRecentContent.format(notificationDateTimeFormatter));
        notification.setEngagement(engagement);

        doThrow(new UncategorizedJmsException("mep")).when(jmsTemplate).convertAndSend(anyString(), any(Object.class));

        //@formatter:off
        given()
                .header("Content-Type", "application/json")
                .body(notification)
                .log().all()
        .when()
                .post("/services/notification/process")
        .then()
                .log().all()
                .statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        //@formatter:on
    }

    @TestConfiguration
    @Configuration
    public static class TestConfig {
        @Bean
        @Primary
        public JmsTemplate mockJmsTemplate() {
            return Mockito.mock(JmsTemplate.class);
        }
    }
}