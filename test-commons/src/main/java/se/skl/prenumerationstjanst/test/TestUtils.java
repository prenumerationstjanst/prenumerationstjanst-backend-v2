package se.skl.prenumerationstjanst.test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author Martin Samuelsson
 */
public class TestUtils {

    public static String resourceFileToString(String filename) throws URISyntaxException, IOException {
        URL resource = TestUtils.class.getClassLoader().getResource(filename);
        @SuppressWarnings("ConstantConditions")
        Path path = Paths.get(resource.toURI());
        return new String(Files.readAllBytes(path), "UTF8");
    }
}
