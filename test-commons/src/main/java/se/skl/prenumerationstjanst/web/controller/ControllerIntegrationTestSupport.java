package se.skl.prenumerationstjanst.web.controller;

import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;
import com.jayway.restassured.module.mockmvc.specification.MockMvcRequestSpecBuilder;
import com.jayway.restassured.module.mockmvc.specification.MockMvcRequestSpecification;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.context.WebApplicationContext;

/**
 * @author Martin Samuelsson
 */
@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext
@ActiveProfiles("test")
@WebIntegrationTest
public class ControllerIntegrationTestSupport {

    private static final Logger logger = LoggerFactory.getLogger(ControllerIntegrationTestSupport.class);

    public static final String USER_CRN = "191212121212";

    public static final String SESSION_ID = "_default_session_id";
    public static final String USER_NAME = "Tolvan Tolvansson";
    public static final String USER_AGE = "100";

    @Value("${pt.userrole.idp_authenticated}")
    protected String idpRole;

    @Value("${pt.userrole.self_authenticated}")
    protected String userRole;

    @Value("${pt.pre_auth_filter.header.ssn}")
    private String subjectSerialNumberHeader;

    @Value("${pt.pre_auth_filter.header.common_name}")
    private String subjectCommonNameHeader;

    @Value("${pt.pre_auth_filter.header.age}")
    private String ageHeader;

    @Value("${pt.pre_auth_filter.header.date_of_birth}")
    private String dateOfBirthHeader;

    @Value("${pt.pre_auth_filter.header.gender}")
    private String genderHeader;

    @Autowired
    protected WebApplicationContext webApplicationContext;

    @Before
    public void setUp() throws Exception {
        RestAssuredMockMvc.webAppContextSetup(webApplicationContext);
    }

    protected MockMvcRequestSpecification getIdpUserRequestSpec() {
        return new MockMvcRequestSpecBuilder()
                .addHeader(subjectSerialNumberHeader, USER_CRN)
                .addHeader(subjectCommonNameHeader, USER_NAME)
                .addHeader(ageHeader, USER_AGE)
                .addHeader(dateOfBirthHeader, "19121212")
                .addHeader(genderHeader, "M")
                .addHeader("ajp_shib-session-id", SESSION_ID) //TODO: prop
                .build();
    }
}
