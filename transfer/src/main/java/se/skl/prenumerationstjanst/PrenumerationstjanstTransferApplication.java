package se.skl.prenumerationstjanst;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.context.WebApplicationContext;
import se.skl.prenumerationstjanst.config.TestConfiguration;

@SpringBootApplication
//exclude classes marked with @TestConfiguration
@ComponentScan(excludeFilters = @ComponentScan.Filter(TestConfiguration.class))
@EnableJms
@EnableJpaAuditing
@EnableAsync
public class PrenumerationstjanstTransferApplication extends SpringBootServletInitializer {

    private static final Logger logger = LoggerFactory.getLogger(PrenumerationstjanstTransferApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(PrenumerationstjanstTransferApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(PrenumerationstjanstTransferApplication.class);
    }

    @Override
    protected WebApplicationContext run(SpringApplication application) {
        MDC.put(PrenumerationstjanstConstants.MDC_PROCESS_ID_KEY, processIdGenerator().generateSanitizedId());
        MDC.put(PrenumerationstjanstConstants.MDC_ACTIVITY_ID_KEY, activityIdGenerator().generateSanitizedId());
        return super.run(application);
    }

    @Bean
    public UuidGenerator processIdGenerator() {
        return new UuidGenerator("PID-" + UuidGenerator.getHostName());
    }

    @Bean
    public UuidGenerator activityIdGenerator() {
        return new UuidGenerator("AID-" + UuidGenerator.getHostName());
    }

}
