package se.skl.prenumerationstjanst.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import se.skl.prenumerationstjanst.service.*;
import se.skl.prenumerationstjanst.service.monitoring.SubscriptionMonitoringService;
import se.skl.prenumerationstjanst.service.monitoring.UserMonitoringService;

/**
 * @author Martin Samuelsson
 */
@Configuration
public class TransferConfig {

    @Bean
    public TransferService transferService() {
        return new TransferService();
    }

    @Bean
    public SubscriptionService subscriptionService() {
        return new SubscriptionService();
    }

    @Bean
    public ApigwConsentService apigwConsentService() {
        return new ApigwConsentService();
    }

    @Bean
    public JobService jobService() {
        return new JobService();
    }

    @Bean
    public TransformationService transformationService() {
        return new TransformationService();
    }

    @Bean
    public SubscriptionMonitoringService subscriptionMonitoringService() {
        return new SubscriptionMonitoringService();
    }

    @Bean
    public PreCheckService preCheckService() {
        return new PreCheckServiceImpl();
    }

    @Bean
    public UserService userService() {
        return new UserService();
    }

    @Bean
    public UserMonitoringService userMonitoringService() {
        return new UserMonitoringService();
    }

    @Bean
    public UserContextService userContextService() {
        return new UserContextService();
    }

    @Bean
    public PhkInfoService phkInfoService() {
        return new PhkInfoServiceImpl();
    }
}
