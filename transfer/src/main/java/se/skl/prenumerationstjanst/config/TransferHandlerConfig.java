package se.skl.prenumerationstjanst.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import se.skl.prenumerationstjanst.job.FetchJobHandler;
import se.skl.prenumerationstjanst.job.PushJobHandler;
import se.skl.prenumerationstjanst.job.TransformationJobHandler;
import se.skl.prenumerationstjanst.model.TransferType;

import javax.xml.parsers.DocumentBuilderFactory;
import java.util.*;

/**
 * @author Martin Samuelsson
 */
@Configuration
public class TransferHandlerConfig {

    private static final Logger logger = LoggerFactory.getLogger(TransferHandlerConfig.class);

    @Autowired
    private Collection<FetchJobHandler> allFetchHandlers;

    @Autowired
    private Collection<PushJobHandler> allPushHandlers;

    @Autowired
    private Collection<TransformationJobHandler> allTransformationHandlers;

    @Bean
    public Map<TransferType, List<FetchJobHandler>> fetchJobHandlers() {
        Map<TransferType, List<FetchJobHandler>> fetchJobHandlers = new EnumMap<>(TransferType.class);
        allFetchHandlers.forEach(fetchJobHandler -> {
                fetchJobHandlers.putIfAbsent(fetchJobHandler.transferType(), new ArrayList<>());
                fetchJobHandlers.get(fetchJobHandler.transferType()).add(fetchJobHandler);
                });
        return fetchJobHandlers;
    }

    @Bean
    public Map<TransferType, PushJobHandler> pushJobHandlers() {
        Map<TransferType, PushJobHandler> pushJobHandlers = new EnumMap<>(TransferType.class);
        allPushHandlers.forEach(pushJobHandler ->
                pushJobHandlers.put(pushJobHandler.transferType(), pushJobHandler));
        return pushJobHandlers;
    }

    @Bean
    public Map<TransferType, List<TransformationJobHandler>> transformationJobHandlers() { //TODO: map with TransferType+Version key
        Map<TransferType, List<TransformationJobHandler>> transformationJobHandlers = new EnumMap<>(TransferType.class);
        allTransformationHandlers.forEach(transformationJobHandler -> {
            transformationJobHandlers.putIfAbsent(transformationJobHandler.transferType(), new ArrayList<>());
            transformationJobHandlers.get(transformationJobHandler.transferType()).add(transformationJobHandler);
            logger.debug("added TransformationJobHandler for {} v{}", transformationJobHandler.transferType(),
                    transformationJobHandler.version());
        });
        return transformationJobHandlers;
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public DocumentBuilderFactory documentBuilderFactory() {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);
        return documentBuilderFactory;
    }
}
