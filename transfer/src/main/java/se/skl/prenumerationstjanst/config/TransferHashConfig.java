package se.skl.prenumerationstjanst.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import se.skl.prenumerationstjanst.service.HashService;
import se.skl.prenumerationstjanst.service.HashServiceSHA256XMLImpl;

/**
 * @author Martin Samuelsson
 */
@Configuration
public class TransferHashConfig {

    @Bean
    public HashService hashService() {
        return new HashServiceSHA256XMLImpl();
    }
}
