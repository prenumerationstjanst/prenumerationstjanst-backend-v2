package se.skl.prenumerationstjanst.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ErrorHandler;
import se.skl.prenumerationstjanst.jms.TransferJmsErrorHandler;

/**
 * @author Martin Samuelsson
 */
@Configuration
public class TransferJmsConfig {

    @Bean
    public ErrorHandler jmsErrorHandler() {
        return new TransferJmsErrorHandler();
    }

}
