package se.skl.prenumerationstjanst.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import se.skl.prenumerationstjanst.service.monitoring.ActiveMQMonitoringService;
import se.skl.prenumerationstjanst.service.monitoring.MonitoringService;

/**
 * @author Martin Samuelsson
 */
@Configuration
public class TransferMonitoringConfig {

    @Bean
    @ConditionalOnMissingBean(value = MonitoringService.class)
    public MonitoringService monitoringService() {
        return new ActiveMQMonitoringService();
    }

}
