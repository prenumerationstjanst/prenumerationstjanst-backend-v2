package se.skl.prenumerationstjanst.config.dev;

import com.google.common.util.concurrent.RateLimiter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Martin Samuelsson
 */
@Configuration
public class TransferRateLimitConfig {

    @Value("${pt.transfer.rate_limit.fetch_job}")
    private double fetchJobRateLimit;

    @Value("${pt.transfer.rate_limit.transformation_job}")
    private double transformationJobRateLimit;

    @Value("${pt.transfer.rate_limit.push_job}")
    private double pushJobRateLimit;

    @Bean
    public RateLimiter fetchJobRateLimiter() {
        return RateLimiter.create(fetchJobRateLimit);
    }

    @Bean
    public RateLimiter transformationJobRateLimiter() {
        return RateLimiter.create(transformationJobRateLimit);
    }

    @Bean
    public RateLimiter pushJobRateLimiter() {
        return RateLimiter.create(pushJobRateLimit);
    }
}
