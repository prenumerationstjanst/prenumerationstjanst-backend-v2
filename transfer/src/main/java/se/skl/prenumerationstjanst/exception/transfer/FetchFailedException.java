package se.skl.prenumerationstjanst.exception.transfer;

import se.skl.prenumerationstjanst.dto.FetchJobDTO;

/**
 * @author Martin Samuelsson
 */
public class FetchFailedException extends RuntimeException {

    private FetchJobDTO fetchJobDTO;

    private int tryCount;

    public FetchFailedException(FetchJobDTO fetchJobDTO) {
        super();
        this.fetchJobDTO = fetchJobDTO;
        this.tryCount = 1;
    }

    public FetchFailedException(String message, FetchJobDTO fetchJobDTO) {
        super(message);
        this.fetchJobDTO = fetchJobDTO;
        this.tryCount = 1;
    }

    public FetchFailedException(String message, Throwable cause, FetchJobDTO fetchJobDTO) {
        super(message, cause);
        this.fetchJobDTO = fetchJobDTO;
    }

    public FetchJobDTO getFetchJobDTO() {
        return fetchJobDTO;
    }

    public int getTryCount() {
        return tryCount;
    }

    @Override
    public String toString() {
        return "FetchFailedException{" +
                "fetchJobDTO=" + fetchJobDTO +
                "} " + super.toString();
    }
}
