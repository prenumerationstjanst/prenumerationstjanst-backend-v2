package se.skl.prenumerationstjanst.exception.transfer;

import se.skl.prenumerationstjanst.dto.PushJobDTO;

/**
 * @author Martin Samuelsson
 */
public class PushFailedException extends RuntimeException {

    private PushJobDTO pushJobDTO;

    public PushFailedException(PushJobDTO pushJobDTO) {
        super();
        this.pushJobDTO = pushJobDTO;
    }

    public PushFailedException(String message, PushJobDTO pushJobDTO) {
        super(message);
        this.pushJobDTO = pushJobDTO;
    }

    public PushFailedException(String message, Throwable cause, PushJobDTO pushJobDTO) {
        super(message, cause);
        this.pushJobDTO = pushJobDTO;
    }

    public PushJobDTO getPushJobDTO() {
        return pushJobDTO;
    }

    @Override
    public String toString() {
        return "PushFailedException{" +
                "pushJobDTO=" + pushJobDTO +
                "} " + super.toString();
    }
}
