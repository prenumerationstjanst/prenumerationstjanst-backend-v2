package se.skl.prenumerationstjanst.exception.transfer;

import se.skl.prenumerationstjanst.dto.PushJobDTO;

/**
 * @author Martin Samuelsson
 */
public class PushUnauthorizedException extends UnrecoverablePushFailedException {
    public PushUnauthorizedException(PushJobDTO pushJobDTO) {
        super(pushJobDTO);
    }

    public PushUnauthorizedException(String message, PushJobDTO pushJobDTO) {
        super(message, pushJobDTO);
    }

    public PushUnauthorizedException(String message, Throwable cause, PushJobDTO pushJobDTO) {
        super(message, cause, pushJobDTO);
    }
}
