package se.skl.prenumerationstjanst.exception.transfer;

import se.skl.prenumerationstjanst.dto.TransformationJobDTO;

/**
 * @author Martin Samuelsson
 */
public class TransformationFailedException extends RuntimeException {

    private TransformationJobDTO transformationJobDTO;

    public TransformationFailedException(TransformationJobDTO transformationJobDTO) {
        super();
        this.transformationJobDTO = transformationJobDTO;
    }

    public TransformationFailedException(String message, TransformationJobDTO transformationJobDTO) {
        super(message);
        this.transformationJobDTO = transformationJobDTO;
    }

    public TransformationFailedException(Throwable cause, TransformationJobDTO transformationJobDTO) {
        super(cause);
        this.transformationJobDTO = transformationJobDTO;
    }

    public TransformationFailedException(String message, Throwable cause, TransformationJobDTO transformationJobDTO) {
        super(message, cause);
        this.transformationJobDTO = transformationJobDTO;
    }

    public TransformationJobDTO getTransformationJobDTO() {
        return transformationJobDTO;
    }

    @Override
    public String toString() {
        return "TransformationFailedException{" +
                "transformationJobDTO=" + transformationJobDTO +
                "} " + super.toString();
    }
}
