package se.skl.prenumerationstjanst.exception.transfer;

import se.skl.prenumerationstjanst.dto.PushJobDTO;

/**
 * Indicates that an unrecoverable error has occurred in the Push flow,
 * for example a permission error or erroneous data.
 *
 * Re-delivery should not be performed.
 *
 * @author Martin Samuelsson
 */
public class UnrecoverablePushFailedException extends Exception {

    private PushJobDTO pushJobDTO;

    public UnrecoverablePushFailedException(PushJobDTO pushJobDTO) {
        super();
        this.pushJobDTO = pushJobDTO;
    }

    public UnrecoverablePushFailedException(String message, PushJobDTO pushJobDTO) {
        super(message);
        this.pushJobDTO = pushJobDTO;
    }

    public UnrecoverablePushFailedException(String message, Throwable cause, PushJobDTO pushJobDTO) {
        super(message, cause);
        this.pushJobDTO = pushJobDTO;
    }

    public PushJobDTO getPushJobDTO() {
        return pushJobDTO;
    }

    @Override
    public String toString() {
        return "UnrecoverablePushFailedException{" +
                "pushJobDTO=" + pushJobDTO +
                "} " + super.toString();
    }
}
