package se.skl.prenumerationstjanst.exception.transfer;

import se.skl.prenumerationstjanst.dto.TransformationJobDTO;

/**
 * @author Martin Samuelsson
 */
public class UnrecoverableTransformationFailedException extends Exception {

    private TransformationJobDTO transformationJobDTO;

    public UnrecoverableTransformationFailedException(TransformationJobDTO transformationJobDTO) {
        super();
        this.transformationJobDTO = transformationJobDTO;
    }

    public UnrecoverableTransformationFailedException(String message, TransformationJobDTO transformationJobDTO) {
        super(message);
        this.transformationJobDTO = transformationJobDTO;
    }

    public UnrecoverableTransformationFailedException(Throwable cause, TransformationJobDTO transformationJobDTO) {
        super(cause);
        this.transformationJobDTO = transformationJobDTO;
    }

    public UnrecoverableTransformationFailedException(String message, Throwable cause, TransformationJobDTO
            transformationJobDTO) {
        super(message, cause);
        this.transformationJobDTO = transformationJobDTO;
    }

    public TransformationJobDTO getTransformationJobDTO() {
        return transformationJobDTO;
    }

    @Override
    public String toString() {
        return "UnrecoverableTransformationFailedException{" +
                "transformationJobDTO=" + transformationJobDTO +
                "} " + super.toString();
    }
}
