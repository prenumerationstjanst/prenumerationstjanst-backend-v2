package se.skl.prenumerationstjanst.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ErrorHandler;
import se.skl.prenumerationstjanst.dto.FetchJobDTO;
import se.skl.prenumerationstjanst.dto.PushJobDTO;
import se.skl.prenumerationstjanst.dto.TransformationJobDTO;
import se.skl.prenumerationstjanst.exception.transfer.FetchFailedException;
import se.skl.prenumerationstjanst.exception.transfer.PushFailedException;
import se.skl.prenumerationstjanst.exception.transfer.TransformationFailedException;
import se.skl.prenumerationstjanst.job.JobMDCMapper;
import se.skl.prenumerationstjanst.model.JobStatus;
import se.skl.prenumerationstjanst.service.JobService;

/**
 * Extension to DefaultJmsErrorHandler tailored to the transfer flow that will
 * provide better insights into where an error occured
 *
 * @author Martin Samuelsson
 */
public class TransferJmsErrorHandler extends DefaultJmsErrorHandler implements ErrorHandler {

    private static final Logger logger = LoggerFactory.getLogger(TransferJmsErrorHandler.class);

    @Autowired
    private JobMDCMapper jobMDCMapper;

    @Autowired
    private JobService jobService;

    @Override
    public void handleError(Throwable throwable) {
        if (throwable.getCause() instanceof FetchFailedException) {
            FetchFailedException ffe = (FetchFailedException) throwable.getCause();
            FetchJobDTO fetchJobDTO = ffe.getFetchJobDTO();
            logger.warn("{} fetchJob with id {} failed", fetchJobDTO.getTransferJob().getTransferType(),
                    fetchJobDTO.getId());
            jobService.updateFetchJobStatus(fetchJobDTO.getId(), JobStatus.FAILED);
        } else if (throwable.getCause() instanceof TransformationFailedException) {
            TransformationFailedException tfe = (TransformationFailedException) throwable.getCause();
            TransformationJobDTO transformationJobDTO = tfe.getTransformationJobDTO();
            logger.warn("{} transformationJob with id {} failed",
                    transformationJobDTO.getTransferJob().getTransferType(), transformationJobDTO.getId());
            jobService.updateTransformationJobStatus(transformationJobDTO.getId(), JobStatus.FAILED);
        } else if (throwable.getCause() instanceof PushFailedException) {
            PushFailedException pfe = (PushFailedException) throwable.getCause();
            PushJobDTO pushJobDTO = pfe.getPushJobDTO();
            logger.warn("{} pushJob with id {} failed", pushJobDTO.getTransferJob().getTransferType(),
                    pushJobDTO.getId());
            jobService.updatePushJobStatus(pushJobDTO.getId(), JobStatus.FAILED);
        } else {
            super.handleError(throwable);
        }
        jobMDCMapper.clearContext();
    }
}
