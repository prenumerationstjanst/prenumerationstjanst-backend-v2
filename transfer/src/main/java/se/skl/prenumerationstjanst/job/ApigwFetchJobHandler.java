package se.skl.prenumerationstjanst.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import se.skl.prenumerationstjanst.dto.FetchJobDTO;
import se.skl.prenumerationstjanst.exception.transfer.FetchFailedException;
import se.skl.prenumerationstjanst.model.TransferType;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.*;

/**
 * APIGW resource server aware FetchJobHandler.
 *
 * This FetchJobHandler will process incoming FetchJobs by extracting the resource server GET url
 * by calling the supplied extractor, call APIGW resource server, determine possible failed source systems
 * and dispatch PushJobs for every document matched by the supplied XPath expression found in the APIGW response
 *
 * Failed source sources will result in new transfers created via TransferService.
 *
 * @author Martin Samuelsson
 */
public abstract class ApigwFetchJobHandler implements FetchJobHandler {

    private static final Logger logger = LoggerFactory.getLogger(ApigwFetchJobHandler.class);

    @Value("${pt.apigw.resource_server.base_uri}")
    protected String resourceServerBaseUri;

    @Autowired
    @Qualifier(value = "resourceServerRestTemplate")
    protected RestTemplate restTemplate;

    private TransferType transferType;

    private int version;

    private String documentsXPath;

    private String sourceSystemXPath;

    /**
     *  @param transferType The handled TransferType
     * @param documentsXPath XPath expression to extract individual documents
     * @param version
     */
    protected ApigwFetchJobHandler(TransferType transferType, String documentsXPath, int version) {
        this.transferType = transferType;
        this.documentsXPath = documentsXPath;
        this.version = version;
    }

    protected ApigwFetchJobHandler(TransferType transferType, String documentsXPath, String sourceSystemXPath, int version) throws XPathExpressionException {
        this.transferType = transferType;
        this.version = version;
        try {
            XPathFactory.newInstance().newXPath().compile(documentsXPath);
        } catch (XPathExpressionException e) {
            logger.error("Could not compile documentsXPath ({})", documentsXPath, e);
            throw e;
        }
        this.documentsXPath = documentsXPath;
        try {
            XPathFactory.newInstance().newXPath().compile(sourceSystemXPath);
        } catch (XPathExpressionException e) {
            logger.error("Could not compile sourceSystemXPath ({})", sourceSystemXPath);
            throw e;
        }
        this.sourceSystemXPath = sourceSystemXPath;
    }

    @Override
    public FetchResult processFetchJob(FetchJobDTO fetchJobDTO) throws FetchFailedException {
        logger.info("processing {} v{} fetchJob", transferType, version);
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = restTemplate.exchange(getRequestEntity(fetchJobDTO), String.class);
        } catch (RestClientException e) { //TODO: 404 must not cause FetchFailedException
            logger.error("caught exception while calling API Gateway", e);
            if (e instanceof HttpStatusCodeException) {
                logger.info("response headers: {}", ((HttpStatusCodeException) e).getResponseHeaders());
            }
            throw new FetchFailedException("error calling API Gateway", e, fetchJobDTO);
        }
        logger.info("response headers: {}", responseEntity.getHeaders());
        if (responseEntity.getStatusCode() == HttpStatus.NOT_FOUND) {
            logger.debug("Got 404 from API Gateway");
            return new FetchResult(Collections.emptySet(), Collections.emptyList(), version);
        }
        String body = responseEntity.getBody();
        Document nodeDocument = createDocument(body);
        Set<String> failedSourceSystemHsaIds = extractFailedSourceSystems(nodeDocument);
        if (!failedSourceSystemHsaIds.isEmpty()) {
            logger.warn("failed source systems for {} v{} fetchJob: {}", fetchJobDTO.getTransferJob().getTransferType(),
                    version, failedSourceSystemHsaIds);
        }
        List<FetchResultDocument> fetchResultDocuments = extractDocuments(nodeDocument);
        if (fetchResultDocuments.isEmpty()) {
            logger.info("no documents found for {} v{} fetchJob", fetchJobDTO.getTransferJob().getTransferType(),
                    version);
        } else {
            logger.info("found {} documents for {} v{} fetchJob", fetchResultDocuments.size(),
                    fetchJobDTO.getTransferJob().getTransferType(), version);
        }
        return new FetchResult(failedSourceSystemHsaIds, fetchResultDocuments, version);
    }

    @Override
    public TransferType transferType() {
        return transferType;
    }

    @Override
    public int version() {
        return version;
    }

    protected RequestEntity getRequestEntity(FetchJobDTO fetchJobDTO) {
        RequestEntity<Void> requestEntity = RequestEntity
                .get(getFetchJobUriExtractor().createURI(resourceServerBaseUri, fetchJobDTO))
                .header("Authorization", "Bearer " + fetchJobDTO.getApigwToken())
                .build();
        logger.info("APIGW request: {}", requestEntity.toString());
        return requestEntity;
    }

    private Document createDocument(String xml) {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true); //need ns aware to get elements by name
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            return documentBuilder.parse(new ByteArrayInputStream(xml.getBytes("UTF-8")));
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace(); //TODO: RuntimeException
        }
        return null;
    }

    private Set<String> extractFailedSourceSystems(Document document) {
        Set<String> failedSourceSystems = new HashSet<>();
        try {
            XPath xPath = XPathFactory.newInstance().newXPath();
            XPathExpression processingStatusListExpr = xPath.compile("//*[local-name()='ProcessingStatusList']");
            NodeList processingStatusListNodes
                    = (NodeList) processingStatusListExpr.evaluate(document, XPathConstants.NODESET);

            for (int i = 0; i < processingStatusListNodes.getLength(); i += 1) {
                Node processingStatusListItem = processingStatusListNodes.item(i);
                if (processingStatusListItem.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) processingStatusListItem;
                    String statusCode = element.getElementsByTagNameNS("*", "statusCode").item(0).getTextContent();
                    String logicalAddress = element.getElementsByTagNameNS("*", "logicalAddress").item(0).getTextContent();
                    logger.debug("ProcessingStatus #{} logicalAddress: {}, statusCode: {}", i+1,logicalAddress, statusCode);
                    if ("NoDataSynchFailed".equalsIgnoreCase(statusCode)) { //TODO: make const. Also, more cases?
                        failedSourceSystems.add(logicalAddress);
                    }
                }
            }
        } catch (XPathExpressionException e) {
            e.printStackTrace(); //TODO: RuntimeException
        }
        return failedSourceSystems;
    }

    private List<FetchResultDocument> extractDocuments(Document document) {
        XPath xPath = XPathFactory.newInstance().newXPath();
        List<FetchResultDocument> documents = new ArrayList<>();
        try {
            XPathExpression documentsExpr = xPath.compile(documentsXPath);
            NodeList documentsNodeList
                    = (NodeList) documentsExpr.evaluate(document, XPathConstants.NODESET);
            for (int i = 0; i < documentsNodeList.getLength(); i += 1) {
                Node documentNode = documentsNodeList.item(i);

                XPathExpression sourceSystemExpr = xPath.compile(sourceSystemXPath);
                String sourceSystem = (String) sourceSystemExpr.evaluate(documentNode, XPathConstants.STRING);

                StringWriter buf = new StringWriter();
                Transformer xform = TransformerFactory.newInstance().newTransformer();
                xform.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                xform.transform(new DOMSource(documentNode), new StreamResult(buf));
                documents.add(new FetchResultDocument(buf.toString(), version, sourceSystem));
            }
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        return documents;
    }

    protected abstract FetchJobUriExtractor getFetchJobUriExtractor();

}
