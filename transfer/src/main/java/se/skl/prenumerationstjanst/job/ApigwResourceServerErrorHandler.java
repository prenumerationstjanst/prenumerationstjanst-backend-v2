package se.skl.prenumerationstjanst.job;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.DefaultResponseErrorHandler;

/**
 * Spring RestTemplate error handler that does not consider 404 an error
 *
 * Useful for handling errors resulting from NTJP VP004
 * (APIGW Resource Server maps this to 404)
 *
 * @author Martin Samuelsson
 */
public class ApigwResourceServerErrorHandler extends DefaultResponseErrorHandler {

    @Override
    protected boolean hasError(HttpStatus statusCode) {
        return statusCode != HttpStatus.NOT_FOUND && super.hasError(statusCode);
    }



}
