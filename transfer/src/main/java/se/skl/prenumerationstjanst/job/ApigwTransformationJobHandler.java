package se.skl.prenumerationstjanst.job;

import com.chbase.thing.oxm.jaxb.base.CodableValue;
import com.chbase.thing.oxm.jaxb.base.CodedValue;
import com.chbase.thing.oxm.jaxb.comment.Comment;
import com.chbase.thing.oxm.jaxb.dates.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.skl.prenumerationstjanst.chbase.CHBaseThing2;
import se.skl.prenumerationstjanst.dto.TransformationJobDTO;
import se.skl.prenumerationstjanst.exception.transfer.TransformationFailedException;
import se.skl.prenumerationstjanst.exception.transfer.UnrecoverableTransformationFailedException;
import se.skl.prenumerationstjanst.model.TransferType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import java.io.StringWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * abstract TransformationJobHandler that provide a standardized implementation of processTransformationJob
 * and forces implementing classes to unmarshal the input document String and transform the unmarshalled object
 * into a TransformationResult. Also provides a few generic utility methods.
 *
 * @param <T> the type of the APIGW input document when unmarshalled
 *
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
public abstract class ApigwTransformationJobHandler<T> implements TransformationJobHandler {

    private static final Logger logger = LoggerFactory.getLogger(ApigwTransformationJobHandler.class);

    private DateTimeFormatter dateTimeFormatter;
    private JAXBContext thingJaxbContext;
    private TransferType transferType;
    private int version;

    public ApigwTransformationJobHandler(TransferType transferType, int version, DateTimeFormatter dateTimeFormatter)
            throws JAXBException {
        this.transferType = transferType;
        this.version = version;
        this.dateTimeFormatter = dateTimeFormatter;
        this.thingJaxbContext = JAXBContext.newInstance(CHBaseThing2.class);
    }

    @Override
    public TransformationResult processTransformationJob(TransformationJobDTO transformationJobDTO)
            throws TransformationFailedException, UnrecoverableTransformationFailedException {
        try {
            T doc = unmarshal(transformationJobDTO.getSource());
            return transform(doc, transformationJobDTO.getSourceFetchTime(), transformationJobDTO);
        } catch (TransformationFailedException e) {
            //if cause is JAXBException, we can not expect a retry with the same input to succeed, so throw Unrecoverable
            if (e.getCause() != null && e.getCause() instanceof JAXBException) {
                logger.error("transform encountered JAXBException, rethrowing as Unrecoverable", e);
                throw new UnrecoverableTransformationFailedException(e.getCause(), transformationJobDTO);
            }
            //if not, just re-throw
            throw e;
        } catch (JAXBException e) {
            logger.error("unmarshal encountered JAXBException, rethrowing as Unrecoverable", e);
            throw new UnrecoverableTransformationFailedException(e, transformationJobDTO);
        }
    }

    /**
     * The actual transformation between an input object and the TransformationResult (pushJobSpecifications)
     *
     * @param doc object to be transformed
     * @param sourceFetchTime source fetch time for the returned TransformationResult
     * @param job original TransformationJobDTO
     * @return
     */
    protected abstract TransformationResult transform(T doc, LocalDateTime sourceFetchTime, TransformationJobDTO job)
            throws TransformationFailedException;

    /**
     * Produces an object of the input type to be used as input for the transformation method
     *
     * @param document String containing a XML representation of the input type
     * @return
     */
    protected abstract T unmarshal(String document) throws JAXBException;

    protected Comment createCommentBase(String rivDateString) {
        Comment comment = new Comment();
        comment.setWhen(new ApproxDateTime(getStructuredApproxDate(rivDateString)));
        return comment;
    }

    protected CodableValue createCommentCategory(String text, String codeValue) {
        return new CodableValue(text, new CodedValue(codeValue, "HFM", "HFM", null));
    }

    protected Comment createRivSourceComment(String rivSourceString) {
        Comment rivSourceComment = createCommentBase(LocalDateTime.now().format(dateTimeFormatter));
        rivSourceComment.setContent(rivSourceString);
        rivSourceComment.setCategory(createCommentCategory("RIV Källa", "RIV_SOURCE"));
        return rivSourceComment;
    }

    protected StructuredApproxDate getStructuredApproxDate(String rivDateString) {
        StructuredApproxDate structured = new StructuredApproxDate();
        ApproxDate value = new ApproxDate();
        LocalDate parse = LocalDate.parse(rivDateString, dateTimeFormatter);
        value.setY(parse.getYear());
        value.setM(parse.getMonthValue());
        value.setD(parse.getDayOfMonth());
        structured.setDate(value);
        return structured;
    }

    protected ApproxDateTime getApproxDateTime(String rivDateTimeString) {
        ApproxDateTime approxDateTime = new ApproxDateTime();
        approxDateTime.setStructured(getStructuredApproxDate(rivDateTimeString));
        return approxDateTime;
    }

    protected DateTime getDateTime(String rivDateTimeString) {
        DateTime dateTime = new DateTime();
        LocalDateTime authorDateTime = LocalDateTime.parse(rivDateTimeString, dateTimeFormatter);
        Date date = new Date();
        date.setY(authorDateTime.getYear());
        date.setM(authorDateTime.getMonthValue());
        date.setD(authorDateTime.getDayOfMonth());
        dateTime.setDate(date);
        Time time = new Time();
        time.setH(authorDateTime.getHour());
        time.setM(authorDateTime.getMinute());
        time.setS(authorDateTime.getSecond());
        time.setF(0);
        dateTime.setTime(time);
        return dateTime;
    }

    protected CHBaseThing2 createThing2(Object data) throws Exception {
        CHBaseThing2 chBaseThing2 = new CHBaseThing2();
        chBaseThing2.setData(data);
        return chBaseThing2;
    }

    protected String marshalCHBaseThing2(CHBaseThing2 thing) throws JAXBException {
        Marshaller marshaller = thingJaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE); //omit xml declaration
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        QName thingQName = new QName(null, "thing");
        JAXBElement<CHBaseThing2> thing2JAXBElement = new JAXBElement<>(thingQName, CHBaseThing2.class, thing);
        StringWriter xmlWriter = new StringWriter();
        marshaller.marshal(thing2JAXBElement, xmlWriter);
        return xmlWriter.toString();
    }

    @Override
    public TransferType transferType() {
        return transferType;
    }

    @Override
    public int version() {
        return version;
    }
}
