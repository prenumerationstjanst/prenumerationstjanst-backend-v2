package se.skl.prenumerationstjanst.job;

import se.skl.prenumerationstjanst.dto.FetchJobDTO;
import se.skl.prenumerationstjanst.exception.transfer.FetchFailedException;

/**
 * FetchJobHandlers are responsible for fetching data from the source system
 * based on parameters in the FetchJobDTO object graph, handle logical errors
 * and create 0..* PushJob:s from the resulting data
 *
 * @author Martin Samuelsson
 */
public interface FetchJobHandler extends JobHandler {
    int version();
    FetchResult processFetchJob(FetchJobDTO fetchJobDTO) throws FetchFailedException;
}
