package se.skl.prenumerationstjanst.job;

import se.skl.prenumerationstjanst.dto.FetchJobDTO;

import java.net.URI;

/**
 * @author Martin Samuelsson
 */
public interface FetchJobUriExtractor {
    URI createURI(String baseUri, FetchJobDTO fetchJobDTO);
}
