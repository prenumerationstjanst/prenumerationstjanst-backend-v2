package se.skl.prenumerationstjanst.job;

import java.util.*;

/**
 * Created by Albert Örwall on 2015-11-13.
 * @author Martin Samuelsson
 */
public class FetchResult {

    private final Set<String> failedSourceSystems;
    private List<FetchResultDocument> documents;
    private final int documentVersion; //TODO: eliminate in favor of id FetchResultDocument?

    public FetchResult(Set<String> failedSourceSystems, List<FetchResultDocument> documents, int documentVersion) {
        this.failedSourceSystems = failedSourceSystems;
        this.documents = documents;
        this.documentVersion = documentVersion;
    }

    public Set<String> getFailedSourceSystems() {
        return failedSourceSystems;
    }

    public List<FetchResultDocument> getDocuments() {
        return documents;
    }

    public int getDocumentVersion() {
        return documentVersion;
    }

    @Override
    public String toString() {
        return "FetchResult{" +
                "failedSourceSystems=" + failedSourceSystems +
                ", documents=" + documents +
                ", documentVersion=" + documentVersion +
                '}';
    }
}