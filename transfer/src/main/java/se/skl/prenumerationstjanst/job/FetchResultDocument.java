package se.skl.prenumerationstjanst.job;

/**
 * @author Martin Samuelsson
 */
public class FetchResultDocument {

    private final String value;
    private final int version;
    private final String sourceSystemId;
    //TODO: doc id?

    public FetchResultDocument(String value, int version, String sourceSystemId) {
        this.value = value;
        this.version = version;
        this.sourceSystemId = sourceSystemId;
    }

    public String getValue() {
        return value;
    }

    public int getVersion() {
        return version;
    }

    public String getSourceSystemId() {
        return sourceSystemId;
    }

    @Override
    public String toString() {
        return "Document{" +
                "value='" + value + '\'' +
                ", version=" + version +
                ", sourceSystemId='" + sourceSystemId + '\'' +
                '}';
    }
}
