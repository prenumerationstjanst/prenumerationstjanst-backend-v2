package se.skl.prenumerationstjanst.job;

import se.skl.prenumerationstjanst.model.TransferType;

/**
 * Common interface for 'Handler' classes
 *
 * @author Martin Samuelsson
 */
public interface JobHandler {
    /**
     * @return the TransferType handled by this JobHandler
     */
    TransferType transferType();
}
