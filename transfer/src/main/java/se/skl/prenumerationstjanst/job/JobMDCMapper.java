package se.skl.prenumerationstjanst.job;

import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import se.skl.prenumerationstjanst.dto.FetchJobDTO;
import se.skl.prenumerationstjanst.dto.PushJobDTO;
import se.skl.prenumerationstjanst.dto.TransferJobDTO;
import se.skl.prenumerationstjanst.dto.TransformationJobDTO;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static se.skl.prenumerationstjanst.PrenumerationstjanstConstants.*;

/**
 * Helper class to be invoked at the start and end of *JobMDP:s that handle
 * the various kind of *JobDTO:s. Use this class to add job related info to
 * the diagnostic context for improved logging.
 *
 * @author Martin Samuelsson
 */
@Component
public class JobMDCMapper {

    /**
     * Adds job related info to the diagnostic context based
     * on the specific kind of *JobDTO provided
     *
     * @param obj a *JobDTO object
     */
    public void addToContext(Object obj) {
        TransferJobDTO transferJobDTO = null;

        if (obj instanceof TransferJobDTO) {
            transferJobDTO = (TransferJobDTO) obj;
        } else if (obj instanceof FetchJobDTO) {
            FetchJobDTO fetchJobDTO = (FetchJobDTO) obj;
            MDC.put(MDC_FETCH_JOB_ID_KEY, fetchJobDTO.getId());
            transferJobDTO = fetchJobDTO.getTransferJob();
        } else if (obj instanceof TransformationJobDTO) {
            TransformationJobDTO transformationJobDTO = (TransformationJobDTO) obj;
            MDC.put(MDC_TRANSFORMATION_JOB_ID_KEY, transformationJobDTO.getId());
            transferJobDTO = transformationJobDTO.getTransferJob();
        } else if (obj instanceof PushJobDTO) {
            PushJobDTO pushJobDTO = (PushJobDTO) obj;
            MDC.put(MDC_PUSH_JOB_ID_KEY, pushJobDTO.getId());
            transferJobDTO = pushJobDTO.getTransferJob();
        }

        if (transferJobDTO != null) {
            MDC.put(MDC_TRANSFER_JOB_ID_KEY, transferJobDTO.getId());

            String transferType = transferJobDTO.getTransferType();
            String sourceSystemHsaId = transferJobDTO.getSourceSystemHsaId();
            LocalDateTime startDateTime = transferJobDTO.getStartDateTime();
            LocalDateTime endDateTime = transferJobDTO.getEndDateTime();
            String origin = transferJobDTO.getOrigin();

            MDC.put(MDC_TRANSFER_TYPE_KEY, transferType);
            MDC.put(MDC_SOURCE_SYSTEM_HSA_ID_KEY, sourceSystemHsaId);
            if (startDateTime != null) {
                MDC.put(MDC_START_DATE_TIME_KEY, startDateTime.format(DateTimeFormatter.ISO_DATE_TIME));
            }
            if (endDateTime != null) {
                MDC.put(MDC_END_DATE_TIME_KEY, endDateTime.format(DateTimeFormatter.ISO_DATE_TIME));
            }
            MDC.put(MDC_TRANSFER_ORGIN_KEY, origin);
            MDC.put(MDC_SUBSCRIPTION_ID_KEY, transferJobDTO.getSubscriptionId());
        }
    }

    /**
     * Removes previously added job related info from the diagnostic context
     */
    public void clearContext() {
        MDC.remove(MDC_SUBSCRIPTION_ID_KEY);
        MDC.remove(MDC_TRANSFER_JOB_ID_KEY);
        MDC.remove(MDC_FETCH_JOB_ID_KEY);
        MDC.remove(MDC_TRANSFORMATION_JOB_ID_KEY);
        MDC.remove(MDC_PUSH_JOB_ID_KEY);
        MDC.remove(MDC_TRANSFER_TYPE_KEY);
        MDC.remove(MDC_SOURCE_SYSTEM_HSA_ID_KEY);
        MDC.remove(MDC_START_DATE_TIME_KEY);
        MDC.remove(MDC_END_DATE_TIME_KEY);
        MDC.remove(MDC_TRANSFER_ORGIN_KEY);
    }
}
