package se.skl.prenumerationstjanst.job;

import com.chbase.HVRequestException;
import com.chbase.StatusCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import se.skl.prenumerationstjanst.dto.PushJobDTO;
import se.skl.prenumerationstjanst.exception.transfer.PushUnauthorizedException;

/**
 * @author Martin Samuelsson
 */
@Component
public class PhkCHBasePushExceptionMapper {

    private static final Logger logger = LoggerFactory.getLogger(PhkCHBasePushExceptionMapper.class);

    public void handleException(Exception e) {
        logger.info("handleException class: {}", e.getClass().getName());
        logger.info("handleException message: {}", e.getMessage());
        if (e instanceof HVRequestException) {
            HVRequestException hvRequestException = (HVRequestException) e;
            StatusCode code = hvRequestException.getCode();
            logger.debug("HVRequestException code: {}", code);
            logger.debug("HVRequestException responseCode: {}", hvRequestException.getResponseCode());
        }
    }

    public Exception handle(Exception e, PushJobDTO pushJobDTO) {
        handleException(e);
        if (e instanceof HVRequestException && ((HVRequestException) e).getCode() == StatusCode.ACCESS_DENIED) {
            return new PushUnauthorizedException(pushJobDTO);
        } else {
            return e;
        }
    }
}
