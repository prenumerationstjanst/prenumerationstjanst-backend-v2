package se.skl.prenumerationstjanst.job;

import com.chbase.methods.jaxb.SimpleRequestTemplate;
import com.chbase.methods.jaxb.putthings2.request.PutThings2Request;
import com.chbase.methods.jaxb.putthings2.response.PutThings2Response;
import com.chbase.thing.oxm.jaxb.thing.Common;
import com.chbase.thing.oxm.jaxb.thing.RelatedThing;
import com.chbase.thing.oxm.jaxb.thing.TypeManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import se.skl.prenumerationstjanst.chbase.CHBaseRequestTemplateFactory;
import se.skl.prenumerationstjanst.chbase.CHBaseThing2;
import se.skl.prenumerationstjanst.dto.PushJobDTO;
import se.skl.prenumerationstjanst.exception.transfer.PushFailedException;
import se.skl.prenumerationstjanst.exception.transfer.UnrecoverablePushFailedException;
import se.skl.prenumerationstjanst.model.TransferType;

import javax.xml.bind.*;
import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * PushJobHandler that expects the PushJobDTO value property to be a CHBase "thing" xml
 * and, if present, the relatedValues also to be CHBase "things".
 *
 * @author Martin Samuelsson
 */
public abstract class PhkCHBasePushJobHandler implements PushJobHandler {

    private static final Logger logger = LoggerFactory.getLogger(PhkCHBasePushJobHandler.class);

    private TransferType transferType;

    private JAXBContext thingJaxbContext;

    @Value("${pt.phk.chbase.source_string}")
    private String sourceString;

    @Autowired
    private CHBaseRequestTemplateFactory chBaseRequestTemplateFactory;

    @Autowired
    private PhkCHBasePushExceptionMapper exceptionMapper;

    public PhkCHBasePushJobHandler(TransferType transferType) throws Exception {
        this.transferType = transferType;
        thingJaxbContext = JAXBContext.newInstance(CHBaseThing2.class);
    }

    @Override
    public TransferType transferType() {
        return transferType;
    }

    @Override
    public PushResult processPushJob(PushJobDTO pushJobDTO) throws PushFailedException, UnrecoverablePushFailedException {
        CHBaseThing2 mainThing;
        try {
            mainThing = unmarshalCHBaseThing2(pushJobDTO.getValue());
            if (mainThing == null || mainThing.getTypeId() == null) {
                throw new UnrecoverablePushFailedException("Could not unmarshal pushJob value to Thing2", pushJobDTO);
            }
        } catch (JAXBException e) {
            throw new UnrecoverablePushFailedException("Could not unmarshal pushJob value to Thing2", e, pushJobDTO);
        }
        String thingType = TypeManager.getClassForType(mainThing.getTypeId().getValue()).getSimpleName();
        logger.info("processing {} thing from {} pushJob", thingType, pushJobDTO.getTransferJob().getTransferType());
        if (mainThing.getCommon() == null) {
            mainThing.setCommon(new Common());
        }
        mainThing.getCommon().setSource(sourceString);
        if (!mainThing.getCommon().getRelatedThing().isEmpty()) {
            logger.info("{} thing is related to thing {}", thingType,
                    mainThing.getCommon().getRelatedThing().get(0).getThingId());
        }
        String personId = pushJobDTO.getPhkPersonId();
        String recordId = pushJobDTO.getPhkRecordId();
        SimpleRequestTemplate template = chBaseRequestTemplateFactory.getJaxbSimpleRequestTemplate(personId, recordId);
        PutThings2Request request = new PutThings2Request();
        request.getThing().add(mainThing);
        List<String> relatedItems;
        PutThings2Response response = null;
        try {
            response = (PutThings2Response) template.makeRequest(request);
        } catch (Exception e) {
            logger.error("Caught exception while calling PHK CHBase", e);
            Exception mep = exceptionMapper.handle(e, pushJobDTO);
            logger.debug("Mapped exception to {}", mep.getClass().getName());
            if (mep instanceof PushFailedException) {
                throw (PushFailedException) mep;
            } else if (mep instanceof UnrecoverablePushFailedException) {
                throw (UnrecoverablePushFailedException) mep;
            } else {
                throw new PushFailedException("Could not push " + thingType + " to CHBase", e, pushJobDTO);
            }
            //TODO: categorize possible CHBase / HV exceptions into unchecked (PushFailedException) / checked (UnrecoverablePushFailedException)
            //currently always throwing unchecked
        }
        String thingId = response.getThingId().get(0).getValue();
        logger.info("successfully pushed {} thing with id {} to CHBase", thingType, thingId);
        relatedItems = pushJobDTO.getRelatedValues().stream()
                .map(relatedValue -> {
                    CHBaseThing2 relatedThing2 = null;
                    try {
                        relatedThing2 = unmarshalCHBaseThing2(relatedValue);
                    } catch (JAXBException e) {
                        logger.error("Could not unmarshal relatedValue, this means it will never get pushed", e);
                        //do not throw since our 'main' push was successful and the related items are more
                        //'good-to-have' than required
                        return null;
                    }
                    Common common = Optional.ofNullable(relatedThing2.getCommon()).orElse(new Common());
                    RelatedThing relatedThing = !common.getRelatedThing().isEmpty()
                            ? common.getRelatedThing().get(0)
                            : new RelatedThing();
                    relatedThing.setThingId(thingId);
                    if (common.getRelatedThing().isEmpty()) {
                        common.getRelatedThing().add(relatedThing);
                    } else {
                        common.getRelatedThing().set(0, relatedThing);
                    }
                    relatedThing2.setCommon(common);
                    try {
                        return marshalCHBaseThing2(relatedThing2);
                    } catch (JAXBException e) {
                        logger.error("Could not marshal related Thing2, this means it will never get pushed", e);
                        //do not throw since our 'main' push was successful and the related items are more
                        //'good-to-have' than required
                        return null;
                    }
                })
                .filter(s -> s != null)
                .collect(Collectors.toList());
        return new PushResult(thingType, relatedItems);
    }

    @SuppressWarnings("Duplicates")
    private String marshalCHBaseThing2(CHBaseThing2 thing) throws JAXBException {
        Marshaller marshaller = thingJaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE); //omit xml declaration
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        QName thingQName = new QName(null, "thing");
        JAXBElement<CHBaseThing2> thing2JAXBElement = new JAXBElement<>(thingQName, CHBaseThing2.class, thing);
        StringWriter xmlWriter = new StringWriter();
        marshaller.marshal(thing2JAXBElement, xmlWriter);
        return xmlWriter.toString();
    }

    protected CHBaseThing2 unmarshalCHBaseThing2(String chbaseThing2Xml) throws JAXBException {
        Unmarshaller unmarshaller = thingJaxbContext.createUnmarshaller();
        JAXBElement<CHBaseThing2> unmarshal = unmarshaller.unmarshal(new StreamSource(new StringReader
                (chbaseThing2Xml)), CHBaseThing2.class);
        return unmarshal.getValue();
    }
}
