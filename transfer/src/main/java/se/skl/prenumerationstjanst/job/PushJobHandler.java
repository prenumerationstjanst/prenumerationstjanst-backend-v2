package se.skl.prenumerationstjanst.job;

import se.skl.prenumerationstjanst.dto.PushJobDTO;
import se.skl.prenumerationstjanst.exception.transfer.PushFailedException;
import se.skl.prenumerationstjanst.exception.transfer.UnrecoverablePushFailedException;

/**
 * PushJobHandlers are responsible for pushing incoming data items to the recipient system and handle
 * logical errors. If applicable the PushJobHandler shall populate related items with the remote data item id
 *
 * @author Martin Samuelsson
 */
public interface PushJobHandler extends JobHandler {
    PushResult processPushJob(PushJobDTO pushJobDTO) throws PushFailedException, UnrecoverablePushFailedException;
}
