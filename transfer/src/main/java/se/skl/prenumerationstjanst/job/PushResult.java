package se.skl.prenumerationstjanst.job;

import java.util.Collections;
import java.util.List;

/**
 * @author Martin Samuelsson
 */
public class PushResult {
    private final String typeOfData;
    private final List<String> relatedItems;

    public PushResult(String typeOfData, List<String> relatedItems) {
        this.typeOfData = typeOfData;
        this.relatedItems = relatedItems != null
                ? Collections.unmodifiableList(relatedItems)
                : Collections.emptyList();
    }

    public String getTypeOfData() {
        return typeOfData;
    }

    public List<String> getRelatedItems() {
        return relatedItems;
    }

    @Override
    public String toString() {
        return "PushResult{" +
                "typeOfData='" + typeOfData + '\'' +
                ", relatedItems=" + relatedItems +
                '}';
    }
}
