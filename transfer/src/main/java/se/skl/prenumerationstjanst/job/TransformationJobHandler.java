package se.skl.prenumerationstjanst.job;

import se.skl.prenumerationstjanst.dto.TransformationJobDTO;
import se.skl.prenumerationstjanst.exception.transfer.TransformationFailedException;
import se.skl.prenumerationstjanst.exception.transfer.UnrecoverableTransformationFailedException;

/**
 * TransformationJobHandlers are responsible for the transformation of source data items
 * into a number of data items with optional related items suitable for pushing to a recipient system
 *
 * @author Martin Samuelsson
 */
public interface TransformationJobHandler extends JobHandler {
    TransformationResult processTransformationJob(TransformationJobDTO transformationJobDTO)
            throws TransformationFailedException, UnrecoverableTransformationFailedException;
    int version();
}
