package se.skl.prenumerationstjanst.job;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Martin Samuelsson
 */
public class TransformationResult {

    private final List<PushJobSpecification> pushJobSpecifications;

    public TransformationResult(List<PushJobSpecification> pushJobSpecifications) {
        this.pushJobSpecifications = pushJobSpecifications;
    }

    public List<PushJobSpecification> getPushJobSpecifications() {
        return pushJobSpecifications;
    }

    @Override
    public String toString() {
        return "TransformationResult{" +
                "pushJobSpecifications=" + pushJobSpecifications +
                '}';
    }

    public static class PushJobSpecification {
        private final String value;
        private final List<String> relatedValues;
        private final LocalDateTime sourceFetchTime;
        private final String sourceId;
        private final int sourceVersion;

        public PushJobSpecification(String value, List<String> relatedValues, LocalDateTime sourceFetchTime,
                                    String sourceId, int sourceVersion) {
            this.value = value;
            this.relatedValues = relatedValues;
            this.sourceFetchTime = sourceFetchTime;
            this.sourceId = sourceId;
            this.sourceVersion = sourceVersion;
        }

        public String getValue() {
            return value;
        }

        public List<String> getRelatedValues() {
            return relatedValues;
        }

        public LocalDateTime getSourceFetchTime() {
            return sourceFetchTime;
        }

        public String getSourceId() {
            return sourceId;
        }

        public int getSourceVersion() {
            return sourceVersion;
        }

        @Override
        public String toString() {
            return "PushJobSpecification{" +
                    "value='" + value + '\'' +
                    ", relatedValues=" + relatedValues +
                    ", sourceFetchTime=" + sourceFetchTime +
                    ", sourceId='" + sourceId + '\'' +
                    ", sourceVersion=" + sourceVersion +
                    '}';
        }
    }
}
