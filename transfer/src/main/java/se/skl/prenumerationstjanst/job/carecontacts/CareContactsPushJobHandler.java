package se.skl.prenumerationstjanst.job.carecontacts;

import se.skl.prenumerationstjanst.job.PhkCHBasePushJobHandler;
import se.skl.prenumerationstjanst.job.PushJobHandler;
import se.skl.prenumerationstjanst.model.TransferType;

/**
 * @author Martin Samuelsson
 */
public class CareContactsPushJobHandler extends PhkCHBasePushJobHandler implements PushJobHandler {

    public CareContactsPushJobHandler() throws Exception {
        super(TransferType.CARE_CONTACTS);
    }
}
