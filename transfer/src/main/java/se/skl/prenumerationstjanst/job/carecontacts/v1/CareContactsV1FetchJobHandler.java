package se.skl.prenumerationstjanst.job.carecontacts.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import se.skl.prenumerationstjanst.job.ApigwFetchJobHandler;
import se.skl.prenumerationstjanst.job.FetchJobUriExtractor;
import se.skl.prenumerationstjanst.model.TransferType;

import javax.xml.xpath.XPathExpressionException;

/**
 * @author Martin Samuelsson
 */
@Component
public class CareContactsV1FetchJobHandler extends ApigwFetchJobHandler {

    @Autowired
    private CareContactsV1FetchJobUriExtractor careContactsV1FetchJobUriExtractor;

    protected CareContactsV1FetchJobHandler() throws XPathExpressionException {
        super(TransferType.CARE_CONTACTS,
                "//*[local-name()='careContact']",
                "*[local-name()='careContactHeader']/*[local-name()='sourceSystemHSAId']",
                1);
    }

    @Override
    protected FetchJobUriExtractor getFetchJobUriExtractor() {
        return careContactsV1FetchJobUriExtractor;
    }
}
