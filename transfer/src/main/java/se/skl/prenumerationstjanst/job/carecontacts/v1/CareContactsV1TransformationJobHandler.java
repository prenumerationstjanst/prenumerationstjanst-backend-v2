package se.skl.prenumerationstjanst.job.carecontacts.v1;

import com.chbase.thing.oxm.jaxb.base.*;
import com.chbase.thing.oxm.jaxb.comment.Comment;
import com.chbase.thing.oxm.jaxb.encounter.Encounter;
import com.chbase.thing.oxm.jaxb.status.Status;
import org.apigw.cc.v1.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.skl.prenumerationstjanst.chbase.CHBaseThing2;
import se.skl.prenumerationstjanst.dto.TransformationJobDTO;
import se.skl.prenumerationstjanst.exception.transfer.TransformationFailedException;
import se.skl.prenumerationstjanst.job.ApigwTransformationJobHandler;
import se.skl.prenumerationstjanst.job.TransformationJobHandler;
import se.skl.prenumerationstjanst.job.TransformationResult;
import se.skl.prenumerationstjanst.model.TransferType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
public class CareContactsV1TransformationJobHandler extends ApigwTransformationJobHandler<CareContactType>
        implements TransformationJobHandler {

    private static final Logger logger = LoggerFactory.getLogger(CareContactsV1TransformationJobHandler.class);

    private static final String RIV_DATETIME_FORMAT = "yyyyMMddHHmmss";

    private JAXBContext careContactJaxbContext;

    public CareContactsV1TransformationJobHandler() throws JAXBException {
        super(TransferType.CARE_CONTACTS, 1, DateTimeFormatter.ofPattern(RIV_DATETIME_FORMAT));
        careContactJaxbContext = JAXBContext.newInstance(CareContactType.class);
    }

    @Override
    protected TransformationResult transform(CareContactType doc, LocalDateTime sourceFetchTime,
                                             TransformationJobDTO job) throws TransformationFailedException {

        PatientSummaryHeaderType header = doc.getCareContactHeader();
        String uniqueDocumentId = header.getSourceSystemHSAId() + "/" + header.getDocumentId();
        CareContactBodyType body = doc.getCareContactBody();

        CHBaseThing2 baseThing = new CHBaseThing2();
        //Encounter is the main result type of this transformation
        Encounter encounter = new Encounter();

        HealthcareProfessionalType accountableHealthcareProfessional = header.getAccountableHealthcareProfessional();

        // authorTime is second choice for encounter.when.
        // Will be overwritten with body.careContactTimePeriod.start if it is set
        encounter.setWhen(getDateTime(accountableHealthcareProfessional.getAuthorTime()));

        if (body.getCareContactCode() != null) {
            int careContactCode = body.getCareContactCode();
            CodableValue encounterType = null;
            switch (careContactCode) {
                case 1:
                    encounterType = new CodableValue("Besök");
                    break;
                case 2: //KV Vårdkontakttyp (1.2.752.129.2.2.2.25) KOD 5
                    encounterType = new CodableValue("Telefon", new CodedValue("5", null, "KV Vårdkontakttyp", null));
                    break;
                case 3: //KV Vårdkontakttyp (1.2.752.129.2.2.2.25) KOD 1
                    encounterType = new CodableValue("Vårdtillfälle", new CodedValue("1", null, "KV Vårdkontakttyp", null));
                    break;
                case 4:
                    encounterType = new CodableValue("Dagsjukvård");
                    break;
                case 5:
                    encounterType = new CodableValue("Annan");
                    break;
            }
            encounter.setType(encounterType);
        }
        encounter.setReason(body.getCareContactReason());
        Organization facility = new Organization();
        if (body.getCareContactOrgUnit() != null) {
            //TKB: 1..1
            facility.setName(body.getCareContactOrgUnit().getOrgUnitName());
        } else {
            facility.setName("");
        }
        encounter.setFacility(facility);
        if (body.getCareContactTimePeriod() != null) {
            TimePeriodType careContactTimePeriod = body.getCareContactTimePeriod();
            if (careContactTimePeriod.getStart() != null && !careContactTimePeriod.getStart().trim().isEmpty()) {
                encounter.setWhen(getDateTime(careContactTimePeriod.getStart()));
                DurationValue duration = new DurationValue();
                duration.setStartDate(getApproxDateTime(careContactTimePeriod.getStart()));
                if (careContactTimePeriod.getEnd() != null && !careContactTimePeriod.getEnd().trim().isEmpty()) {
                    duration.setEndDate(getApproxDateTime(careContactTimePeriod.getEnd()));
                }
                encounter.setDuration(duration);
            }
        }

        //Possibly one Status per encounter
        Status status = null;
        if (body.getCareContactStatus() != null) {
            int careContactStatus = body.getCareContactStatus();
            status = new Status();
            CodableValue statusType = null;
            switch (careContactStatus) {
                case 1:
                    statusType = new CodableValue("ej påbörjad");
                    break;
                case 2:
                    statusType = new CodableValue("inställd vårdkontakt", new CodedValue("53641000052109", null,
                            "Snomed CT", null));
                    break;
                case 3:
                    statusType = new CodableValue("pågående vårdkontakt", new CodedValue("53651000052107", null,
                            "Snomed CT", null));
                    break;
                case 4:
                    statusType = new CodableValue("avbruten vårdkontakt", new CodedValue("53661000052105", null,
                            "Snomed CT", null));
                    break;
                case 5:
                    statusType = new CodableValue("avslutad vårdkontakt", new CodedValue("53671000052101", null,
                            "Snomed CT", null));
                    break;
            }
            status.setStatusType(statusType);
        }


        //One Contact (Person) per Encounter
        Person person = new Person();
        person.setId(accountableHealthcareProfessional.getHealthcareProfessionalHSAId());
        Name personName = new Name();
        if (accountableHealthcareProfessional.getHealthcareProfessionalName() != null &&
                !accountableHealthcareProfessional.getHealthcareProfessionalName().trim().isEmpty()) {
            personName.setFull(accountableHealthcareProfessional.getHealthcareProfessionalName());
        } else {
            personName.setFull("Namn saknas");
        }
        person.setName(personName);
        person.setOrganization(accountableHealthcareProfessional.getHealthcareProfessionalOrgUnit().getOrgUnitName());

        Comment rivSourceComment = createRivSourceComment("riv.clinicalprocess.logistics.logistics" +
                ".getcarecontacts_2.0");

        try {
            baseThing.setData(encounter);
        } catch (Exception e) {
            throw new TransformationFailedException("Could not add Encounter to Thing2", e, job);
        }

        List<String> relatedThings = Stream.of(status, person, rivSourceComment)
                .filter(o -> o != null)
                .map(relatedObj -> {
                    CHBaseThing2 relatedThing = new CHBaseThing2();
                    try {
                        relatedThing.setData(relatedObj);
                        return marshalCHBaseThing2(relatedThing);
                    } catch (Exception e) {
                        logger.error("Could not create related {} Thing2", relatedObj.getClass().getName(), e);
                    }
                    return null;
                })
                .filter(s -> s != null)
                .collect(Collectors.toList());

        try {
            return new TransformationResult(Collections.singletonList(
                    new TransformationResult.PushJobSpecification(marshalCHBaseThing2(baseThing), relatedThings,
                            job.getSourceFetchTime(), uniqueDocumentId, version())));
        } catch (JAXBException e) {
            throw new TransformationFailedException("Could not marshal Encounter Thing2", e, job);
        }
    }

    @Override
    protected CareContactType unmarshal(String document) throws JAXBException {
        Unmarshaller unmarshaller = careContactJaxbContext.createUnmarshaller();
        JAXBElement<CareContactType> ccElem = unmarshaller.unmarshal(new StreamSource(new StringReader(document)),
                CareContactType.class);
        return ccElem.getValue();
    }
}
