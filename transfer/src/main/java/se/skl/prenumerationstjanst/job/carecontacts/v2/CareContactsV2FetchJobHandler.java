package se.skl.prenumerationstjanst.job.carecontacts.v2;

import org.springframework.beans.factory.annotation.Autowired;
import se.skl.prenumerationstjanst.job.ApigwFetchJobHandler;
import se.skl.prenumerationstjanst.job.FetchJobUriExtractor;
import se.skl.prenumerationstjanst.model.TransferType;

import javax.xml.xpath.XPathExpressionException;

/**
 * @author Martin Samuelsson
 */
public class CareContactsV2FetchJobHandler extends ApigwFetchJobHandler {

    @Autowired
    private CareContactsV2FetchJobUriExtractor careContactsV2FetchJobUriExtractor;

    public CareContactsV2FetchJobHandler() throws XPathExpressionException {
        super(TransferType.CARE_CONTACTS,
                "//*[local-name()='careContact']",
                "*[local-name()='careContactHeader']/*[local-name()='sourceSystemHSAId']",
                2);
    }

    @Override
    protected FetchJobUriExtractor getFetchJobUriExtractor() {
        return careContactsV2FetchJobUriExtractor;
    }
}
