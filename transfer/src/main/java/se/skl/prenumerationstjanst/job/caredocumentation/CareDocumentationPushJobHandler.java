package se.skl.prenumerationstjanst.job.caredocumentation;

import org.springframework.stereotype.Component;
import se.skl.prenumerationstjanst.job.PhkCHBasePushJobHandler;
import se.skl.prenumerationstjanst.job.PushJobHandler;
import se.skl.prenumerationstjanst.model.TransferType;

/**
 * @author Martin Samuelsson
 */
@Component
public class CareDocumentationPushJobHandler extends PhkCHBasePushJobHandler implements PushJobHandler {

    public CareDocumentationPushJobHandler() throws Exception {
        super(TransferType.CARE_DOCUMENTATION);
    }
}
