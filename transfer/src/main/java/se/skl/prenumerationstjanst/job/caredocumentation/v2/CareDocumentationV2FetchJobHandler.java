package se.skl.prenumerationstjanst.job.caredocumentation.v2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import se.skl.prenumerationstjanst.job.ApigwFetchJobHandler;
import se.skl.prenumerationstjanst.job.FetchJobUriExtractor;
import se.skl.prenumerationstjanst.model.TransferType;

import javax.xml.xpath.XPathExpressionException;

/**
 * @author Martin Samuelsson
 */
@Component
public class CareDocumentationV2FetchJobHandler extends ApigwFetchJobHandler {

    @Autowired
    private CareDocumentationV2FetchJobUriExtractor careDocumentationV2FetchJobUriExtractor;

    public CareDocumentationV2FetchJobHandler() throws XPathExpressionException {
        super(TransferType.CARE_DOCUMENTATION,
                "//*[local-name()='careDocumentationRecord']",
                "*[local-name()='careDocumentationHeader']/*[local-name()='sourceSystemHSAId']",
                2);
    }

    @Override
    protected FetchJobUriExtractor getFetchJobUriExtractor() {
        return careDocumentationV2FetchJobUriExtractor;
    }
}
