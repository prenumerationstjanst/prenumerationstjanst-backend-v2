package se.skl.prenumerationstjanst.job.caredocumentation.v2;

import com.chbase.thing.oxm.jaxb.annotation.Annotation;
import com.chbase.thing.oxm.jaxb.base.CodableValue;
import com.chbase.thing.oxm.jaxb.base.Name;
import com.chbase.thing.oxm.jaxb.base.Person;
import com.chbase.thing.oxm.jaxb.comment.Comment;
import com.chbase.thing.oxm.jaxb.status.Status;
import org.apigw.cd.v2.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import se.skl.prenumerationstjanst.chbase.CHBaseThing2;
import se.skl.prenumerationstjanst.dto.TransformationJobDTO;
import se.skl.prenumerationstjanst.exception.transfer.TransformationFailedException;
import se.skl.prenumerationstjanst.job.ApigwTransformationJobHandler;
import se.skl.prenumerationstjanst.job.TransformationJobHandler;
import se.skl.prenumerationstjanst.job.TransformationResult;
import se.skl.prenumerationstjanst.model.TransferType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Martin Samuelsson
 */
@Component
public class CareDocumentationV2TransformationJobHandler extends
        ApigwTransformationJobHandler<CareDocumentationRecordType> implements TransformationJobHandler {

    private static final Logger logger = LoggerFactory.getLogger(CareDocumentationV2TransformationJobHandler.class);

    private static final String RIV_DATETIME_FORMAT = "yyyyMMddHHmmss";
    private JAXBContext caredocJaxbContext;

    public CareDocumentationV2TransformationJobHandler() throws JAXBException {
        super(TransferType.CARE_DOCUMENTATION, 2, DateTimeFormatter.ofPattern(RIV_DATETIME_FORMAT));
        caredocJaxbContext = JAXBContext.newInstance(CareDocumentationRecordType.class);
    }

    @Override
    protected TransformationResult transform(CareDocumentationRecordType doc, LocalDateTime sourceFetchTime,
                                             TransformationJobDTO job) throws TransformationFailedException {

        PatientSummaryHeaderType header = doc.getCareDocumentationHeader();
        String uniqueDocumentId = header.getSourceSystemHSAId() + "/" + header.getDocumentId();
        CareDocumentationBodyType body = doc.getCareDocumentationBody();

        CHBaseThing2 baseThing = new CHBaseThing2();
        //(Medical) Annotation is the main result type of this transformation
        Annotation annotation = new Annotation();

        HealthcareProfessionalType accountableHealthcareProfessional = header.getAccountableHealthcareProfessional();
        annotation.setWhen(getDateTime(accountableHealthcareProfessional.getAuthorTime()));
        Person author = new Person();
        author.setId(accountableHealthcareProfessional.getHealthcareProfessionalHSAId());
        Name name = new Name();
        name.setFull(accountableHealthcareProfessional.getHealthcareProfessionalName() != null
                ? accountableHealthcareProfessional.getHealthcareProfessionalName()
                : "Namn saknas");
        author.setName(name);
        if (accountableHealthcareProfessional.getHealthcareProfessionalOrgUnit() != null) {
            author.setOrganization(accountableHealthcareProfessional.getHealthcareProfessionalOrgUnit()
                    .getOrgUnitName());
        }
        annotation.setAuthor(author);

        ClinicalDocumentNoteType clinicalDocumentNote = body.getClinicalDocumentNote();
        //TKB: antingen ClinicalDocumentNoteCode eller ClinicalDocumentTypeCode
        //TKB: ClinicalDocumentTypeCode används endast av NPÖ-adaptrar
        String classification = null;
        if (clinicalDocumentNote.getClinicalDocumentNoteCode() != null) {
            /*
            atb = Åtgärd/Behandling
            auf = Anteckning utan fysiskt möte
            bes = Besöksanteckning
            ins = Inskrivning
            sam = Sammanfattning
            sao = Samordning
            slu = Slutanteckning
            sva = Slutenvårdsanteckning
            utr = Utredning
             */
            switch (clinicalDocumentNote.getClinicalDocumentNoteCode()) {
                case ATB:
                    classification = "Åtgärd/Behandling";
                    break;
                case AUF:
                    classification = "Anteckning utan fysiskt möte";
                    break;
                case BES:
                    classification = "Besöksanteckning";
                    break;
                case INS:
                    classification = "Inskrivning";
                    break;
                case SAM:
                    classification = "Sammanfattning";
                    break;
                case SAO:
                    classification = "Samordning";
                    break;
                case SLU:
                    classification = "Slutanteckning";
                    break;
                case SVA:
                    classification = "Slutenvårdsanteckning";
                    break;
                case UTR:
                    classification = "Utredning";
                    break;
            }
        } else {
            /*
            dag = Daganteckning
            epi = Epikris
            int = Intagninganteckning
            ova = Öppenvårdsanteckning
            ovr = Övrigt dokument
            ovs = Öppenvårdssammanfattning
             */
            switch (clinicalDocumentNote.getClinicalDocumentTypeCode()) {
                case DAG:
                    classification = "Daganteckning";
                    break;
                case EPI:
                    classification = "Epikris";
                    break;
                case INT:
                    classification = "Intagninganteckning";
                    break;
                case OVA:
                    classification = "Öppenvårdsanteckning";
                    break;
                case OVR:
                    classification = "Övrigt dokument";
                    break;
                case OVS:
                    classification = "Öppenvårdssammanfattning";
                    break;
            }
        }
        annotation.setClassification(classification);

        //TKB: Något av clinicalDocumentNoteText eller multimediaEntry
        if (clinicalDocumentNote.getClinicalDocumentNoteText() != null) {
            try {
                annotation.setContent(parseNoteText(clinicalDocumentNote.getClinicalDocumentNoteText()));
            } catch (XMLStreamException e) {
                throw new TransformationFailedException("Could not parse clinicalDocumentNoteText", e, job);
            }
        } else { //TKB: Något av clinicalDocumentNoteText eller multimediaEntry
            //TODO!
            //see: https://www.chbasedeveloperportal.com/chbase/en-US/docs/Develop/Dn783311
        }

        try {
            baseThing.setData(annotation);
        } catch (Exception e) {
            throw new TransformationFailedException("Could not add Annotation to Thing2", e, job);
        }

        //Status - Signerat/Osignerat
        Status status = new Status();
        //APIGW filtrerar bort dokument som inte innehåller signatureTime.
        //signatureTime + förekomst av legalAuthenticationHsaId = signerat
        if (header.getLegalAuthenticator() != null
                && header.getLegalAuthenticator().getLegalAuthenticatorHSAId() != null
                && !header.getLegalAuthenticator().getLegalAuthenticatorHSAId().isEmpty()) {
            status.setStatusType(new CodableValue("Signerat"));
        } else {
            status.setStatusType(new CodableValue("Osignerat"));
        }

        List<Comment> comments = Collections.emptyList();
        if (clinicalDocumentNote.getDissentintOpinion() != null
                && !clinicalDocumentNote.getDissentintOpinion().isEmpty()) {
            comments = clinicalDocumentNote.getDissentintOpinion().stream()
                    .map(dissentingOpinion -> {
                        Comment comment = createCommentBase(dissentingOpinion.getAuthorTime());
                        String opinion = String.format("Avvikande åsikt av %s: %s", "X",
                                dissentingOpinion.getOpinion()); //TODO: personName filtreras bort i APIGW, hur göra?
                        comment.setContent(opinion);
                        comment.setCategory(createCommentCategory("Avvikande åsikt", "GCD_DISSENTINGOPINION"));
                        return comment;
                    })
                    .collect(Collectors.toList());
        }

        Comment rivSourceComment =
                createRivSourceComment("riv.clinicalprocess.healthcond.description.caredocumentation_2.1");
        List<String> relatedThings = Stream.concat(comments.stream(), Stream.of(status, rivSourceComment))
                .map(relatedObj -> {
                    CHBaseThing2 relatedThing = new CHBaseThing2();
                    try {
                        relatedThing.setData(relatedObj);
                        return marshalCHBaseThing2(relatedThing);
                    } catch (Exception e) {
                        logger.error("Could not create related {} Thing2", relatedObj.getClass().getName(), e);
                    }
                    return null;
                })
                .filter(s -> s != null)
                .collect(Collectors.toList());

        try {
            return new TransformationResult(Collections.singletonList(new TransformationResult.PushJobSpecification
                    (marshalCHBaseThing2(baseThing), relatedThings, job.getSourceFetchTime(),
                            uniqueDocumentId, version())));
        } catch (JAXBException e) {
            throw new TransformationFailedException("Could not marshal Annotation Thing2", e, job);
        }
    }

    @Override
    protected CareDocumentationRecordType unmarshal(String document) throws JAXBException {
        Unmarshaller unmarshaller = caredocJaxbContext.createUnmarshaller();
        JAXBElement<CareDocumentationRecordType> careDocRecord = unmarshaller.unmarshal(
                new StreamSource(new StringReader(document)), CareDocumentationRecordType.class);
        return careDocRecord.getValue();
    }

    private String parseNoteText(String noteText) throws XMLStreamException {
        if (!noteText.trim().startsWith("<") //might not be XML TODO: refine a little bit?
                || !noteText.contains("article")) { //must contain an article element
            return noteText;
        }
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new StreamSource(new StringReader
                (noteText)));
        StringBuilder stringBuilder = new StringBuilder();
        String[] lastTwoWrites = {"", ""};
        while (xmlEventReader.hasNext()) {
            XMLEvent xmlEvent = xmlEventReader.nextEvent();
            if (xmlEvent.isStartElement()) {
                String elementName = xmlEvent.asStartElement().getName().getLocalPart();
                String prefix = "";
                switch (elementName) {
                    case "section":
                        if (!isDoubleLineBreak(lastTwoWrites)) {
                            prefix = "\n";
                        }
                        break;
                    case "bibliography":
                        if (!isDoubleLineBreak(lastTwoWrites)) {
                            prefix = "\n";
                        }
                        break;
                    default:
                        prefix = "";
                        break;
                }
                stringBuilder.append(prefix);
                addToLastTwoWrites(lastTwoWrites, prefix);
            } else if (xmlEvent.isCharacters()) {
                String data = xmlEvent.asCharacters().getData().trim();
                if (!data.isEmpty()) {
                    stringBuilder.append(data);
                    addToLastTwoWrites(lastTwoWrites, data);
                }
            } else if (xmlEvent.isEndElement()) {
                String elementName = xmlEvent.asEndElement().getName().getLocalPart();
                String postfix;
                switch (elementName) {
                    case "title":
                        postfix = ":\n";
                        break;
                    case "para":
                        postfix = "\n";
                        break;
                    case "section":
                        postfix = "\n";
                        break;
                    default:
                        postfix = "";
                        break;
                }
                stringBuilder.append(postfix);
                addToLastTwoWrites(lastTwoWrites, postfix);
            }
        }
        return stringBuilder.toString();
    }

    private boolean isDoubleLineBreak(String[] lastTwoWrites) {
        return "\n".equals(lastTwoWrites[0]) && "\n".equals(lastTwoWrites[1]);
    }

    private void addToLastTwoWrites(String[] lastTwoWrites, String addition) {
        lastTwoWrites[0] = lastTwoWrites[1];
        lastTwoWrites[1] = addition;
    }
}
