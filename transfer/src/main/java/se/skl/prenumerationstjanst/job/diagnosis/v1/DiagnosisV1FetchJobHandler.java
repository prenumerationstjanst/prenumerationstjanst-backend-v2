package se.skl.prenumerationstjanst.job.diagnosis.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import se.skl.prenumerationstjanst.job.ApigwFetchJobHandler;
import se.skl.prenumerationstjanst.job.FetchJobUriExtractor;
import se.skl.prenumerationstjanst.model.TransferType;

import javax.xml.xpath.XPathExpressionException;

/**
 * @author Martin Samuelsson
 */
@Component
public class DiagnosisV1FetchJobHandler extends ApigwFetchJobHandler {

    @Autowired
    private DiagnosisV1FetchJobUriExtractor diagnosisV1FetchJobUriExtractor;

    protected DiagnosisV1FetchJobHandler() throws XPathExpressionException {
        super(TransferType.DIAGNOSIS,
                "//*[local-name()='diagnosisRecord']",
                "*[local-name()='diagnosisHeader']/*[local-name()='sourceSystemHSAId']",
                1);
    }

    @Override
    protected FetchJobUriExtractor getFetchJobUriExtractor() {
        return diagnosisV1FetchJobUriExtractor;
    }
}
