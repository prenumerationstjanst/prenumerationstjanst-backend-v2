package se.skl.prenumerationstjanst.job.diagnosis.v1;

import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import se.skl.prenumerationstjanst.dto.FetchJobDTO;
import se.skl.prenumerationstjanst.job.FetchJobUriExtractor;

import java.net.URI;
import java.time.format.DateTimeFormatter;

/**
 * @author Martin Samuelsson
 */
@Component
public class DiagnosisV1FetchJobUriExtractor implements FetchJobUriExtractor {

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.BASIC_ISO_DATE;

    @Override
    public URI createURI(String baseUri, FetchJobDTO fetchJobDTO) {
        String startDate = fetchJobDTO.getTransferJob().getStartDateTime().format(dateTimeFormatter);
        String endDate = fetchJobDTO.getTransferJob().getEndDateTime().format(dateTimeFormatter);
        String sourceSystemHsaId = fetchJobDTO.getTransferJob().getSourceSystemHsaId();
        MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
        queryParams.add("start", startDate);
        queryParams.add("end", endDate);
        if (sourceSystemHsaId != null) {
            queryParams.add("sourceSystem", sourceSystemHsaId);
        }
        return UriComponentsBuilder.fromUriString(baseUri)
                .path("/clinicalprocess/healthcond/description/v1/diagnosis")
                .queryParams(queryParams)
                .build().toUri();
    }
}
