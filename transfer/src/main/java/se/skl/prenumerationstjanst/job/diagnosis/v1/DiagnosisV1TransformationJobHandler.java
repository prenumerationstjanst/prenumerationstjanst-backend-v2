package se.skl.prenumerationstjanst.job.diagnosis.v1;

import com.chbase.thing.oxm.jaxb.base.CodableValue;
import com.chbase.thing.oxm.jaxb.base.CodedValue;
import com.chbase.thing.oxm.jaxb.base.Name;
import com.chbase.thing.oxm.jaxb.base.Person;
import com.chbase.thing.oxm.jaxb.comment.Comment;
import com.chbase.thing.oxm.jaxb.condition.Condition;
import com.chbase.thing.oxm.jaxb.thing.Common;
import com.chbase.thing.oxm.jaxb.thing.RelatedThing;
import org.apigw.diag.v1.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.skl.prenumerationstjanst.chbase.CHBaseThing2;
import se.skl.prenumerationstjanst.dto.TransformationJobDTO;
import se.skl.prenumerationstjanst.exception.transfer.TransformationFailedException;
import se.skl.prenumerationstjanst.job.ApigwTransformationJobHandler;
import se.skl.prenumerationstjanst.job.TransformationJobHandler;
import se.skl.prenumerationstjanst.job.TransformationResult;
import se.skl.prenumerationstjanst.model.TransferType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
public class DiagnosisV1TransformationJobHandler extends ApigwTransformationJobHandler<DiagnosisType>
        implements TransformationJobHandler {

    private static final Logger logger = LoggerFactory.getLogger(DiagnosisV1TransformationJobHandler.class);

    private static final String RIV_DATETIME_FORMAT = "yyyyMMddHHmmss";

    private JAXBContext diagnosisJaxbContext;

    public DiagnosisV1TransformationJobHandler() throws JAXBException {
        super(TransferType.DIAGNOSIS, 1, DateTimeFormatter.ofPattern(RIV_DATETIME_FORMAT));
        diagnosisJaxbContext = JAXBContext.newInstance(DiagnosisType.class);
    }

    @Override
    protected TransformationResult transform(DiagnosisType doc, LocalDateTime sourceFetchTime,
                                             TransformationJobDTO job) throws TransformationFailedException {

        PatientSummaryHeaderType header = doc.getDiagnosisHeader();
        String uniqueDocumentId = header.getSourceSystemHSAId() + "/" + header.getDocumentId();
        DiagnosisBodyType body = doc.getDiagnosisBody();

        CHBaseThing2 baseThing = new CHBaseThing2();
        //Condition is the main result type of this transformation
        Condition condition = new Condition();

        HealthcareProfessionalType accountableHealthcareProfessional = header.getAccountableHealthcareProfessional();
        condition.setOnsetDate(getApproxDateTime(accountableHealthcareProfessional.getAuthorTime()));

        Person authorContact = new Person();
        authorContact.setId(accountableHealthcareProfessional.getHealthcareProfessionalHSAId());
        CodableValue authorContactType = new CodableValue();
        authorContactType.setText("ansvarig");
        CodedValue authorContactTypeCode = new CodedValue();
        authorContactTypeCode.setValue("53281000052102");
        authorContactTypeCode.setType("Snomed CT");
        authorContactType.getCode().add(authorContactTypeCode);
        authorContact.setType(authorContactType);
        Name authorContactName = new Name();
        String acnFull = accountableHealthcareProfessional.getHealthcareProfessionalName() != null
                && !accountableHealthcareProfessional.getHealthcareProfessionalName().trim().isEmpty()
                ? accountableHealthcareProfessional.getHealthcareProfessionalName()
                : "Namn saknas";
        authorContactName.setFull(acnFull);
        authorContact.setName(authorContactName);
        String acOrg = accountableHealthcareProfessional.getHealthcareProfessionalOrgUnit() != null
                ? accountableHealthcareProfessional.getHealthcareProfessionalOrgUnit().getOrgUnitName()
                : null;
        authorContact.setOrganization(acOrg);

        Person signingContact = null;
        if (header.getLegalAuthenticator() != null) {
            LegalAuthenticatorType legalAuthenticator = header.getLegalAuthenticator();
            signingContact = new Person();
            signingContact.setId(legalAuthenticator.getLegalAuthenticatorHSAId());
            CodableValue signingContactType = new CodableValue();
            signingContactType.setText("den som signerar");
            CodedValue signingContactTypeCode = new CodedValue();
            signingContactTypeCode.setValue("53291000052100");
            signingContactTypeCode.setType("Snomed CT");
            signingContactType.getCode().add(signingContactTypeCode);
            signingContact.setType(signingContactType);
            Name signingContactName = new Name();
            String scnFull = legalAuthenticator.getLegalAuthenticatorName() != null
                    && !legalAuthenticator.getLegalAuthenticatorName().trim().isEmpty()
                    ? legalAuthenticator.getLegalAuthenticatorName()
                    : "Namn saknas";
            signingContactName.setFull(scnFull);
            signingContact.setName(signingContactName);
        }
        Comment diagnosisTypeComment = new Comment();
        diagnosisTypeComment.setCategory(createCommentCategory("Typ av diagnos", "GD_TYPEOFDIAGNOSIS"));
        diagnosisTypeComment.setContent(body.getTypeOfDiagnosis().value()); //Huvuddiagnos / Bidiagnos
        String statusText = body.isChronicDiagnosis() != null && body.isChronicDiagnosis()
                ? "Kronisk"
                : "";
        condition.setStatus(new CodableValue(statusText));
        condition.setName(getCodableValue(body.getDiagnosisCode()));

        Comment rivSourceComment = createRivSourceComment("riv.clinicalprocess.healthcond.description.getdiagnosis_2.0");

        try {
            baseThing.setData(condition);
        } catch (Exception e) {
            throw new TransformationFailedException("Could not add Condition to Thing2", e, job);
        }

        List<CHBaseThing2> relatedContacts = new ArrayList<>();
        try {
            CHBaseThing2 contactThing = new CHBaseThing2();
            Common common = new Common();
            RelatedThing relatedThing = new RelatedThing();
            relatedThing.setRelationshipType("ansvarig");
            common.getRelatedThing().add(relatedThing);
            contactThing.setCommon(common);
            contactThing.setData(authorContact);
            relatedContacts.add(contactThing);
        } catch (Exception e) {
            logger.error("Could not create related Contact (Person) for author", e);
        }
        if (signingContact != null) {
            try {
                CHBaseThing2 contactThing = new CHBaseThing2();
                Common common = new Common();
                RelatedThing relatedThing = new RelatedThing();
                relatedThing.setRelationshipType("den som signerar");
                common.getRelatedThing().add(relatedThing);
                contactThing.setCommon(common);
                contactThing.setData(signingContact);
                relatedContacts.add(contactThing);
            } catch (Exception e) {
                logger.error("Could not create related Contact (Person) for signer", e);
            }
        }

        Stream<CHBaseThing2> chBaseThing2Stream = Stream.of(diagnosisTypeComment, rivSourceComment)
                .filter(o -> o != null)
                .map(relatedObj -> {
                    CHBaseThing2 relatedThing = new CHBaseThing2();
                    try {
                        relatedThing.setData(relatedObj);
                        return relatedThing;
                    } catch (Exception e) {
                        logger.error("Could not create related {} Thing2", relatedObj.getClass().getName(), e);
                    }
                    return null;
                });
        List<String> relatedThings = Stream.concat(chBaseThing2Stream, relatedContacts.stream())
                .map(relatedThing -> {
                    try {
                        return marshalCHBaseThing2(relatedThing);
                    } catch (JAXBException e) {
                        logger.error("Could not marshal related Thing2", e);
                    }
                    return null;
                })
                .filter(s -> s != null)
                .collect(Collectors.toList());

        try {
            return new TransformationResult(Collections.singletonList(new TransformationResult.PushJobSpecification(marshalCHBaseThing2(baseThing), relatedThings,
                    job.getSourceFetchTime(), uniqueDocumentId, version())));
        } catch (JAXBException e) {
            throw new TransformationFailedException("Could not marshal Condition Thing2", e, job);
        }
    }

    @Override
    protected DiagnosisType unmarshal(String document) throws JAXBException {
        Unmarshaller unmarshaller = diagnosisJaxbContext.createUnmarshaller();
        JAXBElement<DiagnosisType> diagElem = unmarshaller.unmarshal(new StreamSource(new StringReader(document)),
                DiagnosisType.class);
        return diagElem.getValue();
    }

    private CodableValue getCodableValue(CVType cvType) {
        CodableValue codableValue = new CodableValue();
        //TKB: Om originalText anges kan ingen av de övriga elementen anges.
        if (cvType.getOriginalText() != null && !cvType.getOriginalText().isEmpty()) {
            codableValue.setText(cvType.getOriginalText());
            return codableValue;
        } else if (cvType.getCode() != null) { //TKB: Om code anges skall också codeSystem samt displayName anges.
            codableValue.setText(cvType.getDisplayName());
            if (cvType.getCodeSystemName() != null) {
                CodedValue codedValue = new CodedValue();
                codedValue.setValue(cvType.getCode());
                codedValue.setType(cvType.getCodeSystemName());
                codedValue.setVersion(cvType.getCodeSystemVersion());
                codableValue.getCode().add(codedValue);
            }
            return codableValue;
        }
        return null;
    }
}
