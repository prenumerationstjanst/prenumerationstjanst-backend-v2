package se.skl.prenumerationstjanst.job.laboratoryorderoutcome;

import org.springframework.stereotype.Component;
import se.skl.prenumerationstjanst.job.PhkCHBasePushJobHandler;
import se.skl.prenumerationstjanst.job.PushJobHandler;
import se.skl.prenumerationstjanst.model.TransferType;

/**
 * @author Martin Samuelsson
 */
@Component
public class LaboratoryOrderOutcomePushJobHandler extends PhkCHBasePushJobHandler implements PushJobHandler {

    public LaboratoryOrderOutcomePushJobHandler() throws Exception {
        super(TransferType.LABORATORY_ORDER_OUTCOME);
    }
}
