package se.skl.prenumerationstjanst.job.laboratoryorderoutcome;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import se.skl.prenumerationstjanst.job.ApigwFetchJobHandler;
import se.skl.prenumerationstjanst.job.FetchJobUriExtractor;
import se.skl.prenumerationstjanst.model.TransferType;

import javax.xml.xpath.XPathExpressionException;

/**
 * @author Martin Samuelsson
 */
@Component
public class LaboratoryOrderOutcomeV1FetchJobHandler extends ApigwFetchJobHandler {

    @Autowired
    private LaboratoryOrderOutcomeV1FetchJobUriExtractor laboratoryOrderOutcomeV1FetchJobUriExtractor;

    public LaboratoryOrderOutcomeV1FetchJobHandler() throws XPathExpressionException {
        super(TransferType.LABORATORY_ORDER_OUTCOME,
                "//*[local-name()='laboratoryOrderOutcomeRecord']",
                "*[local-name()='laboratoryOrderOutcomeHeader']/*[local-name()='sourceSystemHSAId']",
                1);
    }

    @Override
    protected FetchJobUriExtractor getFetchJobUriExtractor() {
        return laboratoryOrderOutcomeV1FetchJobUriExtractor;
    }
}
