package se.skl.prenumerationstjanst.job.laboratoryorderoutcome;

import com.chbase.thing.oxm.jaxb.base.CodableValue;
import com.chbase.thing.oxm.jaxb.base.CodedValue;
import com.chbase.thing.oxm.jaxb.base.GeneralMeasurement;
import com.chbase.thing.oxm.jaxb.base.Organization;
import com.chbase.thing.oxm.jaxb.comment.Comment;
import com.chbase.thing.oxm.jaxb.dates.ApproxDateTime;
import com.chbase.thing.oxm.jaxb.labtestresults.*;
import org.apigw.loo.v1.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import se.skl.prenumerationstjanst.chbase.CHBaseThing2;
import se.skl.prenumerationstjanst.dto.TransformationJobDTO;
import se.skl.prenumerationstjanst.exception.transfer.TransformationFailedException;
import se.skl.prenumerationstjanst.job.ApigwTransformationJobHandler;
import se.skl.prenumerationstjanst.job.TransformationJobHandler;
import se.skl.prenumerationstjanst.job.TransformationResult;
import se.skl.prenumerationstjanst.model.TransferType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Martin Samuelsson
 */
@Component
public class LaboratoryOrderOutcomeV1TransformationJobHandler extends
        ApigwTransformationJobHandler<LaboratoryOrderOutcomeType> implements TransformationJobHandler {

    private static final Logger logger = LoggerFactory
            .getLogger(LaboratoryOrderOutcomeV1TransformationJobHandler.class);

    private static final String RIV_DATETIME_FORMAT = "yyyyMMddHHmmss";
    private JAXBContext labJaxbContext;

    public LaboratoryOrderOutcomeV1TransformationJobHandler() throws Exception {
        super(TransferType.LABORATORY_ORDER_OUTCOME, 1, DateTimeFormatter.ofPattern(RIV_DATETIME_FORMAT));
        labJaxbContext = JAXBContext.newInstance(LaboratoryOrderOutcomeType.class.getPackage().getName());
    }

    @Override
    protected TransformationResult transform(LaboratoryOrderOutcomeType doc, LocalDateTime sourceFetchTime,
                                             TransformationJobDTO job) throws TransformationFailedException {
        PatientSummaryHeaderType header = doc.getLaboratoryOrderOutcomeHeader();
        String uniqueDocumentId = header.getSourceSystemHSAId() + "/" + header.getDocumentId();
        LaboratoryOrderOutcomeBodyType body = doc.getLaboratoryOrderOutcomeBody();

        String orderedByOrgUnitName =
                header.getAccountableHealthcareProfessional().getHealthcareProfessionalOrgUnit().getOrgUnitName();

        //1. LabTestResults is the main result type of this transformation
        LabTestResults labTestResults = new LabTestResults();

        String performedByOrgUnitName = body.getAccountableHealthcareProfessional()
                .getHealthcareProfessionalOrgUnit().getOrgUnitName();
        Organization orderedBy = new Organization();
        orderedBy.setName(orderedByOrgUnitName);
        labTestResults.setOrderedBy(orderedBy);
        labTestResults.setWhen(new ApproxDateTime(getStructuredApproxDate(body.getRegistrationTime())));

        String resultType = body.getResultType(); //TKB: 1..1
        String statusText = null;
        switch (resultType.trim()) {
            case "TILL":
                statusText = "Tilläggssvar";
                break;
            case "DEF":
                statusText = "Definitivsvar";
                break;
            default:
                statusText = "";
        }

        LabTestResultsGroupType labTestResultsGroup = new LabTestResultsGroupType();
        labTestResultsGroup.setStatus(new CodableValue(statusText));
        labTestResultsGroup.setGroupName(new CodableValue(body.getDiscipline()));
        Organization laboratoryName = new Organization();
        laboratoryName.setName(performedByOrgUnitName);
        labTestResultsGroup.setLaboratoryName(laboratoryName);

        List<LabTestResultType> labTestResultList = body.getAnalysis().stream()
                .map(analysis -> {
                    LabTestResultType labTestResult = new LabTestResultType();
                    if (analysis.getAnalysisTime() != null) {
                        TimePeriodType analysisTime = analysis.getAnalysisTime();
                        //TKB: Minst ett av start och end skall anges.
                        String start = analysisTime.getStart();
                        String end = analysisTime.getEnd();
                        if (end != null && !end.trim().isEmpty()) { //end is first choice
                            labTestResult.setWhen(new ApproxDateTime(getStructuredApproxDate(end)));
                        } else if (start != null && !start.trim().isEmpty()) {
                            labTestResult.setWhen(new ApproxDateTime(getStructuredApproxDate(start)));
                        }
                    }
                    //TKB: Ett av attributen analysisCode och analysisText ska anges.
                    CodableValue clinicalCode = new CodableValue();
                    if (analysis.getAnalysisCode() != null) {
                        //TKB: code (Kod från kodsystemet NPU.) kardinalitet: 1..1
                        //TKB: codeSystem (OID för NPU-kodsystemet (1.2.752.108.1).) kardinalitet: 1..1
                        //TKB: displayName (Kodens klartext)
                        CVType analysisCode = analysis.getAnalysisCode();
                        clinicalCode.setText(analysisCode.getDisplayName());
                        CodedValue codedValue = new CodedValue();
                        codedValue.setType("NPU");
                        codedValue.setValue(analysisCode.getCode());
                        clinicalCode.getCode().add(codedValue);
                    } else if (analysis.getAnalysisText() != null) {
                        clinicalCode.setText(analysis.getAnalysisText());
                    }
                    labTestResult.setClinicalCode(clinicalCode);

                    labTestResult.setStatus(new CodableValue(
                            analysis.getAttested() != null
                                    ? "Vidimerat"
                                    : "Ovidimerat"));

                    String specimen = analysis.getSpecimen();
                    if (specimen != null && !specimen.trim().isEmpty()) {
                        labTestResult.setSubstance(new CodableValue(specimen));
                    }

                    String method = analysis.getMethod();
                    if (method != null && !method.trim().isEmpty()) {
                        labTestResult.setCollectionMethod(new CodableValue(method));
                    }

                    String resultNote = null;
                    String analysisComment = analysis.getAnalysisComment();
                    if (analysisComment != null && !analysisComment.trim().isEmpty()) {
                        resultNote = "Kommentar analys: " + analysisComment;
                    }

                    if (analysis.getAnalysisOutcome() != null) {
                        AnalysisOutcomeType analysisOutcome = analysis.getAnalysisOutcome();
                        LabTestResultValueType labTestResultValue = new LabTestResultValueType();
                        GeneralMeasurement measurement = new GeneralMeasurement();
                        String measurementDisplay = analysisOutcome.getOutcomeValue();
                        String outcomeUnit = analysisOutcome.getOutcomeUnit();
                        if (outcomeUnit != null && !outcomeUnit.trim().isEmpty()) {
                            measurementDisplay += " " + outcomeUnit;
                        }
                        measurement.setDisplay(measurementDisplay);
                        labTestResultValue.setMeasurement(measurement);

                        if (analysisOutcome.isPathologicalFlag()) {
                            labTestResultValue.getFlag().add(new CodableValue("Utanför referensområde"));
                        } else {
                            labTestResultValue.getFlag().add(new CodableValue("Inom referensområde"));
                        }

                        String referenceInterval = analysisOutcome.getReferenceInterval();
                        if (referenceInterval != null && !referenceInterval.trim().isEmpty()) {
                            TestResultRange testResultRange = new TestResultRange();
                            testResultRange.setText(new CodableValue(referenceInterval + (outcomeUnit != null
                                    ? " " + outcomeUnit
                                    : "")));
                            testResultRange.setType(new CodableValue("Referensområde"));
                            labTestResultValue.getRanges().add(testResultRange);
                        }
                        labTestResult.setValue(labTestResultValue);
                        String outcomeDescription = analysisOutcome.getOutcomeDescription();
                        if (outcomeDescription != null && !outcomeDescription.trim().isEmpty()) {
                            resultNote = resultNote != null
                                    ? resultNote += ", Kommentar resultat: " + outcomeDescription //Add to existing
                                    : "Kommentar resultat: " + outcomeDescription; //Just set to outcomeDescription
                        }
                    }
                    labTestResult.setNote(resultNote);
                    return labTestResult;
                }).collect(Collectors.toList());

        labTestResultsGroup.getResults().addAll(labTestResultList);
        labTestResults.getLabGroup().add(labTestResultsGroup);

        CHBaseThing2 baseThing = null;
        try {
            baseThing = createThing2(labTestResults);
        } catch (Exception e) {
            throw new TransformationFailedException("Could not add LabTestResults to Thing2", e, job);
        }

        List<Comment> comments = new ArrayList<>();

        String resultReport = body.getResultReport();
        if (resultReport != null && !resultReport.trim().isEmpty()) {
            Comment comment = createCommentBase(body.getRegistrationTime());
            comment.setContent(String.format("Sammanfattande utlåtande: %s", resultReport));
            comment.setCategory(createCommentCategory("Sammanfattande utlåtande", "GLOO_RESULTREPORT"));
            comments.add(comment);
        }

        String resultComment = body.getResultComment();
        if (resultComment != null && !resultComment.trim().isEmpty()) {
            Comment comment = createCommentBase(body.getRegistrationTime());
            comment.setContent(String.format("Kommentar resultat: %s", resultComment));
            comment.setCategory(createCommentCategory("Kommentar resultat", "GLOO_RESULTCOMMENT"));
            comments.add(comment);
        }

        Comment rivSourceComment = createRivSourceComment("riv.clinicalprocess.healthcond.actoutcome" +
                ".getLaboratoryOrderOutcome_3.1");
        comments.add(rivSourceComment);

        List<String> relatedThings = comments.stream()
                .map(comment -> {
                    try {
                        return marshalCHBaseThing2(createThing2(comment));
                    } catch (Exception e) {
                        logger.error("Could not create related Comment Thing2, will continue since this" +
                                " is a related item", e);
                    }
                    return null;
                }).filter(s -> s != null)
                .collect(Collectors.toList());
        try {
            TransformationResult.PushJobSpecification pushJobSpecification = new TransformationResult
                    .PushJobSpecification(marshalCHBaseThing2(baseThing), relatedThings, sourceFetchTime,
                    uniqueDocumentId, version());
            return new TransformationResult(Collections.singletonList(pushJobSpecification));
        } catch (JAXBException e) {
            throw new TransformationFailedException("Could not marshal LabTestResults Thing2", e, job);
        }
    }

    @Override
    protected LaboratoryOrderOutcomeType unmarshal(String document) throws JAXBException {
        Unmarshaller unmarshaller = labJaxbContext.createUnmarshaller();
        JAXBElement<LaboratoryOrderOutcomeType> labRecordElement = unmarshaller.unmarshal(
                new StreamSource(new StringReader(document)), LaboratoryOrderOutcomeType.class);
        return labRecordElement.getValue();
    }

}
