package se.skl.prenumerationstjanst.job.maternitymedicalhistory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import se.skl.prenumerationstjanst.job.ApigwFetchJobHandler;
import se.skl.prenumerationstjanst.job.FetchJobUriExtractor;
import se.skl.prenumerationstjanst.model.TransferType;

import javax.xml.xpath.XPathExpressionException;

/**
 * @author Martin Samuelsson
 */
@Component
public class MaternityMedicalHistoryFetchJobHandler extends ApigwFetchJobHandler {

    @Autowired
    private MaternityMedicalHistoryUriExtractor maternityMedicalHistoryUriExtractor;

    public MaternityMedicalHistoryFetchJobHandler() throws XPathExpressionException {
        super(TransferType.MATERNITY_MEDICAL_HISTORY,
                "//*[local-name()='maternityMedicalRecord']",
                "*[local-name()='maternityMedicalRecordHeader']/*[local-name()='sourceSystemHSAId']",
                1);
    }

    @Override
    protected FetchJobUriExtractor getFetchJobUriExtractor() {
        return maternityMedicalHistoryUriExtractor;
    }
}
