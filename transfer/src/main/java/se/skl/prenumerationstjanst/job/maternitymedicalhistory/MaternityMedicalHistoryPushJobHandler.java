package se.skl.prenumerationstjanst.job.maternitymedicalhistory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import se.skl.prenumerationstjanst.job.PhkCHBasePushJobHandler;
import se.skl.prenumerationstjanst.job.PushJobHandler;
import se.skl.prenumerationstjanst.model.TransferType;

/**
 * @author Martin Samuelsson
 */
@Component
    public class MaternityMedicalHistoryPushJobHandler extends PhkCHBasePushJobHandler implements PushJobHandler {

    private static final Logger logger = LoggerFactory.getLogger(MaternityMedicalHistoryPushJobHandler.class);

    public MaternityMedicalHistoryPushJobHandler() throws Exception {
        super(TransferType.MATERNITY_MEDICAL_HISTORY);
    }

}
