package se.skl.prenumerationstjanst.job.vaccinationhistory;

import org.springframework.stereotype.Component;
import se.skl.prenumerationstjanst.job.PhkCHBasePushJobHandler;
import se.skl.prenumerationstjanst.job.PushJobHandler;
import se.skl.prenumerationstjanst.model.TransferType;

/**
 * @author Martin Samuelsson
 */
@Component
public class VaccinationHistoryPushJobHandler extends PhkCHBasePushJobHandler implements PushJobHandler {

    public VaccinationHistoryPushJobHandler() throws Exception {
        super(TransferType.VACCINATION_HISTORY);
    }

}
