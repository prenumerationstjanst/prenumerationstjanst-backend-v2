package se.skl.prenumerationstjanst.job.vaccinationhistory.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import se.skl.prenumerationstjanst.job.ApigwFetchJobHandler;
import se.skl.prenumerationstjanst.job.FetchJobUriExtractor;
import se.skl.prenumerationstjanst.model.TransferType;

import javax.xml.xpath.XPathExpressionException;

/**
 * @author Martin Samuelsson
 */
@Component
public class VaccinationHistoryV1FetchJobHandler extends ApigwFetchJobHandler {

    @Autowired
    private VaccinationHistoryV1FetchJobUriExtractor vaccinationHistoryV1FetchJobUriExtractor;

    public VaccinationHistoryV1FetchJobHandler() throws XPathExpressionException {
        super(TransferType.VACCINATION_HISTORY,
                "//*[local-name()='vaccinationRecord']",
                "*[local-name()='vaccinationMedicalRecordHeader']/*[local-name()='sourceSystemHSAId']",
                1);
    }

    @Override
    protected FetchJobUriExtractor getFetchJobUriExtractor() {
        return vaccinationHistoryV1FetchJobUriExtractor;
    }
}
