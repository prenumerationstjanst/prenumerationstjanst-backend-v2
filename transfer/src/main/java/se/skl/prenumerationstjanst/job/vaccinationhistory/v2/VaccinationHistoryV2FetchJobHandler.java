package se.skl.prenumerationstjanst.job.vaccinationhistory.v2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import se.skl.prenumerationstjanst.job.ApigwFetchJobHandler;
import se.skl.prenumerationstjanst.job.FetchJobUriExtractor;
import se.skl.prenumerationstjanst.model.TransferType;

import javax.xml.xpath.XPathExpressionException;

/**
 * @author Martin Samuelsson
 */
@Component
public class VaccinationHistoryV2FetchJobHandler extends ApigwFetchJobHandler {

    @Autowired
    private VaccinationHistoryV2FetchJobUriExtractor vaccinationHistoryV2FetchJobUriExtractor;

    public VaccinationHistoryV2FetchJobHandler() throws XPathExpressionException {
        super(TransferType.VACCINATION_HISTORY,
                "//*[local-name()='vaccinationRecord']",
                "*[local-name()='vaccinationMedicalRecordHeader']/*[local-name()='sourceSystemHSAId']",
                2);
    }

    @Override
    protected FetchJobUriExtractor getFetchJobUriExtractor() {
        return vaccinationHistoryV2FetchJobUriExtractor;
    }
}
