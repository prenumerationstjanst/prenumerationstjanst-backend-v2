package se.skl.prenumerationstjanst.job.vaccinationhistory.v2;

import com.chbase.thing.oxm.jaxb.base.CodableValue;
import com.chbase.thing.oxm.jaxb.base.CodedValue;
import com.chbase.thing.oxm.jaxb.base.Name;
import com.chbase.thing.oxm.jaxb.base.Person;
import com.chbase.thing.oxm.jaxb.comment.Comment;
import com.chbase.thing.oxm.jaxb.dates.ApproxDateTime;
import com.chbase.thing.oxm.jaxb.dates.StructuredApproxDate;
import com.chbase.thing.oxm.jaxb.immunization.Immunization;
import org.apigw.vh.v2.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import se.skl.prenumerationstjanst.chbase.CHBaseThing2;
import se.skl.prenumerationstjanst.dto.TransformationJobDTO;
import se.skl.prenumerationstjanst.exception.transfer.TransformationFailedException;
import se.skl.prenumerationstjanst.job.ApigwTransformationJobHandler;
import se.skl.prenumerationstjanst.job.TransformationResult;
import se.skl.prenumerationstjanst.model.TransferType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * TransformationJobHandler that will transform a RIV (APIGW) vaccinationRecord (v2) into a number of
 * PushJobSpecifications
 * where value is a CHBase "thing" of type Immunization and the related values will be 0..5 CHBase "things" of
 * type Comment
 *
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
@Component
public class VaccinationHistoryV2TransformationJobHandler extends
        ApigwTransformationJobHandler<VaccinationMedicalRecordType> {

    private static final Logger logger = LoggerFactory.getLogger(VaccinationHistoryV2TransformationJobHandler.class);

    private static final String RIV_DATE_FORMAT = "yyyyMMdd";
    private JAXBContext vaccinationJaxbContext;

    public VaccinationHistoryV2TransformationJobHandler() throws JAXBException {
        super(TransferType.VACCINATION_HISTORY, 2, DateTimeFormatter.ofPattern(RIV_DATE_FORMAT));
        vaccinationJaxbContext = JAXBContext.newInstance(VaccinationMedicalRecordType.class);
    }

    @Override
    protected TransformationResult transform(VaccinationMedicalRecordType doc, LocalDateTime sourceFetchTime,
                                             TransformationJobDTO job) throws TransformationFailedException {
        PatientSummaryHeaderType header = doc.getVaccinationMedicalRecordHeader();
        String uniqueDocumentId = header.getSourceSystemHSAId() + "/" + header.getDocumentId();
        VaccinationMedicalRecordBodyType body = doc.getVaccinationMedicalRecordBody();

        List<TransformationResult.PushJobSpecification> pushJobSpecifications = null;
        if (header.isNullified() == null || !header.isNullified()) {
            RegistrationRecordType regRec = body.getRegistrationRecord();
            List<AdministrationRecordType> filteredRECs = body.getAdministrationRecord().stream()
                    .filter(admRec -> admRec.getVaccineName() != null
                            || admRec.getTypeOfVaccine() != null
                            || (admRec.getVaccineTargetDisease() != null
                            && !admRec.getVaccineTargetDisease().isEmpty()))
                    .collect(Collectors.toList());

            pushJobSpecifications = filteredRECs.stream()
                    .map(admRec -> {
                        CHBaseThing2 baseThing = new CHBaseThing2();
                        Immunization im = new Immunization();
                        //name
                        CodableValue nameCV = new CodableValue();
                        StringJoiner nameJoiner = new StringJoiner(", ");
                        if (admRec.getVaccineName() != null) {
                            nameJoiner.add(extractText(admRec.getVaccineName()));
                        }
                        admRec.getVaccineTargetDisease().forEach(cvType -> nameJoiner.add(extractText(cvType)));
                        nameJoiner.add(extractText(admRec.getTypeOfVaccine()));
                        nameCV.setText(nameJoiner.toString());
                        im.setName(nameCV);

                        //administration date
                        StructuredApproxDate structured = getStructuredApproxDate(regRec.getDate());
                        im.setAdministrationDate(new ApproxDateTime(structured));
                        //administrator
                        Person administrator = null;
                        if (admRec.getPerformer() != null) {
                            administrator = new Person();
                            Name name = new Name();
                            name.setFull(admRec.getPerformer().getPersonName());
                            administrator.setName(name);
                            administrator.setId(admRec.getPerformer().getHsaid());
                            if (admRec.getPerformerOrg() != null) {
                                administrator.setOrganization(admRec.getPerformerOrg().getOrgUnitName());
                            }
                        } else if (admRec.getPrescriberPerson() != null) {
                            administrator = new Person();
                            Name name = new Name();
                            name.setFull(admRec.getPrescriberPerson().getPersonName());
                            administrator.setName(name);
                            administrator.setId(admRec.getPrescriberPerson().getHsaid());
                            if (admRec.getPrescriberOrg() != null) {
                                administrator.setOrganization(admRec.getPrescriberOrg().getOrgUnitName());
                            }
                        }
                        if (administrator != null && administrator.getId() != null
                                && (administrator.getName().getFull() == null
                                || administrator.getName().getFull().isEmpty())) {
                            administrator.getName().setFull("Uppgift saknas");
                        }
                        if (administrator != null && (administrator.getOrganization() == null
                                || administrator.getOrganization().isEmpty())) {
                            administrator.setOrganization("Uppgift saknas");
                        }
                        im.setAdministrator(administrator);
                        //manufacturer
                        if (admRec.getVaccineManufacturer() != null) {
                            im.setManufacturer(new CodableValue(admRec.getVaccineManufacturer()));
                        }
                        //lot
                        im.setLot(admRec.getVaccineBatchId());
                        //route
                        if (admRec.getRoute() != null) {
                            im.setRoute(getCodableValue(admRec.getRoute()));
                        }
                        //sequence
                        String seq = null;
                        if (admRec.isIsDoseComplete() != null && admRec.isIsDoseComplete()) {
                            seq = "Full dos uppnådd";
                        } else if (admRec.getDoseOrdinalNumber() != null && admRec.getNumberOfPrescribedDoses() !=
                                null) {
                            seq = String.format("Dos %s av %s",
                                    admRec.getDoseOrdinalNumber().toString(),
                                    admRec.getNumberOfPrescribedDoses().toString());
                        }
                        im.setSequence(seq);
                        //anatomic-surface
                        if (admRec.getAnatomicalSite() != null) {
                            im.setAnatomicSurface(getCodableValue(admRec.getAnatomicalSite()));
                        }
                        //adverse-event
                        Stream<String> admRecPAEStream = admRec.getPatientAdverseEffect().stream()
                                .map(this::extractText);
                        Stream<String> stringStream1regRecPAEStream = regRec.getPatientAdverseEffect().stream()
                                .map(cvType -> String.format("%s (kan bero på annat vaccin vid samma tillfälle)",
                                        extractText(cvType)));
                        String adverseEvent = Stream.concat(admRecPAEStream, stringStream1regRecPAEStream)
                                .collect(Collectors.joining(", "));
                        im.setAdverseEvent(!adverseEvent.isEmpty() ? adverseEvent : null);

                        try {
                            baseThing.setData(im);
                        } catch (Exception e) {
                            throw new TransformationFailedException("Could not add Immunization to Thing2", e, job);
                        }

                        List<Comment> comments = new ArrayList<>();

                        //Vaccinationsprogram
                        if (admRec.getVaccinationProgramName() != null) {
                            Comment comment = createCommentBase(regRec.getDate());
                            //content
                            comment.setContent(String.format("Vaccinationsprogram: %s", extractText(admRec
                                    .getVaccinationProgramName())));
                            //category
                            comment.setCategory(createCommentCategory("Vaccinationsprogram",
                                    "GVH_VACCINATIONPROGRAMNAME"));
                            comments.add(comment);
                        }

                        //Dos
                        if (admRec.getDose() != null
                                && admRec.getDose().getDisplayName() != null
                                && !admRec.getDose().getDisplayName().isEmpty()) {
                            Comment comment = createCommentBase(regRec.getDate());
                            //content
                            comment.setContent(String.format("Dos: %s", admRec.getDose().getDisplayName()));
                            //category
                            comment.setCategory(createCommentCategory("Dos", "GVH_DOSEDISPLAYNAME"));
                            comments.add(comment);
                        }

                        //Uppgift Hamtad Fran
                        if (admRec.getSourceDescription() != null && !admRec.getSourceDescription().isEmpty()) {
                            Comment comment = createCommentBase(regRec.getDate());
                            //content
                            comment.setContent(String.format("Uppgift hämtad från: %s", admRec
                                    .getSourceDescription()));
                            //category
                            comment.setCategory(createCommentCategory("Uppgift hämtad från",
                                    "GVH_SOURCEDESCRIPTION"));
                            comments.add(comment);
                        }

                        //Ordination
                        if (admRec.getCommentPrescription() != null && !admRec.getCommentPrescription()
                                .isEmpty()) {
                            Comment comment = createCommentBase(regRec.getDate());
                            //content
                            comment.setContent(String.format("Kommentar på förskrivning: %s", admRec
                                    .getCommentPrescription()));
                            //category
                            comment.setCategory(createCommentCategory("Kommentar ordination",
                                    "GVH_COMMENTPRESCRIPTION"));
                            comments.add(comment);
                        }

                        //Administrering
                        if (admRec.getCommentAdministration() != null && !admRec.getCommentAdministration()
                                .isEmpty()) {
                            Comment comment = createCommentBase(regRec.getDate());
                            //content
                            comment.setContent(String.format("Kommentar på administrering: %s", admRec
                                    .getCommentAdministration()));
                            //category
                            comment.setCategory(createCommentCategory("Kommentar administrering",
                                    "GVH_COMMENTADMINISTRATION"));
                            comments.add(comment);
                        }

                        Comment rivSourceComment = createRivSourceComment(
                                "riv.clinicalprocess.activityprescription.actoutcome.getvaccinationhistory_2.0");
                        comments.add(rivSourceComment);

                        List<String> relatedThings = comments.stream()
                                .map(comment -> {
                                    CHBaseThing2 relatedThing = new CHBaseThing2();
                                    try {
                                        relatedThing.setData(comment);
                                        return marshalCHBaseThing2(relatedThing);
                                    } catch (Exception e) {
                                        logger.error("Could not create related Comment Thing2, will continue since this" +
                                                " is a related item", e);
                                    }
                                    return null;
                                })
                                .filter(s -> s != null)
                                .collect(Collectors.toList());
                        try {
                            return new TransformationResult.PushJobSpecification(marshalCHBaseThing2(baseThing),
                                    relatedThings, sourceFetchTime, uniqueDocumentId, version());
                        } catch (JAXBException e) {
                            throw new TransformationFailedException("Could not marshal Immunization Thing2", e, job);
                        }
                    })
                    .filter(sp -> sp != null)
                    .collect(Collectors.toList());
        }
        return new TransformationResult(pushJobSpecifications);
    }

    @Override
    protected VaccinationMedicalRecordType unmarshal(String document) throws JAXBException {
        Unmarshaller unmarshaller = vaccinationJaxbContext.createUnmarshaller();
        JAXBElement<VaccinationMedicalRecordType> vaccMrElem = unmarshaller.unmarshal(new StreamSource(new
                StringReader(document)), VaccinationMedicalRecordType.class);
        return vaccMrElem.getValue();
    }

    /**
     *
     * @param cvType
     * @return displayName if not null, otherwise originalText (might also be null if not following spec)
     */
    private String extractText(CVType cvType) {
        return cvType.getDisplayName() != null ? cvType.getDisplayName() : cvType.getOriginalText();
    }

    private CodableValue getCodableValue(CVType cvType) {
        CodableValue codableValue = new CodableValue();
        //TKB: Om originalText anges kan ingen av de övriga elementen anges.
        if (cvType.getOriginalText() != null && !cvType.getOriginalText().isEmpty()) {
            codableValue.setText(cvType.getOriginalText());
            return codableValue;
        } else if (cvType.getCode() != null) { //TKB: Om code anges skall också codeSystem samt displayName anges.
            codableValue.setText(cvType.getDisplayName());
            if (cvType.getCodeSystemName() != null) {
                CodedValue codedValue = new CodedValue();
                codedValue.setValue(cvType.getCode());
                codedValue.setType(cvType.getCodeSystemName());
                codedValue.setVersion(cvType.getCodeSystemVersion());
                codableValue.getCode().add(codedValue);
            }
            return codableValue;
        }
        return null;
    }
}
