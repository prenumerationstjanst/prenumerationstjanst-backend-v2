package se.skl.prenumerationstjanst.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se.skl.prenumerationstjanst.dto.PushJobDTO;
import se.skl.prenumerationstjanst.model.PushLog;
import se.skl.prenumerationstjanst.model.Subscription;
import se.skl.prenumerationstjanst.repository.PushLogRepository;
import se.skl.prenumerationstjanst.repository.SubscriptionRepository;

import java.time.LocalDateTime;

/**
 * @author Martin Samuelsson
 */
@Service
@Transactional
public class PushLogService {

    private static final Logger logger = LoggerFactory.getLogger(PushLogService.class);

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private PushLogRepository pushLogRepository;

    @Autowired
    private HashService hashService;

    /**
     * Stores information about a success push so that it can later be used to determine
     * if a push of the same data should not be performed
     *
     * @param pushJobDTO
     */
    public void logSuccessfulPush(PushJobDTO pushJobDTO) {
        String subscriptionId = pushJobDTO.getTransferJob().getSubscriptionId();
        String pushJobId = pushJobDTO.getId();
        LocalDateTime sourceFetchTime = pushJobDTO.getTransferJob().getFetchJob().getFetchFinishTime();
        String sourceId = pushJobDTO.getSourceId();
        int sourceVersion = pushJobDTO.getSourceVersion();
        String hash = hashService.calculateHash(pushJobDTO.getValue());

        logger.debug("logSuccessfulPush: subscriptionId[{}], pushJobId[{}], sourceFetchTime[{}], sourceId[{}]",
                subscriptionId, pushJobId, sourceFetchTime, sourceId);

        Subscription subscription = subscriptionRepository.findOne(subscriptionId);
        PushLog pushLog = new PushLog(subscription, sourceId, sourceFetchTime, sourceVersion, hash);
        subscriptionRepository.save(subscription);
        pushLogRepository.save(pushLog);
    }

    /**
     * Determines if a push should be performed based on a PushJobDTO.
     *
     * Parameters checked are:
     * - subscriptionId
     * - sourceFetchTime (when original data was downloaded)
     * - sourceId (unique id source data)
     * - hash value of data to be pushed
     *
     * A push should be performed if:
     *
     * The unique combination of subscriptionId + sourceId does not exist
     * OR
     * The unique combination of subscriptionId + sourceId exists
     *      AND sourceFetchTime is newer than OR equal to the previous
     *      AND hash is different
     *
     * @param pushJobDTO
     * @return true if push should be performed, false if not
     */
    @Transactional(readOnly = true)
    public boolean canPush(PushJobDTO pushJobDTO) {
        String subscriptionId = pushJobDTO.getTransferJob().getSubscriptionId();
        LocalDateTime sourceFetchTime = pushJobDTO.getTransferJob().getFetchJob().getFetchFinishTime();
        String sourceId = pushJobDTO.getSourceId();
        int sourceVersion = pushJobDTO.getSourceVersion();
        String hash = hashService.calculateHash(pushJobDTO.getValue());
        boolean canPush = !pushLogRepository.sameOrNewerPushed(subscriptionId, sourceFetchTime,
                sourceId, sourceVersion, hash);
        logger.debug("canPush: subscriptionId[{}], sourceFetchTime[{}], sourceId[{}], sourceVersion[{}], hash[{}] returning {}",
                subscriptionId, sourceFetchTime, sourceId, sourceVersion, hash, canPush);
        return canPush;
    }

}
