package se.skl.prenumerationstjanst.worker;

import com.google.common.util.concurrent.RateLimiter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import se.skl.prenumerationstjanst.dto.FetchJobDTO;
import se.skl.prenumerationstjanst.dto.TransformationJobDTO;
import se.skl.prenumerationstjanst.exception.transfer.FetchFailedException;
import se.skl.prenumerationstjanst.job.FetchJobHandler;
import se.skl.prenumerationstjanst.job.FetchResult;
import se.skl.prenumerationstjanst.job.FetchResultDocument;
import se.skl.prenumerationstjanst.job.JobMDCMapper;
import se.skl.prenumerationstjanst.model.TransferType;
import se.skl.prenumerationstjanst.service.JobService;
import se.skl.prenumerationstjanst.service.SubscriptionService;
import se.skl.prenumerationstjanst.service.TransferService;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static se.skl.prenumerationstjanst.PrenumerationstjanstConstants.*;
import static se.skl.prenumerationstjanst.model.JobStatus.FAILED_ALL_RETRIES;
import static se.skl.prenumerationstjanst.model.JobStatus.RUNNING;

/**
 * Message driven POJO that acts on incoming FetchJobDTOs
 * TODO: moar
 *
 * @author Martin Samuelsson
 */
@Component
public class FetchJobMDP {

    private static final Logger logger = LoggerFactory.getLogger(FetchJobMDP.class);

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private JobService jobService;

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private TransferService transferService;

    @Resource
    Map<TransferType, List<FetchJobHandler>> fetchJobHandlers;

    @Autowired
    private JobMDCMapper jobMDCMapper;

    @Autowired
    private RateLimiter fetchJobRateLimiter;

    @Transactional
    @JmsListener(destination = FETCH_AGGREGATED_QUEUE, concurrency = "1-10")
    public void fetchAggregated(FetchJobDTO fetchJobDTO) {
        List<FetchResult> fetchResults = fetch(fetchJobDTO);
        if (fetchResults == null) {
            return;
        }

        Set<String> sourceSystemsToRestart = determineFailedSourceSystems(fetchResults);

        List<FetchResultDocument> fetchResultDocuments = fetchResults.stream()
                .flatMap(fetchResult -> fetchResult.getDocuments().stream())
                .filter(fetchResultDocument -> !sourceSystemsToRestart.contains(fetchResultDocument.getSourceSystemId()))
                .collect(Collectors.toList());

        //start transfers for failed sourceSystems
        try {
            sourceSystemsToRestart
                    .forEach(failedSourceSystem -> {
                        transferService.startTransferForFailedSourceSystem(fetchJobDTO.getTransferJob(),
                                failedSourceSystem);
                    });
        } catch (RuntimeException e) {
            if (e instanceof FetchFailedException) {
                throw e;
            } else {
                throw new FetchFailedException(
                        "Caught exception while trying to start transfers for failed source system", e, fetchJobDTO);
            }
        }
        postFetch(fetchJobDTO, fetchResultDocuments);
    }

    @Transactional
    @JmsListener(destination = FETCH_SINGLE_SOURCE_SYSTEM_QUEUE, concurrency = "1-10")
    public void fetchSingleSourceSystem(FetchJobDTO fetchJobDTO) {
        List<FetchResult> fetchResults = fetch(fetchJobDTO);
        if (fetchResults == null) {
            return;
        }
        fetchResults.stream()
                .max(Comparator.comparingInt(FetchResult::getDocumentVersion))
                .filter(fetchResult -> !fetchResult.getFailedSourceSystems().isEmpty())
                .ifPresent(highestVersionFetchResult -> {
                    //highest version fetch result has failed source systems
                    //since we are processing a fetch against a single source system we want to
                    // throw an exception to cause redelivery
                    throw new FetchFailedException("unable to fetch data from " +
                            fetchJobDTO.getTransferJob().getSourceSystemHsaId(), fetchJobDTO);
                });

        List<FetchResultDocument> fetchResultDocuments = fetchResults.stream()
                .flatMap(fetchResult -> fetchResult.getDocuments().stream())
                .collect(Collectors.toList());

        postFetch(fetchJobDTO, fetchResultDocuments);
    }

    private List<FetchResult> fetch(FetchJobDTO fetchJobDTO) {
        logger.info("FETCH {}", fetchJobDTO);
        fetchJobRateLimiter.acquire();
        jobMDCMapper.addToContext(fetchJobDTO);
        String subscriptionId = fetchJobDTO.getTransferJob().getSubscriptionId();
        if (!subscriptionService.isSubscriptionActive(subscriptionId)) {
            logger.warn("subscriptionId[{}] no longer active, will not fetch", subscriptionId);
            return null;
        }
        jobService.updateFetchJobStatus(fetchJobDTO.getId(), RUNNING);
        TransferType transferType = TransferType.valueOf(fetchJobDTO.getTransferJob().getTransferType());
        try {
            return fetchJobHandlers.get(transferType).stream()
                    .map(fetchJobHandler -> fetchJobHandler.processFetchJob(fetchJobDTO))
                    .collect(Collectors.toList());
        } catch (RuntimeException e) {
            if (e instanceof FetchFailedException) {
                throw e;
            }
            throw new FetchFailedException(fetchJobDTO);
        }
    }

    private void postFetch(FetchJobDTO fetchJobDTO, List<FetchResultDocument> fetchResultDocuments) {
        List<FetchResultDocument> documents = removeVersionDuplicatesPerSourceSystem(fetchResultDocuments);
        LocalDateTime fetchFinishTime = LocalDateTime.now();
//        documents.forEach(document -> {
//            sendTransformationJobDTO(new TransformationJobDTO("",
//                    fetchJobDTO.getTransferJob().withFetchJob(FetchJobDTO.create().withId(fetchJobDTO.getId())),
//                    document.getValue(), fetchFinishTime, document.getVersion()));
//        });
        documents.forEach(document -> {
            sendTransformationJobDTO(jobService.createTransformationJob(fetchJobDTO.getTransferJob().getId(),
                    document.getValue(), fetchFinishTime, document.getVersion()));
        });
        jobService.finishFetchJob(fetchJobDTO.getId(), fetchFinishTime);
        jobMDCMapper.clearContext();
    }

    /**
     * Based on a List of FetchResultDocument:s, this method will
     * 1) Group the FetchResultDocument:s by sourceSystemId
     * 2) For each sourceSystem group,
     * 3) determine the highest document version
     * 4) Filter based on the highest document version leaving only documents of the highest version
     * 5) Return a list containing the FetchResultDocument:s of the highest version for each represented source system
     *
     */
    private List<FetchResultDocument> removeVersionDuplicatesPerSourceSystem(List<FetchResultDocument> input) {
        List<FetchResultDocument> result = new ArrayList<>();

        //1. Group FetchResultDocuments by source system
        Map<String, List<FetchResultDocument>> groupedBySourceSystem = input.stream()
                .collect(Collectors.groupingBy(FetchResultDocument::getSourceSystemId));

        //2. Iterate over every source system
        groupedBySourceSystem.keySet().forEach(sourceSystem -> {

            //3. Determine the highest document version for the current source system
            int highestVersionForSourceSystem = groupedBySourceSystem.get(sourceSystem).stream()
                    .mapToInt(FetchResultDocument::getVersion)
                    .max()
                    .orElse(-1);

            //4. Filter based on the highest document version for the current source system
            List<FetchResultDocument> documentsWithHighestVersionForSourceSystem =
                    groupedBySourceSystem.get(sourceSystem).stream()
                            .filter(fetchResultDocument ->
                                    highestVersionForSourceSystem == fetchResultDocument.getVersion())
                            .collect(Collectors.toList());

            //5. Add all documents of the highest version for the current source system to the result list
            result.addAll(documentsWithHighestVersionForSourceSystem);
        });

        return result;
    }

    /**
     * Will calculate a Set<String> of source system id:s that are considered to be failed.
     * Every failed source system id that is not represented in a higher version FetchResultDocument
     * or is among the failed source system id:s of a higher version FetchResult should be considered failed.
     *
     * @param fetchResults
     * @return
     */
    private Set<String> determineFailedSourceSystems(List<FetchResult> fetchResults) {
        return  fetchResults.stream()
                //turn each FetchResult into a Set<String> containing a every failed source system
                //that can not be be found in a higher version FetchResult (either among docs or failed source systems)
                .map(fetchResult -> {
                    int currentVersion = fetchResult.getDocumentVersion();
                    List<FetchResult> higherVersionFetchResults = fetchResults.stream()
                            .filter(fetchResultB -> fetchResultB.getDocumentVersion() > currentVersion)
                            .collect(Collectors.toList());
                    return fetchResult.getFailedSourceSystems().stream()
                            .filter(failedSourceSystem -> {
                                boolean isAmongSourceSystemsInHigherVersion = higherVersionFetchResults.stream()
                                        .filter(higherVersionFetchResult -> {
                                            boolean isAmongFailedInHigherVersion = higherVersionFetchResult
                                                    .getFailedSourceSystems()
                                                    .contains(failedSourceSystem);
                                            boolean isAmongDocsInHigherVersion = higherVersionFetchResult.getDocuments()
                                                    .stream()
                                                    .filter(higherVersionFetchResultDocument ->
                                                            higherVersionFetchResultDocument.getSourceSystemId()
                                                                    .equalsIgnoreCase(failedSourceSystem))
                                                    .findAny()
                                                    .isPresent();
                                            return isAmongFailedInHigherVersion || isAmongDocsInHigherVersion;
                                        })
                                        .findAny()
                                        .isPresent();
                                        //we want to keep any failed source system NOT represented in a higher version result
                                        return !isAmongSourceSystemsInHigherVersion;
                                    }
                            )
                            .collect(Collectors.toSet());
                })
                //flatten the Stream<Set<String>> into a Stream<String>
                .flatMap(Collection::stream)
                //collect into Set<String> (this is our result)
                .collect(Collectors.toSet());
    }

    @Transactional
    @JmsListener(destination = FETCH_AGGREGATED_QUEUE_DLQ)
    public void unrecoverableFetchAggregatedError(FetchJobDTO fetchJobDTO) {
        FetchJobDTO failedFetchJob = jobService.updateFetchJobStatus(fetchJobDTO.getId(), FAILED_ALL_RETRIES);
        logger.error("aggregated fetchJob failed all retries: {}", failedFetchJob);
    }

    @Transactional
    @JmsListener(destination = FETCH_SINGLE_SOURCE_SYSTEM_QUEUE_DLQ)
    public void unrecoverableFetchSingleSourceSystemError(FetchJobDTO fetchJobDTO) {
        FetchJobDTO failedFetchJob = jobService.updateFetchJobStatus(fetchJobDTO.getId(), FAILED_ALL_RETRIES);
        logger.error("single source system fetchJob failed all retries: {}", failedFetchJob);
    }

    private void sendTransformationJobDTO(TransformationJobDTO transformationJobDTO) {
        jmsTemplate.convertAndSend(TRANSFORMATION_QUEUE, transformationJobDTO);
    }

}
