package se.skl.prenumerationstjanst.worker;

import com.google.common.util.concurrent.RateLimiter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import se.skl.prenumerationstjanst.dto.PushJobDTO;
import se.skl.prenumerationstjanst.dto.TransferJobDTO;
import se.skl.prenumerationstjanst.exception.transfer.PushUnauthorizedException;
import se.skl.prenumerationstjanst.exception.transfer.UnrecoverablePushFailedException;
import se.skl.prenumerationstjanst.job.JobMDCMapper;
import se.skl.prenumerationstjanst.job.PushJobHandler;
import se.skl.prenumerationstjanst.job.PushResult;
import se.skl.prenumerationstjanst.model.TransferType;
import se.skl.prenumerationstjanst.service.JobService;
import se.skl.prenumerationstjanst.service.PushLogService;
import se.skl.prenumerationstjanst.service.SubscriptionService;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Map;

import static se.skl.prenumerationstjanst.PrenumerationstjanstConstants.PUSH_QUEUE;
import static se.skl.prenumerationstjanst.PrenumerationstjanstConstants.PUSH_QUEUE_DLQ;
import static se.skl.prenumerationstjanst.model.JobStatus.*;

/**
 * Message driven POJO that acts on incoming PushJobDTOs
 * TODO: moar
 *
 * @author Martin Samuelsson
 */
@Component
public class PushJobMDP {

    private static final Logger logger = LoggerFactory.getLogger(PushJobMDP.class);

    @Autowired
    private JobService jobService;

    @Autowired
    private PushLogService pushLogService;

    @Autowired
    private SubscriptionService subscriptionService;

    @Resource
    private Map<TransferType, PushJobHandler> pushJobHandlers;

    @Autowired
    private JmsTemplate highPriorityJmsTemplate;

    @Autowired
    private JobMDCMapper jobMDCMapper;

    @Autowired
    private RateLimiter pushJobRateLimiter;

    @Transactional
    @JmsListener(destination = PUSH_QUEUE, concurrency = "1-5", selector = "JMSPriority >= 6") //Only high priority messages
    @JmsListener(destination = PUSH_QUEUE, concurrency = "1-5") //All messages
    public void push(PushJobDTO pushJobDTO) {
        jobMDCMapper.addToContext(pushJobDTO);
        pushJobRateLimiter.acquire();
        String subscriptionId = pushJobDTO.getTransferJob().getSubscriptionId();
        if (!subscriptionService.isSubscriptionActive(subscriptionId)) {
            logger.warn("subscription no longer active, will not start");
            return;
        }
        jobService.updatePushJobStatus(pushJobDTO.getId(), RUNNING);
        if (pushLogService.canPush(pushJobDTO)) {
            logger.info("starting {} pushJob", pushJobDTO.getTransferJob().getTransferType());
            TransferJobDTO transferJob = pushJobDTO.getTransferJob();
            TransferType transferType = TransferType.valueOf(transferJob.getTransferType());
            PushResult pushResult = null;
            try {
                pushResult = pushJobHandlers.get(transferType).processPushJob(pushJobDTO);
            } catch (PushUnauthorizedException e) {
                String performingCrn = pushJobDTO.getTransferJob().getUserCrn();
                String representingCrn = pushJobDTO.getTransferJob().getRepresentingCrn();
                TransferType approvedTransferType = TransferType.valueOf(pushJobDTO.getTransferJob().getTransferType());
                logger.warn("push unauthorized, need to DELETE subscription {}:{} {}",
                        performingCrn,
                        representingCrn,
                        approvedTransferType);
                jobService.updatePushJobStatus(pushJobDTO.getId(), FAILED_UNAUTHORIZED);
                subscriptionService.removeSubscription(performingCrn, representingCrn, transferType);
                return;
            } catch (UnrecoverablePushFailedException e) {
                logger.warn("push failed and can not be retried due to unrecoverable error", e);
                jobService.updatePushJobStatus(pushJobDTO.getId(), FAILED);
                return;
            }
            jobService.finishPushJob(pushJobDTO.getId(), pushJobDTO.getSourceId(), pushJobDTO.getSourceVersion(),
                    LocalDateTime.now(), FINISHED, pushResult.getTypeOfData(), pushJobDTO.getRelatedTo());
            pushLogService.logSuccessfulPush(pushJobDTO);
            pushResult.getRelatedItems().forEach(relatedValue -> {
                sendRelatedPushJobDTO(jobService.createPushJob(transferJob.getId())
                        .withValue(relatedValue)
                        .withSourceId(pushJobDTO.getSourceId())
                        .withSourceVersion(pushJobDTO.getSourceVersion())
                        .withRelatedTo(pushJobDTO.getId()));
            });
        } else {
            logger.info("not starting {} pushJob (duplicate)", pushJobDTO.getTransferJob().getTransferType());
            jobService.updatePushJobStatus(pushJobDTO.getId(), DUPLICATE);
        }
        jobMDCMapper.clearContext();
    }

    @Transactional
    @JmsListener(destination = PUSH_QUEUE_DLQ)
    public void unrecoverablePushError(PushJobDTO pushJobDTO) {
        jobService.updatePushJobStatus(pushJobDTO.getId(), FAILED_ALL_RETRIES);
    }

    private void sendRelatedPushJobDTO(PushJobDTO pushJobDTO) {
        highPriorityJmsTemplate.convertAndSend(PUSH_QUEUE, pushJobDTO);
    }
}
