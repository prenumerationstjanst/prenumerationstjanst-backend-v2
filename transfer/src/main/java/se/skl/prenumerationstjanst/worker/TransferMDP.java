package se.skl.prenumerationstjanst.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import se.skl.prenumerationstjanst.dto.FetchJobDTO;
import se.skl.prenumerationstjanst.dto.TransferJobDTO;
import se.skl.prenumerationstjanst.dto.internal.InitialTransferParams;
import se.skl.prenumerationstjanst.dto.internal.SingleSourceSystemTransferParams;
import se.skl.prenumerationstjanst.job.JobMDCMapper;
import se.skl.prenumerationstjanst.model.Subscription;
import se.skl.prenumerationstjanst.model.TransferJob;
import se.skl.prenumerationstjanst.model.TransferType;
import se.skl.prenumerationstjanst.repository.SubscriptionRepository;
import se.skl.prenumerationstjanst.service.JobService;
import se.skl.prenumerationstjanst.service.PreCheckService;
import se.skl.prenumerationstjanst.service.SubscriptionService;

import java.time.LocalDateTime;
import java.time.Year;

import static se.skl.prenumerationstjanst.PrenumerationstjanstConstants.*;

/**
 * 'Message driven POJO' responsible for initiating
 *  the following transfer scenarios:
 *
 * 1. Initial transfers for newly created subscriptions
 * 2. Transfers that target a single source system
 *
 * @author Martin Samuelsson
 */
@Component
public class TransferMDP {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Value("${pt.initial.transferjob.year}")
    private int initialStartYear;

    @Autowired
    private JobService jobService;

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private PreCheckService preCheckService;

    @Autowired
    private JobMDCMapper jobMDCMapper;

    private static final Logger logger = LoggerFactory.getLogger(TransferMDP.class);

    @Transactional
    @JmsListener(destination = INITIAL_TRANSFERS_FOR_SUBSCRIPTION_QUEUE, concurrency = "1")
    public void doStartInitialTransfersForSubscription(InitialTransferParams params) {
        logger.info("creating initial transfers for subscription {}", params.getSubscriptionId());
        Subscription subscription = subscriptionRepository.findOne(params.getSubscriptionId());
        if (subscription != null && !subscription.isScheduledForRemoval()) { //TODO: check that incoming transfer types are still valid
            for (int year = initialStartYear; year <= Year.now().getValue(); year += 1) {
                logger.info("creating transferJob for {}:{}", params.getTransferType(), year);
                TransferJobDTO transferJobDTO = jobService.createTransferJob(
                        params.getSubscriptionId(), TransferType.valueOf(params.getTransferType()), getStartOfYear(year), getEndOfYear(year)
                        , null, TransferJob.Origin.INITIAL);
                startTransferJob(transferJobDTO);
            }
        } else {
            logger.warn("no (active) subscription found with id {}", params.getSubscriptionId());
        }
    }

    @Transactional
    @JmsListener(destination = START_TRANSFER_FOR_SINGLE_SOURCE_SYSTEM_QUEUE, concurrency = "1-10")
    public void doStartSingleSourceSystemTransfer(SingleSourceSystemTransferParams params) {
        if (!subscriptionService.isSubscriptionActive(params.getSubscriptionId())) {
            logger.warn("subscriptionId[{}] no longer active, will not start transfers", params.getSubscriptionId());
            return;
        }
        TransferJobDTO transferJobDTO = jobService.createTransferJob(
                params.getSubscriptionId(),
                params.getTransferType(),
                params.getStartDateTime(),
                params.getEndDateTime(),
                params.getSourceSystemHsaId(),
                params.getOrigin());
        startTransferJob(transferJobDTO);
    }

    private void startTransferJob(TransferJobDTO transferJobDTO) {
        jobMDCMapper.addToContext(transferJobDTO);
        logger.info("starting transferJob {}", transferJobDTO);
        if (preCheckService.preCheck(transferJobDTO.getUserCrn(), transferJobDTO.getRepresentingCrn())) {
            FetchJobDTO fetchJobDTO = jobService.createFetchJob(transferJobDTO.getId());
            if (fetchJobDTO.getTransferJob().getSourceSystemHsaId() == null) {
                jmsTemplate.convertAndSend(FETCH_AGGREGATED_QUEUE, fetchJobDTO);
            } else {
                jmsTemplate.convertAndSend(FETCH_SINGLE_SOURCE_SYSTEM_QUEUE, fetchJobDTO);
            }
        } else {
            logger.warn("preCheck failed for transferJob {}, will not start transfers", transferJobDTO.getId());
        }
        jobMDCMapper.clearContext();
    }

    private LocalDateTime getStartOfYear(int year) {
        return LocalDateTime.of(year, 1, 1, 0, 0, 0, 0);
    }

    private LocalDateTime getEndOfYear(int year) {
        return LocalDateTime.of(year, 12, 31, 23, 59, 59, 0);
    }

}
