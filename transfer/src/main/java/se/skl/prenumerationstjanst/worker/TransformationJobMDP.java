package se.skl.prenumerationstjanst.worker;

import com.google.common.util.concurrent.RateLimiter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import se.skl.prenumerationstjanst.dto.PushJobDTO;
import se.skl.prenumerationstjanst.dto.TransferJobDTO;
import se.skl.prenumerationstjanst.dto.TransformationJobDTO;
import se.skl.prenumerationstjanst.exception.transfer.TransformationFailedException;
import se.skl.prenumerationstjanst.exception.transfer.UnrecoverableTransformationFailedException;
import se.skl.prenumerationstjanst.job.JobMDCMapper;
import se.skl.prenumerationstjanst.job.TransformationJobHandler;
import se.skl.prenumerationstjanst.job.TransformationResult;
import se.skl.prenumerationstjanst.model.JobStatus;
import se.skl.prenumerationstjanst.model.TransferType;
import se.skl.prenumerationstjanst.service.JobService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

import static se.skl.prenumerationstjanst.PrenumerationstjanstConstants.PUSH_QUEUE;
import static se.skl.prenumerationstjanst.PrenumerationstjanstConstants.TRANSFORMATION_QUEUE;

/**
 * @author Martin Samuelsson
 */
@Component
public class TransformationJobMDP {

    private static final Logger logger = LoggerFactory.getLogger(TransformationJobMDP.class);

    @Autowired
    private JobService jobService;

    @Resource
    private Map<TransferType, List<TransformationJobHandler>> transformationJobHandlers;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private JobMDCMapper jobMDCMapper;

    @Autowired
    private RateLimiter transformationJobRateLimiter;

    @Transactional
    @JmsListener(destination = TRANSFORMATION_QUEUE)
    public void transform(TransformationJobDTO transformationJobDTO) {
        jobMDCMapper.addToContext(transformationJobDTO);
        transformationJobRateLimiter.acquire();
        jobService.updateTransformationJobStatus(transformationJobDTO.getId(), JobStatus.RUNNING);
        logger.info("starting {} v{} transformationJob", transformationJobDTO.getTransferJob().getTransferType(),
                transformationJobDTO.getSourceVersion());
        TransferJobDTO transferJobDTO = transformationJobDTO.getTransferJob();
        TransferType transferType = TransferType.valueOf(transferJobDTO.getTransferType());
        try {
            TransformationResult transformationResult = transformationJobHandlers.get(transferType).stream()
                    //TODO: composite key instead to prevent stream-filter-find
                    .filter(tjh -> tjh.version() == transformationJobDTO.getSourceVersion())
                    .findFirst()
                    .orElseGet(ThrowingTransformationJobHandler::new)
                    .processTransformationJob(transformationJobDTO);
            transformationResult.getPushJobSpecifications().forEach(pushJobSpecification -> {
                String sourceId = pushJobSpecification.getSourceId();
                int relatedValues = pushJobSpecification.getRelatedValues().size();
                logger.info("pushJobSpecification with sourceId {} have {} related values", sourceId, relatedValues);
                PushJobDTO pushJobDTO = jobService.createPushJob(transferJobDTO.getId())
                        .withValue(pushJobSpecification.getValue())
                        .withRelatedValues(pushJobSpecification.getRelatedValues())
                        .withSourceId(pushJobSpecification.getSourceId())
                        .withSourceVersion(pushJobSpecification.getSourceVersion());
                sendPushJobDTO(pushJobDTO);
            });
            jobService.finishTransformationJob(transformationJobDTO.getId());
        } catch (UnrecoverableTransformationFailedException e) {
            logger.error("transformation failed and will not be retried due to unrecoverable error", e);
            jobService.updateTransformationJobStatus(transformationJobDTO.getId(), JobStatus.FAILED);
        } catch (NullPointerException e) {
            logger.error("transformation failed and will not be retried due to unrecoverable error " +
                    "(no transformation job handler for transfer type)", e);
            jobService.updateTransformationJobStatus(transformationJobDTO.getId(), JobStatus.FAILED);
        }
        jobMDCMapper.clearContext();
    }

    private void sendPushJobDTO(PushJobDTO pushJobDTO) {
        jmsTemplate.convertAndSend(PUSH_QUEUE, pushJobDTO);
    }

    private static class ThrowingTransformationJobHandler implements TransformationJobHandler {

        private static final Logger logger = LoggerFactory.getLogger(ThrowingTransformationJobHandler.class);

        @Override
        public TransformationResult processTransformationJob(TransformationJobDTO transformationJobDTO) throws
                TransformationFailedException, UnrecoverableTransformationFailedException {
            logger.error("No TransformationJobHandler configured for {} v{}",
                    transformationJobDTO.getTransferJob().getTransferType(), transformationJobDTO.getSourceVersion());
            throw new UnrecoverableTransformationFailedException("No TransformationJobHandler configured", transformationJobDTO);
        }

        @Override
        public int version() {
            return 0;
        }

        @Override
        public TransferType transferType() {
            return null;
        }
    }
}
