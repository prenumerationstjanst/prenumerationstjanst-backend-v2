package se.skl.prenumerationstjanst;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * @author Martin Samuelsson
 */
@Configuration
@Profile({"test", "jmstest"})
public class IntegrationTestConfig {

}
