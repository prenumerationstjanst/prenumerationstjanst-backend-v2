package se.skl.prenumerationstjanst;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.RedeliveryPolicy;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.region.policy.*;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.connection.SingleConnectionFactory;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.ConnectionFactory;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Martin Samuelsson
 */
@Configuration
public class JmsTestConfig {

    private static final Logger logger = LoggerFactory.getLogger(JmsTestConfig.class);

    public static final int MAXIMUM_REDELIVERIES = 2;
    public static final int INITIAL_REDELIVERY_DELAY = 2000;
    public static final int REDELIVERY_DELAY = 2000;

    @Value("${pt.activemq.broker-url}")
    private String brokerUrl;

    @Autowired
    private ConnectionFactory connectionFactory;

    @Bean
    @Profile("jmstest")
    @Primary
    public ConnectionFactory jmsConnectionFactory() {
        SingleConnectionFactory connectionFactory = new SingleConnectionFactory();
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(brokerUrl);
        // we do not want redeliveries to block other incoming deliveries
        // until all possible redeliveries for a given message has been completed
        activeMQConnectionFactory.setNonBlockingRedelivery(true);
        activeMQConnectionFactory.setRedeliveryPolicy(redeliveryPolicy());
        activeMQConnectionFactory.setTrustedPackages(Arrays.asList(
                "java",
                "se.skl.prenumerationstjanst",
                "org.apigw.engagementindex")); //FIXME: need to be same as the value in JMSConfig
        connectionFactory.setTargetConnectionFactory(activeMQConnectionFactory);
        logger.warn("created for TEST: {}", connectionFactory);
        return connectionFactory;
    }

    @Bean
    @Profile("test")
    public ConnectionFactory mockJmsConnectionFactory() {
        ConnectionFactory mock = Mockito.mock(ConnectionFactory.class);
        logger.warn("created for TEST: {}", mock);
        return mock;
    }

    private RedeliveryPolicy redeliveryPolicy() {
        RedeliveryPolicy redeliveryPolicy = new RedeliveryPolicy();
        redeliveryPolicy.setMaximumRedeliveries(MAXIMUM_REDELIVERIES);
        redeliveryPolicy.setInitialRedeliveryDelay(INITIAL_REDELIVERY_DELAY);
        redeliveryPolicy.setRedeliveryDelay(REDELIVERY_DELAY);
        redeliveryPolicy.setUseExponentialBackOff(false);
        return redeliveryPolicy;
    }

    @Bean //need to configure our own jms template in test
    @Profile("jmstest")
    @Primary
    public JmsTemplate jmsTemplate() throws Exception {
        JmsTemplate jmsTemplate = new JmsTemplate(connectionFactory);
        logger.warn("created for TEST: {}", jmsTemplate);
        return jmsTemplate;
    }

    @Bean
    @Profile("test")
    public JmsTemplate mockJmsTemplate() {
        JmsTemplate mock = Mockito.mock(JmsTemplate.class);
        logger.warn("created for TEST: {}", mock);
        return mock;
    }

    @Bean
    @Profile("jmstest")
    @Primary
    public BrokerService brokerService() throws Exception {
        BrokerService broker = new BrokerService();
        broker.setPersistent(false);
        broker.setDeleteAllMessagesOnStartup(true);
        broker.setVmConnectorURI(new URI(brokerUrl));
        broker.setUseJmx(false);
        PolicyMap policyMap = new PolicyMap();
        PolicyEntry defaultEntry = new PolicyEntry();
        defaultEntry.setDispatchPolicy(new StrictOrderDispatchPolicy());
        defaultEntry.setSubscriptionRecoveryPolicy(new LastImageSubscriptionRecoveryPolicy());
        policyMap.setDefaultEntry(defaultEntry);
        PolicyEntry handleAllPolicyEntry = new PolicyEntry();
        handleAllPolicyEntry.setQueue(">"); //all queues
        IndividualDeadLetterStrategy individualDeadLetterStrategy = new IndividualDeadLetterStrategy();
        individualDeadLetterStrategy.setQueuePrefix("DLQ.");
        handleAllPolicyEntry.setDeadLetterStrategy(individualDeadLetterStrategy);
        ArrayList<PolicyEntry> entries = new ArrayList<>();
        entries.add(handleAllPolicyEntry);
        policyMap.setPolicyEntries(entries);
        broker.setDestinationPolicy(policyMap);
        broker.start();
        logger.debug("starting embedded ActiveMQ broker");
        logger.warn("created for TEST: {}", broker);
        return broker;
    }

}
