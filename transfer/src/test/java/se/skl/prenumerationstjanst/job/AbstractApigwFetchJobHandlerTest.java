package se.skl.prenumerationstjanst.job;

import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;
import se.skl.prenumerationstjanst.dto.ApigwConsentDTO;
import se.skl.prenumerationstjanst.dto.FetchJobDTO;
import se.skl.prenumerationstjanst.dto.SubscriptionDTO;
import se.skl.prenumerationstjanst.dto.TransferJobDTO;
import se.skl.prenumerationstjanst.model.TransferType;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

/**
 * Base class for testing TransferType specific APIGW FetchJobHandler:s
 *
 * Extending classes should supply TestInput:s for each test case
 *
 * @author Martin Samuelsson
 */
@Ignore
public abstract class AbstractApigwFetchJobHandlerTest {

    private static final Logger logger = LoggerFactory.getLogger(AbstractApigwFetchJobHandlerTest.class);

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private SubscriptionDTO subscriptionDTO;

    @Mock
    private ApigwConsentDTO apigwConsentDTO;

    @Mock
    private TransferJobDTO transferJobDTO;

    @Mock
    private FetchJobDTO fetchJobDTO;

    @Before
    public void setUp() throws Exception {
        ReflectionTestUtils.setField(getFetchJobHandlerUnderTest(), "resourceServerBaseUri", "baseUri");
        when(getFetchJobUriExtractor().createURI(anyString(), eq(fetchJobDTO)))
                .thenReturn(new URI("mep"));
        when(fetchJobDTO.getTransferJob()).thenReturn(transferJobDTO);
        when(transferJobDTO.getSubscriptionId()).thenReturn("subId");
        when(transferJobDTO.getId()).thenReturn("tjId");
        when(subscriptionDTO.getApigwConsent()).thenReturn(apigwConsentDTO);
        when(apigwConsentDTO.getTokenValue()).thenReturn("token");
        XMLUnit.setIgnoreWhitespace(true);
        XMLUnit.setIgnoreComments(true);
        XMLUnit.setIgnoreAttributeOrder(true);
        XMLUnit.setNormalizeWhitespace(true);
    }

    @Test
    public void testTransferType() throws Exception {
        assertEquals(getExpectedTransferType(), getFetchJobHandlerUnderTest().transferType());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testProcessFetchJob_TwoDocumentsZeroFailedSourceSystems() throws Exception {
        doTest(getTwoDocumentsZeroFailedSourceSystemsInput());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testProcessFetchJob_OneDocumentOneFailedSourceSystem() throws Exception {
        doTest(getOneDocumentOneFailedSourceSystemInput());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testProcessFetchJob_ZeroDocumentsTwoFailedSourceSystems() throws Exception {
        doTest(getZeroDocumentsTwoFailedSourceSystemsInput());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testProcessFetchJob_ZeroDocumentsZeroFailedSourceSystems() throws Exception {
        doTest(getZeroDocumentsZeroFailedSourceSystemsInput());
    }

    @SuppressWarnings("unchecked")
    private void doTest(TestInput testInput) throws Exception {
        ResponseEntity responseEntity = mock(ResponseEntity.class);
        when(responseEntity.getBody()).thenReturn(resourceFileToString(testInput.getInputXml()));
        when(restTemplate.exchange(any(RequestEntity.class), any(Class.class))).thenReturn(responseEntity);
        FetchResult fetchResult = getFetchJobHandlerUnderTest().processFetchJob(fetchJobDTO);
        verify(getFetchJobUriExtractor()).createURI(eq("baseUri"), eq(fetchJobDTO));
        ArgumentCaptor<RequestEntity> requestEntityArgumentCaptor = ArgumentCaptor.forClass(RequestEntity.class);
        verify(restTemplate).exchange(requestEntityArgumentCaptor.capture(), eq(String.class));
        assertEquals("mep", requestEntityArgumentCaptor.getValue().getUrl().toString());
        verify(responseEntity).getBody();
        verify(fetchJobDTO).getApigwToken();
        assertEquals(testInput.getOrderedExpectedOutputXmls().size(), fetchResult.getDocuments().size());
        for (int i = 0; i < fetchResult.getDocuments().size(); i += 1) {
            XMLAssert.assertXMLEqual(resourceFileToString(testInput.getOrderedExpectedOutputXmls().get(i)),
                    fetchResult.getDocuments().get(i).getValue());
        }
        assertEquals(testInput.getOrderedExpectedOutputSourceSystemIds().size(), fetchResult.getDocuments().size());
        for (int i = 0; i < fetchResult.getDocuments().size(); i += 1) {
            assertEquals(testInput.getOrderedExpectedOutputSourceSystemIds().get(i),
                    fetchResult.getDocuments().get(i).getSourceSystemId());
        }
        assertEquals(testInput.getExpectedFailedSourceSystemHsaIds().size(), fetchResult.getFailedSourceSystems().size());
        assertTrue(testInput.getExpectedFailedSourceSystemHsaIds().containsAll(fetchResult.getFailedSourceSystems()));
        verifyNoMoreInteractions(restTemplate);
    }

    private String resourceFileToString(String filename) throws URISyntaxException, IOException {
        URL resource = getClass().getClassLoader().getResource(filename);
        @SuppressWarnings("ConstantConditions")
        Path path = Paths.get(resource.toURI());
        return new String(Files.readAllBytes(path), "UTF8");
    }

    /**
     *
     * @return ApigwFetchJobHandler under test
     */
    protected abstract ApigwFetchJobHandler getFetchJobHandlerUnderTest();

    /**
     *
     * @return FetchJobUriExtractor injected in ApigwFetchJobHandler under test
     * by the extending test class
     */
    protected abstract FetchJobUriExtractor getFetchJobUriExtractor();

    /**
     *
     * @return TransferType expected to be returned by ApigwFetchJobHandler under test
     */
    protected abstract TransferType getExpectedTransferType();

    /**
     * Extending test class should provide a TestInput containing:
     * a) input xml
     * b) two expected output xmls
     *
     * @return TestInput with input xml, two expected output xmls
     */
    protected abstract TestInput getTwoDocumentsZeroFailedSourceSystemsInput();

    /**
     * Extending test class should provide a TestInput containing:
     * a) input xml
     * b) one expected output xml
     * c) one expected failed source system
     *
     * @return TestInput with input xml, one expected output xml, one expected failed source system
     */
    protected abstract TestInput getOneDocumentOneFailedSourceSystemInput();

    /**
     * Extending test class should provide a TestInput containing:
     * a) input xml
     * b) two expected failed source systems
     *
     * @return TestInput with input xml, two expected failed source systems
     */
    protected abstract TestInput getZeroDocumentsTwoFailedSourceSystemsInput();

    /**
     * Extending test class should provide a TestInput containing:
     * a) input xml
     *
     * @return TestInput with input xml
     */
    protected abstract TestInput getZeroDocumentsZeroFailedSourceSystemsInput();

    protected static class TestInput {
        private String inputXml;
        private List<String> orderedExpectedOutputXmls = new ArrayList<>();
        private List<String> orderedExpectedOutputSourceSystemIds = new ArrayList<>();
        private Set<String> expectedFailedSourceSystemHsaIds = new HashSet<>();

        private TestInput() {
        }

        private TestInput(String inputXml, List<String> orderedExpectedOutputXmls,
                          Set<String> expectedFailedSourceSystemHsaIds, List<String> orderedExpectedOutputSourceSystemIds) {
            this.inputXml = inputXml;
            this.orderedExpectedOutputXmls = orderedExpectedOutputXmls;
            this.expectedFailedSourceSystemHsaIds = expectedFailedSourceSystemHsaIds;
            this.orderedExpectedOutputSourceSystemIds = orderedExpectedOutputSourceSystemIds;
        }

        public static TestInput newInstance() {
            return new TestInput();
        }

        public TestInput withInputXml(String inputXml) {
            return new TestInput(inputXml, orderedExpectedOutputXmls, expectedFailedSourceSystemHsaIds,
                    orderedExpectedOutputSourceSystemIds);
        }

        public TestInput addExpectedOutputXml(String expectedOutputXml) {
            List<String> newOrderedExpectedOutputXmls = new ArrayList<>(orderedExpectedOutputXmls);
            newOrderedExpectedOutputXmls.add(expectedOutputXml);
            return new TestInput(inputXml, newOrderedExpectedOutputXmls, expectedFailedSourceSystemHsaIds,
                    orderedExpectedOutputSourceSystemIds);
        }

        public TestInput addExpectedSourceSystemId(String expectedSourceSystemId) {
            List<String> newOrderedExpectedSourceSystemIds = new ArrayList<>(orderedExpectedOutputSourceSystemIds);
            newOrderedExpectedSourceSystemIds.add(expectedSourceSystemId);
            return new TestInput(inputXml, orderedExpectedOutputXmls, expectedFailedSourceSystemHsaIds,
                    newOrderedExpectedSourceSystemIds);
        }

        public TestInput addExpectedFailedSourceSystemHsaId(String expectedFailedSourceSystemHsaId) {
            Set<String> newOrderedExpectedFailedSourceSystems = new HashSet<>(expectedFailedSourceSystemHsaIds);
            newOrderedExpectedFailedSourceSystems.add(expectedFailedSourceSystemHsaId);
            return new TestInput(inputXml, orderedExpectedOutputXmls, newOrderedExpectedFailedSourceSystems,
                    orderedExpectedOutputSourceSystemIds);
        }

        public String getInputXml() {
            return inputXml;
        }

        public List<String> getOrderedExpectedOutputXmls() {
            return orderedExpectedOutputXmls;
        }

        public List<String> getOrderedExpectedOutputSourceSystemIds() {
            return orderedExpectedOutputSourceSystemIds;
        }

        public Set<String> getExpectedFailedSourceSystemHsaIds() {
            return expectedFailedSourceSystemHsaIds;
        }
    }
}
