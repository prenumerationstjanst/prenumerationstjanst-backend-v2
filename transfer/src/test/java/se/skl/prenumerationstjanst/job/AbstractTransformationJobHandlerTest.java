package se.skl.prenumerationstjanst.job;

import org.custommonkey.xmlunit.*;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import se.skl.prenumerationstjanst.dto.*;

import java.io.IOException;
import java.net.URISyntaxException;

import static se.skl.prenumerationstjanst.test.TestUtils.resourceFileToString;

/**
 * @author Martin Samuelsson
 */
@Ignore
public abstract class AbstractTransformationJobHandlerTest {

    private static final Logger logger = LoggerFactory.getLogger(AbstractTransformationJobHandlerTest.class);

    protected void assertPushJobSpecification(TransformationResult.PushJobSpecification pushJobSpecification,
                                            String mainBase) throws Exception {
        String expectedMain = resourceFileToString(mainBase + ".xml");
        String main = pushJobSpecification.getValue();
        logger.debug("main: {}", main);
        XMLAssert.assertXMLEqual(expectedMain, pushJobSpecification.getValue());
        for (int i = 0; i < pushJobSpecification.getRelatedValues().size(); i++) {
            boolean lastIteration = i == (pushJobSpecification.getRelatedValues().size() - 1);
            String related = pushJobSpecification.getRelatedValues().get(i);
            logger.debug("related: {}", related);
            Diff diff = XMLUnit.compareXML(resourceFileToString(
                    mainBase + "_rv" + (i + 1) + ".xml"), related);
            diff.overrideDifferenceListener(new RivSourceCommentDifferenceListener());
            XMLAssert.assertXMLEqual(diff, true);
        }
    }

    protected static TransformationJobDTO createTransformationJob(String filename) throws IOException, URISyntaxException {
        return new TransformationJobDTO("id", TransferJobDTO.create(), resourceFileToString(filename), null, 2);

    }

    /**
     * XMLUnit Difference Listener that ignores differences in the comment/when part of the XML
     * (date is generated from now())
     */
    public class RivSourceCommentDifferenceListener implements DifferenceListener {

        @Override
        public int differenceFound(Difference difference) {
            if (difference.getTestNodeDetail().getXpathLocation().startsWith("/thing[1]/data-xml[1]/comment[1]/when[1]")) {
                logger.debug("ignoring difference: {}", difference.toString());
                return RETURN_IGNORE_DIFFERENCE_NODES_SIMILAR;
            }
            return RETURN_ACCEPT_DIFFERENCE;
        }

        @Override
        public void skippedComparison(Node node, Node node1) {
        }
    }
}
