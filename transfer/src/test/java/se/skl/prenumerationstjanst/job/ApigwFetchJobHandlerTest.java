package se.skl.prenumerationstjanst.job;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;
import se.skl.prenumerationstjanst.config.TestConfiguration;
import se.skl.prenumerationstjanst.dto.ApigwConsentDTO;
import se.skl.prenumerationstjanst.dto.FetchJobDTO;
import se.skl.prenumerationstjanst.dto.SubscriptionDTO;
import se.skl.prenumerationstjanst.dto.TransferJobDTO;
import se.skl.prenumerationstjanst.exception.transfer.FetchFailedException;
import se.skl.prenumerationstjanst.model.TransferType;

import javax.xml.xpath.XPathExpressionException;
import java.net.URI;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

/**
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApigwFetchJobHandlerTest.TestConfig.class)
public class ApigwFetchJobHandlerTest {

    private static final Logger logger = LoggerFactory.getLogger(ApigwFetchJobHandlerTest.class);
    public static final String REQ_URL = "http://www.example.com";
    public static final int DOCUMENT_VERSION = 1;
    public static final String TOKEN_VALUE = "tokenValue";

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ApigwFetchJobHandler apigwFetchJobHandler;

    private MockRestServiceServer mockServer;

    @Before
    public void setUp() throws Exception {
        mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    @Test
    public void testProcessFetchJob_EmptyResponse() throws Exception {
        mockServer.expect(requestTo(REQ_URL))
                .andExpect(method(HttpMethod.GET))
                .andExpect(header("Authorization", "Bearer " + TOKEN_VALUE))
                .andRespond(withSuccess(createDummyResponse(0), MediaType.APPLICATION_XML));
        FetchResult fetchResult = apigwFetchJobHandler.processFetchJob(createFetchJobDTO());
        assertEquals(0, fetchResult.getDocuments().size());
        assertEquals(0, fetchResult.getFailedSourceSystems().size());
        assertEquals(DOCUMENT_VERSION, fetchResult.getDocumentVersion());
    }

    @Test
    public void testProcessFetchJob_NoDocsOneFailedSourceSystem() throws Exception {
        mockServer.expect(requestTo(REQ_URL))
                .andExpect(method(HttpMethod.GET))
                .andExpect(header("Authorization", "Bearer " + TOKEN_VALUE))
                .andRespond(withSuccess(createDummyResponse(0, "fail"), MediaType.APPLICATION_XML));
        FetchResult fetchResult = apigwFetchJobHandler.processFetchJob(createFetchJobDTO());
        assertEquals(0, fetchResult.getDocuments().size());
        assertEquals(1, fetchResult.getFailedSourceSystems().size());
        assertEquals(DOCUMENT_VERSION, fetchResult.getDocumentVersion());
    }

    @Test
    public void testProcessFetchJob_TwoDocsOneFailedSourceSystem() throws Exception {
        mockServer.expect(requestTo(REQ_URL))
                .andExpect(method(HttpMethod.GET))
                .andExpect(header("Authorization", "Bearer " + TOKEN_VALUE))
                .andRespond(withSuccess(createDummyResponse(2, "fail"), MediaType.APPLICATION_XML));
        FetchResult fetchResult = apigwFetchJobHandler.processFetchJob(createFetchJobDTO());
        assertEquals(2, fetchResult.getDocuments().size());
        for (int i = 0; i < fetchResult.getDocuments().size(); i++) {
            assertEquals(DOCUMENT_VERSION, fetchResult.getDocuments().get(i).getVersion());
            assertEquals("ss-" + (i+1), fetchResult.getDocuments().get(i).getSourceSystemId());
        }
        assertEquals(1, fetchResult.getFailedSourceSystems().size());
        assertEquals(DOCUMENT_VERSION, fetchResult.getDocumentVersion());
    }

    @Test
    public void testProcessFetchJob_TwoDocsNoFailedSourceSystem() throws Exception {
        mockServer.expect(requestTo(REQ_URL))
                .andExpect(method(HttpMethod.GET))
                .andExpect(header("Authorization", "Bearer " + TOKEN_VALUE))
                .andRespond(withSuccess(createDummyResponse(2), MediaType.APPLICATION_XML));
        FetchResult fetchResult = apigwFetchJobHandler.processFetchJob(createFetchJobDTO());
        assertEquals(2, fetchResult.getDocuments().size());
        for (int i = 0; i < fetchResult.getDocuments().size(); i++) {
            assertEquals(DOCUMENT_VERSION, fetchResult.getDocuments().get(i).getVersion());
            assertEquals("ss-" + (i+1), fetchResult.getDocuments().get(i).getSourceSystemId());
        }
        assertEquals(0, fetchResult.getFailedSourceSystems().size());
        assertEquals(DOCUMENT_VERSION, fetchResult.getDocumentVersion());
    }

    @Test(expected = FetchFailedException.class)
    public void testProcessFetchJob_500() throws Exception {
        mockServer.expect(requestTo(REQ_URL))
                .andExpect(method(HttpMethod.GET))
                .andExpect(header("Authorization", "Bearer " + TOKEN_VALUE))
                .andRespond(withStatus(HttpStatus.INTERNAL_SERVER_ERROR));
        apigwFetchJobHandler.processFetchJob(createFetchJobDTO());
    }

    @Test
    public void testProcessFetchJob_404() throws Exception {
        mockServer.expect(requestTo(REQ_URL))
                .andExpect(method(HttpMethod.GET))
                .andExpect(header("Authorization", "Bearer " + TOKEN_VALUE))
                .andRespond(withStatus(HttpStatus.NOT_FOUND));
        FetchResult fetchResult = apigwFetchJobHandler.processFetchJob(createFetchJobDTO());
        assertEquals(0, fetchResult.getDocuments().size());
        assertEquals(0, fetchResult.getFailedSourceSystems().size());
        assertEquals(DOCUMENT_VERSION, fetchResult.getDocumentVersion());
    }

    private String createDummyResponse(int docs, String... failedSourceSystems) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<foo:root xmlns:foo=\"urn:foo\" xmlns:st=\"urn:org:apigw:status\">");
        if (failedSourceSystems.length > 0) {
            stringBuilder.append("<st:processingStatus>");
            for (String failedSourceSystem : failedSourceSystems) {
                stringBuilder.append("<st:ProcessingStatusList>");
                stringBuilder.append("<st:logicalAddress>");
                stringBuilder.append(failedSourceSystem);
                stringBuilder.append("</st:logicalAddress>");
                stringBuilder.append("<st:statusCode>NoDataSynchFailed</st:statusCode>");
                stringBuilder.append("</st:ProcessingStatusList>");
            }
            stringBuilder.append("</st:processingStatus>");
        }
        for (int i = 0; i < docs; i += 1) {
            stringBuilder.append("<foo:doc>");
            stringBuilder.append("<foo:ss>ss-");
            stringBuilder.append(i + 1);
            stringBuilder.append("</foo:ss>");
            stringBuilder.append("</foo:doc>");
        }
        stringBuilder.append("</foo:root>");
        return stringBuilder.toString();
    }

    @Test
    public void testTransferType() throws Exception {
        assertEquals(TransferType.VACCINATION_HISTORY, apigwFetchJobHandler.transferType());
    }

    @Test
    public void testVersion() throws Exception {
        assertEquals(DOCUMENT_VERSION, apigwFetchJobHandler.version());
    }

    private static class TestUriExtractor implements FetchJobUriExtractor {

        @Override
        public URI createURI(String baseUri, FetchJobDTO fetchJobDTO) {
            return URI.create(REQ_URL);
        }
    }

    private FetchJobDTO createFetchJobDTO() {
        return FetchJobDTO.create()
                .withApigwToken(TOKEN_VALUE)
                .withTransferJob(TransferJobDTO.create());
    }

    @TestConfiguration
    @Configuration
    public static class TestConfig {

        @Bean(name = "resourceServerRestTemplate")
        @Primary
        public RestTemplate restTemplate() {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.setErrorHandler(new ApigwResourceServerErrorHandler());
            //TODO: we can not depend on the correct initialization in the test config
            return restTemplate;
        }

        @Bean
        @Primary
        public ApigwFetchJobHandler apigwFetchJobHandler() throws XPathExpressionException {
            return new ApigwFetchJobHandler(
                    TransferType.VACCINATION_HISTORY,
                    "//*[local-name()='doc']",
                    "*[local-name()='ss']",
                    DOCUMENT_VERSION) {
                @Override
                protected FetchJobUriExtractor getFetchJobUriExtractor() {
                    return new TestUriExtractor();
                }
            };
        }

    }
}