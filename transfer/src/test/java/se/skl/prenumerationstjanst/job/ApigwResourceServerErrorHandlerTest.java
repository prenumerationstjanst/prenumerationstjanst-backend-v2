package se.skl.prenumerationstjanst.job;

import org.junit.Test;
import org.springframework.http.HttpStatus;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * @author Martin Samuelsson
 */
public class ApigwResourceServerErrorHandlerTest {

    private ApigwResourceServerErrorHandler apigwResourceServerErrorHandler = new ApigwResourceServerErrorHandler();

    /**
     * Make sure 404 is not considered an error
     * and all other client/server error codes are errors
     *
     * @throws Exception
     */
    @Test
    public void testHasError() throws Exception {
        assertFalse(apigwResourceServerErrorHandler.hasError(HttpStatus.NOT_FOUND));
        Arrays.asList(HttpStatus.values()).stream()
                .filter(httpStatus ->
                        (httpStatus.is5xxServerError() || httpStatus.is4xxClientError())
                        && httpStatus != HttpStatus.NOT_FOUND
                )
                .forEach(httpClientErrorStatus ->
                        assertTrue(apigwResourceServerErrorHandler.hasError(httpClientErrorStatus))
                );
    }

}