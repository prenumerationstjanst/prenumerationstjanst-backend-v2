package se.skl.prenumerationstjanst.job;

import org.junit.Test;
import se.skl.prenumerationstjanst.dto.TransferJobDTO;
import se.skl.prenumerationstjanst.dto.TransformationJobDTO;
import se.skl.prenumerationstjanst.exception.transfer.TransformationFailedException;
import se.skl.prenumerationstjanst.exception.transfer.UnrecoverableTransformationFailedException;
import se.skl.prenumerationstjanst.model.TransferType;

import javax.xml.bind.JAXBException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
public class ApigwTransformationJobHandlerTest {

    private static final String SOURCE_ID = "sourceId";
    public static final int SOURCE_VERSION = 1;

    @Test
    public void processTransformationJob_OK() throws Exception {
        ApigwTransformationJobHandler apigwTransformationJobHandler =
                new ApigwTransformationJobHandler<String>(TransferType.VACCINATION_HISTORY, 1, null) {
                    @Override
                    protected TransformationResult transform(String doc, LocalDateTime sourceFetchTime,
                                                             TransformationJobDTO job) throws
                            TransformationFailedException {
                        return new TransformationResult(Collections.singletonList(
                                new TransformationResult.PushJobSpecification(doc, Collections.emptyList(),
                                        sourceFetchTime,
                                        SOURCE_ID, SOURCE_VERSION)));
                    }

                    @Override
                    protected String unmarshal(String document) throws JAXBException {
                        return document;
                    }
                };

        String inputSource = "mep";
        LocalDateTime inputTime = LocalDateTime.now();

        TransformationResult transformationResult = apigwTransformationJobHandler.processTransformationJob(new
                TransformationJobDTO("id", TransferJobDTO.create(), inputSource, inputTime, 1));

        List<TransformationResult.PushJobSpecification> pushJobSpecifications = transformationResult
                .getPushJobSpecifications();
        assertEquals(1, pushJobSpecifications.size());
        TransformationResult.PushJobSpecification pushJobSpecification = pushJobSpecifications.get(0);
        assertEquals(inputSource, pushJobSpecification.getValue());
        assertEquals(inputTime, pushJobSpecification.getSourceFetchTime());
        assertEquals(SOURCE_ID, pushJobSpecification.getSourceId());
        assertEquals(SOURCE_VERSION, pushJobSpecification.getSourceVersion());
        assertEquals(0, pushJobSpecification.getRelatedValues().size());
    }

    @Test(expected = UnrecoverableTransformationFailedException.class)
    public void processTransformationJob_FailedUnmarshal() throws Exception {
        ApigwTransformationJobHandler apigwTransformationJobHandler =
                new ApigwTransformationJobHandler<String>(TransferType.VACCINATION_HISTORY, 1, null) {
                    @Override
                    protected TransformationResult transform(String doc, LocalDateTime sourceFetchTime,
                                                             TransformationJobDTO job) throws
                            TransformationFailedException {
                        return null;
                    }

                    @Override
                    protected String unmarshal(String document) throws JAXBException {
                        throw new JAXBException("");
                    }
                };

        apigwTransformationJobHandler.processTransformationJob(new
                TransformationJobDTO("id", TransferJobDTO.create(), null, null, 0));
    }

    @Test(expected = UnrecoverableTransformationFailedException.class)
    public void processTransformationJob_JAXBExceptionInTransform() throws Exception {
        ApigwTransformationJobHandler apigwTransformationJobHandler =
                new ApigwTransformationJobHandler<String>(TransferType.VACCINATION_HISTORY, 1, null) {
                    @Override
                    protected TransformationResult transform(String doc, LocalDateTime sourceFetchTime,
                                                             TransformationJobDTO job) throws
                            TransformationFailedException {
                        throw new TransformationFailedException(new JAXBException(""), job);
                    }

                    @Override
                    protected String unmarshal(String document) throws JAXBException {
                        return document;
                    }
                };

        apigwTransformationJobHandler.processTransformationJob(new
                TransformationJobDTO("id", TransferJobDTO.create(), null, null, 0));
    }

    @Test(expected = TransformationFailedException.class)
    public void processTransformationJob_OtherCausingExceptionInTransform() throws Exception {
        ApigwTransformationJobHandler apigwTransformationJobHandler =
                new ApigwTransformationJobHandler<String>(TransferType.VACCINATION_HISTORY, 1, null) {
                    @Override
                    protected TransformationResult transform(String doc, LocalDateTime sourceFetchTime,
                                                             TransformationJobDTO job) throws
                            TransformationFailedException {
                        throw new TransformationFailedException(new RuntimeException(), job);
                    }

                    @Override
                    protected String unmarshal(String document) throws JAXBException {
                        return document;
                    }
                };

        apigwTransformationJobHandler.processTransformationJob(new
                TransformationJobDTO("id", TransferJobDTO.create(), null, null, 0));
    }

    @Test(expected = TransformationFailedException.class)
    public void processTransformationJob_TransformationFailedExceptionWithNoCauseInTransform() throws Exception {
        ApigwTransformationJobHandler apigwTransformationJobHandler =
                new ApigwTransformationJobHandler<String>(TransferType.VACCINATION_HISTORY, 1, null) {
                    @Override
                    protected TransformationResult transform(String doc, LocalDateTime sourceFetchTime,
                                                             TransformationJobDTO job) throws
                            TransformationFailedException {
                        throw new TransformationFailedException(job);
                    }

                    @Override
                    protected String unmarshal(String document) throws JAXBException {
                        return document;
                    }
                };

        apigwTransformationJobHandler.processTransformationJob(new
                TransformationJobDTO("id", TransferJobDTO.create(), null, null, 0));
    }

    @Test
    public void transferType() throws Exception {
        assertEquals(TransferType.VACCINATION_HISTORY, new TTVersionCheck(TransferType.VACCINATION_HISTORY, 0)
                .transferType());
        assertNotEquals(TransferType.LABORATORY_ORDER_OUTCOME, new TTVersionCheck(TransferType.VACCINATION_HISTORY,
                0).transferType());
    }

    @Test
    public void version() throws Exception {
        assertEquals(5, new TTVersionCheck(TransferType.VACCINATION_HISTORY, 5).version());
        assertNotEquals(1, new TTVersionCheck(TransferType.VACCINATION_HISTORY, 5).version());
    }

    private static class TTVersionCheck extends ApigwTransformationJobHandler {

        public TTVersionCheck(TransferType transferType, int version) throws
                JAXBException {
            super(transferType, version, null);
        }

        @Override
        protected TransformationResult transform(Object doc, LocalDateTime sourceFetchTime, TransformationJobDTO job)
                throws TransformationFailedException {
            return null;
        }

        @Override
        protected Object unmarshal(String document) throws JAXBException {
            return null;
        }
    }
}