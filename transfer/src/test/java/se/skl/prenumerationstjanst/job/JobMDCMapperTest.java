package se.skl.prenumerationstjanst.job;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.MDC;
import se.skl.prenumerationstjanst.dto.*;

import java.time.LocalDateTime;

import static org.junit.Assert.*;
import static se.skl.prenumerationstjanst.PrenumerationstjanstConstants.*;

/**
 * @author Martin Samuelsson
 */
public class JobMDCMapperTest {

    private static final String TRANSFER_JOB_ID = "tjId";
    private static final String TRANSFER_TYPE = "tt";
    private static final String ORIGIN = "or";
    private static final LocalDateTime END_DATE_TIME = LocalDateTime.now().plusHours(1);
    private static final String SOURCE_SYSTEM_HSA_ID = "ssid";
    private static final String SUBSCRIPTION_ID = "sid";
    private static final String FETCH_JOB_ID = "fjId";
    private static final String PUSH_JOB_ID = "pjId";
    private JobMDCMapper jobMDCMapper;

    @Before
    public void setUp() throws Exception {
        jobMDCMapper = new JobMDCMapper();
        MDC.clear();
    }

    @After
    public void tearDown() throws Exception {
        MDC.clear();
    }

    @Test
    public void addToContext_TransferJobDTO() throws Exception {
        TransferJobDTO transferJobDTO = getTransferJobDTO();
        jobMDCMapper.addToContext(transferJobDTO);
        assertTransferJobData();
    }

    @Test
    public void addToContext_FetchJobDTO() throws Exception {
        FetchJobDTO fetchJobDTO = FetchJobDTO.create()
                .withTransferJob(getTransferJobDTO())
                .withId(FETCH_JOB_ID);
        jobMDCMapper.addToContext(fetchJobDTO);
        assertEquals(FETCH_JOB_ID, MDC.get(MDC_FETCH_JOB_ID_KEY));
        assertTransferJobData();
    }

    @Test
    public void addToContext_TransformationJobDTO() throws Exception {
        TransformationJobDTO transformationJobDTO =
                new TransformationJobDTO("id", getTransferJobDTO(), null, null, 0);
        jobMDCMapper.addToContext(transformationJobDTO);
        assertEquals(transformationJobDTO.getId(), MDC.get(MDC_TRANSFORMATION_JOB_ID_KEY));
        assertTransferJobData();

    }

    @Test
    public void addToContext_PushJobDTO() throws Exception {
        PushJobDTO pushJobDTO = PushJobDTO.create()
                .withTransferJob(getTransferJobDTO())
                .withId(PUSH_JOB_ID);
        jobMDCMapper.addToContext(pushJobDTO);
        assertEquals(PUSH_JOB_ID, MDC.get(MDC_PUSH_JOB_ID_KEY));
        assertTransferJobData();
    }

    @Test
    public void clearContext_AfterTransferJobDTO() throws Exception {
        MDC.put("unrelated", "mep");
        assertEquals(1, MDC.getCopyOfContextMap().size());
        addToContext_TransferJobDTO();
        assertNotNull(MDC.getCopyOfContextMap());
        assertNotEquals(1, MDC.getCopyOfContextMap().size());
        jobMDCMapper.clearContext();
        assertTrue(MDC.getCopyOfContextMap().size() == 1);
        assertEquals("mep", MDC.get("unrelated"));
    }

    @Test
    public void clearContext_AfterFetchJobDTO() throws Exception {
        MDC.put("unrelated", "mep");
        assertEquals(1, MDC.getCopyOfContextMap().size());
        addToContext_FetchJobDTO();
        assertNotNull(MDC.getCopyOfContextMap());
        assertNotEquals(1, MDC.getCopyOfContextMap().size());
        jobMDCMapper.clearContext();
        assertTrue(MDC.getCopyOfContextMap().size() == 1);
        assertEquals("mep", MDC.get("unrelated"));
    }

    @Test
    public void clearContext_AfterTransformationJobDTO() throws Exception {
        MDC.put("unrelated", "mep");
        assertEquals(1, MDC.getCopyOfContextMap().size());
        addToContext_TransformationJobDTO();
        assertNotNull(MDC.getCopyOfContextMap());
        assertNotEquals(1, MDC.getCopyOfContextMap().size());
        jobMDCMapper.clearContext();
        assertTrue(MDC.getCopyOfContextMap().size() == 1);
        assertEquals("mep", MDC.get("unrelated"));
    }

    @Test
    public void clearContext_AfterPushJobDTO() throws Exception {
        MDC.put("unrelated", "mep");
        assertEquals(1, MDC.getCopyOfContextMap().size());
        addToContext_PushJobDTO();
        assertNotNull(MDC.getCopyOfContextMap());
        assertNotEquals(1, MDC.getCopyOfContextMap().size());
        jobMDCMapper.clearContext();
        assertTrue(MDC.getCopyOfContextMap().size() == 1);
        assertEquals("mep", MDC.get("unrelated"));
    }

    private void assertTransferJobData() throws Exception {
        assertEquals(TRANSFER_JOB_ID, MDC.get(MDC_TRANSFER_JOB_ID_KEY));
        assertEquals(TRANSFER_TYPE, MDC.get(MDC_TRANSFER_TYPE_KEY));
        assertEquals(ORIGIN, MDC.get(MDC_TRANSFER_ORGIN_KEY));
        assertNotNull(MDC.get(MDC_START_DATE_TIME_KEY));
        assertNotNull(MDC.get(MDC_END_DATE_TIME_KEY));
        assertEquals(SOURCE_SYSTEM_HSA_ID, MDC.get(MDC_SOURCE_SYSTEM_HSA_ID_KEY));
        assertEquals(SUBSCRIPTION_ID, MDC.get(MDC_SUBSCRIPTION_ID_KEY));
    }

    private TransferJobDTO getTransferJobDTO() {
        LocalDateTime START_DATE_TIME = LocalDateTime.now().minusHours(1);
        return TransferJobDTO.create()
                .withId(TRANSFER_JOB_ID)
                .withTransferType(TRANSFER_TYPE)
                .withOrigin(ORIGIN)
                .withStartDateTime(START_DATE_TIME)
                .withEndDateTime(END_DATE_TIME)
                .withSourceSystemHsaId(SOURCE_SYSTEM_HSA_ID)
                .withSubscriptionId(SUBSCRIPTION_ID);
    }

}