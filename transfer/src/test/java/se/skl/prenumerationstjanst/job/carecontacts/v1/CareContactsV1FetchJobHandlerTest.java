package se.skl.prenumerationstjanst.job.carecontacts.v1;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import se.skl.prenumerationstjanst.job.AbstractApigwFetchJobHandlerTest;
import se.skl.prenumerationstjanst.job.ApigwFetchJobHandler;
import se.skl.prenumerationstjanst.job.FetchJobUriExtractor;
import se.skl.prenumerationstjanst.model.TransferType;

/**
 * @author Martin Samuelsson
 */
@RunWith(MockitoJUnitRunner.class)
public class CareContactsV1FetchJobHandlerTest extends AbstractApigwFetchJobHandlerTest {

    @Mock
    private CareContactsV1FetchJobUriExtractor fetchJobUriExtractor;

    @InjectMocks
    private CareContactsV1FetchJobHandler fetchJobHandler;

    @Override
    protected ApigwFetchJobHandler getFetchJobHandlerUnderTest() {
        return fetchJobHandler;
    }

    @Override
    protected FetchJobUriExtractor getFetchJobUriExtractor() {
        return fetchJobUriExtractor;
    }

    @Override
    protected TransferType getExpectedTransferType() {
        return TransferType.CARE_CONTACTS;
    }

    @Override
    protected TestInput getTwoDocumentsZeroFailedSourceSystemsInput() {
        return TestInput
                .newInstance()
                .withInputXml("CareContactsV1FetchJobHandlerTest/getTwoDocumentsZeroFailedSourceSystemsInput_input.xml")
                .addExpectedOutputXml("CareContactsV1FetchJobHandlerTest/getTwoDocumentsZeroFailedSourceSystemsInput_output1.xml")
                .addExpectedSourceSystemId("5565594230")
                .addExpectedOutputXml("CareContactsV1FetchJobHandlerTest/getTwoDocumentsZeroFailedSourceSystemsInput_output2.xml")
                .addExpectedSourceSystemId("5565594230");
    }

    @Override
    protected TestInput getOneDocumentOneFailedSourceSystemInput() {
        return TestInput
                .newInstance()
                .withInputXml("CareContactsV1FetchJobHandlerTest/getOneDocumentOneFailedSourceSystemInput_input.xml")
                .addExpectedOutputXml("CareContactsV1FetchJobHandlerTest/getOneDocumentOneFailedSourceSystemInput_output1.xml")
                .addExpectedSourceSystemId("5565594230")
                .addExpectedFailedSourceSystemHsaId("HSAID-FAILED");
    }

    @Override
    protected TestInput getZeroDocumentsTwoFailedSourceSystemsInput() {
        return TestInput
                .newInstance()
                .withInputXml("CareContactsV1FetchJobHandlerTest/getZeroDocumentsTwoFailedSourceSystemsInput_input.xml")
                .addExpectedFailedSourceSystemHsaId("HSAID-FAILED")
                .addExpectedFailedSourceSystemHsaId("HSAID-FAILED-2");
    }

    @Override
    protected TestInput getZeroDocumentsZeroFailedSourceSystemsInput() {
        return TestInput
                .newInstance()
                .withInputXml("CareContactsV1FetchJobHandlerTest/getZeroDocumentsZeroFailedSourceSystemsInput_input.xml");
    }
}