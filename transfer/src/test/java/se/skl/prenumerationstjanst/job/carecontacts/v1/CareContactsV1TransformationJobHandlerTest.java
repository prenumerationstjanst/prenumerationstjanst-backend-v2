package se.skl.prenumerationstjanst.job.carecontacts.v1;

import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.skl.prenumerationstjanst.job.AbstractTransformationJobHandlerTest;
import se.skl.prenumerationstjanst.job.TransformationResult;

import static org.junit.Assert.assertEquals;

/**
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
public class CareContactsV1TransformationJobHandlerTest extends AbstractTransformationJobHandlerTest {

    private static final Logger logger = LoggerFactory.getLogger(CareContactsV1TransformationJobHandlerTest.class);

    private CareContactsV1TransformationJobHandler careContactsV1TransformationJobHandler;

    @Before
    public void setUp() throws Exception {
        careContactsV1TransformationJobHandler = new CareContactsV1TransformationJobHandler();
        XMLUnit.setIgnoreWhitespace(true);
        XMLUnit.setIgnoreComments(true);
        XMLUnit.setIgnoreAttributeOrder(true);
        XMLUnit.setNormalizeWhitespace(true);
    }

    @Test
    public void processTransformationJob() throws Exception {
        TransformationResult transformationResult = careContactsV1TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob("CareContactsV1TransformationJobHandlerTest/input1.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "CareContactsV1TransformationJobHandlerTest/input1_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(3, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationJob_NoCareContactTimePeriod() throws Exception {
        TransformationResult transformationResult = careContactsV1TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob("CareContactsV1TransformationJobHandlerTest/input2.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "CareContactsV1TransformationJobHandlerTest/input2_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(3, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationJob_NoCareContactOrgUnit() throws Exception {
        TransformationResult transformationResult = careContactsV1TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob("CareContactsV1TransformationJobHandlerTest/input3.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "CareContactsV1TransformationJobHandlerTest/input3_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(3, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationJob_NoCareContactStatus() throws Exception {
        TransformationResult transformationResult = careContactsV1TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob("CareContactsV1TransformationJobHandlerTest/input4.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "CareContactsV1TransformationJobHandlerTest/input4_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(2, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationJob_NoHealthcareProfessionalName() throws Exception {
        TransformationResult transformationResult = careContactsV1TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob("CareContactsV1TransformationJobHandlerTest/input5.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "CareContactsV1TransformationJobHandlerTest/input5_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(3, pushJobSpecification.getRelatedValues().size());
    }

}