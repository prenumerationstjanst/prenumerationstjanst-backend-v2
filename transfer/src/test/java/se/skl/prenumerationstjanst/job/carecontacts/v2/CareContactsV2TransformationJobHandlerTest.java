package se.skl.prenumerationstjanst.job.carecontacts.v2;

import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.skl.prenumerationstjanst.job.AbstractTransformationJobHandlerTest;
import se.skl.prenumerationstjanst.job.TransformationResult;

import static org.junit.Assert.assertEquals;

/**
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
public class CareContactsV2TransformationJobHandlerTest extends AbstractTransformationJobHandlerTest {

    private static final Logger logger = LoggerFactory.getLogger(CareContactsV2TransformationJobHandlerTest.class);

    private CareContactsV2TransformationJobHandler careContactsV2TransformationJobHandler;

    @Before
    public void setUp() throws Exception {
        careContactsV2TransformationJobHandler = new CareContactsV2TransformationJobHandler();
        XMLUnit.setIgnoreWhitespace(true);
        XMLUnit.setIgnoreComments(true);
        XMLUnit.setIgnoreAttributeOrder(true);
        XMLUnit.setNormalizeWhitespace(true);
    }

    @Test
    public void processTransformationJob() throws Exception {
        TransformationResult transformationResult = careContactsV2TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob("CareContactsV2TransformationJobHandlerTest/input1.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "CareContactsV2TransformationJobHandlerTest/input1_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(3, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationJob_NoCareContactTimePeriod() throws Exception {
        TransformationResult transformationResult = careContactsV2TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob("CareContactsV2TransformationJobHandlerTest/input2.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "CareContactsV2TransformationJobHandlerTest/input2_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(3, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationJob_NoCareContactOrgUnit() throws Exception {
        TransformationResult transformationResult = careContactsV2TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob("CareContactsV2TransformationJobHandlerTest/input3.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "CareContactsV2TransformationJobHandlerTest/input3_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(3, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationJob_NoCareContactStatus() throws Exception {
        TransformationResult transformationResult = careContactsV2TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob("CareContactsV2TransformationJobHandlerTest/input4.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "CareContactsV2TransformationJobHandlerTest/input4_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(2, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationJob_NoHealthcareProfessionalName() throws Exception {
        TransformationResult transformationResult = careContactsV2TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob("CareContactsV2TransformationJobHandlerTest/input5.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "CareContactsV2TransformationJobHandlerTest/input5_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(3, pushJobSpecification.getRelatedValues().size());
    }

}