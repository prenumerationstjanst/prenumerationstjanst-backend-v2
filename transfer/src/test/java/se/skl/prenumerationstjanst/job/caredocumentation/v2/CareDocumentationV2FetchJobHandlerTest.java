package se.skl.prenumerationstjanst.job.caredocumentation.v2;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import se.skl.prenumerationstjanst.job.AbstractApigwFetchJobHandlerTest;
import se.skl.prenumerationstjanst.job.ApigwFetchJobHandler;
import se.skl.prenumerationstjanst.job.FetchJobUriExtractor;
import se.skl.prenumerationstjanst.model.TransferType;

/**
 * @author Martin Samuelsson
 */
@RunWith(MockitoJUnitRunner.class)
public class CareDocumentationV2FetchJobHandlerTest extends AbstractApigwFetchJobHandlerTest {

    @Mock
    private CareDocumentationV2FetchJobUriExtractor fetchJobUriExtractor;

    @InjectMocks
    private CareDocumentationV2FetchJobHandler fetchJobHandler;

    @Override
    protected ApigwFetchJobHandler getFetchJobHandlerUnderTest() {
        return fetchJobHandler;
    }

    @Override
    protected FetchJobUriExtractor getFetchJobUriExtractor() {
        return fetchJobUriExtractor;
    }

    @Override
    protected TransferType getExpectedTransferType() {
        return TransferType.CARE_DOCUMENTATION;
    }

    @Override
    protected TestInput getTwoDocumentsZeroFailedSourceSystemsInput() {
        return TestInput
                .newInstance()
                .withInputXml("CareDocumentationV2FetchJobHandlerTest/getTwoDocumentsZeroFailedSourceSystemsInput_input.xml")
                .addExpectedOutputXml("CareDocumentationV2FetchJobHandlerTest/getTwoDocumentsZeroFailedSourceSystemsInput_output1.xml")
                .addExpectedSourceSystemId("5565594230")
                .addExpectedOutputXml("CareDocumentationV2FetchJobHandlerTest/getTwoDocumentsZeroFailedSourceSystemsInput_output2.xml")
                .addExpectedSourceSystemId("5565594230");
    }

    @Override
    protected TestInput getOneDocumentOneFailedSourceSystemInput() {
        return TestInput
                .newInstance()
                .withInputXml("CareDocumentationV2FetchJobHandlerTest/getOneDocumentOneFailedSourceSystemInput_input.xml")
                .addExpectedOutputXml("CareDocumentationV2FetchJobHandlerTest/getOneDocumentOneFailedSourceSystemInput_output1.xml")
                .addExpectedSourceSystemId("5565594230")
                .addExpectedFailedSourceSystemHsaId("HSAID-FAILED");
    }

    @Override
    protected TestInput getZeroDocumentsTwoFailedSourceSystemsInput() {
        return TestInput
                .newInstance()
                .withInputXml("CareDocumentationV2FetchJobHandlerTest/getZeroDocumentsTwoFailedSourceSystemsInput_input.xml")
                .addExpectedFailedSourceSystemHsaId("HSAID-FAILED")
                .addExpectedFailedSourceSystemHsaId("HSAID-FAILED-2");
    }

    @Override
    protected TestInput getZeroDocumentsZeroFailedSourceSystemsInput() {
        return TestInput
                .newInstance()
                .withInputXml("CareDocumentationV2FetchJobHandlerTest/getZeroDocumentsZeroFailedSourceSystemsInput_input.xml");
    }
}