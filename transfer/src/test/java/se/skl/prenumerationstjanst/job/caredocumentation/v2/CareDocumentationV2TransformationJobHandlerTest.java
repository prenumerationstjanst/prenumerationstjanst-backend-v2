package se.skl.prenumerationstjanst.job.caredocumentation.v2;

import org.custommonkey.xmlunit.*;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.skl.prenumerationstjanst.job.AbstractTransformationJobHandlerTest;
import se.skl.prenumerationstjanst.job.TransformationResult;

import static org.junit.Assert.assertEquals;

/**
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
public class CareDocumentationV2TransformationJobHandlerTest extends AbstractTransformationJobHandlerTest {

    private static final Logger logger = LoggerFactory.getLogger(CareDocumentationV2TransformationJobHandlerTest.class);

    private CareDocumentationV2TransformationJobHandler careDocumentationV2TransformationJobHandler;

    @Before
    public void setUp() throws Exception {
        careDocumentationV2TransformationJobHandler = new CareDocumentationV2TransformationJobHandler();
        XMLUnit.setIgnoreWhitespace(true);
        XMLUnit.setIgnoreComments(true);
        XMLUnit.setIgnoreAttributeOrder(true);
        XMLUnit.setNormalizeWhitespace(true);
    }

    @Test
    public void processTransformationJobUnsigned() throws Exception {
        TransformationResult transformationResult = careDocumentationV2TransformationJobHandler
                .processTransformationJob(createTransformationJob(
                        "CareDocumentationV2TransformationJobHandlerTest/input1.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification, "CareDocumentationV2TransformationJobHandlerTest/input1_output_main1");
        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(2, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationJobWithDissentingOpinion() throws Exception {
        TransformationResult transformationResult = careDocumentationV2TransformationJobHandler
                .processTransformationJob(createTransformationJob(
                        "CareDocumentationV2TransformationJobHandlerTest/input2.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification, "CareDocumentationV2TransformationJobHandlerTest/input2_output_main1");
        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(3, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationJobSigned() throws Exception {
        TransformationResult transformationResult = careDocumentationV2TransformationJobHandler
                .processTransformationJob(createTransformationJob(
                        "CareDocumentationV2TransformationJobHandlerTest/input3.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification, "CareDocumentationV2TransformationJobHandlerTest/input3_output_main1");
        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(2, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationJobNonDocBook() throws Exception {
        TransformationResult transformationResult = careDocumentationV2TransformationJobHandler
                .processTransformationJob(createTransformationJob(
                        "CareDocumentationV2TransformationJobHandlerTest/input4.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification, "CareDocumentationV2TransformationJobHandlerTest/input4_output_main1");
        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(2, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationJobHealthcareProfessionalName() throws Exception {
        TransformationResult transformationResult = careDocumentationV2TransformationJobHandler
                .processTransformationJob(createTransformationJob(
                        "CareDocumentationV2TransformationJobHandlerTest/input5.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification, "CareDocumentationV2TransformationJobHandlerTest/input5_output_main1");
        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(2, pushJobSpecification.getRelatedValues().size());
    }

}