package se.skl.prenumerationstjanst.job.diagnosis.v1;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import se.skl.prenumerationstjanst.job.AbstractApigwFetchJobHandlerTest;
import se.skl.prenumerationstjanst.job.ApigwFetchJobHandler;
import se.skl.prenumerationstjanst.job.FetchJobUriExtractor;
import se.skl.prenumerationstjanst.model.TransferType;

/**
 * @author Martin Samuelsson
 */
@RunWith(MockitoJUnitRunner.class)
public class DiagnosisV1FetchJobHandlerTest extends AbstractApigwFetchJobHandlerTest {

    private static final String RESOURCE_DIR = "DiagnosisV1FetchJobHandlerTest";
    @Mock
    private DiagnosisV1FetchJobUriExtractor fetchJobUriExtractor;

    @InjectMocks
    private DiagnosisV1FetchJobHandler fetchJobHandler;

    @Override
    protected ApigwFetchJobHandler getFetchJobHandlerUnderTest() {
        return fetchJobHandler;
    }

    @Override
    protected FetchJobUriExtractor getFetchJobUriExtractor() {
        return fetchJobUriExtractor;
    }

    @Override
    protected TransferType getExpectedTransferType() {
        return TransferType.DIAGNOSIS;
    }

    @Override
    protected TestInput getTwoDocumentsZeroFailedSourceSystemsInput() {
        return TestInput
                .newInstance()
                .withInputXml(RESOURCE_DIR + "/getTwoDocumentsZeroFailedSourceSystemsInput_input.xml")
                .addExpectedOutputXml(RESOURCE_DIR + "/getTwoDocumentsZeroFailedSourceSystemsInput_output1.xml")
                .addExpectedSourceSystemId("5565594230")
                .addExpectedOutputXml(RESOURCE_DIR + "/getTwoDocumentsZeroFailedSourceSystemsInput_output2.xml")
                .addExpectedSourceSystemId("5565594230");
    }

    @Override
    protected TestInput getOneDocumentOneFailedSourceSystemInput() {
        return TestInput
                .newInstance()
                .withInputXml(RESOURCE_DIR + "/getOneDocumentOneFailedSourceSystemInput_input.xml")
                .addExpectedOutputXml(RESOURCE_DIR + "/getOneDocumentOneFailedSourceSystemInput_output1.xml")
                .addExpectedSourceSystemId("5565594230")
                .addExpectedFailedSourceSystemHsaId("HSAID-FAILED");
    }

    @Override
    protected TestInput getZeroDocumentsTwoFailedSourceSystemsInput() {
        return TestInput
                .newInstance()
                .withInputXml(RESOURCE_DIR + "/getZeroDocumentsTwoFailedSourceSystemsInput_input.xml")
                .addExpectedFailedSourceSystemHsaId("HSAID-FAILED")
                .addExpectedFailedSourceSystemHsaId("HSAID-FAILED-2");
    }

    @Override
    protected TestInput getZeroDocumentsZeroFailedSourceSystemsInput() {
        return TestInput
                .newInstance()
                .withInputXml(RESOURCE_DIR + "/getZeroDocumentsZeroFailedSourceSystemsInput_input.xml");
    }
}