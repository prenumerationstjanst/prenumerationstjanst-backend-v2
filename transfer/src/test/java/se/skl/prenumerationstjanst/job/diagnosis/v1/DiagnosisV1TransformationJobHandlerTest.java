package se.skl.prenumerationstjanst.job.diagnosis.v1;

import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.skl.prenumerationstjanst.job.AbstractTransformationJobHandlerTest;
import se.skl.prenumerationstjanst.job.TransformationResult;

import static org.junit.Assert.assertEquals;

/**
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
public class DiagnosisV1TransformationJobHandlerTest extends AbstractTransformationJobHandlerTest {

    private static final Logger logger = LoggerFactory.getLogger(DiagnosisV1TransformationJobHandlerTest.class);
    private static final String RESOURCE_DIR = "DiagnosisV1TransformationJobHandlerTest";

    private DiagnosisV1TransformationJobHandler diagnosisV1TransformationJobHandler;

    @Before
    public void setUp() throws Exception {
        diagnosisV1TransformationJobHandler = new DiagnosisV1TransformationJobHandler();
        XMLUnit.setIgnoreWhitespace(true);
        XMLUnit.setIgnoreComments(true);
        XMLUnit.setIgnoreAttributeOrder(true);
        XMLUnit.setNormalizeWhitespace(true);
    }

    @Test
    public void processTransformationJob() throws Exception {
        TransformationResult transformationResult = diagnosisV1TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob(RESOURCE_DIR + "/input1.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                RESOURCE_DIR + "/input1_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(4, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationJob_NoHealthcareProfessionalName() throws Exception {
        TransformationResult transformationResult = diagnosisV1TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob(RESOURCE_DIR + "/input2.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                RESOURCE_DIR + "/input2_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(4, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationJob_NoHealthcareProfessionalOrgUnit() throws Exception {
        TransformationResult transformationResult = diagnosisV1TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob(RESOURCE_DIR + "/input3.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                RESOURCE_DIR + "/input3_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(4, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationJob_ChronisDiagnosis() throws Exception {
        TransformationResult transformationResult = diagnosisV1TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob(RESOURCE_DIR + "/input4.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                RESOURCE_DIR + "/input4_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(4, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationJob_Bidiagnos() throws Exception {
        TransformationResult transformationResult = diagnosisV1TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob(RESOURCE_DIR + "/input5.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                RESOURCE_DIR + "/input5_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(4, pushJobSpecification.getRelatedValues().size());
    }

}