package se.skl.prenumerationstjanst.job.laboratoryorderoutcome;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import se.skl.prenumerationstjanst.job.AbstractApigwFetchJobHandlerTest;
import se.skl.prenumerationstjanst.job.ApigwFetchJobHandler;
import se.skl.prenumerationstjanst.job.FetchJobUriExtractor;
import se.skl.prenumerationstjanst.model.TransferType;

/**
 * @author Martin Samuelsson
 */
@RunWith(MockitoJUnitRunner.class)
public class LaboratoryOrderOutcomeV1FetchJobHandlerTest extends AbstractApigwFetchJobHandlerTest {

    @Mock
    private LaboratoryOrderOutcomeV1FetchJobUriExtractor fetchJobUriExtractor;

    @InjectMocks
    private LaboratoryOrderOutcomeV1FetchJobHandler fetchJobHandler;

    @Override
    protected ApigwFetchJobHandler getFetchJobHandlerUnderTest() {
        return fetchJobHandler;
    }

    @Override
    protected FetchJobUriExtractor getFetchJobUriExtractor() {
        return fetchJobUriExtractor;
    }

    @Override
    protected TransferType getExpectedTransferType() {
        return TransferType.LABORATORY_ORDER_OUTCOME;
    }

    @Override
    protected TestInput getTwoDocumentsZeroFailedSourceSystemsInput() {
        return TestInput
                .newInstance()
                .withInputXml("LaboratoryOrderOutcomeV1FetchJobHandlerTest" +
                        "/getTwoDocumentsZeroFailedSourceSystemsInput_input.xml")
                .addExpectedOutputXml
                        ("LaboratoryOrderOutcomeV1FetchJobHandlerTest" +
                                "/getTwoDocumentsZeroFailedSourceSystemsInput_output1.xml")
                .addExpectedSourceSystemId("1-5565594230")
                .addExpectedOutputXml
                        ("LaboratoryOrderOutcomeV1FetchJobHandlerTest" +
                                "/getTwoDocumentsZeroFailedSourceSystemsInput_output2.xml")
                .addExpectedSourceSystemId("2-5565594230");
    }

    @Override
    protected TestInput getOneDocumentOneFailedSourceSystemInput() {
        return TestInput
                .newInstance()
                .withInputXml("LaboratoryOrderOutcomeV1FetchJobHandlerTest" +
                        "/getOneDocumentOneFailedSourceSystemInput_input.xml")
                .addExpectedOutputXml
                        ("LaboratoryOrderOutcomeV1FetchJobHandlerTest" +
                                "/getOneDocumentOneFailedSourceSystemInput_output1.xml")
                .addExpectedSourceSystemId("5565594230")
                .addExpectedFailedSourceSystemHsaId("HSAID-FAILED");
    }

    @Override
    protected TestInput getZeroDocumentsTwoFailedSourceSystemsInput() {
        return TestInput
                .newInstance()
                .withInputXml("LaboratoryOrderOutcomeV1FetchJobHandlerTest" +
                        "/getZeroDocumentsTwoFailedSourceSystemsInput_input.xml")
                .addExpectedFailedSourceSystemHsaId("HSAID-FAILED")
                .addExpectedFailedSourceSystemHsaId("HSAID-FAILED-2");

    }

    @Override
    protected TestInput getZeroDocumentsZeroFailedSourceSystemsInput() {
        return TestInput
                .newInstance()
                .withInputXml("LaboratoryOrderOutcomeV1FetchJobHandlerTest" +
                        "/getZeroDocumentsZeroFailedSourceSystemsInput_input.xml");
    }
}