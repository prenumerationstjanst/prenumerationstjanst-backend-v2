package se.skl.prenumerationstjanst.job.laboratoryorderoutcome;

import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.skl.prenumerationstjanst.job.AbstractTransformationJobHandlerTest;
import se.skl.prenumerationstjanst.job.TransformationResult;
import se.skl.prenumerationstjanst.model.TransferType;

import static org.junit.Assert.assertEquals;

/**
 * @author Martin Samuelsson
 */
public class LaboratoryOrderOutcomeV1TransformationJobHandlerTest extends AbstractTransformationJobHandlerTest {

    private static final Logger logger = LoggerFactory.getLogger(LaboratoryOrderOutcomeV1TransformationJobHandlerTest
            .class);

    private LaboratoryOrderOutcomeV1TransformationJobHandler laboratoryOrderOutcomeV1TransformationJobHandler;

    @Before
    public void setUp() throws Exception {
        laboratoryOrderOutcomeV1TransformationJobHandler = new LaboratoryOrderOutcomeV1TransformationJobHandler();
        XMLUnit.setIgnoreWhitespace(true);
        XMLUnit.setIgnoreComments(true);
        XMLUnit.setIgnoreAttributeOrder(true);
        XMLUnit.setNormalizeWhitespace(true);
    }

    @Test
    public void processTransformationJob() throws Exception {
        TransformationResult transformationResult = laboratoryOrderOutcomeV1TransformationJobHandler
                .processTransformationJob(createTransformationJob(
                        "LaboratoryOrderOutcomeV1TransformationJobHandlerTest/input1.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "LaboratoryOrderOutcomeV1TransformationJobHandlerTest/input1_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(3, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationJobDefinitivsvar() throws Exception {
        TransformationResult transformationResult = laboratoryOrderOutcomeV1TransformationJobHandler
                .processTransformationJob(createTransformationJob(
                        "LaboratoryOrderOutcomeV1TransformationJobHandlerTest/input2.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "LaboratoryOrderOutcomeV1TransformationJobHandlerTest/input2_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(1, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationJobTillaggsvar() throws Exception {
        TransformationResult transformationResult = laboratoryOrderOutcomeV1TransformationJobHandler
                .processTransformationJob(createTransformationJob(
                        "LaboratoryOrderOutcomeV1TransformationJobHandlerTest/input3.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "LaboratoryOrderOutcomeV1TransformationJobHandlerTest/input3_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(1, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationJobPreferEndAnalysisTime() throws Exception {
        TransformationResult transformationResult = laboratoryOrderOutcomeV1TransformationJobHandler
                .processTransformationJob(createTransformationJob(
                        "LaboratoryOrderOutcomeV1TransformationJobHandlerTest/input4.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "LaboratoryOrderOutcomeV1TransformationJobHandlerTest/input4_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(1, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationJobPreferOnlyStartAnalysisTime() throws Exception {
        TransformationResult transformationResult = laboratoryOrderOutcomeV1TransformationJobHandler
                .processTransformationJob(createTransformationJob(
                        "LaboratoryOrderOutcomeV1TransformationJobHandlerTest/input5.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "LaboratoryOrderOutcomeV1TransformationJobHandlerTest/input5_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(1, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationJobAnalysisText() throws Exception {
        TransformationResult transformationResult = laboratoryOrderOutcomeV1TransformationJobHandler
                .processTransformationJob(createTransformationJob(
                        "LaboratoryOrderOutcomeV1TransformationJobHandlerTest/input6.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "LaboratoryOrderOutcomeV1TransformationJobHandlerTest/input6_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(1, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationJobVidimerat() throws Exception {
        TransformationResult transformationResult = laboratoryOrderOutcomeV1TransformationJobHandler
                .processTransformationJob(createTransformationJob(
                        "LaboratoryOrderOutcomeV1TransformationJobHandlerTest/input7.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "LaboratoryOrderOutcomeV1TransformationJobHandlerTest/input7_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(1, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationNoteWithoutAnalysisComment() throws Exception {
        TransformationResult transformationResult = laboratoryOrderOutcomeV1TransformationJobHandler
                .processTransformationJob(createTransformationJob(
                        "LaboratoryOrderOutcomeV1TransformationJobHandlerTest/input8.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "LaboratoryOrderOutcomeV1TransformationJobHandlerTest/input8_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(1, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationNoteWithoutOutcomeDescription() throws Exception {
        TransformationResult transformationResult = laboratoryOrderOutcomeV1TransformationJobHandler
                .processTransformationJob(createTransformationJob(
                        "LaboratoryOrderOutcomeV1TransformationJobHandlerTest/input9.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "LaboratoryOrderOutcomeV1TransformationJobHandlerTest/input9_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(1, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void processTransformationPathologicalFlag() throws Exception {
        TransformationResult transformationResult = laboratoryOrderOutcomeV1TransformationJobHandler
                .processTransformationJob(createTransformationJob(
                        "LaboratoryOrderOutcomeV1TransformationJobHandlerTest/input10.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "LaboratoryOrderOutcomeV1TransformationJobHandlerTest/input10_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(1, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void version() throws Exception {
        assertEquals(1, laboratoryOrderOutcomeV1TransformationJobHandler.version());
    }

    @Test
    public void transferType() throws Exception {
        assertEquals(TransferType.LABORATORY_ORDER_OUTCOME,
                laboratoryOrderOutcomeV1TransformationJobHandler.transferType());
    }

}