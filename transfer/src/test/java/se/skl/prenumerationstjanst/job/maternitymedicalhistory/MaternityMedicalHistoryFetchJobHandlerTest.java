package se.skl.prenumerationstjanst.job.maternitymedicalhistory;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.skl.prenumerationstjanst.job.AbstractApigwFetchJobHandlerTest;
import se.skl.prenumerationstjanst.job.ApigwFetchJobHandler;
import se.skl.prenumerationstjanst.job.FetchJobUriExtractor;
import se.skl.prenumerationstjanst.model.TransferType;

/**
 * @author Martin Samuelsson
 */
@RunWith(MockitoJUnitRunner.class)
public class MaternityMedicalHistoryFetchJobHandlerTest extends AbstractApigwFetchJobHandlerTest {

    private static final Logger logger = LoggerFactory.getLogger(MaternityMedicalHistoryFetchJobHandlerTest.class);

    @Mock
    private MaternityMedicalHistoryUriExtractor fetchJobUriExtractor;

    @InjectMocks
    private MaternityMedicalHistoryFetchJobHandler fetchJobHandler;

    @Override
    protected ApigwFetchJobHandler getFetchJobHandlerUnderTest() {
        return fetchJobHandler;
    }

    @Override
    protected FetchJobUriExtractor getFetchJobUriExtractor() {
        return fetchJobUriExtractor;
    }

    @Override
    protected TransferType getExpectedTransferType() {
        return TransferType.MATERNITY_MEDICAL_HISTORY;
    }

    @Override
    protected TestInput getTwoDocumentsZeroFailedSourceSystemsInput() {
        return TestInput
                .newInstance()
                .withInputXml("MaternityMedicalHistoryFetchJobHandlerTest/getTwoDocumentsZeroFailedSourceSystemsInput_input.xml")
                .addExpectedOutputXml("MaternityMedicalHistoryFetchJobHandlerTest/getTwoDocumentsZeroFailedSourceSystemsInput_output1.xml")
                .addExpectedSourceSystemId("1-5565594230")
                .addExpectedOutputXml("MaternityMedicalHistoryFetchJobHandlerTest/getTwoDocumentsZeroFailedSourceSystemsInput_output2.xml")
                .addExpectedSourceSystemId("2-5565594230");
    }

    @Override
    protected TestInput getOneDocumentOneFailedSourceSystemInput() {
        return TestInput
                .newInstance()
                .withInputXml("MaternityMedicalHistoryFetchJobHandlerTest/getOneDocumentOneFailedSourceSystemInput_input.xml")
                .addExpectedOutputXml("MaternityMedicalHistoryFetchJobHandlerTest/getOneDocumentOneFailedSourceSystemInput_output1.xml")
                .addExpectedSourceSystemId("5565594230")
                .addExpectedFailedSourceSystemHsaId("HSAID-FAILED");
    }

    @Override
    protected TestInput getZeroDocumentsTwoFailedSourceSystemsInput() {
        return TestInput
                .newInstance()
                .withInputXml("MaternityMedicalHistoryFetchJobHandlerTest/getZeroDocumentsTwoFailedSourceSystemsInput_input.xml")
                .addExpectedFailedSourceSystemHsaId("HSAID-FAILED")
                .addExpectedFailedSourceSystemHsaId("HSAID-FAILED-2");
    }

    @Override
    protected TestInput getZeroDocumentsZeroFailedSourceSystemsInput() {
        return TestInput
                .newInstance()
                .withInputXml("MaternityMedicalHistoryFetchJobHandlerTest/getZeroDocumentsZeroFailedSourceSystemsInput_input.xml");
    }
}