package se.skl.prenumerationstjanst.job.vaccinationhistory;

import com.chbase.methods.jaxb.SimpleRequestTemplate;
import com.chbase.methods.jaxb.putthings2.request.PutThings2Request;
import com.chbase.methods.jaxb.putthings2.response.PutThings2Response;
import com.chbase.thing.oxm.jaxb.thing.ThingKey;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.skl.prenumerationstjanst.chbase.CHBaseRequestTemplateFactory;
import se.skl.prenumerationstjanst.dto.*;
import se.skl.prenumerationstjanst.exception.transfer.UnrecoverablePushFailedException;
import se.skl.prenumerationstjanst.job.PushResult;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.when;

/**
 * @author Martin Samuelsson
 */
@RunWith(MockitoJUnitRunner.class)
public class VaccinationHistoryPushJobHandlerTest {

    private static final Logger logger = LoggerFactory.getLogger(VaccinationHistoryPushJobHandlerTest.class);

    @Mock
    private CHBaseRequestTemplateFactory chBaseRequestTemplateFactory;

    @Mock
    private SimpleRequestTemplate simpleRequestTemplate;

    @InjectMocks
    VaccinationHistoryPushJobHandler vaccinationHistoryPushJobHandler;

    @Before
    public void setUp() throws Exception {
        when(chBaseRequestTemplateFactory.getJaxbSimpleRequestTemplate(anyString(), anyString()))
                .thenReturn(simpleRequestTemplate);
    }

    @Test
    public void testProcessPushJobNoRelated() throws Exception {
        PushJobDTO pushJobDTO = PushJobDTO.create()
                .withTransferJob(TransferJobDTO.create())
                .withValue(resourceFileToString("VaccinationHistoryPushJobHandlerTest/input1.xml"));
        PutThings2Response value = new PutThings2Response();
        ThingKey thingKey = new ThingKey();
        thingKey.setValue("MEPMEP");
        value.getThingId().add(thingKey);
        when(simpleRequestTemplate.makeRequest(any(PutThings2Request.class))).thenReturn(value);
        PushResult pushResult = vaccinationHistoryPushJobHandler.processPushJob(pushJobDTO);
        assertTrue(pushResult.getRelatedItems().isEmpty());
        logger.debug("pushResult: {}", pushResult);
    }

    @Test
    public void testProcessPushJobTwoRelated() throws Exception {
        PushJobDTO pushJobDTO = PushJobDTO.create()
                .withTransferJob(TransferJobDTO.create())
                .withValue(resourceFileToString("VaccinationHistoryPushJobHandlerTest/input2_value.xml"));

        List<String> relatedValues = new ArrayList<>();
        relatedValues.add(resourceFileToString("VaccinationHistoryPushJobHandlerTest/input2_rel1.xml"));
        relatedValues.add(resourceFileToString("VaccinationHistoryPushJobHandlerTest/input2_rel2.xml"));
        pushJobDTO = pushJobDTO.withRelatedValues(relatedValues);

        PutThings2Response value = new PutThings2Response();
        ThingKey thingKey = new ThingKey();
        thingKey.setValue("TC2");
        value.getThingId().add(thingKey);
        when(simpleRequestTemplate.makeRequest(any(PutThings2Request.class))).thenReturn(value);
        PushResult pushResult = vaccinationHistoryPushJobHandler.processPushJob(pushJobDTO);
        assertEquals(2, pushResult.getRelatedItems().size());
        //TODO: xmlunit
        pushResult.getRelatedItems().forEach(rv -> assertTrue(rv.contains("<thing-id>TC2</thing-id>")));
        logger.debug("pushResult: {}", pushResult);
    }

    @Test(expected = UnrecoverablePushFailedException.class)
    public void testProcessPushJobBadInput() throws Exception {
        PushJobDTO pushJobDTO = PushJobDTO.create()
                .withTransferJob(TransferJobDTO.create())
                .withValue("<xml><mep /></xml>");

        vaccinationHistoryPushJobHandler.processPushJob(pushJobDTO);
    }



    private String resourceFileToString(String filename) throws URISyntaxException, IOException {
        URL resource = getClass().getClassLoader().getResource(filename);
        @SuppressWarnings("ConstantConditions")
        Path path = Paths.get(resource.toURI());
        return new String(Files.readAllBytes(path), "UTF8");
    }
}