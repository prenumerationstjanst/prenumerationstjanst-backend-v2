package se.skl.prenumerationstjanst.job.vaccinationhistory.v1;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.skl.prenumerationstjanst.job.AbstractApigwFetchJobHandlerTest;
import se.skl.prenumerationstjanst.job.ApigwFetchJobHandler;
import se.skl.prenumerationstjanst.job.FetchJobUriExtractor;
import se.skl.prenumerationstjanst.model.TransferType;

/**
 * @author Martin Samuelsson
 */
@RunWith(MockitoJUnitRunner.class)
public class VaccinationHistoryV1FetchJobHandlerTest extends AbstractApigwFetchJobHandlerTest {

    private static final Logger logger = LoggerFactory.getLogger(VaccinationHistoryV1FetchJobHandlerTest.class);

    @Mock
    private VaccinationHistoryV1FetchJobUriExtractor fetchJobUriExtractor;

    @InjectMocks
    private VaccinationHistoryV1FetchJobHandler fetchJobHandler;

    @Override
    protected ApigwFetchJobHandler getFetchJobHandlerUnderTest() {
        return fetchJobHandler;
    }

    @Override
    protected FetchJobUriExtractor getFetchJobUriExtractor() {
        return fetchJobUriExtractor;
    }

    @Override
    protected TransferType getExpectedTransferType() {
        return TransferType.VACCINATION_HISTORY;
    }

    @Override
    protected TestInput getTwoDocumentsZeroFailedSourceSystemsInput() {
        return TestInput
                .newInstance()
                .withInputXml("VaccinationHistoryV1FetchJobHandlerTest/getTwoDocumentsZeroFailedSourceSystemsInput_input.xml")
                .addExpectedOutputXml("VaccinationHistoryV1FetchJobHandlerTest/getTwoDocumentsZeroFailedSourceSystemsInput_output1.xml")
                .addExpectedSourceSystemId("SS1-5565594230")
                .addExpectedOutputXml("VaccinationHistoryV1FetchJobHandlerTest/getTwoDocumentsZeroFailedSourceSystemsInput_output2.xml")
                .addExpectedSourceSystemId("SS2-5565594230");
    }

    @Override
    protected TestInput getOneDocumentOneFailedSourceSystemInput() {
        return TestInput
                .newInstance()
                .withInputXml("VaccinationHistoryV1FetchJobHandlerTest/getOneDocumentOneFailedSourceSystemInput_input.xml")
                .addExpectedOutputXml("VaccinationHistoryV1FetchJobHandlerTest/getOneDocumentOneFailedSourceSystemInput_output1.xml")
                .addExpectedSourceSystemId("5565594230")
                .addExpectedFailedSourceSystemHsaId("HSA-ID-FAIL-1");
    }

    @Override
    protected TestInput getZeroDocumentsTwoFailedSourceSystemsInput() {
        return TestInput
                .newInstance()
                .withInputXml("VaccinationHistoryV1FetchJobHandlerTest/getZeroDocumentsTwoFailedSourceSystemsInput_input.xml")
                .addExpectedFailedSourceSystemHsaId("HSA-ID-FAIL-1")
                .addExpectedFailedSourceSystemHsaId("HSA-ID-FAIL-2");
    }

    @Override
    protected TestInput getZeroDocumentsZeroFailedSourceSystemsInput() {
        return TestInput
                .newInstance()
                .withInputXml("VaccinationHistoryV1FetchJobHandlerTest/getZeroDocumentsZeroFailedSourceSystemsInput_input.xml");
    }
}