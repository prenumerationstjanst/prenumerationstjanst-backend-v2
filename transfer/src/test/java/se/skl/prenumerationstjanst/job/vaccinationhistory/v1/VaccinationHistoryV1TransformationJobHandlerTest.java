package se.skl.prenumerationstjanst.job.vaccinationhistory.v1;

import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.skl.prenumerationstjanst.job.AbstractTransformationJobHandlerTest;
import se.skl.prenumerationstjanst.job.TransformationResult;

import static org.junit.Assert.assertEquals;

/**
 * @author Martin Samuelsson
 */
public class VaccinationHistoryV1TransformationJobHandlerTest extends AbstractTransformationJobHandlerTest {

    private static final Logger logger = LoggerFactory.getLogger(VaccinationHistoryV1TransformationJobHandlerTest.class);

    private VaccinationHistoryV1TransformationJobHandler vaccinationHistoryV1TransformationJobHandler;

    @Before
    public void setUp() throws Exception {
        vaccinationHistoryV1TransformationJobHandler = new VaccinationHistoryV1TransformationJobHandler();
        XMLUnit.setIgnoreWhitespace(true);
        XMLUnit.setIgnoreComments(true);
        XMLUnit.setIgnoreAttributeOrder(true);
        XMLUnit.setNormalizeWhitespace(true);
    }

    @Test
    public void testProcessTransformationJob() throws Exception {
        TransformationResult transformationResult = vaccinationHistoryV1TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob("VaccinationHistoryV1TransformationJobHandlerTest/input1.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "VaccinationHistoryV1TransformationJobHandlerTest/input1_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(6, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void testProcessTransformationJobNoNameOrTypeOrTargetDisease() throws Exception {
        TransformationResult transformationResult = vaccinationHistoryV1TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob("VaccinationHistoryV1TransformationJobHandlerTest/input2.xml"));

        assertEquals(0, transformationResult.getPushJobSpecifications().size());
    }

    @Test
    public void testProcessTransformationName() throws Exception {
        TransformationResult transformationResult = vaccinationHistoryV1TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob("VaccinationHistoryV1TransformationJobHandlerTest/input3.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "VaccinationHistoryV1TransformationJobHandlerTest/input3_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(1, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void testProcessTransformationPreferPerformerForAdministrator() throws Exception {
        TransformationResult transformationResult = vaccinationHistoryV1TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob("VaccinationHistoryV1TransformationJobHandlerTest/input4.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "VaccinationHistoryV1TransformationJobHandlerTest/input4_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(1, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void testProcessTransformationPrescriberPersonForAdministrator() throws Exception {
        TransformationResult transformationResult = vaccinationHistoryV1TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob("VaccinationHistoryV1TransformationJobHandlerTest/input5.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "VaccinationHistoryV1TransformationJobHandlerTest/input5_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(1, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void testProcessTransformationFullDosage() throws Exception {
        TransformationResult transformationResult = vaccinationHistoryV1TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob("VaccinationHistoryV1TransformationJobHandlerTest/input6.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "VaccinationHistoryV1TransformationJobHandlerTest/input6_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(1, pushJobSpecification.getRelatedValues().size());
    }

}