package se.skl.prenumerationstjanst.job.vaccinationhistory.v2;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import se.skl.prenumerationstjanst.job.AbstractApigwFetchJobHandlerTest;
import se.skl.prenumerationstjanst.job.ApigwFetchJobHandler;
import se.skl.prenumerationstjanst.job.FetchJobUriExtractor;
import se.skl.prenumerationstjanst.model.TransferType;

/**
 * @author Martin Samuelsson
 */
@RunWith(MockitoJUnitRunner.class)
public class VaccinationHistoryV2FetchJobHandlerTest extends AbstractApigwFetchJobHandlerTest {

    @Mock
    private VaccinationHistoryV2FetchJobUriExtractor fetchJobUriExtractor;

    @InjectMocks
    private VaccinationHistoryV2FetchJobHandler fetchJobHandler;

    @Override
    protected ApigwFetchJobHandler getFetchJobHandlerUnderTest() {
        return fetchJobHandler;
    }

    @Override
    protected FetchJobUriExtractor getFetchJobUriExtractor() {
        return fetchJobUriExtractor;
    }

    @Override
    protected TransferType getExpectedTransferType() {
        return TransferType.VACCINATION_HISTORY;
    }

    @Override
    protected TestInput getTwoDocumentsZeroFailedSourceSystemsInput() {
        return TestInput
                .newInstance()
                .withInputXml("VaccinationHistoryV2FetchJobHandlerTest/getTwoDocumentsZeroFailedSourceSystemsInput_input.xml")
                .addExpectedOutputXml("VaccinationHistoryV2FetchJobHandlerTest/getTwoDocumentsZeroFailedSourceSystemsInput_output1.xml")
                .addExpectedSourceSystemId("1-HSA-VACC-V2-SOSY")
                .addExpectedOutputXml("VaccinationHistoryV2FetchJobHandlerTest/getTwoDocumentsZeroFailedSourceSystemsInput_output2.xml")
                .addExpectedSourceSystemId("2-HSA-VACC-V2-SOSY");
    }

    @Override
    protected TestInput getOneDocumentOneFailedSourceSystemInput() {
        return TestInput
                .newInstance()
                .withInputXml("VaccinationHistoryV2FetchJobHandlerTest/getOneDocumentOneFailedSourceSystemInput_input.xml")
                .addExpectedOutputXml("VaccinationHistoryV2FetchJobHandlerTest/getOneDocumentOneFailedSourceSystemInput_output1.xml")
                .addExpectedSourceSystemId("HSA-VACC-V2-SOSY")
                .addExpectedFailedSourceSystemHsaId("HSA-ID-FAIL-1");
    }

    @Override
    protected TestInput getZeroDocumentsTwoFailedSourceSystemsInput() {
        return TestInput
                .newInstance()
                .withInputXml("VaccinationHistoryV2FetchJobHandlerTest/getZeroDocumentsTwoFailedSourceSystemsInput_input.xml")
                .addExpectedFailedSourceSystemHsaId("HSA-ID-FAIL-1")
                .addExpectedFailedSourceSystemHsaId("HSA-ID-FAIL-2");
    }

    @Override
    protected TestInput getZeroDocumentsZeroFailedSourceSystemsInput() {
        return TestInput
                .newInstance()
                .withInputXml("VaccinationHistoryV2FetchJobHandlerTest/getZeroDocumentsZeroFailedSourceSystemsInput_input.xml");
    }
}