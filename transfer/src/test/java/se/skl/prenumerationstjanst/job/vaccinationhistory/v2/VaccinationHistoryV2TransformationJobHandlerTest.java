package se.skl.prenumerationstjanst.job.vaccinationhistory.v2;

import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.skl.prenumerationstjanst.job.AbstractTransformationJobHandlerTest;
import se.skl.prenumerationstjanst.job.TransformationResult;

import static org.junit.Assert.*;

/**
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
public class VaccinationHistoryV2TransformationJobHandlerTest extends AbstractTransformationJobHandlerTest {

    private static final Logger logger = LoggerFactory.getLogger(VaccinationHistoryV2TransformationJobHandlerTest
            .class);

    private VaccinationHistoryV2TransformationJobHandler vaccinationHistoryV2TransformationJobHandler;

    @Before
    public void setUp() throws Exception {
        vaccinationHistoryV2TransformationJobHandler = new VaccinationHistoryV2TransformationJobHandler();
        XMLUnit.setIgnoreWhitespace(true);
        XMLUnit.setIgnoreComments(true);
        XMLUnit.setIgnoreAttributeOrder(true);
        XMLUnit.setNormalizeWhitespace(true);
    }

    @Test
    public void testProcessTransformationJob() throws Exception {
        TransformationResult transformationResult = vaccinationHistoryV2TransformationJobHandler
                .processTransformationJob(
                createTransformationJob("VaccinationHistoryV2TransformationJobHandlerTest/input1.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "VaccinationHistoryV2TransformationJobHandlerTest/input1_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(6, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void testProcessTransformationJobNoNameOrTypeOrTargetDisease() throws Exception {
        TransformationResult transformationResult = vaccinationHistoryV2TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob("VaccinationHistoryV2TransformationJobHandlerTest/input2.xml"));

        assertEquals(0, transformationResult.getPushJobSpecifications().size());
    }

    @Test
    public void testProcessTransformationName() throws Exception {
        TransformationResult transformationResult = vaccinationHistoryV2TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob("VaccinationHistoryV2TransformationJobHandlerTest/input3.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "VaccinationHistoryV2TransformationJobHandlerTest/input3_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(1, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void testProcessTransformationPreferPerformerForAdministrator() throws Exception {
        TransformationResult transformationResult = vaccinationHistoryV2TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob("VaccinationHistoryV2TransformationJobHandlerTest/input4.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "VaccinationHistoryV2TransformationJobHandlerTest/input4_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(1, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void testProcessTransformationPrescriberPersonForAdministrator() throws Exception {
        TransformationResult transformationResult = vaccinationHistoryV2TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob("VaccinationHistoryV2TransformationJobHandlerTest/input5.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "VaccinationHistoryV2TransformationJobHandlerTest/input5_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(1, pushJobSpecification.getRelatedValues().size());
    }

    @Test
    public void testProcessTransformationFullDosage() throws Exception {
        TransformationResult transformationResult = vaccinationHistoryV2TransformationJobHandler
                .processTransformationJob(
                        createTransformationJob("VaccinationHistoryV2TransformationJobHandlerTest/input6.xml"));

        TransformationResult.PushJobSpecification pushJobSpecification = transformationResult
                .getPushJobSpecifications().get(0);

        assertPushJobSpecification(pushJobSpecification,
                "VaccinationHistoryV2TransformationJobHandlerTest/input6_output_main1");

        assertEquals(1, transformationResult.getPushJobSpecifications().size());
        assertEquals(1, pushJobSpecification.getRelatedValues().size());
    }




}