package se.skl.prenumerationstjanst.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import se.skl.prenumerationstjanst.PrenumerationstjanstTransferApplication;
import se.skl.prenumerationstjanst.dto.FetchJobDTO;
import se.skl.prenumerationstjanst.dto.PushJobDTO;
import se.skl.prenumerationstjanst.dto.SubscriptionDTO;
import se.skl.prenumerationstjanst.dto.TransferJobDTO;
import se.skl.prenumerationstjanst.model.PushLog;
import se.skl.prenumerationstjanst.model.Subscription;
import se.skl.prenumerationstjanst.model.TransferType;
import se.skl.prenumerationstjanst.repository.PushLogRepository;
import se.skl.prenumerationstjanst.repository.SubscriptionRepository;

import java.time.LocalDateTime;

import static org.junit.Assert.*;

/**
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = PrenumerationstjanstTransferApplication.class)
@DirtiesContext
@ActiveProfiles("test")
@Transactional
public class PushLogServiceITest {

    @Autowired
    private PushLogRepository pushLogRepository;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private PushLogService pushLogService;

    @Autowired
    private HashService hashService;

    public static final String SUBSCRIPTION_ID = "subId";
    public static final String PUSH_JOB_ID = "pjId";
    public static final String SOURCE_ID = "sourceId";
    public static final int SOURCE_VERSION = 1;
    public static final LocalDateTime SOURCE_FETCH_TIME = LocalDateTime.now();

    @Before
    public void setUp() throws Exception {
        subscriptionRepository.deleteAll();
        pushLogRepository.deleteAll();
    }

    @After
    public void cleanUp() throws Exception {
        subscriptionRepository.deleteAll();
        pushLogRepository.deleteAll();
    }

    @Test
    public void testLogSuccessfulPush() throws Exception {
        assertEquals(0, pushLogRepository.count());
        String subscriptionId = createAndInsertSubscription().getId();
        String pushValue = "<handle><foo>bar</foo></handle>";
        PushJobDTO pushJobDTO = PushJobDTO.create()
                .withId(PUSH_JOB_ID)
                .withTransferJob(TransferJobDTO.create()
                        .withSubscriptionId(subscriptionId)
                        .withFetchJob(
                                FetchJobDTO.create()
                                        .withFetchFinishTime(SOURCE_FETCH_TIME)
                        )
                )
                .withValue(pushValue)
                .withSourceId(SOURCE_ID)
                .withSourceVersion(SOURCE_VERSION);
        pushLogService.logSuccessfulPush(pushJobDTO);
        assertEquals(1, pushLogRepository.count());
        PushLog pushLog = pushLogRepository.findAll().iterator().next();
        assertNotNull(pushLog.getHash());
        assertNotEquals(pushLog.getHash(), pushJobDTO.getValue());
        assertEquals(pushLog.getHash(), hashService.calculateHash(pushValue));
        assertEquals(1, subscriptionRepository.count());
        Subscription sub = subscriptionRepository.findAll().iterator().next();
        assertEquals(sub.getId(), pushLog.getSubscription().getId());
    }

    @Test
    public void testCanPush_NoPrevious() throws Exception {
        String pushValue = "<handle><foo>bar</foo></handle>";
        PushJobDTO pushJobDTO = PushJobDTO.create()
                .withId(PUSH_JOB_ID)
                .withTransferJob(TransferJobDTO.create()
                        .withSubscriptionId(SUBSCRIPTION_ID)
                        .withFetchJob(
                                FetchJobDTO.create()
                                        .withFetchFinishTime(SOURCE_FETCH_TIME)
                        )
                )
                .withValue(pushValue)
                .withSourceId(SOURCE_ID)
                .withSourceVersion(SOURCE_VERSION);
        assertTrue(pushLogService.canPush(pushJobDTO));
    }

    @Test
    public void testCanPush_TooOld() throws Exception {
        String pushValue = "<handle><foo>bar</foo></handle>";
        String pushValueHash = hashService.calculateHash(pushValue);
        String subscriptionId = createAndInsertSubscription().getId();
        createAndInsertPushLog(subscriptionId, SOURCE_ID, SOURCE_FETCH_TIME, SOURCE_VERSION, pushValueHash);
        PushJobDTO pushJobDTO = PushJobDTO.create()
                .withId(PUSH_JOB_ID)
                .withTransferJob(TransferJobDTO.create()
                        .withSubscriptionId(subscriptionId)
                        .withFetchJob(
                                FetchJobDTO.create()
                                        .withFetchFinishTime(SOURCE_FETCH_TIME.minusSeconds(1))
                        )
                )
                .withValue(pushValue)
                .withSourceId(SOURCE_ID)
                .withSourceVersion(SOURCE_VERSION);
        assertFalse(pushLogService.canPush(pushJobDTO));
    }

    @Test
    public void testCanPush_NewerWithSameHash() throws Exception {
        String pushValue = "<handle><foo>bar</foo></handle>";
        String pushValueHash = hashService.calculateHash(pushValue);
        String subscriptionId = createAndInsertSubscription().getId();
        createAndInsertPushLog(subscriptionId, SOURCE_ID, SOURCE_FETCH_TIME, SOURCE_VERSION, pushValueHash);
        PushJobDTO pushJobDTO = PushJobDTO.create()
                .withId(PUSH_JOB_ID)
                .withTransferJob(TransferJobDTO.create()
                        .withSubscriptionId(subscriptionId)
                        .withFetchJob(
                                FetchJobDTO.create()
                                        .withFetchFinishTime(SOURCE_FETCH_TIME.plusSeconds(1))
                        )
                )
                .withValue(pushValue)
                .withSourceId(SOURCE_ID)
                .withSourceVersion(SOURCE_VERSION);
        assertFalse(pushLogService.canPush(pushJobDTO));
    }

    @Test
    public void testCanPush_NewerWithOtherHash() throws Exception {
        String pushValue = "<handle><foo>bar</foo></handle>";
        String pushValueHash = hashService.calculateHash(pushValue);
        String subscriptionId = createAndInsertSubscription().getId();
        createAndInsertPushLog(subscriptionId, SOURCE_ID, SOURCE_FETCH_TIME, SOURCE_VERSION, pushValueHash);
        PushJobDTO pushJobDTO = PushJobDTO.create()
                .withId(PUSH_JOB_ID)
                .withTransferJob(TransferJobDTO.create()
                        .withSubscriptionId(subscriptionId)
                        .withFetchJob(
                                FetchJobDTO.create()
                                        .withFetchFinishTime(SOURCE_FETCH_TIME.plusSeconds(1))
                        )
                )
                .withValue("<root>" + pushValue + pushValue + "</root>")
                .withSourceId(SOURCE_ID)
                .withSourceVersion(SOURCE_VERSION);
        assertTrue(pushLogService.canPush(pushJobDTO));
    }

    private Subscription createAndInsertSubscription() {
        return subscriptionRepository.save(new Subscription(null, TransferType.VACCINATION_HISTORY));
    }

    private PushLog createAndInsertPushLog(String subscriptionId, String sourceId, LocalDateTime sourceFetchTime,
                                           int sourceVersion, String hash) {
        Subscription sub = subscriptionRepository.findOne(subscriptionId);
        PushLog pushLog = pushLogRepository.save(new PushLog(sub, sourceId, sourceFetchTime, sourceVersion, hash));
        sub.getPushLogs().add(pushLog);
        subscriptionRepository.save(sub);
        return pushLog;
    }
}