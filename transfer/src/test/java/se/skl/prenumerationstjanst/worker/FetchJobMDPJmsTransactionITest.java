package se.skl.prenumerationstjanst.worker;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.stubbing.StubberImpl;
import org.mockito.stubbing.Stubber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import se.skl.prenumerationstjanst.JmsTestConfig;
import se.skl.prenumerationstjanst.PrenumerationstjanstConstants;
import se.skl.prenumerationstjanst.PrenumerationstjanstTransferApplication;
import se.skl.prenumerationstjanst.config.TestConfiguration;
import se.skl.prenumerationstjanst.dto.FetchJobDTO;
import se.skl.prenumerationstjanst.dto.TransferJobDTO;
import se.skl.prenumerationstjanst.job.FetchJobHandler;
import se.skl.prenumerationstjanst.job.FetchResult;
import se.skl.prenumerationstjanst.job.FetchResultDocument;
import se.skl.prenumerationstjanst.model.*;
import se.skl.prenumerationstjanst.repository.*;
import se.skl.prenumerationstjanst.service.TransferService;
import se.skl.prenumerationstjanst.service.TransformationService;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

/**
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {
        PrenumerationstjanstTransferApplication.class,
        JmsTestConfig.class,
        FetchJobMDPJmsTransactionITest.FetchJobMDPJmsTransactionTestConfiguration.class
})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ActiveProfiles({"test", "jmstest"})
public class FetchJobMDPJmsTransactionITest {

    public static final String USER_CRN = "191212121212";

    private static final Logger logger = LoggerFactory.getLogger(FetchJobMDPJmsTransactionITest.class);

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private TransformationService transformationService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserContextRepository userContextRepository;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private TransferJobRepository transferJobRepository;

    @Autowired
    private FetchJobRepository fetchJobRepository;

    @Autowired
    private PushJobRepository pushJobRepository;

    @Autowired
    private TransferService transferService;

    @Resource
    private Map<TransferType, List<FetchJobHandler>> fetchJobHandlers;

    private FetchJobHandler v1FetchJobHandler;

    private FetchJobHandler v2FetchJobHandler;

    private UserContext savedUserContext;

    private Subscription subscription;

    @Before
    public void setUp() throws Exception {
        v1FetchJobHandler = Mockito.mock(FetchJobHandler.class);
        v2FetchJobHandler = Mockito.mock(FetchJobHandler.class);
        UserContext userContext = new UserContext(userRepository.save(new User(USER_CRN, null)),
                USER_CRN, "phkRecordId");
        savedUserContext = userContextRepository.save(userContext);
        subscription = subscriptionRepository.save(new Subscription(savedUserContext, TransferType.MATERNITY_MEDICAL_HISTORY));
        when(v1FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(new HashSet<>(), new ArrayList<>(), 1)); //default
        when(v2FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(new HashSet<>(), new ArrayList<>(), 2)); //default
        List<FetchJobHandler> handlers = new ArrayList<>();
        handlers.add(v1FetchJobHandler);
        handlers.add(v2FetchJobHandler);
        when(fetchJobHandlers.get(eq(TransferType.MATERNITY_MEDICAL_HISTORY))).thenReturn(handlers);
    }

    @After
    public void tearDown() throws Exception {
        userRepository.deleteAll();
        userContextRepository.deleteAll();
        subscriptionRepository.deleteAll();
        fetchJobRepository.deleteAll();
        transferJobRepository.deleteAll();
        pushJobRepository.deleteAll();
    }

    @Test
    public void testFetchAggregatedZeroDocumentsZeroFailedSourceSystems() throws Exception {
        TransferJob transferJob = transferJobRepository.save(
                new TransferJob(subscription.getUserContext().getUser().getCrn(),
                        subscription.getUserContext().getRepresentingCrn(),
                        subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY,
                        LocalDateTime.now(), LocalDateTime.now(), TransferJob.Origin.INITIAL));
        FetchJob fetchJob = fetchJobRepository.save(new FetchJob(transferJob, null));
        assertEquals(JobStatus.CREATED, fetchJob.getStatus());
        FetchJobDTO fetchJobDTO = transformationService.toDTO(fetchJob);
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.FETCH_AGGREGATED_QUEUE, fetchJobDTO);
        Thread.sleep(1000);
        FetchJob updatedFetchJob = fetchJobRepository.findOne(fetchJob.getId());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getStatus());
        assertEquals(0, pushJobRepository.count());
        verify(transferService, times(0)).startTransferForFailedSourceSystem(any(TransferJobDTO.class), anyString());
        updatedFetchJob.getJobStates().forEach(jobState -> {
            logger.info("jobState: {} ({})", jobState.getStatus(), jobState.getCreated());
        });
        assertEquals(3, updatedFetchJob.getJobStates().size());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getJobStates().get(2).getStatus());
    }

    @Test
    public void testFetchAggregatedTwoDocumentsZeroFailedSourceSystems() throws Exception {
        TransferJob transferJob = transferJobRepository.save(
                new TransferJob(subscription.getUserContext().getUser().getCrn(),
                        subscription.getUserContext().getRepresentingCrn(),
                        subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY,
                LocalDateTime.now(), LocalDateTime.now(), TransferJob.Origin.INITIAL));
        FetchJob fetchJob = fetchJobRepository.save(new FetchJob(transferJob, null));
        assertEquals(JobStatus.CREATED, fetchJob.getStatus());
        FetchJobDTO fetchJobDTO = transformationService.toDTO(fetchJob);
        List<FetchResultDocument> documents = new ArrayList<>();
        documents.add(new FetchResultDocument("doc1", 1, "ssId1"));
        documents.add(new FetchResultDocument("doc2", 1, "ssId1"));
        when(v1FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(new HashSet<>(), documents, 1));
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.FETCH_AGGREGATED_QUEUE, fetchJobDTO);
        Thread.sleep(1000);
        FetchJob updatedFetchJob = fetchJobRepository.findOne(fetchJob.getId());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getStatus());
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, 2);
        verify(transferService, times(0)).startTransferForFailedSourceSystem(any(TransferJobDTO.class), anyString());
        assertEquals(3, updatedFetchJob.getJobStates().size());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getJobStates().get(2).getStatus());
    }

    @Test
    public void testFetchAggregatedTwoPlusTwoDocumentsZeroFailedSourceSystems() throws Exception {
        TransferJob transferJob = transferJobRepository.save(
                new TransferJob(subscription.getUserContext().getUser().getCrn(),
                        subscription.getUserContext().getRepresentingCrn(),
                        subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY,
                LocalDateTime.now(), LocalDateTime.now(), TransferJob.Origin.INITIAL));
        FetchJob fetchJob = fetchJobRepository.save(new FetchJob(transferJob, null));
        assertEquals(JobStatus.CREATED, fetchJob.getStatus());
        FetchJobDTO fetchJobDTO = transformationService.toDTO(fetchJob);
        List<FetchResultDocument> documents = new ArrayList<>();
        documents.add(new FetchResultDocument("doc1", 1, "ssId1"));
        documents.add(new FetchResultDocument("doc2", 1, "ssId1"));
        when(v1FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(new HashSet<>(), documents, 1));
        when(v2FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(new HashSet<>(), documents, 2));
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.FETCH_AGGREGATED_QUEUE, fetchJobDTO);
        Thread.sleep(1000);
        FetchJob updatedFetchJob = fetchJobRepository.findOne(fetchJob.getId());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getStatus());
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, 4);
        verify(transferService, times(0)).startTransferForFailedSourceSystem(any(TransferJobDTO.class), anyString());
        assertEquals(3, updatedFetchJob.getJobStates().size());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getJobStates().get(2).getStatus());
    }

    @Test
    public void testFetchAggregatedTwoDocumentTwoFailedSourceSystems() throws Exception {
        TransferJob transferJob = transferJobRepository.save(
                new TransferJob(subscription.getUserContext().getUser().getCrn(),
                        subscription.getUserContext().getRepresentingCrn(),
                        subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY,
                LocalDateTime.now(), LocalDateTime.now(), TransferJob.Origin.INITIAL));
        FetchJob fetchJob = fetchJobRepository.save(new FetchJob(transferJob, null));
        assertEquals(JobStatus.CREATED, fetchJob.getStatus());
        FetchJobDTO fetchJobDTO = transformationService.toDTO(fetchJob);
        Set<String> failedSourceSystems = new HashSet<>();
        failedSourceSystems.add("fss1");
        failedSourceSystems.add("fss2");
        List<FetchResultDocument> documents = new ArrayList<>();
        documents.add(new FetchResultDocument("doc1", 1, "ssId1"));
        documents.add(new FetchResultDocument("doc2", 1, "ssId1"));
        when(v1FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(failedSourceSystems, documents, 1))
                .thenReturn(new FetchResult(Collections.emptySet(), Collections.emptyList(), 1)); //failed source systems
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.FETCH_AGGREGATED_QUEUE, fetchJobDTO);
        Thread.sleep(1000);
        FetchJob updatedFetchJob = fetchJobRepository.findOne(fetchJob.getId());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getStatus());
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, 2);
        verify(transferService, times(2)).startTransferForFailedSourceSystem(any(TransferJobDTO.class), anyString());
        assertEquals(3, updatedFetchJob.getJobStates().size());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getJobStates().get(2).getStatus());
    }

    @Test
    public void testFetchAggregatedTwoPlusThreeDocumentsTwoPlusOneFailedSourceSystems() throws Exception {
        TransferJob transferJob = transferJobRepository.save(
                new TransferJob(subscription.getUserContext().getUser().getCrn(),
                        subscription.getUserContext().getRepresentingCrn(),
                        subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY,
                LocalDateTime.now(), LocalDateTime.now(), TransferJob.Origin.INITIAL));
        FetchJob fetchJob = fetchJobRepository.save(new FetchJob(transferJob, null));
        assertEquals(JobStatus.CREATED, fetchJob.getStatus());
        FetchJobDTO fetchJobDTO = transformationService.toDTO(fetchJob);
        Set<String> failedSourceSystemsV1 = new HashSet<>();
        failedSourceSystemsV1.add("fss1");
        failedSourceSystemsV1.add("fss2");
        Set<String> failedSourceSystemsV2 = new HashSet<>();
        failedSourceSystemsV1.add("fss3");
        List<FetchResultDocument> documentsV1 = new ArrayList<>();
        documentsV1.add(new FetchResultDocument("doc1", 1, "ssId1"));
        documentsV1.add(new FetchResultDocument("doc2", 1, "ssId1"));
        List<FetchResultDocument> documentsV2 = new ArrayList<>();
        documentsV2.add(new FetchResultDocument("doc3", 2, "ssId2"));
        documentsV2.add(new FetchResultDocument("doc4", 2, "ssId2"));
        documentsV2.add(new FetchResultDocument("doc5", 2, "ssId2"));
        when(v1FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(failedSourceSystemsV1, documentsV1, 1))
                .thenReturn(new FetchResult(Collections.emptySet(), Collections.emptyList(), 1));
        when(v2FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(failedSourceSystemsV2, documentsV2, 2))
                .thenReturn(new FetchResult(Collections.emptySet(), Collections.emptyList(), 2));
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.FETCH_AGGREGATED_QUEUE, fetchJobDTO);
        Thread.sleep(1000);
        FetchJob updatedFetchJob = fetchJobRepository.findOne(fetchJob.getId());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getStatus());
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, 5);
        verify(transferService, times(3)).startTransferForFailedSourceSystem(any(TransferJobDTO.class), anyString());
        assertEquals(3, updatedFetchJob.getJobStates().size());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getJobStates().get(2).getStatus());
    }

    @Test //TODO: make unit?
    public void testFetchAggregatedTwoPlusThreeDocumentsTwoPlusOneFailedSourceSystems_DupeCheck1() throws Exception {
        TransferJob transferJob = transferJobRepository.save(
                new TransferJob(subscription.getUserContext().getUser().getCrn(),
                        subscription.getUserContext().getRepresentingCrn(),
                        subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY,
                LocalDateTime.now(), LocalDateTime.now(), TransferJob.Origin.INITIAL));
        FetchJob fetchJob = fetchJobRepository.save(new FetchJob(transferJob, null));
        assertEquals(JobStatus.CREATED, fetchJob.getStatus());
        FetchJobDTO fetchJobDTO = transformationService.toDTO(fetchJob);
        Set<String> failedSourceSystemsV1 = new HashSet<>();
        failedSourceSystemsV1.add("ssId2"); //should not trigger new fetch since it is represented in v2
        Set<String> failedSourceSystemsV2 = new HashSet<>();
        failedSourceSystemsV2.add("ssId1"); //should trigger new fetch since it only has documents in v1
        List<FetchResultDocument> documentsV1 = new ArrayList<>();
        documentsV1.add(new FetchResultDocument("doc1", 1, "ssId1"));
        documentsV1.add(new FetchResultDocument("doc2", 1, "ssId1"));
        List<FetchResultDocument> documentsV2 = new ArrayList<>();
        documentsV2.add(new FetchResultDocument("doc3", 2, "ssId2"));
        documentsV2.add(new FetchResultDocument("doc4", 2, "ssId2"));
        documentsV2.add(new FetchResultDocument("doc5", 2, "ssId2"));
        when(v1FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(failedSourceSystemsV1, documentsV1, 1));
        when(v2FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(failedSourceSystemsV2, documentsV2, 2));
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.FETCH_AGGREGATED_QUEUE, fetchJobDTO);
        Thread.sleep(2000);
        FetchJob updatedFetchJob = fetchJobRepository.findOne(fetchJob.getId());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getStatus());
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, 3);
        verify(transferService, times(1)).startTransferForFailedSourceSystem(any(TransferJobDTO.class), anyString());
        assertEquals(3, updatedFetchJob.getJobStates().size());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getJobStates().get(2).getStatus());
    }

    @Test //TODO: make unit?
    public void testFetchAggregatedTwoPlusThreeDocumentsTwoPlusOneFailedSourceSystems_DupeCheck2() throws Exception {
        TransferJob transferJob = transferJobRepository.save(
                new TransferJob(subscription.getUserContext().getUser().getCrn(),
                        subscription.getUserContext().getRepresentingCrn(),
                        subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY,
                LocalDateTime.now(), LocalDateTime.now(), TransferJob.Origin.INITIAL));
        FetchJob fetchJob = fetchJobRepository.save(new FetchJob(transferJob, null));
        assertEquals(JobStatus.CREATED, fetchJob.getStatus());
        FetchJobDTO fetchJobDTO = transformationService.toDTO(fetchJob);
        Set<String> failedSourceSystemsV1 = new HashSet<>();
        Set<String> failedSourceSystemsV2 = new HashSet<>();
        failedSourceSystemsV2.add("ssId1");
        failedSourceSystemsV2.add("ssId2");
        List<FetchResultDocument> documentsV1 = new ArrayList<>();
        documentsV1.add(new FetchResultDocument("doc1", 1, "ssId1"));
        documentsV1.add(new FetchResultDocument("doc2", 1, "ssId1"));
        documentsV1.add(new FetchResultDocument("doc2", 1, "ssId2"));
        documentsV1.add(new FetchResultDocument("doc2", 1, "ssId2"));
        documentsV1.add(new FetchResultDocument("doc2", 1, "ssId2"));
        List<FetchResultDocument> documentsV2 = new ArrayList<>();
        when(v1FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(failedSourceSystemsV1, documentsV1, 1));
        when(v2FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(failedSourceSystemsV2, documentsV2, 2));
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.FETCH_AGGREGATED_QUEUE, fetchJobDTO);
        Thread.sleep(2000);
        FetchJob updatedFetchJob = fetchJobRepository.findOne(fetchJob.getId());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getStatus());
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, 0);
        verify(transferService, times(2)).startTransferForFailedSourceSystem(any(TransferJobDTO.class), anyString());
        assertEquals(3, updatedFetchJob.getJobStates().size());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getJobStates().get(2).getStatus());
    }

    @Test
    public void testFetchAggregatedTwoPlusExceptionTwoFailedSourceSystems() throws Exception {
        TransferJob transferJob = transferJobRepository.save(
                new TransferJob(subscription.getUserContext().getUser().getCrn(),
                        subscription.getUserContext().getRepresentingCrn(),
                        subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY,
                LocalDateTime.now(), LocalDateTime.now(), TransferJob.Origin.INITIAL));
        FetchJob fetchJob = fetchJobRepository.save(new FetchJob(transferJob, null));
        assertEquals(JobStatus.CREATED, fetchJob.getStatus());
        FetchJobDTO fetchJobDTO = transformationService.toDTO(fetchJob);
        Set<String> failedSourceSystemsV1 = new HashSet<>();
        failedSourceSystemsV1.add("fss1");
        failedSourceSystemsV1.add("fss2");
        List<FetchResultDocument> documentsV1 = new ArrayList<>();
        documentsV1.add(new FetchResultDocument("doc1", 1, "ssId1"));
        documentsV1.add(new FetchResultDocument("doc2", 1, "ssId1"));
        when(v1FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(failedSourceSystemsV1, documentsV1, 1));
        when(v2FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenThrow(new RuntimeException());
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.FETCH_AGGREGATED_QUEUE, fetchJobDTO);
        Thread.sleep(5000);
        FetchJob updatedFetchJob = fetchJobRepository.findOne(fetchJob.getId());
        assertEquals(JobStatus.FAILED_ALL_RETRIES, updatedFetchJob.getStatus());
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, 0);
        verify(transferService, times(0)).startTransferForFailedSourceSystem(any(TransferJobDTO.class), anyString());
        updatedFetchJob.getJobStates().forEach(jobState -> {
            logger.info("jobState: {} ({})", jobState.getStatus(), jobState.getCreated());
        });
        assertEquals(8, updatedFetchJob.getJobStates().size());
        assertEquals(JobStatus.FAILED_ALL_RETRIES, updatedFetchJob.getJobStates().get(7).getStatus());
    }

    @Test
    public void testFetchAggregatedTwoPlusExceptionTwoFailedSourceSystems_SuccessOnRetry() throws Exception {
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, 0);
        TransferJob transferJob = transferJobRepository.save(
                new TransferJob(subscription.getUserContext().getUser().getCrn(),
                        subscription.getUserContext().getRepresentingCrn(),
                        subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY,
                LocalDateTime.now(), LocalDateTime.now(), TransferJob.Origin.INITIAL));
        FetchJob fetchJob = fetchJobRepository.save(new FetchJob(transferJob, null));
        assertEquals(JobStatus.CREATED, fetchJob.getStatus());
        FetchJobDTO fetchJobDTO = transformationService.toDTO(fetchJob);
        Set<String> failedSourceSystemsV1 = new HashSet<>();
        failedSourceSystemsV1.add("fss1");
        failedSourceSystemsV1.add("fss2");
        List<FetchResultDocument> documentsV1 = new ArrayList<>();
        documentsV1.add(new FetchResultDocument("doc1", 1, "ssId1"));
        documentsV1.add(new FetchResultDocument("doc2", 1, "ssId1"));
        when(v1FetchJobHandler.processFetchJob(any(FetchJobDTO.class))).then(answer -> {
            String fetchJobId = ((FetchJobDTO) answer.getArguments()[0]).getId();
            if (fetchJobId.equals(fetchJobDTO.getId())) {
                return new FetchResult(failedSourceSystemsV1, documentsV1, 1);
            } else { //restarted for failed source systems
                return new FetchResult(Collections.emptySet(), Collections.emptyList(), 1);
            }
        });
        when(v2FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenThrow(new RuntimeException())
                .thenReturn(new FetchResult(Collections.emptySet(), Collections.emptyList(), 2));
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.FETCH_AGGREGATED_QUEUE, fetchJobDTO);
        Thread.sleep(5000);
        FetchJob updatedFetchJob = fetchJobRepository.findOne(fetchJob.getId());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getStatus());
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, 2);
        verify(transferService, times(2)).startTransferForFailedSourceSystem(any(TransferJobDTO.class), anyString());
        assertEquals(5, updatedFetchJob.getJobStates().size());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getJobStates().get(4).getStatus());
    }

    @Test
    public void testFetchAggregatedTwoDocumentTwoFailedSourceSystems_FailInStartTransferForFailedSourceSystem()
            throws Exception {
        TransferJob transferJob = transferJobRepository.save(
                new TransferJob(subscription.getUserContext().getUser().getCrn(),
                        subscription.getUserContext().getRepresentingCrn(),
                        subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY,
                LocalDateTime.now(), LocalDateTime.now(), TransferJob.Origin.INITIAL));
        FetchJob fetchJob = fetchJobRepository.save(new FetchJob(transferJob, null));
        assertEquals(JobStatus.CREATED, fetchJob.getStatus());
        FetchJobDTO fetchJobDTO = transformationService.toDTO(fetchJob);
        Set<String> failedSourceSystems = new HashSet<>();
        failedSourceSystems.add("fss1");
        failedSourceSystems.add("fss2");
        List<FetchResultDocument> documents = new ArrayList<>();
        documents.add(new FetchResultDocument("doc1", 1, "ssId1"));
        documents.add(new FetchResultDocument("doc2", 1, "ssId1"));
        prepareTransferServiceSpy(1);
        when(v1FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(failedSourceSystems, documents, 1))
                .thenReturn(new FetchResult(failedSourceSystems, documents, 1)) //first retry
                .thenReturn(new FetchResult(Collections.emptySet(), Collections.emptyList(), 1)); //failed source systems
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.FETCH_AGGREGATED_QUEUE, fetchJobDTO);
        Thread.sleep(3000);
        FetchJob updatedFetchJob = fetchJobRepository.findOne(fetchJob.getId());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getStatus());
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, 2);
        verify(transferService, times(3)).startTransferForFailedSourceSystem(any(TransferJobDTO.class), anyString());
        assertEquals(5, updatedFetchJob.getJobStates().size());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getJobStates().get(4).getStatus());
    }

    @Test
    public void testFetchSingleSourceSystemZeroDocumentsZeroFailedSourceSystems() throws Exception {
        TransferJob transferJob = transferJobRepository.save(
                new TransferJob(subscription.getUserContext().getUser().getCrn(),
                        subscription.getUserContext().getRepresentingCrn(),
                        subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY,
                LocalDateTime.now(), LocalDateTime.now(), "sourceSystem", TransferJob.Origin.INITIAL));
        FetchJob fetchJob = fetchJobRepository.save(new FetchJob(transferJob, null));
        assertEquals(JobStatus.CREATED, fetchJob.getStatus());
        FetchJobDTO fetchJobDTO = transformationService.toDTO(fetchJob);
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.FETCH_SINGLE_SOURCE_SYSTEM_QUEUE, fetchJobDTO);
        Thread.sleep(1000);
        FetchJob updatedFetchJob = fetchJobRepository.findOne(fetchJob.getId());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getStatus());
        verify(transferService, times(0)).startTransferForFailedSourceSystem(any(TransferJobDTO.class), anyString());
        assertEquals(3, updatedFetchJob.getJobStates().size());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getJobStates().get(2).getStatus());
    }

    @Test
    public void testFetchSingleSourceSystemTwoDocumentsZeroFailedSourceSystems() throws Exception {
        TransferJob transferJob = transferJobRepository.save(
                new TransferJob(subscription.getUserContext().getUser().getCrn(),
                        subscription.getUserContext().getRepresentingCrn(),
                        subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY,
                LocalDateTime.now(), LocalDateTime.now(), "sourceSystem", TransferJob.Origin.INITIAL));
        FetchJob fetchJob = fetchJobRepository.save(new FetchJob(transferJob, null));
        assertEquals(JobStatus.CREATED, fetchJob.getStatus());
        FetchJobDTO fetchJobDTO = transformationService.toDTO(fetchJob);
        List<FetchResultDocument> documents = new ArrayList<>();
        documents.add(new FetchResultDocument("doc1", 1, "ssId1"));
        documents.add(new FetchResultDocument("doc2", 1, "ssId1"));
        when(v1FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(new HashSet<>(), documents, 1));
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.FETCH_SINGLE_SOURCE_SYSTEM_QUEUE, fetchJobDTO);
        Thread.sleep(1000);
        FetchJob updatedFetchJob = fetchJobRepository.findOne(fetchJob.getId());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getStatus());
        verify(transferService, times(0)).startTransferForFailedSourceSystem(any(TransferJobDTO.class), anyString());
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, 2);
        assertEquals(3, updatedFetchJob.getJobStates().size());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getJobStates().get(2).getStatus());
    }

    @Test
    public void testFetchSingleSourceSystemTwoPlusThreeDocumentsZeroFailedSourceSystems() throws Exception {
        TransferJob transferJob = transferJobRepository.save(
                new TransferJob(subscription.getUserContext().getUser().getCrn(),
                        subscription.getUserContext().getRepresentingCrn(),
                        subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY,
                LocalDateTime.now(), LocalDateTime.now(), "sourceSystem", TransferJob.Origin.INITIAL));
        FetchJob fetchJob = fetchJobRepository.save(new FetchJob(transferJob, null));
        assertEquals(JobStatus.CREATED, fetchJob.getStatus());
        FetchJobDTO fetchJobDTO = transformationService.toDTO(fetchJob);
        List<FetchResultDocument> documentsV1 = new ArrayList<>();
        documentsV1.add(new FetchResultDocument("doc1", 1, "ssId1"));
        documentsV1.add(new FetchResultDocument("doc2", 1, "ssId1"));
        List<FetchResultDocument> documentsV2 = new ArrayList<>();
        documentsV2.add(new FetchResultDocument("doc1", 2, "ssId1"));
        documentsV2.add(new FetchResultDocument("doc2", 2, "ssId1"));
        documentsV2.add(new FetchResultDocument("doc3", 2, "ssId1"));
        when(v1FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(new HashSet<>(), documentsV1, 1));
        when(v2FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(new HashSet<>(), documentsV2, 2));
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.FETCH_SINGLE_SOURCE_SYSTEM_QUEUE, fetchJobDTO);
        Thread.sleep(1000);
        FetchJob updatedFetchJob = fetchJobRepository.findOne(fetchJob.getId());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getStatus());
        verify(transferService, times(0)).startTransferForFailedSourceSystem(any(TransferJobDTO.class), anyString());
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, 3);
        assertEquals(3, updatedFetchJob.getJobStates().size());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getJobStates().get(2).getStatus());
    }

    @Test
    public void testFetchSingleSourceSystemTwoDocumentsOneFailedSourceSystems_FailAlsoOnRetry() throws Exception {
        TransferJob transferJob = transferJobRepository.save(
                new TransferJob(subscription.getUserContext().getUser().getCrn(),
                        subscription.getUserContext().getRepresentingCrn(),
                        subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY, LocalDateTime.now(), LocalDateTime.now(), "sourceSystem",
                TransferJob.Origin.INITIAL));
        FetchJob fetchJob = fetchJobRepository.save(new FetchJob(transferJob, null));
        assertEquals(JobStatus.CREATED, fetchJob.getStatus());
        FetchJobDTO fetchJobDTO = transformationService.toDTO(fetchJob);
        Set<String> failedSourceSystems = new HashSet<>();
        failedSourceSystems.add("ssId1");
        List<FetchResultDocument> documentsV1 = new ArrayList<>();
        documentsV1.add(new FetchResultDocument("doc1", 1, "ssId1"));
        documentsV1.add(new FetchResultDocument("doc2", 1, "ssId1"));
        when(v1FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(Collections.emptySet(), documentsV1, 1));
        when(v2FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(failedSourceSystems, Collections.emptyList(), 2));
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.FETCH_SINGLE_SOURCE_SYSTEM_QUEUE, fetchJobDTO);
        Thread.sleep(5000);
        FetchJob updatedFetchJob = fetchJobRepository.findOne(fetchJob.getId());
        assertEquals(JobStatus.FAILED_ALL_RETRIES, updatedFetchJob.getStatus());
        verify(transferService, times(0)).startTransferForFailedSourceSystem(any(TransferJobDTO.class), anyString());
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, 0);
        assertEquals(8, updatedFetchJob.getJobStates().size());
        assertEquals(JobStatus.FAILED_ALL_RETRIES, updatedFetchJob.getJobStates().get(7).getStatus());
    }

    @Test
    public void testFetchSingleSourceSystemZeroPlusZeroDocumentsOneFailedSourceSystems_FailAlsoOnRetry() throws Exception {
        TransferJob transferJob = transferJobRepository.save(
                new TransferJob(subscription.getUserContext().getUser().getCrn(),
                        subscription.getUserContext().getRepresentingCrn(),
                        subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY, LocalDateTime.now(), LocalDateTime.now(), "sourceSystem",
                TransferJob.Origin.INITIAL));
        FetchJob fetchJob = fetchJobRepository.save(new FetchJob(transferJob, null));
        assertEquals(JobStatus.CREATED, fetchJob.getStatus());
        FetchJobDTO fetchJobDTO = transformationService.toDTO(fetchJob);
        Set<String> failedSourceSystems = new HashSet<>();
        failedSourceSystems.add("sourceSystem");
        when(v1FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(Collections.emptySet(), Collections.emptyList(), 1));
        when(v2FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(failedSourceSystems, Collections.emptyList(), 2));
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.FETCH_SINGLE_SOURCE_SYSTEM_QUEUE, fetchJobDTO);
        Thread.sleep(5000);
        FetchJob updatedFetchJob = fetchJobRepository.findOne(fetchJob.getId());
        assertEquals(JobStatus.FAILED_ALL_RETRIES, updatedFetchJob.getStatus());
        verify(transferService, times(0)).startTransferForFailedSourceSystem(any(TransferJobDTO.class), anyString());
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, 0);
        assertEquals(8, updatedFetchJob.getJobStates().size());
        assertEquals(JobStatus.FAILED_ALL_RETRIES, updatedFetchJob.getJobStates().get(7).getStatus());
    }

    @Test
    public void testFetchSingleSourceSystemTwoPlusExceptionZeroFailedSourceSystems() throws Exception {
        TransferJob transferJob = transferJobRepository.save(
                new TransferJob(subscription.getUserContext().getUser().getCrn(),
                        subscription.getUserContext().getRepresentingCrn(),
                        subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY, LocalDateTime.now(), LocalDateTime.now(), "sourceSystem",
                TransferJob.Origin.INITIAL));
        FetchJob fetchJob = fetchJobRepository.save(new FetchJob(transferJob, null));
        assertEquals(JobStatus.CREATED, fetchJob.getStatus());
        FetchJobDTO fetchJobDTO = transformationService.toDTO(fetchJob);
        List<FetchResultDocument> documentsV1 = new ArrayList<>();
        documentsV1.add(new FetchResultDocument("doc1", 1, "ssId1"));
        documentsV1.add(new FetchResultDocument("doc2", 1, "ssId1"));
        when(v1FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(Collections.emptySet(), documentsV1, 1));
        when(v2FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenThrow(new RuntimeException());
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.FETCH_SINGLE_SOURCE_SYSTEM_QUEUE, fetchJobDTO);
        Thread.sleep(5000);
        FetchJob updatedFetchJob = fetchJobRepository.findOne(fetchJob.getId());
        assertEquals(JobStatus.FAILED_ALL_RETRIES, updatedFetchJob.getStatus());
        verify(transferService, times(0)).startTransferForFailedSourceSystem(any(TransferJobDTO.class), anyString());
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, 0);
        assertEquals(8, updatedFetchJob.getJobStates().size());
        assertEquals(JobStatus.FAILED_ALL_RETRIES, updatedFetchJob.getJobStates().get(7).getStatus());
    }

    @Test
    public void testFetchSingleSourceSystemTwoPlusExceptionZeroFailedSourceSystems_SuccessOnRetry() throws Exception {
        TransferJob transferJob = transferJobRepository.save(
                new TransferJob(subscription.getUserContext().getUser().getCrn(),
                        subscription.getUserContext().getRepresentingCrn(),
                        subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY, LocalDateTime.now(), LocalDateTime.now(), "sourceSystem",
                TransferJob.Origin.INITIAL));
        FetchJob fetchJob = fetchJobRepository.save(new FetchJob(transferJob, null));
        assertEquals(JobStatus.CREATED, fetchJob.getStatus());
        FetchJobDTO fetchJobDTO = transformationService.toDTO(fetchJob);
        List<FetchResultDocument> documentsV1 = new ArrayList<>();
        documentsV1.add(new FetchResultDocument("doc1", 1, "ssId1"));
        documentsV1.add(new FetchResultDocument("doc2", 1, "ssId1"));
        when(v1FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(Collections.emptySet(), documentsV1, 1));
        when(v2FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenThrow(new RuntimeException())
                .thenReturn(new FetchResult(Collections.emptySet(), Collections.emptyList(), 1));
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.FETCH_SINGLE_SOURCE_SYSTEM_QUEUE, fetchJobDTO);
        Thread.sleep(3000);
        FetchJob updatedFetchJob = fetchJobRepository.findOne(fetchJob.getId());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getStatus());
        verify(transferService, times(0)).startTransferForFailedSourceSystem(any(TransferJobDTO.class), anyString());
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, 2);
    }

    @Test
    public void testFetchSingleSourceSystemTwoPlusExceptionZeroFailedSourceSystems_SuccessOnRetryButWithFailedSourceSystem() throws Exception {
        TransferJob transferJob = transferJobRepository.save(
                new TransferJob(subscription.getUserContext().getUser().getCrn(),
                        subscription.getUserContext().getRepresentingCrn(),
                        subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY, LocalDateTime.now(), LocalDateTime.now(), "sourceSystem",
                TransferJob.Origin.INITIAL));
        FetchJob fetchJob = fetchJobRepository.save(new FetchJob(transferJob, null));
        assertEquals(JobStatus.CREATED, fetchJob.getStatus());
        FetchJobDTO fetchJobDTO = transformationService.toDTO(fetchJob);
        List<FetchResultDocument> documentsV1 = new ArrayList<>();
        documentsV1.add(new FetchResultDocument("doc1", 1, "ssId1"));
        documentsV1.add(new FetchResultDocument("doc2", 1, "ssId1"));
        when(v1FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(Collections.emptySet(), documentsV1, 1));
        when(v2FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenThrow(new RuntimeException())
                .thenReturn(new FetchResult(Collections.singleton("failed"), Collections.emptyList(), 2));
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.FETCH_SINGLE_SOURCE_SYSTEM_QUEUE, fetchJobDTO);
        Thread.sleep(5000);
        FetchJob updatedFetchJob = fetchJobRepository.findOne(fetchJob.getId());
        updatedFetchJob.getJobStates().forEach(jobState -> {
            logger.info("jobState: {} ({})", jobState.getStatus(), jobState.getCreated());
        });
        assertEquals(JobStatus.FAILED_ALL_RETRIES, updatedFetchJob.getStatus());
        verify(transferService, times(0)).startTransferForFailedSourceSystem(any(TransferJobDTO.class), anyString());
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, 0);
        assertEquals(8, updatedFetchJob.getJobStates().size());
        assertEquals(JobStatus.FAILED_ALL_RETRIES, updatedFetchJob.getJobStates().get(7).getStatus());
    }

    @Test
    public void testFetchSingleSourceSystemTwoDocumentsOneFailedSourceSystems_SuccessOnRetry() throws Exception {
        TransferJob transferJob = transferJobRepository.save(
                new TransferJob(subscription.getUserContext().getUser().getCrn(),
                        subscription.getUserContext().getRepresentingCrn(),
                        subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY,
                LocalDateTime.now(), LocalDateTime.now(), "sourceSystem", TransferJob.Origin.INITIAL));
        FetchJob fetchJob = fetchJobRepository.save(new FetchJob(transferJob, null));
        assertEquals(JobStatus.CREATED, fetchJob.getStatus());
        FetchJobDTO fetchJobDTO = transformationService.toDTO(fetchJob);
        List<FetchResultDocument> documentsV1 = new ArrayList<>();
        documentsV1.add(new FetchResultDocument("doc1", 1, "ssId1"));
        documentsV1.add(new FetchResultDocument("doc2", 1, "ssId1"));
        Set failedSourceSystemsSetMock = Mockito.mock(Set.class);
        when(failedSourceSystemsSetMock.isEmpty()).thenReturn(false, true);
        when(v1FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(Collections.emptySet(), documentsV1, 1));
        when(v2FetchJobHandler.processFetchJob(any(FetchJobDTO.class)))
                .thenReturn(new FetchResult(failedSourceSystemsSetMock, Collections.emptyList(), 2));
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.FETCH_SINGLE_SOURCE_SYSTEM_QUEUE, fetchJobDTO);
        Thread.sleep(3000);
        FetchJob updatedFetchJob = fetchJobRepository.findOne(fetchJob.getId());
        assertEquals(JobStatus.FINISHED, updatedFetchJob.getStatus());
        verify(transferService, times(0)).startTransferForFailedSourceSystem(any(TransferJobDTO.class), anyString());
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, 2);
    }

    @Test
    public void testUnrecoverableFetchAggregatedError() throws Exception {
        TransferJob transferJob = transferJobRepository.save(
                new TransferJob(subscription.getUserContext().getUser().getCrn(),
                        subscription.getUserContext().getRepresentingCrn(),
                        subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY,
                LocalDateTime.now(), LocalDateTime.now(), "sourceSystem", TransferJob.Origin.INITIAL));
        FetchJob fetchJob = fetchJobRepository.save(new FetchJob(transferJob, null));
        assertEquals(JobStatus.CREATED, fetchJob.getStatus());
        FetchJobDTO fetchJobDTO = transformationService.toDTO(fetchJob);
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.FETCH_AGGREGATED_QUEUE_DLQ, fetchJobDTO);
        Thread.sleep(1000);
        FetchJob updatedFetchJob = fetchJobRepository.findOne(fetchJob.getId());
        assertEquals(JobStatus.FAILED_ALL_RETRIES, updatedFetchJob.getStatus());
    }

    @Test
    public void testUnrecoverableFetchSingleSourceSystemError() throws Exception {
        TransferJob transferJob = transferJobRepository.save(
                new TransferJob(subscription.getUserContext().getUser().getCrn(),
                        subscription.getUserContext().getRepresentingCrn(),
                        subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY,
                LocalDateTime.now(), LocalDateTime.now(), "sourceSystem", TransferJob.Origin.INITIAL));
        FetchJob fetchJob = fetchJobRepository.save(new FetchJob(transferJob, null));
        assertEquals(JobStatus.CREATED, fetchJob.getStatus());
        FetchJobDTO fetchJobDTO = transformationService.toDTO(fetchJob);
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.FETCH_SINGLE_SOURCE_SYSTEM_QUEUE_DLQ, fetchJobDTO);
        Thread.sleep(1000);
        FetchJob updatedFetchJob = fetchJobRepository.findOne(fetchJob.getId());
        assertEquals(JobStatus.FAILED_ALL_RETRIES, updatedFetchJob.getStatus());
    }

    private void assertQueueCount(String queueName, int expectedMsgCount) {
        jmsTemplate.browse(queueName, (session, browser) -> {
            int msgCount = 0;
            Enumeration enumeration = browser.getEnumeration();
            while(enumeration.hasMoreElements()) {
                enumeration.nextElement();
                msgCount += 1;
            }
            logger.debug("{} msg count: {}", queueName, msgCount);
            assertEquals(expectedMsgCount, msgCount);
            return null;
        });
    }

    private void prepareTransferServiceSpy(int startTransferForFailedSourceSystemInvocationToFailOn) {
        Stubber stubber = new StubberImpl();
        for (int i = 1; i <= (startTransferForFailedSourceSystemInvocationToFailOn + 1); i += 1) {
            if (i == startTransferForFailedSourceSystemInvocationToFailOn) {
                stubber = stubber.doThrow(new RuntimeException("startTransferForFailedSourceSystem failed on invocation " +
                        startTransferForFailedSourceSystemInvocationToFailOn));
            } else {
                stubber = stubber.doCallRealMethod();
            }
        }
        stubber.when(transferService).startTransferForFailedSourceSystem(any(TransferJobDTO.class), anyString());
    }

    @TestConfiguration
    @Configuration
    public static class FetchJobMDPJmsTransactionTestConfiguration {

        @Autowired
        private AutowireCapableBeanFactory beanFactory;

        @Bean
        public FetchJobHandler v1FetchJobHandler() {
            FetchJobHandler mock = Mockito.mock(FetchJobHandler.class);
            return mock;
        }

        @Bean
        public FetchJobHandler v2FetchJobHandler() {
            FetchJobHandler mock = Mockito.mock(FetchJobHandler.class);
            return mock;
        }

        @Bean
        @Primary
        public Map<TransferType, List<FetchJobHandler>> fetchJobHandlers() {
            Map<TransferType, List<FetchJobHandler>> fetchJobHandlers = Mockito.mock(EnumMap.class);
            beanFactory.autowireBean(fetchJobHandlers); //TODO: eh
            return fetchJobHandlers;
        }

        @Bean
        @Primary
        public TransformationJobMDP transformationJobMDP() {
            return null;
        }

        @Bean
        @Primary
        public TransferMDP transferMDP() {
            return null;
        }

        @Bean
        @Primary
        public TransferService transferService() {
            TransferService transferService = new TransferService();
            TransferService spy = Mockito.spy(transferService);
            beanFactory.autowireBean(transferService);
            return spy;
        }

    }
}