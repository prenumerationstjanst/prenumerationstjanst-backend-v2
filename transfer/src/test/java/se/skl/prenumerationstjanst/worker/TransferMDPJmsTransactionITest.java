package se.skl.prenumerationstjanst.worker;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import se.skl.prenumerationstjanst.JmsTestConfig;
import se.skl.prenumerationstjanst.PrenumerationstjanstConstants;
import se.skl.prenumerationstjanst.PrenumerationstjanstTransferApplication;
import se.skl.prenumerationstjanst.config.TestConfiguration;
import se.skl.prenumerationstjanst.dto.FetchJobDTO;
import se.skl.prenumerationstjanst.dto.TransferJobDTO;
import se.skl.prenumerationstjanst.dto.internal.InitialTransferParams;
import se.skl.prenumerationstjanst.dto.internal.SingleSourceSystemTransferParams;
import se.skl.prenumerationstjanst.model.*;
import se.skl.prenumerationstjanst.repository.*;
import se.skl.prenumerationstjanst.service.JobService;
import se.skl.prenumerationstjanst.service.PreCheckService;

import java.time.LocalDateTime;
import java.time.Year;
import java.util.Enumeration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.anyString;

/**
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {
        PrenumerationstjanstTransferApplication.class,
        JmsTestConfig.class,
        TransferMDPJmsTransactionITest.TransferServiceJmsTestConfig.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ActiveProfiles({"test", "jmstest"})
public class TransferMDPJmsTransactionITest {

    private static final Logger logger = LoggerFactory.getLogger(TransferMDPJmsTransactionITest.class);

    public static final String USER_CRN = "191212121212";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserContextRepository userContextRepository;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private TransferJobRepository transferJobRepository;

    @Autowired
    private FetchJobRepository fetchJobRepository;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private PreCheckService preCheckService;

    @Value("${pt.initial.transferjob.year}")
    private int initialYear;

    private Subscription subscription;

    private static int createTransferJobInvocation;
    private static int createTransferJobInvocationToFailOn;
    private static int createFetchJobInvocation;
    private static int createFetchJobInvocationToFailOn;

    @Before
    public void setUp() throws Exception {
        userRepository.deleteAll();
        userContextRepository.deleteAll();
        subscriptionRepository.deleteAll();
        createTransferJobInvocation = 0;
        createTransferJobInvocationToFailOn = 0;
        createFetchJobInvocation = 0;
        createFetchJobInvocationToFailOn = 0;
        assertFalse(Mockito.mockingDetails(jmsTemplate).isMock());
        UserContext userContext = new UserContext(userRepository.save(new User(USER_CRN, "phkPersonId")),
                USER_CRN, "phkRecordId");
        userContext = userContextRepository.save(userContext);
        Subscription sub = new Subscription(userContext,
                TransferType.MATERNITY_MEDICAL_HISTORY);
        ApigwConsent apigwConsent = new ApigwConsent();
        apigwConsent.setTokenValue("tokenValue");
        sub.setApigwConsent(apigwConsent);
        subscription = subscriptionRepository.save(sub);
        Mockito.when(preCheckService.preCheck(anyString(), anyString())).thenReturn(Boolean.TRUE);
    }

    @After
    public void tearDown() throws Exception {
        userRepository.deleteAll();
        userContextRepository.deleteAll();
        subscriptionRepository.deleteAll();
        transferJobRepository.deleteAll();
    }

    @Test
    public void testStartInitialTransfersForSubscription() throws Exception {
        int expectedJobs = Year.now().getValue() - Year.of(initialYear).getValue() + 1;
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.INITIAL_TRANSFERS_FOR_SUBSCRIPTION_QUEUE,
                new InitialTransferParams(subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY.name()));
        logger.debug("sleeping 2 sec");
        Thread.sleep(2000);
        assertQueueCount(PrenumerationstjanstConstants.FETCH_AGGREGATED_QUEUE, expectedJobs); //one transfer type
        assertEquals(expectedJobs, transferJobRepository.count());
        assertEquals(expectedJobs, fetchJobRepository.count());
    }

    @Test
    public void testStartInitialTransfersForSubscriptionCreateTransferJobFails() throws Exception {
        createTransferJobInvocationToFailOn = 2;
        int expectedJobs = Year.now().getValue() - Year.of(initialYear).getValue() + 1;
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.INITIAL_TRANSFERS_FOR_SUBSCRIPTION_QUEUE,
                new InitialTransferParams(subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY.name()));
        logger.debug("sleeping 5 sec");
        Thread.sleep(5000); //must be longer than redelivery delay + some extra time
        assertQueueCount(PrenumerationstjanstConstants.FETCH_AGGREGATED_QUEUE, expectedJobs);
        assertEquals(expectedJobs, transferJobRepository.count());
        assertEquals(expectedJobs, fetchJobRepository.count());
    }

    @Test
    public void testStartInitialTransfersForSubscriptionCreateFetchJobFails() throws Exception {
        createFetchJobInvocationToFailOn = 2;
        int expectedJobs = Year.now().getValue() - Year.of(initialYear).getValue() + 1;
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.INITIAL_TRANSFERS_FOR_SUBSCRIPTION_QUEUE,
                new InitialTransferParams(subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY.name()));
        logger.debug("sleeping 5 sec");
        Thread.sleep(5000); //must be longer than redelivery delay + some extra time
        assertQueueCount(PrenumerationstjanstConstants.FETCH_AGGREGATED_QUEUE, expectedJobs);
        assertEquals(expectedJobs, transferJobRepository.count());
        assertEquals(expectedJobs, fetchJobRepository.count());
    }

    @Test
    public void testStartTransferForFailedSourceSystem() throws Exception {
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.START_TRANSFER_FOR_SINGLE_SOURCE_SYSTEM_QUEUE,
                new SingleSourceSystemTransferParams(subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY,
                        LocalDateTime.now(),
                        LocalDateTime.now(),
                        "failedSS",
                        TransferJob.Origin.FAILED_SOURCE_SYSTEM));
        logger.debug("sleeping 2 sec");
        Thread.sleep(2000);
        assertQueueCount(PrenumerationstjanstConstants.FETCH_SINGLE_SOURCE_SYSTEM_QUEUE, 1);
        assertEquals(1, transferJobRepository.count());
        assertEquals(1, fetchJobRepository.count());
    }

    @Test
    public void testStartTransferForFailedSourceSystemCreateTransferJobFails() throws Exception {
        createTransferJobInvocationToFailOn = 1;
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.START_TRANSFER_FOR_SINGLE_SOURCE_SYSTEM_QUEUE,
                new SingleSourceSystemTransferParams(subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY,
                        LocalDateTime.now(),
                        LocalDateTime.now(),
                        "failedSS",
                        TransferJob.Origin.FAILED_SOURCE_SYSTEM));
        logger.debug("sleeping 5 sec");
        Thread.sleep(5000);
        assertQueueCount(PrenumerationstjanstConstants.FETCH_SINGLE_SOURCE_SYSTEM_QUEUE, 1);
        assertEquals(1, transferJobRepository.count());
        assertEquals(1, fetchJobRepository.count());
    }

    @Test
    public void testStartTransferForFailedSourceSystemCreateFetchJobFails() throws Exception {
        createFetchJobInvocationToFailOn = 1;
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.START_TRANSFER_FOR_SINGLE_SOURCE_SYSTEM_QUEUE,
                new SingleSourceSystemTransferParams(subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY,
                        LocalDateTime.now(),
                        LocalDateTime.now(),
                        "failedSS",
                        TransferJob.Origin.FAILED_SOURCE_SYSTEM));
        logger.debug("sleeping 5 sec");
        Thread.sleep(5000);
        assertQueueCount(PrenumerationstjanstConstants.FETCH_SINGLE_SOURCE_SYSTEM_QUEUE, 1);
        assertEquals(1, transferJobRepository.count());
        assertEquals(1, fetchJobRepository.count());
    }

    @Test
    public void testStartTransferForProcessNotification() throws Exception {
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.START_TRANSFER_FOR_SINGLE_SOURCE_SYSTEM_QUEUE,
                new SingleSourceSystemTransferParams(subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY,
                        LocalDateTime.now(),
                        LocalDateTime.now(),
                        "ss",
                        TransferJob.Origin.EI_NOTIFICATION));
        logger.debug("sleeping 2 sec");
        Thread.sleep(2000);
        assertQueueCount(PrenumerationstjanstConstants.FETCH_SINGLE_SOURCE_SYSTEM_QUEUE, 1);
        assertEquals(1, transferJobRepository.count());
        assertEquals(1, fetchJobRepository.count());
    }

    @Test
    public void testStartTransferForProcessNotificationCreateTransferJobFails() throws Exception {
        createTransferJobInvocationToFailOn = 1;
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.START_TRANSFER_FOR_SINGLE_SOURCE_SYSTEM_QUEUE,
                new SingleSourceSystemTransferParams(subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY,
                        LocalDateTime.now(),
                        LocalDateTime.now(),
                        "ss",
                        TransferJob.Origin.EI_NOTIFICATION));
        logger.debug("sleeping 5 sec");
        Thread.sleep(5000);
        assertQueueCount(PrenumerationstjanstConstants.FETCH_SINGLE_SOURCE_SYSTEM_QUEUE, 1);
        assertEquals(1, transferJobRepository.count());
        assertEquals(1, fetchJobRepository.count());
    }

    @Test
    public void testStartTransferForProcessNotificationCreateFetchJobFails() throws Exception {
        createFetchJobInvocationToFailOn = 1;
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.START_TRANSFER_FOR_SINGLE_SOURCE_SYSTEM_QUEUE,
                new SingleSourceSystemTransferParams(subscription.getId(),
                        TransferType.MATERNITY_MEDICAL_HISTORY,
                        LocalDateTime.now(),
                        LocalDateTime.now(),
                        "ss",
                        TransferJob.Origin.EI_NOTIFICATION));
        logger.debug("sleeping 5 sec");
        Thread.sleep(5000);
        assertQueueCount(PrenumerationstjanstConstants.FETCH_SINGLE_SOURCE_SYSTEM_QUEUE, 1);
        assertEquals(1, transferJobRepository.count());
        assertEquals(1, fetchJobRepository.count());
    }

    private void assertQueueCount(String queueName, int expectedMsgCount) {
        jmsTemplate.browse(queueName, (session, browser) -> {
            int msgCount = 0;
            Enumeration enumeration = browser.getEnumeration();
            while(enumeration.hasMoreElements()) {
                enumeration.nextElement();
                msgCount += 1;
            }
            logger.debug("{} msg count: {}", queueName, msgCount);
            assertEquals(expectedMsgCount, msgCount);
            return null;
        });
    }

    @TestConfiguration
    @Configuration
    public static class TransferServiceJmsTestConfig {

        private static final Logger logger = LoggerFactory.getLogger(TransferServiceJmsTestConfig.class);

        @Autowired
        private AutowireCapableBeanFactory beanFactory;

        @Bean
        @Primary
        public FetchJobMDP fetchJobMDP() {
            logger.debug("creating null FetchJobMDP");
            return null;
        }

        @Bean
        @Primary
        public JobService jobService() {
            JobService jobService = new JobService() {

                @Override
                public TransferJobDTO createTransferJob(String subscriptionId, TransferType transferType,
                                                        LocalDateTime startDateTime, LocalDateTime endDateTime,
                                                        String sourceSystemHsaId, TransferJob.Origin origin) {
                    createTransferJobInvocation += 1;
                    if (createTransferJobInvocation == createTransferJobInvocationToFailOn) {
                        throw new RuntimeException("createTransferJob failed on invocation " +
                                createTransferJobInvocation);
                    }
                    return super.createTransferJob(subscriptionId, transferType, startDateTime, endDateTime, sourceSystemHsaId, origin);
                }

                @Override
                public FetchJobDTO createFetchJob(String transferJobId) {
                    createFetchJobInvocation += 1;
                    if (createFetchJobInvocation == createFetchJobInvocationToFailOn) {
                        throw new RuntimeException("createFetchJob failed on invocation " + createFetchJobInvocation);
                    }
                    return super.createFetchJob(transferJobId);
                }
            };
            beanFactory.autowireBean(jobService);
            return jobService;
        }

        @Bean
        @Primary
        public PreCheckService preCheckService() {
            return Mockito.mock(PreCheckService.class);
        }
    }
}