package se.skl.prenumerationstjanst.worker;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.util.ReflectionTestUtils;
import se.skl.prenumerationstjanst.dto.*;
import se.skl.prenumerationstjanst.dto.internal.InitialTransferParams;
import se.skl.prenumerationstjanst.dto.internal.SingleSourceSystemTransferParams;
import se.skl.prenumerationstjanst.job.JobMDCMapper;
import se.skl.prenumerationstjanst.model.Subscription;
import se.skl.prenumerationstjanst.model.TransferJob;
import se.skl.prenumerationstjanst.model.TransferType;
import se.skl.prenumerationstjanst.repository.SubscriptionRepository;
import se.skl.prenumerationstjanst.service.JobService;
import se.skl.prenumerationstjanst.service.PreCheckService;
import se.skl.prenumerationstjanst.service.SubscriptionService;

import java.time.LocalDateTime;
import java.time.Year;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static se.skl.prenumerationstjanst.PrenumerationstjanstConstants.FETCH_AGGREGATED_QUEUE;
import static se.skl.prenumerationstjanst.PrenumerationstjanstConstants.FETCH_SINGLE_SOURCE_SYSTEM_QUEUE;

/**
 * @author Martin Samuelsson
 */
@RunWith(MockitoJUnitRunner.class)
public class TransferMDPTest {

    private static final Logger logger = LoggerFactory.getLogger(TransferMDPTest.class);
    private static final String REPRESENTING_CRN = "representing";
    private static final String USER_CRN = "user";

    private int startYear;
    private int currentYear;

    @Mock
    private JmsTemplate jmsTemplate;

    @Mock
    private PreCheckService preCheckService;

    @Mock
    private JobService jobService;

    private TransferJobDTO transferJobDTO;

    @Mock
    private FetchJobDTO fetchJobDTO;

    @Mock
    private Subscription subscription;

    @Mock
    private SubscriptionService subscriptionService;

    @Mock
    private SubscriptionRepository subscriptionRepository;

    @Mock
    private JobMDCMapper jobMDCMapper;

    @InjectMocks
    private TransferMDP transferMDP;

    public void setUp() throws Exception {
        currentYear = Year.now().getValue();
        startYear = Year.now().minusYears(20).getValue();
        ReflectionTestUtils.setField(transferMDP, "initialStartYear", startYear);
        when(jobService.createTransferJob(anyString(), any(TransferType.class), any(), any(), anyString(), any
                (TransferJob.Origin.class)))
                .thenReturn(transferJobDTO);
        when(jobService.createFetchJob(anyString()))
                .thenReturn(fetchJobDTO);
        when(fetchJobDTO.getTransferJob())
                .thenReturn(transferJobDTO);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testDoStartInitialTransfersForSubscription() throws Exception {
        transferJobDTO = createTransferJobDTO(null);
        setUp();
        when(subscriptionRepository.findOne(eq("subId"))).thenReturn(subscription);
        when(preCheckService.preCheck(USER_CRN, REPRESENTING_CRN)).thenReturn(Boolean.TRUE);
        transferMDP.doStartInitialTransfersForSubscription(new InitialTransferParams("subId",
                TransferType.MATERNITY_MEDICAL_HISTORY.name()));

        verify(subscriptionRepository).findOne(eq("subId"));

        ArgumentCaptor<TransferType> transferTypeArgumentCaptor = ArgumentCaptor.forClass(TransferType.class);
        ArgumentCaptor<LocalDateTime> startDateTimeCaptor = ArgumentCaptor.forClass(LocalDateTime.class);
        ArgumentCaptor<LocalDateTime> endDateTimeCaptor = ArgumentCaptor.forClass(LocalDateTime.class);

        int years = currentYear - startYear + 1;
        int expectedJobCount = years;

        logger.debug("years[{}], expectedJobCount[{}]", years, expectedJobCount);

        verify(jobService, times(expectedJobCount)).createTransferJob(
                eq("subId"),
                transferTypeArgumentCaptor.capture(),
                startDateTimeCaptor.capture(),
                endDateTimeCaptor.capture(),
                anyString(), eq(TransferJob.Origin.INITIAL));

        List<TransferType> capturedTransferTypes = transferTypeArgumentCaptor.getAllValues();
        List<LocalDateTime> capturedStartDateTimes = startDateTimeCaptor.getAllValues();
        List<LocalDateTime> capturedEndDateTimes = endDateTimeCaptor.getAllValues();

        assertEquals(expectedJobCount, capturedTransferTypes.size());
        assertEquals(expectedJobCount, capturedStartDateTimes.size());
        assertEquals(expectedJobCount, capturedEndDateTimes.size());

        assertEquals(expectedJobCount, capturedTransferTypes.stream()
                .filter(transferType -> transferType == TransferType.MATERNITY_MEDICAL_HISTORY)
                .count());

        for (int year = startYear; year <= currentYear; year += 1) {
            final int finalYear = year;
            assertEquals(expectedJobCount / years, capturedStartDateTimes.stream()
                    .filter(startDateTime -> startDateTime.getYear() == finalYear)
                    .filter(startDateTime -> startDateTime.isEqual(createStartOfYear(finalYear)))
                    .count());
            assertEquals(expectedJobCount / years, capturedEndDateTimes.stream()
                    .filter(endDateTime -> endDateTime.getYear() == startYear)
                    .filter(endDateTime -> endDateTime.isEqual(createEndOfYear(startYear)))
                    .count());
        }

        verify(jobService, times(expectedJobCount)).createFetchJob(anyString());

        ArgumentCaptor<FetchJobDTO> fetchJobDTOArgumentCaptor = ArgumentCaptor.forClass(FetchJobDTO.class);
        verify(jmsTemplate, times(expectedJobCount)).convertAndSend(eq(FETCH_AGGREGATED_QUEUE),
                fetchJobDTOArgumentCaptor.capture());
        List<FetchJobDTO> capturedFetchJobs = fetchJobDTOArgumentCaptor.getAllValues();

        assertEquals(expectedJobCount, capturedFetchJobs.size());
        capturedFetchJobs.forEach(capturedFetchJob -> assertSame(fetchJobDTO, capturedFetchJob));

        verifyNoMoreInteractions(jobService, jmsTemplate);
    }

    @Test
    public void testDoStartSingleSourceSystemTransfer() throws Exception {
        transferJobDTO = createTransferJobDTO("failedSourceSystem");
        setUp();
        when(subscriptionService.isSubscriptionActive("subId")).thenReturn(true);
        when(preCheckService.preCheck(USER_CRN, REPRESENTING_CRN)).thenReturn(Boolean.TRUE);
        transferMDP.doStartSingleSourceSystemTransfer(new SingleSourceSystemTransferParams(
                "subId",
                TransferType.MATERNITY_MEDICAL_HISTORY,
                LocalDateTime.MIN,
                LocalDateTime.MAX,
                "failedSourceSystem",
                TransferJob.Origin.FAILED_SOURCE_SYSTEM
        ));

        verify(jobService).createTransferJob(
                eq("subId"),
                eq(TransferType.MATERNITY_MEDICAL_HISTORY),
                eq(LocalDateTime.MIN),
                eq(LocalDateTime.MAX),
                eq("failedSourceSystem"),
                eq(TransferJob.Origin.FAILED_SOURCE_SYSTEM));

        verify(jobService).createFetchJob(anyString());

        verify(jmsTemplate).convertAndSend(eq(FETCH_SINGLE_SOURCE_SYSTEM_QUEUE), eq(fetchJobDTO));

        verifyNoMoreInteractions(jobService, jmsTemplate);
    }

    @Test
    public void testStartTransferJob_PreCheckFailes() throws Exception {
        transferJobDTO = createTransferJobDTO("failedSourceSystem");
        setUp();
        when(subscriptionService.isSubscriptionActive("subId")).thenReturn(true);
        when(preCheckService.preCheck(USER_CRN, REPRESENTING_CRN)).thenReturn(Boolean.FALSE);
        transferMDP.doStartSingleSourceSystemTransfer(new SingleSourceSystemTransferParams(
                "subId",
                TransferType.MATERNITY_MEDICAL_HISTORY,
                LocalDateTime.MIN,
                LocalDateTime.MAX,
                "failedSourceSystem",
                TransferJob.Origin.FAILED_SOURCE_SYSTEM
        ));

        verify(jobService).createTransferJob(
                eq("subId"),
                eq(TransferType.MATERNITY_MEDICAL_HISTORY),
                eq(LocalDateTime.MIN),
                eq(LocalDateTime.MAX),
                eq("failedSourceSystem"),
                eq(TransferJob.Origin.FAILED_SOURCE_SYSTEM));

        verifyNoMoreInteractions(jobService, jmsTemplate);
    }

    @Test
    public void testDoStartSingleSourceSystemTransfer_NotActiveSubscription() throws Exception {
        when(subscriptionService.isSubscriptionActive("subId")).thenReturn(false);
        verifyNoMoreInteractions(jobService, jmsTemplate, preCheckService);
    }

    private LocalDateTime createStartOfYear(int year) {
        return LocalDateTime.of(year, 1, 1, 0, 0, 0, 0);
    }

    private LocalDateTime createEndOfYear(int year) {
        return LocalDateTime.of(year, 12, 31, 23, 59, 59, 0);
    }

    private TransferJobDTO createTransferJobDTO(String sourceSystemId) {
        return TransferJobDTO.create()
                .withUserCrn(USER_CRN)
                .withRepresentingCrn(REPRESENTING_CRN)
                .withSourceSystemHsaId(sourceSystemId);
    }
}