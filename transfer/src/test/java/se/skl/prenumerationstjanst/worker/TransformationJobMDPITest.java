package se.skl.prenumerationstjanst.worker;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import se.skl.prenumerationstjanst.JmsTestConfig;
import se.skl.prenumerationstjanst.PrenumerationstjanstConstants;
import se.skl.prenumerationstjanst.PrenumerationstjanstTransferApplication;
import se.skl.prenumerationstjanst.config.TestConfiguration;
import se.skl.prenumerationstjanst.dto.TransferJobDTO;
import se.skl.prenumerationstjanst.dto.TransformationJobDTO;
import se.skl.prenumerationstjanst.exception.transfer.TransformationFailedException;
import se.skl.prenumerationstjanst.job.TransformationJobHandler;
import se.skl.prenumerationstjanst.job.TransformationResult;
import se.skl.prenumerationstjanst.model.*;
import se.skl.prenumerationstjanst.repository.*;
import se.skl.prenumerationstjanst.service.JobService;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

/**
 * @author Martin Samuelsson
 */
@SuppressWarnings("Duplicates")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {
        PrenumerationstjanstTransferApplication.class,
        JmsTestConfig.class,
        TransformationJobMDPITest.TransformationJobMDPITestConfiguration.class
})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ActiveProfiles({"test", "jmstest"})
public class TransformationJobMDPITest {

    private static final Logger logger = LoggerFactory.getLogger(TransformationJobMDPITest.class);

    public static final String USER_CRN = "191212121212";
    private static final TransferType TRANSFER_TYPE = TransferType.VACCINATION_HISTORY;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserContextRepository userContextRepository;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private TransferJobRepository transferJobRepository;

    @Autowired
    private TransformationJobRepository transformationJobRepository;

    @Autowired
    private JobService jobService;

    @Resource
    private Map<TransferType, List<TransformationJobHandler>> transformationJobHandlers;

    private TransformationJobHandler v1TransformationJobHandler;

    private TransformationJobHandler v2TransformationJobHandler;

    private UserContext savedUserContext;

    private TransferJob transferJob;

    private String baseTransformationJobId;

    private JobService jobServiceSpy;


    @Before
    public void setUp() throws Exception {
        tearDown();
        v1TransformationJobHandler = Mockito.mock(TransformationJobHandler.class);
        v2TransformationJobHandler = Mockito.mock(TransformationJobHandler.class);
        when(v1TransformationJobHandler.version()).thenReturn(1);
        when(v1TransformationJobHandler.processTransformationJob(any(TransformationJobDTO.class)))
                .thenReturn(new TransformationResult(new ArrayList<>())); //default to empty list
        when(v2TransformationJobHandler.version()).thenReturn(2);
        when(v2TransformationJobHandler.processTransformationJob(any(TransformationJobDTO.class)))
                .thenReturn(new TransformationResult(new ArrayList<>())); //default to empty list
        List<TransformationJobHandler> handlers = new ArrayList<>();
        handlers.add(v1TransformationJobHandler);
        handlers.add(v2TransformationJobHandler);
        when(transformationJobHandlers.get(eq(TRANSFER_TYPE))).thenReturn(handlers);

        savedUserContext = userContextRepository.save(new UserContext(userRepository.save(new User(USER_CRN, "phkPersonId")),
                USER_CRN, "phkRecordId"));

        Subscription subscription = subscriptionRepository.save(new Subscription(savedUserContext, TRANSFER_TYPE));

        transferJob = transferJobRepository.save(new TransferJob(subscription.getUserContext().getUser().getCrn(),
                subscription.getUserContext().getRepresentingCrn(), subscription.getId(), TRANSFER_TYPE,
                LocalDateTime.now(), LocalDateTime.now(), TransferJob.Origin.INITIAL));

        baseTransformationJobId = insertBaseTransformationJob();

        jobServiceSpy = Mockito.spy(jobService); //create spy after insertBase...
    }

    @After
    public void tearDown() throws Exception {
        userRepository.deleteAll();
        userContextRepository.deleteAll();
        subscriptionRepository.deleteAll();
        transferJobRepository.deleteAll();
    }

    @Test
    public void testTransformResultingInZero() throws Exception {
        TransferJobDTO transferJobDTO = TransferJobDTO.create()
                .withId(transferJob.getId())
                .withTransferType(TRANSFER_TYPE.name());
        LocalDateTime sourceFetchTime = LocalDateTime.now();
        TransformationJobDTO transformationJobDTO = new TransformationJobDTO(baseTransformationJobId, transferJobDTO, "",
                sourceFetchTime, 1);
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, transformationJobDTO);
        Thread.sleep(200);
        ArgumentCaptor<TransformationJobDTO> transformationJobDTOArgumentCaptor = ArgumentCaptor.forClass
                (TransformationJobDTO.class);
        verify(v1TransformationJobHandler).processTransformationJob(transformationJobDTOArgumentCaptor.capture());
        TransformationJobDTO incoming = transformationJobDTOArgumentCaptor.getValue();
        assertEquals(incoming.getId(), transformationJobDTO.getId());
        assertQueueCount(PrenumerationstjanstConstants.PUSH_QUEUE, 0);
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE_DLQ, 0);
        TransformationJob transformationJob = transformationJobRepository.findOne(baseTransformationJobId);
        assertEquals(3, transformationJob.getJobStates().size());
        assertEquals(JobStatus.FINISHED, transformationJob.getJobStates().get(2).getStatus());
    }

    @Test
    public void testTransformResultingInOne() throws Exception {
        ArrayList<TransformationResult.PushJobSpecification> pushJobSpecifications = new ArrayList<>();
        TransformationResult.PushJobSpecification pushJobSpecification = new TransformationResult
                .PushJobSpecification("", new ArrayList<>(), LocalDateTime.now(), "", 1);
        pushJobSpecifications.add(pushJobSpecification);
        when(v1TransformationJobHandler.processTransformationJob(any(TransformationJobDTO.class)))
                .thenReturn(new TransformationResult(pushJobSpecifications));
        TransferJobDTO transferJobDTO = TransferJobDTO.create()
                .withId(transferJob.getId())
                .withTransferType(TRANSFER_TYPE.name());
        LocalDateTime sourceFetchTime = LocalDateTime.now();
        TransformationJobDTO transformationJobDTO = new TransformationJobDTO(baseTransformationJobId, transferJobDTO, "",
                sourceFetchTime, 1);
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, transformationJobDTO);
        Thread.sleep(200);
        ArgumentCaptor<TransformationJobDTO> transformationJobDTOArgumentCaptor = ArgumentCaptor.forClass
                (TransformationJobDTO.class);
        verify(v1TransformationJobHandler).processTransformationJob(transformationJobDTOArgumentCaptor.capture());
        TransformationJobDTO incoming = transformationJobDTOArgumentCaptor.getValue();
        assertEquals(incoming.getId(), transformationJobDTO.getId());
        assertQueueCount(PrenumerationstjanstConstants.PUSH_QUEUE, 1);
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE_DLQ, 0);
        TransformationJob transformationJob = transformationJobRepository.findOne(baseTransformationJobId);
        assertEquals(3, transformationJob.getJobStates().size());
        assertEquals(JobStatus.FINISHED, transformationJob.getJobStates().get(2).getStatus());
    }

    @Test
    public void testTransformResultingInTwo() throws Exception {
        ArrayList<TransformationResult.PushJobSpecification> pushJobSpecifications = new ArrayList<>();
        TransformationResult.PushJobSpecification pushJobSpecification1 = new TransformationResult
                .PushJobSpecification("", new ArrayList<>(), LocalDateTime.now(), "", 1);
        TransformationResult.PushJobSpecification pushJobSpecification2 = new TransformationResult
                .PushJobSpecification("", new ArrayList<>(), LocalDateTime.now(), "", 1);
        pushJobSpecifications.add(pushJobSpecification1);
        pushJobSpecifications.add(pushJobSpecification2);
        when(v1TransformationJobHandler.processTransformationJob(any(TransformationJobDTO.class)))
                .thenReturn(new TransformationResult(pushJobSpecifications));
        TransferJobDTO transferJobDTO = TransferJobDTO.create()
                .withId(transferJob.getId())
                .withTransferType(TRANSFER_TYPE.name());
        LocalDateTime sourceFetchTime = LocalDateTime.now();
        TransformationJobDTO transformationJobDTO = new TransformationJobDTO(baseTransformationJobId, transferJobDTO, "",
                sourceFetchTime, 1);
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, transformationJobDTO);
        Thread.sleep(200);
        ArgumentCaptor<TransformationJobDTO> transformationJobDTOArgumentCaptor = ArgumentCaptor.forClass
                (TransformationJobDTO.class);
        verify(v1TransformationJobHandler).processTransformationJob(transformationJobDTOArgumentCaptor.capture());
        TransformationJobDTO incoming = transformationJobDTOArgumentCaptor.getValue();
        assertEquals(incoming.getId(), transformationJobDTO.getId());
        assertQueueCount(PrenumerationstjanstConstants.PUSH_QUEUE, 2);
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE_DLQ, 0);
        TransformationJob transformationJob = transformationJobRepository.findOne(baseTransformationJobId);
        assertEquals(3, transformationJob.getJobStates().size());
        assertEquals(JobStatus.FINISHED, transformationJob.getJobStates().get(2).getStatus());
    }

    @Test
    public void testTransformResultingInRuntimeExcThenOne() throws Exception {
        ArrayList<TransformationResult.PushJobSpecification> pushJobSpecifications = new ArrayList<>();
        TransformationResult.PushJobSpecification pushJobSpecification = new TransformationResult
                .PushJobSpecification("", new ArrayList<>(), LocalDateTime.now(), "", 1);
        pushJobSpecifications.add(pushJobSpecification);
        TransferJobDTO transferJobDTO = TransferJobDTO.create()
                .withId(transferJob.getId())
                .withTransferType(TRANSFER_TYPE.name());
        LocalDateTime sourceFetchTime = LocalDateTime.now();
        TransformationJobDTO transformationJobDTO = new TransformationJobDTO(baseTransformationJobId, transferJobDTO, "",
                sourceFetchTime, 1);
        when(v1TransformationJobHandler.processTransformationJob(any(TransformationJobDTO.class)))
                .thenThrow(new TransformationFailedException(transformationJobDTO))
                .thenReturn(new TransformationResult(pushJobSpecifications));
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, transformationJobDTO);
        Thread.sleep(3000);
        ArgumentCaptor<TransformationJobDTO> transformationJobDTOArgumentCaptor = ArgumentCaptor.forClass
                (TransformationJobDTO.class);
        verify(v1TransformationJobHandler, times(2)).processTransformationJob(transformationJobDTOArgumentCaptor.capture());
        TransformationJobDTO incoming = transformationJobDTOArgumentCaptor.getValue();
        assertEquals(incoming.getId(), transformationJobDTO.getId());
        assertQueueCount(PrenumerationstjanstConstants.PUSH_QUEUE, 1);
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE_DLQ, 0);
        TransformationJob transformationJob = transformationJobRepository.findOne(baseTransformationJobId);
        assertEquals(5, transformationJob.getJobStates().size()); //create, run, fail, run, finish
        assertEquals(JobStatus.FINISHED, transformationJob.getJobStates().get(4).getStatus());
    }

    @Test
    public void testTransformResultingInRuntimeExcForAllDeliveries() throws Exception {
        TransferJobDTO transferJobDTO = TransferJobDTO.create()
                .withId(transferJob.getId())
                .withTransferType(TRANSFER_TYPE.name());
        LocalDateTime sourceFetchTime = LocalDateTime.now();
        TransformationJobDTO transformationJobDTO = new TransformationJobDTO(baseTransformationJobId, transferJobDTO, "",
                sourceFetchTime, 1);
        when(v1TransformationJobHandler.processTransformationJob(any(TransformationJobDTO.class)))
                .thenThrow(new TransformationFailedException(transformationJobDTO));
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, transformationJobDTO);
        Thread.sleep(5000);
        ArgumentCaptor<TransformationJobDTO> transformationJobDTOArgumentCaptor = ArgumentCaptor.forClass
                (TransformationJobDTO.class);
        verify(v1TransformationJobHandler, times(3)).processTransformationJob(transformationJobDTOArgumentCaptor.capture());
        TransformationJobDTO incoming = transformationJobDTOArgumentCaptor.getValue();
        assertEquals(incoming.getId(), transformationJobDTO.getId());
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE_DLQ, 1);
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, 0);
        TransformationJob transformationJob = transformationJobRepository.findOne(baseTransformationJobId);
        transformationJob.getJobStates().forEach(jobState -> {
            logger.info("jobState: {} ({})", jobState.getStatus(), jobState.getCreated());
        });
        assertEquals(7, transformationJob.getJobStates().size()); //create, run, fail, run, fail, run, fail
        assertEquals(JobStatus.FAILED, transformationJob.getJobStates().get(6).getStatus());
    }

    @Test
    public void testThrowingTransformationJobHandler() throws Exception {
        TransferJobDTO transferJobDTO = TransferJobDTO.create()
                .withId(transferJob.getId())
                .withTransferType(TRANSFER_TYPE.name());
        LocalDateTime sourceFetchTime = LocalDateTime.now();
        TransformationJobDTO transformationJobDTO = new TransformationJobDTO(baseTransformationJobId, transferJobDTO, "",
                sourceFetchTime, 3);
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, transformationJobDTO);
        Thread.sleep(200);
        assertQueueCount(PrenumerationstjanstConstants.PUSH_QUEUE, 0);
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE_DLQ, 0);
        TransformationJob transformationJob = transformationJobRepository.findOne(baseTransformationJobId);
        assertEquals(3, transformationJob.getJobStates().size()); //create, run, fail
        assertEquals(JobStatus.FAILED, transformationJob.getJobStates().get(2).getStatus());
    }

    @Test
    public void testNoTransformationJobHandlerForTransferType() throws Exception {
        TransferJobDTO transferJobDTO = TransferJobDTO.create()
                .withId(transferJob.getId())
                .withTransferType(TRANSFER_TYPE.name());
        LocalDateTime sourceFetchTime = LocalDateTime.now();
        TransformationJobDTO transformationJobDTO = new TransformationJobDTO(baseTransformationJobId, transferJobDTO, "",
                sourceFetchTime, 3);
        jmsTemplate.convertAndSend(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE, transformationJobDTO);
        Thread.sleep(200);
        assertQueueCount(PrenumerationstjanstConstants.PUSH_QUEUE, 0);
        assertQueueCount(PrenumerationstjanstConstants.TRANSFORMATION_QUEUE_DLQ, 0);
        TransformationJob transformationJob = transformationJobRepository.findOne(baseTransformationJobId);
        transformationJob.getJobStates().forEach(jobState -> {
            logger.info("jobState: {} ({})", jobState.getStatus(), jobState.getCreated());
        });
        assertEquals(3, transformationJob.getJobStates().size()); //create, run, fail
        assertEquals(JobStatus.FAILED, transformationJob.getJobStates().get(2).getStatus());
    }


    private void assertQueueCount(String queueName, int expectedMsgCount) {
        jmsTemplate.browse(queueName, (session, browser) -> {
            int msgCount = 0;
            Enumeration enumeration = browser.getEnumeration();
            while(enumeration.hasMoreElements()) {
                enumeration.nextElement();
                msgCount += 1;
            }
            logger.debug("{} msg count: {}", queueName, msgCount);
            assertEquals(expectedMsgCount, msgCount);
            return null;
        });
    }

    private String insertBaseTransformationJob() {
        return jobService.createTransformationJob(
                        transferJobRepository.findOne(insertBaseTransferJob("subId")).getId(), "", LocalDateTime.now(), 1)
                .getId();
    }

    private String insertBaseTransferJob(String subscriptionId) {
        TransferJob transferJob = new TransferJob("", "", subscriptionId, TransferType.VACCINATION_HISTORY,
                LocalDateTime.now(),
                LocalDateTime.now(), TransferJob.Origin.INITIAL);
        return transferJobRepository.save(transferJob).getId();
    }

    @TestConfiguration
    @Configuration
    public static class TransformationJobMDPITestConfiguration {

        @SuppressWarnings("unchecked")
        @Bean
        @Primary
        public Map<TransferType, List<TransformationJobHandler>> transformationJobHandlers() {
            return Mockito.mock(EnumMap.class);
        }
    }
}